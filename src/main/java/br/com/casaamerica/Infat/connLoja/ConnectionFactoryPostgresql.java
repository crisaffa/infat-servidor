package br.com.casaamerica.Infat.connLoja;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactoryPostgresql {
	public static Connection getConnection() throws Exception  {
        try {
        	
        	Class.forName("org.postgresql.Driver");	  

        	return DriverManager.getConnection("jdbc:postgresql://35.199.83.225/casaamerica", "postgres", "postgres");
        	            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
