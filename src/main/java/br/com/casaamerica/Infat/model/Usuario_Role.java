package br.com.casaamerica.Infat.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class Usuario_Role {

	
	@Id
	private String usuario_usuario;
	private String roles_nome;

	public String getUsuario_usuario() {
		return usuario_usuario;
	}

	public void setUsuario_usuario(String usuario_usuario) {
		this.usuario_usuario = usuario_usuario;
	}

	public String getRoles_nome() {
		return roles_nome;
	}

	public void setRoles_nome(String roles_nome) {
		this.roles_nome = roles_nome;
	}

}
