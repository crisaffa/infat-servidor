package br.com.casaamerica.Infat.model;

public class Relatorio {
	
	private String dataEmissao;
	
	private String pedido;
	
	private double valorProdutos;
	
	private double valorFrete;
	
	private double valorTotal;
	
	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getPedido() {
		return pedido;
	}

	public void setPedido(String pedido) {
		this.pedido = pedido;
	}

	public double getValorProdutos() {
		return valorProdutos;
	}

	public void setValorProdutos(double valorProdutos) {
		this.valorProdutos = valorProdutos;
	}

	public double getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(double valorFrete) {
		this.valorFrete = valorFrete;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

			
}
