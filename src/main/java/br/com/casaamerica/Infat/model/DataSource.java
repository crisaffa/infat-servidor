package br.com.casaamerica.Infat.model;


import java.awt.Image;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * 
 * Classe utilizada para retornar os campos dos beans para preencher os relatórios.
 * Seu construtor recebe uma Collection retornada de uma consulta reslizada através dos DAO's.
 * O relatório a ser preenchido deve possuir os campos com nomes idênticos aos nomes das variáveis-membro declaradas no bean em questão.
 * 
 * Caso o objeto DAO retorne um Set que contenha outros Set, estes serão retornados como um Object DataSource e devem ser adicionados ao relatório como um subreport que recebe um Object data source.
 * Se um campo deste subreport deve ser impresso apenas uma vez, ele deve ser nomeado no arquivo-fonte do relatório como o nome da variável-membro mais a palavra reservada "_1".
 * Exemplo:
 * <pre>
 * 		O campo email deve ser impresso
 * 			- Uma vez:    		$F{email_1}
 * 			- Todos emails:		$F{email}
 * </pre>
 * 
 * @author Wesley
 *
 */
public class DataSource implements JRDataSource {
	
	protected Collection resultado;
	protected Object bean;
	private Iterator it;
	private Pattern p;
	private Matcher m;

	/**
	 * Construtor do DataSource que recebe uma Collection vinda de uma consulta Hibernate, por exemplo.
	 * @param res Collection que pode representar o resultado de uma consulta Hibernate.
	 */
	public DataSource(Collection res) {
		if(res != null) {
			this.resultado = res;
			this.it = this.resultado.iterator();
			this.p = Pattern.compile("_1");
		}
	}
	
	public boolean next() throws JRException {
		bean = it.hasNext() ? it.next() : null;
		return (bean != null);
	}

	/* (non-Javadoc)
	 * Pega o valor do campo representado prlo JRField campo, que deve possuir o mesmo nome do atributo declarado no bean a ser lido.
	 * 
	 * @see net.sf.jasperreports.engine.JRDataSource#getFieldValue(net.sf.jasperreports.engine.JRField)
	 */
	public Object getFieldValue(JRField campo) throws JRException {
		String nomeCampo = campo.getName();
		Class c = campo.getValueClass();
		m = p.matcher(nomeCampo);
		/*if(m.find()) {
			nomeCampo = nomeCampo.replaceAll("_1", "");
			while(it.hasNext())
				it.next();
		}*/
		String[] nomesCampos = nomeCampo.split("\\.");
		Object bean_return = bean;
		int i = 0;
		try {
			while(i < nomesCampos.length && bean_return != null) {
				String field = nomesCampos[i++];
				m = p.matcher(field);
				if(m.find()) {
					field = field.replaceAll("_1", "");
					bean_return = PropertyUtils.getProperty(bean_return, field);
					if(bean_return instanceof Collection) {
						bean_return = ((Collection)bean_return).iterator().next();
						if(i < nomesCampos.length) {
							field = nomesCampos[i++];
						}
						else {
							return bean_return;
						}
					}
				}
				bean_return = PropertyUtils.getProperty(bean_return, field);
				if(bean_return instanceof Collection)
					return new DataSource((Collection)bean_return);
				if(bean_return instanceof String || bean_return instanceof Number || bean_return instanceof Date || bean_return instanceof Boolean || bean_return instanceof Image || bean_return instanceof Character) {
					return bean_return;
				}
			}
		}
		catch (Exception e) { 
			e.printStackTrace(); 
		}
		if(c.getName().endsWith("String"))
			return "";
		else if(c.getName().endsWith("Integer"))
			return new Integer(0);
		else if(c.getName().endsWith("Double"))
			return new Double(0);
		else if(c.getName().endsWith("Date"))
			return new Date();
		else if(c.getName().endsWith("Boolean"))
			return new Boolean(false);
		return null;
	}

}
