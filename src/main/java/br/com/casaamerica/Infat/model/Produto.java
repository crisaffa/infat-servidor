package br.com.casaamerica.Infat.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Produto {

	@Id
	private String id;

	private String id_order;
	private String id_produto;
	private Long quantidade;
	private String descricao;
	private String preco;
	private String frete;
	private String status;
	private String cpfCliente;
	private String valortotal;
	private String fretetotal;
	private String imagem;
	private String id_venda_caixa;
	private String desconto;
	private String descontototal;
	private String peso;
		
	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

	public String getDesconto() {
		return desconto;
	}

	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}

	public String getDescontoTotal() {
		return descontototal;
	}

	public void setDescontoTotal(String descontototal) {
		this.descontototal = descontototal;
	}

	public String getId_venda_caixa() {
		return id_venda_caixa;
	}

	public void setId_venda_caixa(String id_venda_caixa) {
		this.id_venda_caixa = id_venda_caixa;
	}

	public String getValortotal() {
		return valortotal;
	}

	public void setValortotal(String valortotal) {
		this.valortotal = valortotal;
	}

	public String getFretetotal() {
		return fretetotal;
	}

	public void setFretetotal(String fretetotal) {
		this.fretetotal = fretetotal;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFrete() {
		return frete;
	}

	public void setFrete(String frete) {
		this.frete = frete;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCpfCliente() {
		return cpfCliente;
	}

	public void setCpfCliente(String cpfCliente) {
		this.cpfCliente = cpfCliente;
	}

	public String getId_order() {
		return id_order;
	}

	public void setId_order(String id_order) {
		this.id_order = id_order;
	}

	public String getId_produto() {
		return id_produto;
	}

	public void setId_produto(String id_produto) {
		this.id_produto = id_produto;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getPreco() {
		return preco;
	}

	public void setPreco(String preco) {
		this.preco = preco;
	}

}
