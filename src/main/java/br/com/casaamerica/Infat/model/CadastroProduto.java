package br.com.casaamerica.Infat.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CadastroProduto {
    
    @Id
    private int id_produto;
    
    private String nome;
    private String data;
    private String exibir;
    private int id_grupo;
    private String marca;
    private String descricao;
    private int estoque;
    private String preco_venda;
    private String preco_promocao;
    private String codigo_gtin;
    private String id_ncm;
    private String cor;
    private String peso;
    private String comprimento;
    private String largura;
    private String altura;
    private String status;
    private String imagem;
    
    public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public int getId_produto() {
        return id_produto;
    }
    public void setId_produto(int id_produto) {
        this.id_produto = id_produto;
    }
    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
    public String getExibir() {
        return exibir;
    }
    public void setExibir(String exibir) {
        this.exibir = exibir;
    }
    public int getId_grupo() {
        return id_grupo;
    }
    public void setId_grupo(int id_grupo) {
        this.id_grupo = id_grupo;
    }
    public String getMarca() {
        return marca;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public int getEstoque() {
        return estoque;
    }
    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }   
    public String getPreco_venda() {
		return preco_venda;
	}
	public void setPreco_venda(String preco_venda) {
		this.preco_venda = preco_venda;
	}
	public String getPreco_promocao() {
		return preco_promocao;
	}
	public void setPreco_promocao(String preco_promocao) {
		this.preco_promocao = preco_promocao;
	}
	public String getCodigo_gtin() {
        return codigo_gtin;
    }
    public void setCodigo_gtin(String codigo_gtin) {
        this.codigo_gtin = codigo_gtin;
    }
    public String getId_ncm() {
        return id_ncm;
    }
    public void setId_ncm(String id_ncm) {
        this.id_ncm = id_ncm;
    }
    public String getCor() {
        return cor;
    }
    public void setCor(String cor) {
        this.cor = cor;
    }
    public String getPeso() {
        return peso;
    }
    public void setPeso(String peso) {
        this.peso = peso;
    }
    public String getComprimento() {
        return comprimento;
    }
    public void setComprimento(String comprimento) {
        this.comprimento = comprimento;
    }
    public String getLargura() {
        return largura;
    }
    public void setLargura(String largura) {
        this.largura = largura;
    }
    public String getAltura() {
        return altura;
    }
    public void setAltura(String altura) {
        this.altura = altura;
    }
    
    
    

}