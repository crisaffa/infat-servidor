package br.com.casaamerica.Infat.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Correios {
	
	@Id
	private String identificador;
		
	private String cartaoPostagem;
	private String contrato;
	private String codigoAdministrativo;	
	private String usuario;
	private String senha;
	private String escolhido;	
	private String codigoServicoPostagem;
	
	public String getCodigoServicoPostagem() {
		return codigoServicoPostagem;
	}

	public void setCodigoServicoPostagem(String codigoServicoPostagem) {
		this.codigoServicoPostagem = codigoServicoPostagem;
	}

	public String getIdentificador() {
		return identificador;
	}
	
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}	
	
	public String getCartaoPostagem() {
		return cartaoPostagem;
	}
	
	public void setCartaoPostagem(String cartaoPostagem) {
		this.cartaoPostagem = cartaoPostagem;
	}
	
	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	
	public String getCodigoAdministrativo() {
		return codigoAdministrativo;
	}
	
	public void setCodigoAdministrativo(String codigoAdministrativo) {
		this.codigoAdministrativo = codigoAdministrativo;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String getEscolhido() {
		return escolhido;
	}

	public void setEscolhido(String escolhido) {
		this.escolhido = escolhido;
	}
	
}
