package br.com.casaamerica.Infat.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Pedido {

    @Id
    private String id_order;
    
    private String nome;
    private String orderId;
    private String status;
    private String data;
    private String data_emissao;
    private String status_nota;
    private String msg_sefaz;
    private String periodo_vencido;
    private String motivo_cancelamento;
    private String link;
    private String ref_nota;
    private String devolucao;
    private String etiqueta_impressa;
    private String altura;
    private String largura;
    private String comprimento;
    private String tipo_pagamento;
    private String plp;
    private String rastreio;
    private String user_fat;
    private String user_can;
    private String user_plp;
    private String user_dev;    
    private Double produtos_total;
    private Double desconto_total;
    private Double frete_total;
    private Double taxa_total;
    
    
    
    public Double getTaxa_total() {
		return taxa_total;
	}

	public void setTaxa_total(Double taxa_total) {
		this.taxa_total = taxa_total;
	}

	public Double getProdutos_total() {
		return produtos_total;
	}

	public void setProdutos_total(Double produtos_total) {
		this.produtos_total = produtos_total;
	}

	public Double getDesconto_total() {
		return desconto_total;
	}

	public void setDesconto_total(Double desconto_total) {
		this.desconto_total = desconto_total;
	}

	public Double getFrete_total() {
		return frete_total;
	}

	public void setFrete_total(Double frete_total) {
		this.frete_total = frete_total;
	}

	public String getTipo_pagamento() {
		return tipo_pagamento;
	}

	public void setTipo_pagamento(String tipo_pagamento) {
		this.tipo_pagamento = tipo_pagamento;
	}

	public String getData_emissao() {
		return data_emissao;
	}

	public void setData_emissao(String data_emissao) {
		this.data_emissao = data_emissao;
	}

	public String getRastreio() {
		return rastreio;
	}

	public void setRastreio(String rastreio) {
		this.rastreio = rastreio;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getLargura() {
		return largura;
	}

	public void setLargura(String largura) {
		this.largura = largura;
	}

	public String getComprimento() {
		return comprimento;
	}

	public void setComprimento(String comprimento) {
		this.comprimento = comprimento;
	}

	public String getPlp() {
		return plp;
	}

	public void setPlp(String plp) {
		this.plp = plp;
	}

	public String getEtiqueta_impressa() {
		return etiqueta_impressa;
	}

	public void setEtiqueta_impressa(String etiqueta_impressa) {
		this.etiqueta_impressa = etiqueta_impressa;
	}

	public String getDevolucao() {
		return devolucao;
	}

	public void setDevolucao(String devolucao) {
		this.devolucao = devolucao;
	}

	public String getRef_nota() {
		return ref_nota;
	}

	public void setRef_nota(String ref_nota) {
		this.ref_nota = ref_nota;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPeriodo_vencido() {
		return periodo_vencido;
	}

	public void setPeriodo_vencido(String periodo_vencido) {
		this.periodo_vencido = periodo_vencido;
	}

	public String getMsg_sefaz() {
		return msg_sefaz;
	}

	public void setMsg_sefaz(String msg_sefaz) {
		this.msg_sefaz = msg_sefaz;
	}

	public String getStatus_nota() {
		return status_nota;
	}

	public void setStatus_nota(String status_nota) {
		this.status_nota = status_nota;
	}

	public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMotivo_cancelamento() {
		return motivo_cancelamento;
	}

	public void setMotivo_cancelamento(String motivo_cancelamento) {
		this.motivo_cancelamento = motivo_cancelamento;
	}

	public String getUser_fat() {
		return user_fat;
	}

	public void setUser_fat(String user_fat) {
		this.user_fat = user_fat;
	}

	public String getUser_can() {
		return user_can;
	}

	public void setUser_can(String user_can) {
		this.user_can = user_can;
	}

	public String getUser_plp() {
		return user_plp;
	}

	public void setUser_plp(String user_plp) {
		this.user_plp = user_plp;
	}

	public String getUser_dev() {
		return user_dev;
	}

	public void setUser_dev(String user_dev) {
		this.user_dev = user_dev;
	}
	
}