package br.com.casaamerica.Infat.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.casaamerica.Infat.model.Empresa;


public class EmpresaValidation implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Empresa.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "cnpj", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "nome", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "nomeFantasia", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "logradouro", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "numero", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "bairro", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "municipio", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "uf", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "inscricaoEstadual", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "cep", "field.required");
    }

}