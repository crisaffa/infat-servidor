package br.com.casaamerica.Infat.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.casaamerica.Infat.model.Usuario;

public class UsuarioValidation implements Validator{
	
	 @Override
	    public boolean supports(Class<?> clazz) {
	        return Usuario.class.isAssignableFrom(clazz);
	    }

	    @Override
	    public void validate(Object target, Errors errors) {
	        ValidationUtils.rejectIfEmpty(errors, "usuario", "field.required");
	        ValidationUtils.rejectIfEmpty(errors, "email", "field.required");
	        ValidationUtils.rejectIfEmpty(errors, "senha", "field.required");
	       
	    }

}
