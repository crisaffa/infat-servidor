package br.com.casaamerica.Infat.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.casaamerica.Infat.model.Tarefa;

public class TarefaValidation implements Validator  {
	
	  @Override
	    public boolean supports(Class<?> clazz) {
	        return Tarefa.class.isAssignableFrom(clazz);
	    }

	    @Override
	    public void validate(Object target, Errors errors) {	             
	        ValidationUtils.rejectIfEmpty(errors, "emails", "field.required");	        
	    }

}
