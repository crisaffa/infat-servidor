package br.com.casaamerica.Infat.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.casaamerica.Infat.model.Correios;


public class CorreiosValidation implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Correios.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "identificador", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "cartaoPostagem", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "contrato", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "codigoAdministrativo", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "usuario", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "senha", "field.required");        
    }

}