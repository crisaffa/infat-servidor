package br.com.casaamerica.Infat.list;

public class ListaTotal {

	private String valortotal;
	private String fretetotal;
	private String descontototal;
	private String valorNF;
	private String taxa_total;
	
	public String getTaxa_total() {
		return taxa_total;
	}

	public void setTaxa_total(String taxa_total) {
		this.taxa_total = taxa_total;
	}

	public String getValorNF() {
		return valorNF;
	}

	public void setValorNF(String valorNF) {
		this.valorNF = valorNF;
	}

	public String getValortotal() {
		return valortotal;
	}

	public void setValortotal(String valortotal) {
		this.valortotal = valortotal;
	}

	public String getFretetotal() {
		return fretetotal;
	}

	public void setFretetotal(String fretetotal) {
		this.fretetotal = fretetotal;
	}

	public String getDescontototal() {
		return descontototal;
	}

	public void setDescontototal(String descontototal) {
		this.descontototal = descontototal;
	}
	
}
