package br.com.casaamerica.Infat.list;

public class ListaPedidos {

	private String id_order;
	private String nome;
	private String orderId;
	private String status;
	private String data;
	private String dataAtualização;
	private Long quantidade;

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public String getDataAtualização() {
		return dataAtualização;
	}

	public void setDataAtualização(String dataAtualização) {
		this.dataAtualização = dataAtualização;
	}

	public String getId_order() {
		return id_order;
	}

	public void setId_order(String id_order) {
		this.id_order = id_order;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
