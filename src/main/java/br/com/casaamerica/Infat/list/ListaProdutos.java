package br.com.casaamerica.Infat.list;

public class ListaProdutos {

	private String id;
	private String cpfcliente;
	private String descricao;
	private String frete;
	private String id_order;
	private String id_produto;
	private String preco;
	private Long quantidade;
	private String valortotal;
	private String fretetotal;
	private String imagem;
	
	public String getValortotal() {
		return valortotal;
	}

	public void setValortotal(String valortotal) {
		this.valortotal = valortotal;
	}

	public String getFretetotal() {
		return fretetotal;
	}

	public void setFretetotal(String fretetotal) {
		this.fretetotal = fretetotal;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getValorTotal() {
		return valortotal;
	}

	public void setValorTotal(String valortotal) {
		this.valortotal = valortotal;
	}

	public String getFreteTotal() {
		return fretetotal;
	}

	public void setFreteTotal(String fretetotal) {
		this.fretetotal = fretetotal;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCpfcliente() {
		return cpfcliente;
	}

	public void setCpfcliente(String cpfcliente) {
		this.cpfcliente = cpfcliente;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getFrete() {
		return frete;
	}

	public void setFrete(String frete) {
		this.frete = frete;
	}

	public String getId_order() {
		return id_order;
	}

	public void setId_order(String id_order) {
		this.id_order = id_order;
	}

	public String getId_produto() {
		return id_produto;
	}

	public void setId_produto(String id_produto) {
		this.id_produto = id_produto;
	}

	public String getPreco() {
		return preco;
	}

	public void setPreco(String preco) {
		this.preco = preco;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

}
