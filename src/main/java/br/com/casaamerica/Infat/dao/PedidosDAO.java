package br.com.casaamerica.Infat.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.connLoja.ConnectionFactoryPostgresqlBucardo;
import br.com.casaamerica.Infat.model.Cliente;
import br.com.casaamerica.Infat.model.DataSource;
import br.com.casaamerica.Infat.model.Pedido;
import br.com.casaamerica.Infat.model.Produto;
import br.com.casaamerica.Infat.model.Relatorio;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;

@Repository
@Transactional
public class PedidosDAO {

	@PersistenceContext
	EntityManager manager;
	
	//final String conexao = "jdbc:postgresql://10.11.248.2/infatAffa";
	final String conexao = "jdbc:postgresql://35.199.83.225/infat";
	
	public List<Pedido> listaPedidos() {

		List<Pedido> pedidos = manager.createQuery("select e from Pedido e where status != 'window-to-cancel' and status !='waiting-ffmt-authorization'", Pedido.class).getResultList();
		
		return pedidos;

	}
	
	public List<Pedido> listarPedidosEmitidos(){
		
		List<Pedido> pedidos = manager.createQuery("select e from Pedido e where status = 'NFE EMITIDA' and rastreio is NOT NULL", Pedido.class).getResultList();
		
		return pedidos;
		
	}
	
	public List<Pedido> findPedido(String id) {		

		return manager.createQuery("select e from Pedido e where e.id_order = '" + id + "'", Pedido.class).getResultList();

	}
	
	public String retornarStatusPedido(String id) {
		
		Pedido pedido = manager.createQuery("select e from Pedido e where e.id_order = '" + id + "'", Pedido.class).getSingleResult();
		
		return pedido.getStatus();
	}
	
	public String retornarValorRastreio(String id) {
		
		Pedido pedido = manager.createQuery("select e from Pedido e where e.id_order = '" + id + "'", Pedido.class).getSingleResult();
		
		return pedido.getRastreio();
	}
	
	public String retornarStatusEtiqueta(String id) {		

		Pedido pedido = manager.createQuery("select e from Pedido e where e.id_order = '" + id + "'", Pedido.class).getSingleResult();
		
		return pedido.getEtiqueta_impressa();

	}
	
	public String retornarCodigoRastreio(String id) {		

		Pedido pedido = manager.createQuery("select e from Pedido e where e.id_order = '" + id + "'", Pedido.class).getSingleResult();
		
		return pedido.getRastreio();

	}
	
	public List<Cliente> findClientes(String id) {

		List<Produto> q = manager.createQuery("select e from Produto e where id_order = '"+id+"'", Produto.class).getResultList();

		String idOrder = "";
		for (int i = 0; i < q.size(); i++) {

			idOrder = q.get(i).getId_order();
			
		}				
		
		List<Cliente> cliente = manager.createQuery("select e from Cliente e where idOrder = '"+idOrder+"'", Cliente.class).getResultList();
				
		return cliente;

	}

	public String findOrderId(String id_order) {
		System.out.println(id_order);

		Pedido p = new Pedido();

		try {

			p = manager.createQuery("select e from Pedido e where e.id_order = :id_order", Pedido.class).setParameter("id_order", id_order).getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println();
		return p.getOrderId();

	}

	public String atualiza() {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date date = new Date();
		
		return dateFormat.format(date);
	}
	
	public String bucardo() throws Exception {
			
		Connection connection = ConnectionFactoryPostgresqlBucardo.getConnection();
		Statement statement = connection.createStatement();
		
		boolean sql = statement.execute("SELECT to_char((NOW()::timestamp - ended), 'DD \"d\" HH24 \"h\" MI \"m\" SS \"s\"') FROM syncrun order by ended desc limit 1");
		ResultSet rs = statement.getResultSet();
		
		String buc = "";
		
		while(rs.next()) {
			buc = rs.getString("to_char");
		}		
	
		return buc;	
	}
	
	public List<Produto> separarPedidos() {
	
		List<Pedido> pedidos = manager.createQuery("select e from Pedido e where e.status = 'PREPARANDO ENTREGA'", Pedido.class).getResultList();
		
		for(int i = 0; i < pedidos.size(); i++) {					
			
			manager.createQuery("update Produto set status = 'PREPARANDO ENTREGA' where id_order = '"+pedidos.get(i).getId_order()+"'").executeUpdate();
		}
		
		List<Produto> produtos = manager.createQuery("select e from Produto e where e.status = 'PREPARANDO ENTREGA'", Produto.class).getResultList();
		
		return produtos;
		
	}
	
	public int cancelaPedido(String id, String orderId, String status, String motivo, String user) throws Exception {
		
		String url = "https://casaamerica.vtexcommercestable.com.br/api/oms/pvt/orders/"+orderId+"/cancel";
		HttpResponse response = null;
				
		HttpPost post = new HttpPost(url);
		CloseableHttpClient client = HttpClientBuilder.create().build();
		post.addHeader("cache-control", "no-cache");
		post.addHeader("content-type", "application/json");
		post.addHeader("X-VTEX-API-AppKey", "vtexappkey-casaamerica-ABHYEZ");
		post.addHeader("X-VTEX-API-AppToken",
				"QAXGZRVYBWXDEERBUCNUPRYDLHLXIVRAXPQESTYXSKGGCJLWQUXTAGMZCUUSFXPFEXGPONIBEPCEKRACWPHOYYZRCRBXYGSUVBCFCWHCMWPKEIOKUSWQPBHGXPMXLKCY");
		
		if((status == "AGUARDANDO PAGAMENTO")) {
			response = client.execute(post);						
		} else {			
			for(int i=0;i<2;i++) {
				response = client.execute(post);				
			}			
		}					
				
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());		
		
		manager.createQuery("update Pedido set status ='CANCELADO' where orderId = '"+orderId+"'").executeUpdate(); 
		
		manager.createQuery("update Produto set status ='CANCELADO' where id_order = '"+id+"'").executeUpdate(); 
		
		manager.createQuery("update Pedido set motivo_cancelamento ='"+motivo+"' where orderId = '"+orderId+"'").executeUpdate();
		
		manager.createQuery("update Pedido set user_can ='"+user+"' where orderId = '"+orderId+"'").executeUpdate();
		
		return response.getStatusLine().getStatusCode();
		
	}
	
	
	public boolean verificarMarketplace(String market, String orderid) {
		
		if(market.equals("America") && orderid.contains("csmr")) {			
			return true;
		}
		if(market.equals("Zoom") && orderid.contains("ZZZ")) {
			return true;
		}
		if(market.equals("Dia") && orderid.contains("DLD")) {
			return true;
		}
		if(market.equals("Carrefour") && orderid.contains("CRF")) {
			return true;
		}
		/*
		if(market.equals("Colombo") && orderid.contains("???"))  {
			return true;
		}
		if(market.equals("WebContinental") && orderid.contains("???")) {
			return true;
		}
		if(market.equals("Consul") && orderid.contains("???")) {
			return true;
		}
		if(market.equals("Brastemp") && orderid.contains("???")) {
			return true;
		}
		
		*/
		if(market.equals("MadeiraMadeira") && orderid.contains("MDM")) {
			return true;
		}		
		if(market.equals("CSU")) {
			if(orderid.contains("CSW")) {
				return true;
			}
			if(orderid.contains("CSN")) {
				return true;
			}
			if(orderid.contains("CVS")) {
				return true;
			}
			if(orderid.contains("CPS")) {
				return true;
			}
			if(orderid.contains("CSL")) {
				return true;
			}
			if(orderid.contains("CBT")) {
				return true;
			}			
			if(orderid.contains("CBC")) {
				return true;
			}
			if(orderid.contains("CSP")) {
				return true;
			}
			if(orderid.contains("LBC")) {
				return true;
			}
			if(orderid.contains("LBG")) {
				return true;
			}
			if(orderid.contains("LBS")) {
				return true;
			}
			if(orderid.contains("BCC")) {
				return true;
			}
			if(orderid.contains("CLL")) {
				return true;
			}
			if(orderid.contains("VVV")) {
				return true;
			}			
		}
		
		return false;		
	}
	
	public boolean verificarTipoPagamento(String pgto, String tipo) {
		
		if(pgto.equals("Boleto Bancário") && tipo.equals("Boleto Bancário")) {			
			return true;
		}
		if(pgto.equals("Mercado Pago") && tipo.equals("Mercado Pago")) {			
			return true;
		}
		if(pgto.equals("PayPal") && tipo.equals("PayPal")) {			
			return true;
		}
		
		return false;		
	}
	
	public void gerarRelatorioPedidosTipoPgto(String dataInicial, String dataFinal, String pgto, String email) throws Exception {
		
		System.out.println("Entrou aqui para gerar o relatório dos Pedidos por tipo de Pagamento!");
		
		String caminho = "/opt/pedidos/reports/";
		
		HashMap param = new HashMap();
		
		ArrayList<Relatorio> relatorios = new ArrayList<Relatorio>();
		
		String diaInicial = dataInicial.substring(0, 2);
		String mesInicial = dataInicial.substring(3, 5);
		String anoInicial = dataInicial.substring(6, 10);
		
		String diaFinal = dataFinal.substring(0, 2);
		String mesFinal = dataFinal.substring(3, 5);
		String anoFinal = dataFinal.substring(6, 10);
		
		int dia = Integer.parseInt(diaInicial);
		int diaf = Integer.parseInt(diaFinal);
		int mes = Integer.parseInt(mesInicial);
		int mesf = Integer.parseInt(mesFinal);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		ResultSet rs = null;
		
		double soma = 0.0;
		double totalFrete = 0.0;
		double totalProd = 0.0;
		
		if(pgto.equals("Boleto")) {
			pgto = "Boleto Bancário";
		} else if(pgto.equals("Mercado")) {
			pgto = "Mercado Pago";
		} else if(pgto.equals("Todos")) {
			pgto = "Todos";
		} else {
			pgto = "PayPal";
		}
		
		if(anoInicial.equals(anoFinal)) {
			if(mesInicial.equals(mesFinal)) {
				//mesmo mes e ano:
				for(int i=dia;i<=diaf;i++) {
					if(i<10) {
						boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
									+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+mesInicial+"/"+anoInicial+" %%:%%:%%'");
						rs = statement.getResultSet();			
						while (rs.next()) {
							if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {							
								Relatorio rel = new Relatorio();
								rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
								rel.setPedido(rs.getString("id_order"));
								rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
								rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
								rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
								relatorios.add(rel);
								
								double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
								soma += valor;
								totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
								totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
							}
						}														
					} else {
						boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
									+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+mesInicial+"/"+anoInicial+" %%:%%:%%'");
						rs = statement.getResultSet();			
						while (rs.next()) {
							if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
								Relatorio rel = new Relatorio();
								rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
								rel.setPedido(rs.getString("id_order"));
								rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
								rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
								rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
								relatorios.add(rel);
								
								double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
								soma += valor;
								totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
								totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
							}
						}					
					}
				}
			} else if(!mesInicial.equals(mesFinal)) {
				//meses diferentes:
				
					for(int j=mes;j<=mesf;j++) {						
						//se é o último mês:
						if(j == mesf) {
							for(int i=1;i<=diaf;i++) {
								if(j < 10) {
									if(i < 10) {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));											
												relatorios.add(rel);
							
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}														
									} else {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));											
												relatorios.add(rel);
									
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}					
									}
								} else {
									if(i < 10) {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));											
												relatorios.add(rel);
							
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}														
									} else {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));											
												relatorios.add(rel);
									
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}					
									}
								}
							}
							
							//se está nos outros meses: 
						} else {
							
							if(j == 2 && mesf != 2  && mes != 2) {
								for(int i=1;i<=28;i++) {						
									if(i < 10) {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												relatorios.add(rel);
										
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}														
									} else {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												relatorios.add(rel);
										
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}					
									}										
								}
							}
							
							if(mesInicial.equals("02")) {
								for(int i=dia;i<=28;i++) {						
									if(i < 10) {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												relatorios.add(rel);
										
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}														
									} else {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												relatorios.add(rel);
										
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}					
									}
								}
							}
							
							if((j == 4) || (j == 6) || (j == 9) || (j == 11)) {
								
								int val;
								if(j == mes) {
									val = dia;
								} else {
									val = 1;
								}
								
								for(int i=val;i<=30;i++) {
									if(j == 11) {
										if(i < 10) {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
									
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}														
										} else {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
											
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}					
										}
									} else {
										if(i < 10) {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
									
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}														
										} else {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
											
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}					
										}
									}
								}							
							} else if(j == 1 || j == 3 || j == 5 || j == 7 || j == 8 || j == 10 || j == 12) {
								
								int val;
								if(j == mes) {
									val = dia;
								} else {
									val = 1;
								}
								
								for(int i=val;i<=31;i++) {
									if(j == 10 || j == 12) {
										if(i < 10) {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
										
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}														
										} else {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
												
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}					
										}
									} else {
										if(i < 10) {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
										
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}														
										} else {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
												
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}					
										}
									}								
								}		
							}
						}					
					}				
				} 
			} else {
				//anos diferentes:
				for(int j=mes;j<=12;j++) {
					if(j == 2) {
						for(int i=dia;i<=28;i++) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/02/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/02/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}	
						}
					}
					
					if(j == 2 && mesf != 2  && mes != 2) {
						for(int i=1;i<=28;i++) {						
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {										
											Relatorio rel = new Relatorio();
											rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
											rel.setPedido(rs.getString("id_order"));
											rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
											rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
											rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
											relatorios.add(rel);
								
											double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
											soma += valor;
											totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
											totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));										
									}
								}					
							}
						}
					}
						
				
				if((j == 4) || (j == 6) || (j == 9) || (j == 11)) {
					int val;
					if(j == mes) {
						val = dia;
					} else {
						val = 1;
					}
					for(int i=val;i<30;i++) {
						if(j == 11) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
					
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						} else {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
					
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}
					}
				} else if(j == 1 || j == 3 || j == 5 || j == 7 || j == 8 || j == 10 || j == 12) {
					int val;
					if(j == mes) {
						val = dia;
					} else {
						val = 1;
					}
					for(int i=val;i<=31;i++) {
						if(j == 10 || j == 12) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
									
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						} else {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
									
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}				
					}
				}
			}//final do ano inicial
			//inicio do ano final:
				
				System.out.println("mes final: "+mesf);
				
				for(int j=1;j<=mesf;j++) {					
					if(j == 2 && mesf == 2) {
						for(int i=1;i<=diaf;i++) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/02/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/02/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}	
						}
					}
					
					if(j == 2 && mesf != 2  && mes != 2) {
						for(int i=1;i<=28;i++) {						
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}
					}
						
				
				if((j == 4) || (j == 6) || (j == 9) || (j == 11)) {
					int val;
					if(j == mesf) {
						val = diaf;
					} else {
						val = 30;
					}
					for(int i=1;i<val;i++) {
						if(j == 11) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
					
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						} else {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
					
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}
					}
				} else if(j == 1 || j == 3 || j == 5 || j == 7 || j == 8 || j == 10 || j == 12) {
					int val;
					if(j == mesf) {
						val = diaf;
					} else {
						val = 31;
					}
					for(int i=1;i<=val;i++) {
						if(j == 10 || j == 12) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
									
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						} else {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal,tipo_pagamento from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(rs.getString("orderid").contains("csmr") && (verificarTipoPagamento(pgto,rs.getString("tipo_pagamento")) || (pgto.equals("Todos")))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
									
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}				
					}
				} else {
					
				}
			}				
		}
		
		param.put("dataInicial", dataInicial);
		param.put("dataFinal", dataFinal);
		param.put("totalProdutos", totalProd);
		param.put("totalFrete", totalFrete);
		param.put("total", soma);
		param.put("marketplace", pgto);
		
		if(pgto.equals("Boleto Bancário")) {
			pgto = "BoletoBancario";
		}
		if(pgto.equals("Mercado Pago")) {
			pgto = "MercadoPago";
		}
				
		geraRelatorio(email,"relatorio_"+pgto+"", "Relatorio PDF", relatorios, caminho, "RelatorioPedidos.jasper", param, true);
		
	}
	
	public void gerarRelatorioPedidosMarket(String dataInicial, String dataFinal, String market, String email) throws Exception {
		
		System.out.println("Entrou aqui para gerar o relatório dos Pedidos por marketplace!");
		
		String caminho = "/opt/pedidos/reports/";
		
		HashMap param = new HashMap();
		
		ArrayList<Relatorio> relatorios = new ArrayList<Relatorio>();
		
		String diaInicial = dataInicial.substring(0, 2);
		String mesInicial = dataInicial.substring(3, 5);
		String anoInicial = dataInicial.substring(6, 10);
		
		String diaFinal = dataFinal.substring(0, 2);
		String mesFinal = dataFinal.substring(3, 5);
		String anoFinal = dataFinal.substring(6, 10);
		
		int dia = Integer.parseInt(diaInicial);
		int diaf = Integer.parseInt(diaFinal);
		int mes = Integer.parseInt(mesInicial);
		int mesf = Integer.parseInt(mesFinal);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		ResultSet rs = null;
		
		double soma = 0.0;
		double totalFrete = 0.0;
		double totalProd = 0.0;
		
		if(anoInicial.equals(anoFinal)) {
			if(mesInicial.equals(mesFinal)) {
				//mesmo mes e ano:
				for(int i=dia;i<=diaf;i++) {
					if(i<10) {
						boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
									+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+mesInicial+"/"+anoInicial+" %%:%%:%%'");
						rs = statement.getResultSet();			
						while (rs.next()) {
							if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {								
								Relatorio rel = new Relatorio();
								rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
								rel.setPedido(rs.getString("id_order"));
								rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
								rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
								rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
								relatorios.add(rel);
								
								double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
								soma += valor;
								totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
								totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
							}
						}														
					} else {
						boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
									+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+mesInicial+"/"+anoInicial+" %%:%%:%%'");
						rs = statement.getResultSet();			
						while (rs.next()) {
							if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
								Relatorio rel = new Relatorio();
								rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
								rel.setPedido(rs.getString("id_order"));
								rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
								rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
								rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
								relatorios.add(rel);
								
								double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
								soma += valor;
								totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
								totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
							}
						}					
					}
				}
			} else if(!mesInicial.equals(mesFinal)) {
				//meses diferentes:
				
					for(int j=mes;j<=mesf;j++) {						
						//se é o último mês:
						if(j == mesf) {
							for(int i=1;i<=diaf;i++) {
								if(j < 10) {
									if(i < 10) {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));											
												relatorios.add(rel);
							
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}														
									} else {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));											
												relatorios.add(rel);
									
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}					
									}
								} else {
									if(i < 10) {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));											
												relatorios.add(rel);
							
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}														
									} else {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));											
												relatorios.add(rel);
									
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}					
									}
								}
							}
							
							//se está nos outros meses: 
						} else {
							
							if(j == 2 && mesf != 2  && mes != 2) {
								for(int i=1;i<=28;i++) {						
									if(i < 10) {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												relatorios.add(rel);
										
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}														
									} else {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												relatorios.add(rel);
										
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}					
									}										
								}
							}
							
							if(mesInicial.equals("02")) {
								for(int i=dia;i<=28;i++) {						
									if(i < 10) {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												relatorios.add(rel);
										
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}														
									} else {
										boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
												+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
										rs = statement.getResultSet();			
										while (rs.next()) {
											if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
												Relatorio rel = new Relatorio();
												rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
												rel.setPedido(rs.getString("id_order"));
												rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
												rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
												relatorios.add(rel);
										
												double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												soma += valor;
												totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
												totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
											}
										}					
									}
								}
							}
							
							if((j == 4) || (j == 6) || (j == 9) || (j == 11)) {
								
								int val;
								if(j == mes) {
									val = dia;
								} else {
									val = 1;
								}
								
								for(int i=val;i<=30;i++) {
									if(j == 11) {
										if(i < 10) {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
									
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}														
										} else {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
											
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}					
										}
									} else {
										if(i < 10) {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
									
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}														
										} else {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
											
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}					
										}
									}
								}							
							} else if(j == 1 || j == 3 || j == 5 || j == 7 || j == 8 || j == 10 || j == 12) {
								
								int val;
								if(j == mes) {
									val = dia;
								} else {
									val = 1;
								}
								
								for(int i=val;i<=31;i++) {
									if(j == 10 || j == 12) {
										if(i < 10) {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
										
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}														
										} else {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
												
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}					
										}
									} else {
										if(i < 10) {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
										
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}														
										} else {
											boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
													+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
											rs = statement.getResultSet();			
											while (rs.next()) {
												if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
													Relatorio rel = new Relatorio();
													rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
													rel.setPedido(rs.getString("id_order"));
													rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
													rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
													relatorios.add(rel);
												
													double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													soma += valor;
													totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
													totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
												}
											}					
										}
									}								
								}		
							}
						}					
					}				
				} 
			} else {
				//anos diferentes:
				for(int j=mes;j<=12;j++) {
					if(j == 2) {
						for(int i=dia;i<=28;i++) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/02/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/02/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}	
						}
					}
					
					if(j == 2 && mesf != 2  && mes != 2) {
						for(int i=1;i<=28;i++) {						
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
											Relatorio rel = new Relatorio();
											rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
											rel.setPedido(rs.getString("id_order"));
											rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
											rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
											rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
											relatorios.add(rel);
								
											double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
											soma += valor;
											totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
											totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
										}
									}
								}					
							}
						}
					}
						
				
				if((j == 4) || (j == 6) || (j == 9) || (j == 11)) {
					int val;
					if(j == mes) {
						val = dia;
					} else {
						val = 1;
					}
					for(int i=val;i<30;i++) {
						if(j == 11) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
					
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						} else {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
					
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}
					}
				} else if(j == 1 || j == 3 || j == 5 || j == 7 || j == 8 || j == 10 || j == 12) {
					int val;
					if(j == mes) {
						val = dia;
					} else {
						val = 1;
					}
					for(int i=val;i<=31;i++) {
						if(j == 10 || j == 12) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
									
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						} else {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoInicial+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
									
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}				
					}
				}
			}//final do ano inicial
			//inicio do ano final:
				
				System.out.println("mes final: "+mesf);
				
				for(int j=1;j<=mesf;j++) {					
					if(j == 2 && mesf == 2) {
						for(int i=1;i<=diaf;i++) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/02/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/02/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}	
						}
					}
					
					if(j == 2 && mesf != 2  && mes != 2) {
						for(int i=1;i<=28;i++) {						
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
								
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}
					}
						
				
				if((j == 4) || (j == 6) || (j == 9) || (j == 11)) {
					int val;
					if(j == mesf) {
						val = diaf;
					} else {
						val = 30;
					}
					for(int i=1;i<val;i++) {
						if(j == 11) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
					
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						} else {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
					
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}
					}
				} else if(j == 1 || j == 3 || j == 5 || j == 7 || j == 8 || j == 10 || j == 12) {
					int val;
					if(j == mesf) {
						val = diaf;
					} else {
						val = 31;
					}
					for(int i=1;i<=val;i++) {
						if(j == 10 || j == 12) {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
									
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						} else {
							if(i < 10) {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
							
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}														
							} else {
								boolean sql = statement.execute("select distinct ped.orderid,ped.data_emissao,ped.id_order,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
										+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+j+"/"+anoFinal+" %%:%%:%%'");		
								rs = statement.getResultSet();			
								while (rs.next()) {
									if(market.equals("Todos") || verificarMarketplace(market,rs.getString("orderid"))) {
										Relatorio rel = new Relatorio();
										rel.setDataEmissao(rs.getString("data_emissao").substring(0,10));
										rel.setPedido(rs.getString("id_order"));
										rel.setValorFrete(Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										rel.setValorProdutos(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")));
										rel.setValorTotal(Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", ".")));
										relatorios.add(rel);
									
										double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										soma += valor;
										totalFrete += Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
										totalProd += Double.parseDouble(rs.getString("valortotal").replaceAll(",", "."));
									}
								}					
							}
						}				
					}
				} else {
					
				}
			}				
		}
		
		param.put("dataInicial", dataInicial);
		param.put("dataFinal", dataFinal);
		param.put("totalProdutos", totalProd);
		param.put("totalFrete", totalFrete);
		param.put("total", soma);
		if(market.equals("America")) {
			market = "Casa América";
		}
		param.put("marketplace", market);
		
		geraRelatorio(email,"relatorio_"+market+"", "Relatorio PDF", relatorios, caminho, "RelatorioPedidos.jasper", param, true);
		
	}
	
	public void geraRelatorio(String email, String nome, String titulo, Collection dados, String caminho_arquivo_jasper, String nome_arquivo_jasper, Map parametros, boolean mensagem) throws Exception {		
		
		System.out.println("entrou aqui para gerar o relatório em PDF!");
		
		String pathPdf = "/opt/pedidos/reports/"+nome+".pdf";
		
		DataSource ds = new DataSource(dados);
		
		try {		
			
			JasperReport jasperReport = (JasperReport)JRLoader.loadObject(new File(caminho_arquivo_jasper + nome_arquivo_jasper));				
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, ds);
			
			JRPdfExporter exporter = new JRPdfExporter();
			 
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pathPdf));
			 
			SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
			reportConfig.setSizePageToContent(true);
			reportConfig.setForceLineBreakPolicy(false);
			 
			SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
			exportConfig.setMetadataAuthor("infat");
			exportConfig.setEncrypted(true);
			exportConfig.setAllowedPermissionsHint("PRINTING");
			 
			exporter.setConfiguration(reportConfig);
			exporter.setConfiguration(exportConfig);
			 
			exporter.exportReport();
			
		} catch (JRException e) {
			e.printStackTrace(); 
		}		
		enviarEmailRelatorio(pathPdf,email);		
	}
	
	public void enviarEmailRelatorio(String pathPdf, String email) throws Exception {
		
		System.out.println("entrou aqui para enviar o relatório por e-mail!");
		
		Calendar cal = Calendar.getInstance();
		int mes = cal.get(Calendar.MONTH) + 1;
		int ano = cal.get(Calendar.YEAR);
		int dia = cal.get(Calendar.DAY_OF_MONTH);
		int hora = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		
		String diaStr = "";
		String mesStr = "";		
		
		if(dia<10) {
			diaStr = "0"+dia;
		} else {
			diaStr = ""+dia;
		}
		if(mes<10) {
			mesStr = "0"+mes;
		} else {
			mesStr = ""+mes;
		}
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		
		Properties props = new Properties();
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.from", "no-reply@casaamerica.com.br");
	    props.put("mail.smtp.starttls.enable", "true");
	    props.put("mail.smtp.ssl.enable", "false");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.port", "587");

	    Authenticator authenticator = new Authenticator();
	    props.setProperty("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());

	    Session session = Session.getInstance(props, authenticator);
	    MimeMessage msg = new MimeMessage(session);
	    msg.setFrom();
	    msg.setRecipients(Message.RecipientType.TO, email);
	    msg.setSubject("Relatório de Pedidos");
	    
	    String mensagem = "Olá, \n\n" +
	    		"Segue em anexo o relatório de Pedidos solicitado no portal.\n\n" + 	    		
	    		"Atenciosamente.\n\n" + 
	    		"Portal de integração e faturamento.\n\n";
	    	   
	    MimeBodyPart mbp1 = new MimeBodyPart();
	    mbp1.setText(mensagem);
	    
	    MimeBodyPart mbp2 = new MimeBodyPart();
	    FileDataSource fds = new FileDataSource(pathPdf);
	    mbp2.setDataHandler(new DataHandler(fds));
	    mbp2.setFileName(fds.getName());

	    Multipart mp = new MimeMultipart();
	    mp.addBodyPart(mbp2);
	    mp.addBodyPart(mbp1);
	    
	    msg.setContent(mp);
	    
	    Transport transport;
	    transport = session.getTransport("smtp");
	    transport.connect();
	    msg.saveChanges();
	    transport.sendMessage(msg, msg.getAllRecipients());
	    transport.close();
	    
	    boolean sql = statement.execute(
				"insert into historico (data, nome, mensagem, status) "
						+ "values ('"+diaStr+"/"+mesStr+"/"+ano+" "+hora+":"+min+"', 'Relatório de Pedidos', 'Enviar para "+email+"', 'Processo com sucesso')");
		
	    System.out.println("enviou e-mail com sucesso!");
	    	    	
	}
	
	public class Authenticator extends javax.mail.Authenticator {

        private PasswordAuthentication passwordAuthentication;

        public Authenticator() {

            String username = "no-reply@casaamerica.com.br";
            String password = "america13579";
            passwordAuthentication = new PasswordAuthentication(username, password);
        }

        protected PasswordAuthentication getPasswordAuthentication() {
            return passwordAuthentication;
        }
}

}

