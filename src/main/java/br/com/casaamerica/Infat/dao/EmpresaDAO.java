package br.com.casaamerica.Infat.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.swing.text.MaskFormatter;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.model.Correios;
import br.com.casaamerica.Infat.model.Empresa;

@Repository
@Transactional
public class EmpresaDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public String configuracao(Empresa empresa) {
		
		String resposta = "";
		try {
			List<Empresa> emp = manager.createQuery("select e from Empresa e where e.cnpj = '"+empresa.getCnpj()+"'", Empresa.class).getResultList();
			
			System.out.println("tamanho : " + emp.size());
			
			if(emp.size() <= 0 ) {
				empresa.setEscolhido("NAO");
				manager.persist(empresa);
				System.out.println("empresa nao cadastrada");
				resposta = "NAO";
			} else {
				System.out.println("empresa cadastrada");
				resposta = "SIM";
			}
		
		} catch (Exception e) {
			e.getMessage();
		}
		return resposta;
	}
	
	
	public List<Empresa> findCnpj() {
		
		 return manager.createQuery("select e from Empresa e", Empresa.class).getResultList();
		
	}
	
	public List<Empresa> findCnpjEscolhido() {
		
		 return manager.createQuery("select e from Empresa e where escolhido='SIM'", Empresa.class).getResultList();
		
	}
	
	public String escolhido(String cnpj) throws Exception {
		
		MaskFormatter mask = new MaskFormatter("##.###.###/####-##");
	    mask.setValueContainsLiteralCharacters(false);
	    String c = mask.valueToString(cnpj).replaceAll(" ", "");
	    
	    Empresa empresa = manager.createQuery("select e from Empresa e where cnpj = '"+c+"'", Empresa.class).getSingleResult();
		
		if(empresa.getEscolhido().equals("SIM")) {
			manager.createQuery("update Empresa set escolhido ='NAO' where cnpj = '"+c+"'").executeUpdate();	
			return "nao"+c;
		} else {
			manager.createQuery("update Empresa set escolhido ='SIM' where cnpj = '"+c+"'").executeUpdate();
			return "sim"+c;
		}			    	    
	   
	}
	
	public String removerCnpj(String cnpj) throws Exception {
		
		System.out.println("entrou na remoção de dados");

        MaskFormatter mask = new MaskFormatter("##.###.###/####-##");
        mask.setValueContainsLiteralCharacters(false);
        String c = mask.valueToString(cnpj).replaceAll(" ", "");
        
        System.out.println("remove: " + c + "");
        
        manager.createQuery("delete from Empresa where cnpj = '"+c+"'").executeUpdate();
        
        return c;
		
		
	}
	
	public void updateEmpresa(Empresa empresa) {
		System.out.println("entrou na página de update");		
		manager.merge(empresa);		
	}

}
