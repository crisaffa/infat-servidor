package br.com.casaamerica.Infat.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.model.Pedido;
import br.com.casaamerica.Infat.model.Produto;

@Repository
@Transactional
public class HomeDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	final Locale ptBR = new Locale("pt", "BR");
	
//	final String conexao = "jdbc:postgresql://10.11.248.2/infatAffa";
//	final String conex = "jdbc:postgresql://10.11.248.2/casaamerica";
	final String conexao = "jdbc:postgresql://35.199.83.225/infat";
	final String conex = "jdbc:postgresql://35.199.83.225/casaamerica";
	
	public void somaFrete() throws Exception {
		List<Pedido> pedidos = manager.createQuery("select e from Pedido e where status = 'NFE EMITIDA'", Pedido.class).getResultList();		
		double somaFrete = 0.0;		
		for (Pedido pedido : pedidos) {
			System.out.println("frete: "+pedido.getFrete_total());
			somaFrete += pedido.getFrete_total();
		}		 
		System.out.println("somaFrete: "+somaFrete);		
	}
	
	public void atualizarFinanceiro() throws Exception {
		List<Pedido> pedidos = manager.createQuery("select e from Pedido e where status = 'NFE EMITIDA'", Pedido.class).getResultList();
		for (Pedido pedido : pedidos) {
			Connection con = DriverManager.getConnection(conex, "postgres", "postgres");
			Statement st = con.createStatement();
			boolean sql = st.execute("select * from vendas_vendas where id_order = '"+pedido.getId_order()+"'");
			ResultSet rs = st.getResultSet();
			int id_venda = 0; 
			while(rs.next()) {
				id_venda = rs.getInt("id_venda");								
			}
			double valor = (pedido.getProdutos_total() + pedido.getFrete_total()) - pedido.getDesconto_total();			
			if(id_venda != 0) {				
				boolean sql4 = st.execute("update financeiro_movimentacoes set valor = "+valor+", valor_pago = "+valor+" where id_venda = "+id_venda+"");
				boolean sql5 = st.execute("update financeiro_carteiras_movimentacoes set valor = "+valor+" where numero_cheque = "+id_venda+"");
				boolean sql6 = st.execute("update vendas_vendas set valor_total = "+valor+" where id_venda = "+id_venda+"");
			}
			con.close();						
			System.out.println("executou o update!");			
		}		
		System.out.println("acabou de rodar o atualizar Financeiro!");		
	}
	
	public void preencherbanco() {
		List<Pedido> pedidos = manager.createQuery("select e from Pedido e where status = 'NFE EMITIDA'", Pedido.class).getResultList();
		
		for (Pedido ped : pedidos) {
			
			List<Produto> total2 = manager.createQuery("select p from Produto p where id_order = '"+ped.getId_order()+"'", Produto.class).getResultList();
			
			for (int i = 0; i < (total2.size()/total2.size()); i++) {

				double valor = Double.parseDouble(total2.get(i).getValortotal().replaceAll(",", "."));
				double valor2 = Double.parseDouble(total2.get(i).getFretetotal().replaceAll(",", "."));
				double valor3 = Double.parseDouble(total2.get(i).getDescontoTotal().replaceAll(",", "."));
				
				manager.createQuery("update Pedido set produtos_total ='"+valor+"' where id_order = '"+ped.getId_order()+"'").executeUpdate();
				manager.createQuery("update Pedido set frete_total ='"+valor2+"' where id_order = '"+ped.getId_order()+"'").executeUpdate();
				manager.createQuery("update Pedido set desconto_total ='"+valor3+"' where id_order = '"+ped.getId_order()+"'").executeUpdate();
			}
		}		
	}
	
	public Long vendas() {
		
		Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA'");		
		Long vendas = (Long) query.getSingleResult();
		
		return vendas;
	}
	
	public Long vendasAnuais() {
		
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		
		Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data like '%%/%%/"+ano+" %%:%%'");		
		Long vendas = (Long) query.getSingleResult();
		
		return vendas;
	}
	
	public Long vendasMensais() {
		
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		int mes = cal.get(Calendar.MONTH) + 1;
		
		Long vendas;
		
		if(mes!=10 || mes!=11 || mes!=12) {		
			Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '%%/0"+mes+"/"+ano+" %%:%%:%%'");		
			vendas = (Long) query.getSingleResult();		
		} else {
			Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '%%/"+mes+"/"+ano+" %%:%%:%%'");		
			vendas = (Long) query.getSingleResult();
		}
		
		return vendas;
	}
	
	public Long vendasMensaisPassado() {
		
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);		
		int mes = cal.get(Calendar.MONTH);
		
		Long vendas;
		
		if(mes!=10 || mes!=11 || mes!=12) {		
			Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '%%/0"+mes+"/"+ano+" %%:%%:%%'");		
			vendas = (Long) query.getSingleResult();		
		} else {
			if(mes == 12) {
				int anopassado = cal.get(Calendar.YEAR) - 1;
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '%%/"+mes+"/"+anopassado+" %%:%%:%%'");		
				vendas = (Long) query.getSingleResult();				
			} else {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '%%/"+mes+"/"+ano+" %%:%%:%%'");		
				vendas = (Long) query.getSingleResult();	
			}			
		}		
		
		return vendas;
	}
	
	public Long vendasHoje() {
		
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		int mes = cal.get(Calendar.MONTH) + 1;
		int dia = cal.get(Calendar.DAY_OF_MONTH);
		
		Long vendas;
		
		if(dia > 9) {
			if(mes!=10 || mes!=11 || mes!=12) {						
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '"+dia+"/0"+mes+"/"+ano+" %%:%%:%%'");		
				vendas = (Long) query.getSingleResult();		
			} else {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '"+dia+"/"+mes+"/"+ano+" %%:%%:%%'");		
				vendas = (Long) query.getSingleResult();
			}
		} else {
			if(mes!=10 || mes!=11 || mes!=12) {		
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '0"+dia+"/0"+mes+"/"+ano+" %%:%%:%%'");		
				vendas = (Long) query.getSingleResult();		
			} else {				
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '"+dia+"/"+mes+"/"+ano+" %%:%%:%%'");		
				vendas = (Long) query.getSingleResult();
			}			
		}
		
		return vendas;
	}
	
	public Long vendasSemana() {
		
		Calendar cal = Calendar.getInstance();
		int sem = cal.get(Calendar.DAY_OF_WEEK);
		
		//domingo anterior:
		int domingoAnterior = sem - 1;				
		GregorianCalendar today = new GregorianCalendar();
		today.add(Calendar.DAY_OF_MONTH, -domingoAnterior);
		Date sunday = today.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		String dataDomingo = fmt.format(sunday);
				
		//sabado proximo:
		int sabadoProximo = 7-sem;
		GregorianCalendar today2 = new GregorianCalendar();
		today2.add(Calendar.DAY_OF_MONTH, +sabadoProximo);
		Date saturday = today2.getTime();
		SimpleDateFormat fmt2 = new SimpleDateFormat("dd/MM/yyyy");
		String dataSabado = fmt2.format(saturday);
		
		String mesDomingo = dataDomingo.substring(3, 5);
		String mesSabado = dataSabado.substring(3, 5);
		
		String anoDomingo = dataDomingo.substring(6, 10);
		String anoSabado = dataSabado.substring(6, 10);
		
		String diaDomingo = dataDomingo.substring(0, 2);
		String diaSabado = dataSabado.substring(0, 2);
		
		Long vendas = 0L;
		
		int diaDom = Integer.parseInt(diaDomingo);
		int diaSab = Integer.parseInt(diaSabado);
		
		if(!anoDomingo.equals(anoSabado)) {
			//virada de ano:			
			for(int i=diaDom;i<=31;i++) {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '"+i+"/12/"+anoDomingo+" %%:%%:%%'");		
				vendas += (Long) query.getSingleResult();
			}			
			for(int i=1;i<=diaSab;i++) {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/01/"+anoSabado+" %%:%%:%%'");		
				vendas += (Long) query.getSingleResult();
			}			
		} else {			
			if(!mesDomingo.equals(mesSabado)) {
				//virada de mes:
				for(int i=diaDom;i<=31;i++) {
					Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+mesDomingo+"/"+anoDomingo+" %%:%%:%%'");
					vendas += (Long) query.getSingleResult();
				}
				for(int i=1;i<=diaSab;i++) {					
					Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+mesSabado+"/"+anoSabado+" %%:%%:%%'");
					vendas += (Long) query.getSingleResult();
				}				
			} else {
				//mesmo mes:
				for(int i=diaDom;i<=diaSab;i++) {
					if(i < 10) {
						Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+mesDomingo+"/"+anoDomingo+" %%:%%:%%'");
						vendas += (Long) query.getSingleResult();
					} else {
						Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+mesDomingo+"/"+anoDomingo+" %%:%%:%%'");
						vendas += (Long) query.getSingleResult();	
					}									
				}
			}
		}
		
		return vendas;
	}
	

	public String ticketMedioAnual(Long vendas) throws Exception {
			
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		
		boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
				+ "where ped.status = 'NFE EMITIDA' and data_emissao like '%%/%%/"+ano+" %%:%%:%%'");
		ResultSet rs = statement.getResultSet();
		
		double soma = 0.0;		
		
		while (rs.next()) {
			double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
			soma += valor;
		}
		
		return NumberFormat.getCurrencyInstance(ptBR).format(soma/vendas);
	}
	
	public String ticketMedioMensal(Long vendas) throws Exception {
		
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		int mes = cal.get(Calendar.MONTH) + 1;
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();				
		ResultSet rs;
		
		if(mes!=10 || mes!=11 || mes!=12) {
			boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
					+ "where ped.status = 'NFE EMITIDA' and data_emissao like '%%/0"+mes+"/"+ano+" %%:%%:%%'");
			rs = statement.getResultSet();			
		} else {
			boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
					+ "where ped.status = 'NFE EMITIDA' and data_emissao like '%%/"+mes+"/"+ano+" %%:%%:%%'");
			rs = statement.getResultSet();
		}
		
		double soma = 0.0;		
		
		while (rs.next()) {
			double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
			soma += valor;
		}
		
		return NumberFormat.getCurrencyInstance(ptBR).format(soma/vendas);
	}

	public String receitaAnual() throws Exception {

		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		
		boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
				+ "where ped.status = 'NFE EMITIDA' and data_emissao like '%%/%%/"+ano+" %%:%%:%%'");
		
		ResultSet rs = statement.getResultSet();
		
		double soma = 0.0;				
		
		while (rs.next()) {
			double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
			soma += valor;		
		}
		
		return NumberFormat.getCurrencyInstance(ptBR).format(soma);
	}
	
	public String receitaMensal() throws Exception {

		Calendar cal = Calendar.getInstance();
		int mes = cal.get(Calendar.MONTH) + 1;
		int ano = cal.get(Calendar.YEAR);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		ResultSet rs = null;
		
		if(mes!=10 || mes!=11 || mes!=12) {
			boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '%%/0"+mes+"/"+ano+" %%:%%:%%'");		
			rs = statement.getResultSet();			
		} else {
			boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '%%/"+mes+"/"+ano+" %%:%%:%%'");		
			rs = statement.getResultSet();							
		}
		
		double soma = 0.0;		
		
		while (rs.next()) {
			double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
			soma += valor;
		}
		
		return NumberFormat.getCurrencyInstance(ptBR).format(soma);
	}
	
	public String receitaMensalPassado() throws Exception {

		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);		
		int mes = cal.get(Calendar.MONTH);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		ResultSet rs = null;
		
		double soma = 0.0;
		
		if(mes!=10 || mes!=11 || mes!=12) {
			boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '%%/0"+mes+"/"+ano+" %%:%%:%%'");		
			rs = statement.getResultSet();			
			while (rs.next()) {
				double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
				soma += valor;
			}			
		} else {
			if(mes == 12) {				
				int anopassado = cal.get(Calendar.YEAR) - 1;				
				boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '%%/"+mes+"/"+anopassado+" %%:%%:%%'");		
				rs = statement.getResultSet();	
				while (rs.next()) {
					double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
					soma += valor;
				}
			} else {
				boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '%%/"+mes+"/"+ano+" %%:%%:%%'");		
				rs = statement.getResultSet();
				while (rs.next()) {
					double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
					soma += valor;
				}
			}										
		}
		
		return NumberFormat.getCurrencyInstance(ptBR).format(soma);
	}
	
	public String receitaHoje() throws Exception {

		Calendar cal = Calendar.getInstance();
		int mes = cal.get(Calendar.MONTH) + 1;
		int ano = cal.get(Calendar.YEAR);
		int dia = cal.get(Calendar.DAY_OF_MONTH);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		ResultSet rs = null;
		
		if(dia > 9) {
			if(mes!=10 || mes!=11 || mes!=12) {
				boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+dia+"/0"+mes+"/"+ano+" %%:%%:%%'");		
				rs = statement.getResultSet();			
			} else {
				boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+dia+"/"+mes+"/"+ano+" %%:%%:%%'");		
				rs = statement.getResultSet();							
			}
		} else {
			if(mes!=10 || mes!=11 || mes!=12) {
				boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+dia+"/0"+mes+"/"+ano+" %%:%%:%%'");		
				rs = statement.getResultSet();			
			} else {
				boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+dia+"/"+mes+"/"+ano+" %%:%%:%%'");		
				rs = statement.getResultSet();							
			}
		}
		
		double soma = 0.0;		
		
		while (rs.next()) {
			double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
			soma += valor;
		}
		
		return NumberFormat.getCurrencyInstance(ptBR).format(soma);
	}
	
	
	public String receitaVetorMensalAtual() throws Exception {
		
		Calendar cal = Calendar.getInstance();
		int mes = cal.get(Calendar.MONTH) + 1; //mes atual
		int ano = cal.get(Calendar.YEAR);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		ResultSet rs = null;
				
		String vetor = "";
		
		for(int i=1;i<32;i++) {
			double soma = 0.0;
			rs = null;
			if(i > 9) {
				if(mes!=10 || mes!=11 || mes!=12) {					
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+mes+"/"+ano+" %%:%%:%%'");		
					rs = statement.getResultSet();			
				} else {					
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+mes+"/"+ano+" %%:%%:%%'");		
					rs = statement.getResultSet();							
				}
			} else {
				if(mes!=10 || mes!=11 || mes!=12) {					
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+mes+"/"+ano+" %%:%%:%%'");		
					rs = statement.getResultSet();			
				} else {					
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+mes+"/"+ano+" %%:%%:%%'");		
					rs = statement.getResultSet();							
				}
			}
			
			while (rs.next()) {
				double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));				
				soma += valor;
			}		
			
			if (i==31) {
				vetor = vetor.concat(String.format("%.2f",soma).replace(",","."));
			} else {
				vetor = vetor.concat(String.format("%.2f",soma).replace(",",".")+";");	
			}							
		}
		
		return vetor;
	}
	
	public String receitaVetorMensalPassado() throws Exception {
		
		Calendar cal = Calendar.getInstance();
		int mes = cal.get(Calendar.MONTH); //mes passado
		int ano = cal.get(Calendar.YEAR);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		ResultSet rs = null;
				
		String vetor = "";
		
		for(int i=1;i<32;i++) {
			double soma = 0.0;
			rs = null;
			if(i > 9) {
				if(mes!=10 || mes!=11 || mes!=12) {					
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/0"+mes+"/"+ano+" %%:%%:%%'");		
					rs = statement.getResultSet();			
				} else {					
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+mes+"/"+ano+" %%:%%:%%'");		
					rs = statement.getResultSet();							
				}
			} else {
				if(mes!=10 || mes!=11 || mes!=12) {					
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/0"+mes+"/"+ano+" %%:%%:%%'");		
					rs = statement.getResultSet();			
				} else {					
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+mes+"/"+ano+" %%:%%:%%'");		
					rs = statement.getResultSet();							
				}
			}
			
			while (rs.next()) {
				double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));				
				soma += valor;
			}
			
			if (i==31) {
				vetor = vetor.concat(String.format("%.2f",soma).replace(",","."));
			} else {
				vetor = vetor.concat(String.format("%.2f",soma).replace(",",".")+";");	
			}			 							
		}
		
		return vetor;
	}
	
	public String receitaSemanal() throws Exception {
		
		Calendar cal = Calendar.getInstance();
		int sem = cal.get(Calendar.DAY_OF_WEEK);
		
		//domingo anterior:
		int domingoAnterior = sem - 1;				
		GregorianCalendar today = new GregorianCalendar();
		today.add(Calendar.DAY_OF_MONTH, -domingoAnterior);
		Date sunday = today.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		String dataDomingo = fmt.format(sunday);		
				
		//sabado proximo:
		int sabadoProximo = 7-sem;
		GregorianCalendar today2 = new GregorianCalendar();
		today2.add(Calendar.DAY_OF_MONTH, +sabadoProximo);
		Date saturday = today2.getTime();
		SimpleDateFormat fmt2 = new SimpleDateFormat("dd/MM/yyyy");
		String dataSabado = fmt2.format(saturday);		
		
		String mesDomingo = dataDomingo.substring(3, 5);
		String mesSabado = dataSabado.substring(3, 5);
		
		String anoDomingo = dataDomingo.substring(6, 10);
		String anoSabado = dataSabado.substring(6, 10);
		
		String diaDomingo = dataDomingo.substring(0, 2);
		String diaSabado = dataSabado.substring(0, 2);
		
		int diaDom = Integer.parseInt(diaDomingo);
		int diaSab = Integer.parseInt(diaSabado);
		
		Connection con = DriverManager.getConnection(conexao, "postgres", "postgres");
		Statement statement = con.createStatement();
		ResultSet rs = null;
		
		double soma = 0.0;
		
		if(!anoDomingo.equals(anoSabado)) {
			//virada de ano:			
			for(int i=diaDom;i<=31;i++) {				
				boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/12/"+anoDomingo+" %%:%%:%%'");		
				rs = statement.getResultSet();
				while (rs.next()) {
					double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
					soma += valor;
				}				
			}			
			for(int i=1;i<=diaSab;i++) {
				boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
						+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/01/"+anoSabado+" %%:%%:%%'");		
				rs = statement.getResultSet();
				while (rs.next()) {
					double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
					soma += valor;
				}				
			}			
		} else {			
			if(!mesDomingo.equals(mesSabado)) {
				//virada de mes:
				for(int i=diaDom;i<=31;i++) {
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+mesDomingo+"/"+anoDomingo+" %%:%%:%%'");		
					rs = statement.getResultSet();
					while (rs.next()) {
						double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
						soma += valor;
					}
				}
				for(int i=1;i<=diaSab;i++) {
					boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
							+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+mesSabado+"/"+anoSabado+" %%:%%:%%'");		
					rs = statement.getResultSet();
					while (rs.next()) {
						double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
						soma += valor;
					}					
				}				
			} else {
				//mesmo mes:
				for(int i=diaDom;i<=diaSab;i++) {
					
					if(i < 10) {
						boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
								+ "where ped.status = 'NFE EMITIDA' and data_emissao like '0"+i+"/"+mesDomingo+"/"+anoDomingo+" %%:%%:%%'");		
						rs = statement.getResultSet();
						while (rs.next()) {
							double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
							soma += valor;
						}	
					} else {					
						boolean sql = statement.execute("select distinct orderid,fretetotal,valortotal from pedido ped inner join produto p on p.id_order = ped.id_order "
								+ "where ped.status = 'NFE EMITIDA' and data_emissao like '"+i+"/"+mesDomingo+"/"+anoDomingo+" %%:%%:%%'");		
						rs = statement.getResultSet();
						while (rs.next()) {
							double valor = Double.parseDouble(rs.getString("valortotal").replaceAll(",", ".")) + Double.parseDouble(rs.getString("fretetotal").replaceAll(",", "."));
							soma += valor;
						}		
					}
				}
			}
		}
		
		return NumberFormat.getCurrencyInstance(ptBR).format(soma);
				
	}
	
	public Long retornaQtdPedidosCanceladosAno() {
		
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		
		Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'CANCELADO' and data like '%%/%%/"+ano+" %%:%%'");
		Long cancelados = (Long) query.getSingleResult();
		return cancelados;		
	}
	
	public Long retornaQtdPedidosCanceladosMesAtual() {
		
		Calendar cal = Calendar.getInstance();
		int mes = cal.get(Calendar.MONTH) + 1;
		int ano = cal.get(Calendar.YEAR);
		
		if(mes!=10 || mes!=11 || mes!=12) {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'CANCELADO' and data like '%%/0"+mes+"/"+ano+" %%:%%'");
				Long cancelados = (Long) query.getSingleResult();
				return cancelados;			
		} else {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'CANCELADO' and data like '%%/"+mes+"/"+ano+" %%:%%'");
				Long cancelados = (Long) query.getSingleResult();
				return cancelados;
		}
	}
	
	public Long retornaQtdPedidosFaturadosAno() {
		
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		
		Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data like '%%/%%/"+ano+" %%:%%'");
		Long faturados = (Long) query.getSingleResult();
		return faturados;		
	}
	
	public Long retornaQtdPedidosFaturadosMesAtual() {
		
		Calendar cal = Calendar.getInstance();
		int mes = cal.get(Calendar.MONTH) + 1;
		int ano = cal.get(Calendar.YEAR);
		
		if(mes!=10 || mes!=11 || mes!=12) {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '%%/0"+mes+"/"+ano+" %%:%%:%%'");
				Long faturados = (Long) query.getSingleResult();
				return faturados;
		} else {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'NFE EMITIDA' and data_emissao like '%%/"+mes+"/"+ano+" %%:%%:%%'");
				Long faturados = (Long) query.getSingleResult();
				return faturados;
		}						
	}
	
	public Long retornaQtdPedidosPrepEntregaAno(){
		
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		
		Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'PREPARANDO ENTREGA' and data like '%%/%%/"+ano+" %%:%%'");
		Long preparandoEntrega = (Long) query.getSingleResult();
		return preparandoEntrega;
	}	
	
	public Long retornaQtdPedidosPrepEntregaMesAtual() {
		
		Calendar cal = Calendar.getInstance();
		int mes = cal.get(Calendar.MONTH) + 1;
		int ano = cal.get(Calendar.YEAR);
		
		if(mes!=10 || mes!=11 || mes!=12) {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'PREPARANDO ENTREGA' and data like '%%/0"+mes+"/"+ano+" %%:%%'");
				Long preparandoEntrega = (Long) query.getSingleResult();
				return preparandoEntrega;
		} else {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'PREPARANDO ENTREGA' and data between '%%/"+mes+"/"+ano+" %%:%%'");
				Long preparandoEntrega = (Long) query.getSingleResult();
				return preparandoEntrega;
		}
	}
	
	public Long retornaQtdPedidosAgPgtoAno(){
		
		Calendar cal = Calendar.getInstance();
		int ano = cal.get(Calendar.YEAR);
		
		Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'AGUARDANDO PAGAMENTO' and data like '%%/%%/"+ano+" %%:%%'");
		Long aguardandoPgto = (Long) query.getSingleResult();
		return aguardandoPgto;
	}
	
	public Long retornaQtdPedidosAgPgtoMesAtual() {
		
		Calendar cal = Calendar.getInstance();
		int mes = cal.get(Calendar.MONTH) + 1;
		int ano = cal.get(Calendar.YEAR);
		
		if(mes!=10 || mes!=11 || mes!=12) {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'AGUARDANDO PAGAMENTO' and data like '%%/0"+mes+"/"+ano+" %%:%%'");
				Long preparandoEntrega = (Long) query.getSingleResult();
				return preparandoEntrega;
		} else {
				Query query = manager.createQuery("select count(e) from Pedido e where e.status = 'AGUARDANDO PAGAMENTO' and data between '%%/"+mes+"/"+ano+" %%:%%'");
				Long preparandoEntrega = (Long) query.getSingleResult();
				return preparandoEntrega;
		}
	}

}
