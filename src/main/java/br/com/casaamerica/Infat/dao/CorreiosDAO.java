package br.com.casaamerica.Infat.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.model.Correios;

@Repository
@Transactional
public class CorreiosDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public String configuracao(Correios correio) {
        
        String resposta = "";
        
        try {
            List<Correios> correios = manager.createQuery("select e from Correios e where e.identificador = '"+correio.getIdentificador()+"'", Correios.class).getResultList();
            
            System.out.println("tamanho : " + correios.size());
            
            if(correios.size() <= 0 ) {
                correio.setEscolhido("NAO");
                String ident = (correio.getIdentificador()).replace(".", "").replace("/", "").replace("-", "");
                correio.setIdentificador(ident);
                manager.persist(correio);
                System.out.println("conta nao cadastrada");
                resposta = "NAO";
            } else {
                System.out.println("conta cadastrada");
                resposta = "SIM";
            }
        
        } catch (Exception e) {
            e.getMessage();
        }
        
        return resposta;
    }
	
	public List<Correios> findContasCorreios() {
		
		 return manager.createQuery("select e from Correios e", Correios.class).getResultList();
		
	}
	
	public int findContaCorreiosEscolhido() {
		
		List<Correios> correios = manager.createQuery("select e from Correios e where escolhido='SIM'", Correios.class).getResultList();
		
		if(correios.size() == 0) {		
			return 0;
		} else {
			return 1;
		}
		
	}
	
	public List<Correios> findContaCorreioEscolhido() {
		
		 return manager.createQuery("select e from Correios e where escolhido='SIM'", Correios.class).getResultList();
		
	}
	
	public Correios retornaContaCorreioEscolhido() {
		
		return manager.createQuery("select e from Correios e where escolhido='SIM'", Correios.class).getSingleResult();
				
	}
	
	public String escolhido(String cnpj) throws Exception {
		
		Correios correio = manager.createQuery("select e from Correios e where identificador='"+cnpj+"'", Correios.class).getSingleResult();
		
		if(correio.getEscolhido().equals("SIM")) {
			manager.createQuery("update Correios set escolhido ='NAO' where identificador = '"+cnpj+"'").executeUpdate();
			return "nao"+correio.getUsuario();
		} else {
			manager.createQuery("update Correios set escolhido ='NAO'").executeUpdate();
			manager.createQuery("update Correios set escolhido ='SIM' where identificador = '"+cnpj+"'").executeUpdate();
			return "sim"+correio.getUsuario();
		}			
	}
	
	public String removerCnpj(String cnpj) throws Exception {
		
		Correios correio = manager.createQuery("select e from Correios e where identificador='"+cnpj+"'", Correios.class).getSingleResult();
		
		manager.createQuery("delete from Correios where identificador = '"+cnpj+"'").executeUpdate();                
        
        return correio.getUsuario();                
	}
	
	public void updateCorreios(Correios correio) {
		System.out.println("entrou na página de update");
		manager.merge(correio);		
	}
	
}
