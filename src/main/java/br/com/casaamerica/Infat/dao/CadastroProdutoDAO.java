package br.com.casaamerica.Infat.dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.model.CadastroProduto;

@Repository
@Transactional
public class CadastroProdutoDAO {

	@PersistenceContext
	EntityManager manager;
	
	public final String keyLoja = "vtexappkey-casaamerica-ABHYEZ";
	public final String tokenLoja = "QAXGZRVYBWXDEERBUCNUPRYDLHLXIVRAXPQESTYXSKGGCJLWQUXTAGMZCUUSFXPFEXGPONIBEPCEKRACWPHOYYZRCRBXYGSUVBCFCWHCMWPKEIOKUSWQPBHGXPMXLKCY";
	
	public void loadDadosVtex(String id) throws Exception {
		
		System.out.println("entrou para atualizar os dados do produto: "+id);
	
		String url = "https://casaamerica.vtexcommercestable.com.br/api/catalog_system/pvt/sku/stockkeepingunitbyid/3"+id+"";
		HttpGet get = new HttpGet(url);

		CloseableHttpClient client = HttpClientBuilder.create().build();
		get.addHeader("cache-control", "no-cache");
		get.addHeader("content-type", "charset=UTF-8");
		get.addHeader("content-type", "application/json");
		get.addHeader("X-VTEX-API-AppKey", keyLoja);
		get.addHeader("X-VTEX-API-AppToken", tokenLoja);

		HttpResponse response = client.execute(get);
		System.out.println("[sendGET COMTELE] REQUEST: ");
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

		if (response.getStatusLine().getStatusCode() != 200) {
			String resposta = "[tratarResposta COMTELE] Response Code : " + response.getStatusLine().getStatusCode();
			resposta += "\nReason : " + response.getStatusLine().getReasonPhrase();
			System.out.println("resposta da API Vtex: "+resposta);
		} else {
			System.out.println("resposta da API Vtex: 200 - OK!");	
		}
		StringBuffer result = new StringBuffer();
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		JSONParser parser = new JSONParser();
		if (response.getStatusLine().getStatusCode() == 200) {
			JSONArray results = (JSONArray) parser.parse("[" + result.toString() + "]");
			for (int i = 0; i < results.size(); i++) {
				JSONObject jsonObject = (JSONObject) results.get(i);
				Boolean ativo = (Boolean) jsonObject.get("IsActive");
				if (ativo == true) {
					Query query = manager.createQuery("update CadastroProduto set status = 'ATIVO' where id_produto = " + id + "");
					query.executeUpdate();
				} else {
					Query query = manager.createQuery("update CadastroProduto set status = 'DESATIVADO' where id_produto = " + id + "");
					query.executeUpdate();					
				}
				
				//atualiza imagem:
				Query query = manager.createQuery("update CadastroProduto set imagem = '"+jsonObject.get("ImageUrl")+"' where id_produto = " + id + "");
				query.executeUpdate();	
					
				//atualiza skuName:
				Query query2 = manager.createQuery("update CadastroProduto set nome = '"+jsonObject.get("SkuName")+"' where id_produto = " + id + "");
				query2.executeUpdate();
					
				//atualiza descrição:
				String descricao = (String)jsonObject.get("ProductDescription");
				if(!descricao.equals("")) {
					if(descricao.contains("<")) {
						int tam = descricao.indexOf("</b>") + 8;				
						descricao = descricao.substring(tam, descricao.length());
						Query query3 = manager.createQuery("update CadastroProduto set descricao = '"+descricao+"' where id_produto = " + id + "");
						query3.executeUpdate();	
					} else {
						Query query3 = manager.createQuery("update CadastroProduto set descricao = '"+descricao+"' where id_produto = " + id + "");
						query3.executeUpdate();
					}						
				}
				
				//atualiza estoque:
				int estoque = retornarEstoque(id);
				Query query4 = manager.createQuery("update CadastroProduto set estoque = "+estoque+" where id_produto = " + id + "");
				query4.executeUpdate();
					
				//atualiza preço:
				String preco = retornarPreco(id);
				if(retornarPromocao(id).equals("null") || retornarPromocao(id).equals("0") || retornarPromocao(id).contains(",")) {
					Query query5 = manager.createQuery("update CadastroProduto set preco_promocao = '"+preco+"' where id_produto = " + id + "");
					query5.executeUpdate();
					Query query6 = manager.createQuery("update CadastroProduto set preco_venda = '"+preco+"' where id_produto = " + id + "");
					query6.executeUpdate();
				} else if(retornarPromocao(id).contains(".")) {
					double prom = Double.parseDouble(retornarPromocao(id).replaceAll(",", "."));
					double valorPreco = Double.parseDouble(preco.replaceAll(",", "."));
					double valor = valorPreco / (1 - prom);
					String valorString = (String.format("%.1f", valor)).replace(".",",");
					Query query5 = manager.createQuery("update CadastroProduto set preco_promocao = '"+preco+"' where id_produto = " + id + "");
					query5.executeUpdate();
					Query query6 = manager.createQuery("update CadastroProduto set preco_venda = '"+valorString+"' where id_produto = " + id + "");
					query6.executeUpdate();
				} 
				
				//atualiza cor:
				JSONArray results2 = (JSONArray) parser.parse("" + jsonObject.get("SkuSpecifications") + "");
				
				if((results2.toString()).length() != 2) {
					for (int j = 0; j < results2.size(); j++) {
						JSONObject jsonObject2 = (JSONObject) results2.get(j);
						String stringJsonObject = jsonObject2.toString();
						int tam1 = stringJsonObject.indexOf("[\"") + 2;
						int tam2 = stringJsonObject.indexOf("],") - 1;					
						String cor = stringJsonObject.substring(tam1, tam2);
						if(cor.equals("null")) {
							cor = "";
						}
						Query query7 = manager.createQuery("update CadastroProduto set cor = '"+cor+"' where id_produto = " + id + "");
						query7.executeUpdate();
					}
				} else {
					Query query7 = manager.createQuery("update CadastroProduto set cor = ' ' where id_produto = " + id + "");
					query7.executeUpdate();
				}
				
				//atualiza dimensões:
				JSONArray results3 = (JSONArray) parser.parse("[" + jsonObject.get("Dimension") + "]");
				for (int k = 0; k < results3.size(); k++) {
					JSONObject jsonObject3 = (JSONObject) results3.get(k);
					String height = String.format("%.1f", (Double) jsonObject3.get("height")).replace(".",",");
					String length = String.format("%.1f", (Double) jsonObject3.get("length")).replace(".",",");
					String weight = String.format("%.1f", (Double) jsonObject3.get("weight")).replace(".",",");
					String width = String.format("%.1f", (Double) jsonObject3.get("width")).replace(".",",");
					if(height.equals("null")) {
						height = "";
					}
					if(length.equals("null")) {
						length = "";
					}
					if(weight.equals("null")) {
						weight = "";
					} else if(weight.length()==3) {
						weight = weight.charAt(2)+"00,0";
					}
					if(width.equals("null")) {
						width = "";
					}
					Query query8 = manager.createQuery("update CadastroProduto set altura = '"+height+"' where id_produto = " + id + "");
					query8.executeUpdate();
					Query query9 = manager.createQuery("update CadastroProduto set comprimento = '"+length+"' where id_produto = " + id + "");
					query9.executeUpdate();
					Query query10 = manager.createQuery("update CadastroProduto set peso = '"+weight+"' where id_produto = " + id + "");
					query10.executeUpdate();
					Query query11 = manager.createQuery("update CadastroProduto set largura = '"+width+"' where id_produto = " + id + "");
					query11.executeUpdate();
				}				
			}			
		} else {
			System.out.println("produto pendente");			
		}		
	}
	
	public List<CadastroProduto> listaProdutos() throws Exception {
		List<CadastroProduto> produtos = manager.createQuery("select e from CadastroProduto e where exibir = 'SIM'", CadastroProduto.class).getResultList();
		return produtos;
	}

	public CadastroProduto retornaProdutoEspecifico(String id_produto) {
		CadastroProduto produto = manager.createQuery("select e from CadastroProduto e where id_produto = " + id_produto + "", CadastroProduto.class).getSingleResult();
		return produto;
	}

	public String retornarPromocao(String id_produto) {
		CadastroProduto produto = manager.createQuery("select e from CadastroProduto e where id_produto = " + id_produto + "", CadastroProduto.class).getSingleResult();
		return produto.getPreco_promocao();
	}
	
	public Object totalProdutosInativos() {
		Query query = manager.createQuery("select count(e) from CadastroProduto e where status = 'DESATIVADO'");
		return query.getSingleResult();
	}

	public Object totalProdutosAtivos() {
		Query query = manager.createQuery("select count(e) from CadastroProduto e where status = 'ATIVO'");
		return query.getSingleResult();
	}

	public Object totalProdutosPendentes() {
		Query query = manager.createQuery("select count(e) from CadastroProduto e where status = 'PENDENTE'");
		return query.getSingleResult();
	}
	
	public void updateProduto(CadastroProduto cad) {
		manager.merge(cad);
	}
	
	public int retornarEstoque(String id) throws Exception {
		
		System.out.println("entrou aqui pra pegar o estoque atualizado o id: "+id+"");
		
		String url = "http://casaamerica.vtexcommercestable.com.br/api/logistics/pvt/inventory/skus/3"+id+"";
		HttpGet get = new HttpGet(url);

		CloseableHttpClient client = HttpClientBuilder.create().build();
		get.addHeader("cache-control", "no-cache");
		get.addHeader("content-type", "charset=UTF-8");
		get.addHeader("content-type", "application/json");
		get.addHeader("X-VTEX-API-AppKey", keyLoja);
		get.addHeader("X-VTEX-API-AppToken", tokenLoja);

		HttpResponse response = client.execute(get);
		System.out.println("[sendGET COMTELE] REQUEST: ");
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

		if (response.getStatusLine().getStatusCode() != 200) {
			String resposta = "[tratarResposta COMTELE] Response Code : " + response.getStatusLine().getStatusCode();
			resposta += "\nReason : " + response.getStatusLine().getReasonPhrase();
			System.out.println("resposta da API Vtex: "+resposta);
		} else {
			System.out.println("resposta da API Vtex: 200 - OK!");	
		}
		StringBuffer result = new StringBuffer();
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		JSONParser parser = new JSONParser();
		if (response.getStatusLine().getStatusCode() == 200) {
			JSONArray results = (JSONArray) parser.parse("[" + result.toString() + "]");			
			for (int i = 0; i < results.size(); i++) {				
				JSONObject jsonObject = (JSONObject) results.get(i);				
				JSONArray results2 = (JSONArray) parser.parse("" + jsonObject.get("balance") + "");
				for (int c = 0; c < results2.size(); c++) {
					JSONObject jsonObject2 = (JSONObject) results2.get(c);					
					String loja = (String) jsonObject2.get("warehouseName");
					if(loja.equals("Deposito Alves e Mohallem")) {
						Long totalQuantity = (Long) jsonObject2.get("totalQuantity");
						return Integer.valueOf(totalQuantity.toString());						
					}					
				}
			}
		}
		return 0;
	}
	
	public String retornarPreco(String id) throws Exception {

		System.out.println("entrou aqui pra pegar o preço atualizado do id: "+id+"");
		
		String url = "http://casaamerica.vtexcommercestable.com.br/api/pricing/prices/3"+id+"";
		HttpGet get = new HttpGet(url);

		CloseableHttpClient client = HttpClientBuilder.create().build();
		get.addHeader("cache-control", "no-cache");
		get.addHeader("content-type", "charset=UTF-8");
		get.addHeader("content-type", "application/json");
		get.addHeader("X-VTEX-API-AppKey", keyLoja);
		get.addHeader("X-VTEX-API-AppToken", tokenLoja);

		HttpResponse response = client.execute(get);
		System.out.println("[sendGET COMTELE] REQUEST: ");
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

		if (response.getStatusLine().getStatusCode() != 200) {
			String resposta = "[tratarResposta COMTELE] Response Code : " + response.getStatusLine().getStatusCode();
			resposta += "\nReason : " + response.getStatusLine().getReasonPhrase();
			System.out.println("resposta da API Vtex: "+resposta);
		} else {
			System.out.println("resposta da API Vtex: 200 - OK!");	
		}
		StringBuffer result = new StringBuffer();
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		JSONParser parser = new JSONParser();
		String valor = "";
		if (response.getStatusLine().getStatusCode() == 200) {
			JSONArray results = (JSONArray) parser.parse("[" + result.toString() + "]");			
			for (int i = 0; i < results.size(); i++) {				
				JSONObject jsonObject = (JSONObject) results.get(i);
				double preco = (Double) jsonObject.get("basePrice");
				valor = (String.format("%.2f", preco)).replace(".",",");				
			}
		}		
		return valor;		
	}	
}