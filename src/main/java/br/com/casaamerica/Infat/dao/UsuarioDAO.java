package br.com.casaamerica.Infat.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import br.com.casaamerica.Infat.model.Usuario;

@Repository
public class UsuarioDAO implements UserDetailsService{
	
	@PersistenceContext
	private EntityManager manager;
	
	public Usuario loadUserByUsername(String usuario) {
		
		System.out.println( "Login: " + usuario);
		List<Usuario> usuarios = manager.createQuery("select u from Usuario u where u.usuario = :usuario", Usuario.class)
                .setParameter("usuario", usuario)
                .getResultList();
		
		if(usuarios.isEmpty()) {
			throw new UsernameNotFoundException("O usuário " + usuario + " não foi encontrado");
		}
		return usuarios.get(0);
	}
	
}
