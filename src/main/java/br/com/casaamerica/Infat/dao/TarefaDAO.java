package br.com.casaamerica.Infat.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.model.Tarefa;

@Repository
@Transactional
public class TarefaDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public String ativar(int id) throws Exception {
		
		Tarefa t = manager.createQuery("select e from Tarefa e where e.id = '"+id+"'", Tarefa.class).getSingleResult();
		
		String nome = "";
		
		if(t.isAtivo()) {
			manager.createQuery("update Tarefa set ativo ='false' where id = '"+id+"'").executeUpdate();
			nome = "nao"+t.getNome();
		} else {
			manager.createQuery("update Tarefa set ativo ='true' where id = '"+id+"'").executeUpdate();			
			nome = "sim"+t.getNome();
		}			
		
		return nome;
	}
	
	public boolean validarEmail(int id) {
		
		Tarefa t = manager.createQuery("select e from Tarefa e where e.id = '"+id+"'", Tarefa.class).getSingleResult();
		
		if(t.getNome().equals("CSU")) {
			if((t.getEmails() == null) || (t.getEmails().equals(""))){
				return false;
			} else {
				return true;
			}	
		} else {
			return true;
		}
				
	}
	
	public List<Tarefa> listarTarefas() {
		
		return manager.createQuery("select t from Tarefa t", Tarefa.class).getResultList();
		
	}
	
	public void updateTarefa(int id, String emails, String emailsCc) {
		System.out.println("entrou na página de update");		
		manager.createQuery("update Tarefa set emails ='"+emails+"' where id = '"+id+"'").executeUpdate();
		manager.createQuery("update Tarefa set emailscc ='"+emailsCc+"' where id = '"+id+"'").executeUpdate();				
	}
		
	
}
