package br.com.casaamerica.Infat.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import br.com.casaamerica.Infat.model.Cliente;
import br.com.casaamerica.Infat.model.Correios;
import br.com.casaamerica.Infat.model.DataSource;
import br.com.casaamerica.Infat.model.Empresa;
import br.com.casaamerica.Infat.model.Pedido;
import br.com.casaamerica.Infat.model.Produto;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;

@Repository
@Transactional
public class EtiquetaDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	@Autowired
	FaturarDAO dao;
	
	@Autowired
	PedidosDAO ped;

	//Conta Correios-ACIEI
	/*public static final String identificador = "17861584000192";	
	public static final String idServico = "124884";	
	public static final String usuario = "17861584000192";
	public static final String senha = "9wptig";
	public static final String cartaoPostagem = "0072943734";
	public static final String idContrato = "9912407401";
	public static final String codAdministrativo = "16355733";
	public static final String codServicoPostagem = "04669";	
	
	//Conta Correios-Homologação
	public static final String identificador = "34028316000103";	
	public static final String idServico = "124884";	
	public static final String usuario = "sigep";
	public static final String senha = "n5f9t8";
	public static final String cartaoPostagem = "0067599079";
	public static final String idContrato = "9992157880";
	public static final String codAdministrativo = "17000190";
	public static final String codServicoPostagem = "04162";
	*/
	
	public File gerarXmlBuscaCliente(Correios correios) throws IOException{
		
		System.out.println("entrou para gerar o XML para buscar rastreio");
		
		File file;								
		File dir = new File("/opt/pedidos/correios/xml");
		
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		file = new File(dir, "buscaCliente.xml");					
		file.createNewFile();			
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		
		bw.write("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cli=\"http://cliente.bean.master.sigep.bsb.correios.com.br/\">");		
		bw.newLine();				
		bw.write("	<soapenv:Header/>");
		bw.newLine();		
		bw.write("	<soapenv:Body>");
		bw.newLine();		
		bw.write("		<cli:buscaCliente>");
		bw.newLine();		
		bw.write("			<idContrato>"+correios.getContrato()+"</idContrato>");
		bw.newLine();		
		bw.write("			<idCartaoPostagem>"+correios.getCartaoPostagem()+"</idCartaoPostagem>");
		bw.newLine();		
		bw.write("			<usuario>"+correios.getUsuario()+"</usuario>");
		bw.newLine();		
		bw.write("			<senha>"+correios.getSenha()+"</senha>");
		bw.newLine();		
		bw.write("		</cli:buscaCliente>");
		bw.newLine();		
		bw.write("	</soapenv:Body>");
		bw.newLine();		
		bw.write("</soapenv:Envelope>");
		bw.close();
		
		return file;
		
	}
	
	public ArrayList<String> buscarCliente(File input) throws ClientProtocolException, IOException{
        
        System.out.println("entrou para buscar Cliente!");		
        
        //String url = "https://apphom.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl"; //HOMOLOGAÇÃO
        String url = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl";//PRODUÇÃO 
        
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);

        String dadosNFe = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(
		new FileInputStream(input), "UTF-8"));

        String line = br.readLine();
        while (line != null) {
        	dadosNFe += line;
        	line = br.readLine();
        }
        br.close();

        String teste = Normalizer.normalize(dadosNFe, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");

        StringEntity stringEntity = new StringEntity(teste);
        stringEntity.setContentEncoding("UTF-8");
        stringEntity.setContentType("text/xml");

        post.setEntity(stringEntity);
        post.addHeader("cache-control", "no-cache");
        post.addHeader("User-Agent", "Jakarta Commons-HttpClient/3.0.1");		
        post.addHeader("Connection", "close");
        
        HttpResponse response = httpClient.execute(post);

        System.out.println("\nSending 'POST' request to URL: " + url);
        System.out.println("Post parameters: " + post.getEntity());

        int responseCode = response.getStatusLine().getStatusCode();

        System.out.println("Response Code: " + responseCode);
        
        ArrayList<String> lista = new ArrayList<String>();
        
        if(responseCode == 200) {
        	
        	BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            line = "";
            while ((line = rd.readLine()) != null) {
            	result.append(line);
            }

            httpClient.close();
            
            String[] aux = (result.toString()).split("<descricao>PAC CONTRATO AGENCIA");
            
            String parte = aux[0].substring((aux[0].length())-150, aux[0].length());                
            
            String[] parteCodigo = parte.split("<codigo>");

            String codServicoPostagem = parteCodigo[1].substring(0, 5);
            
            String[] auxServico = (aux[1].toString()).split("<id>");
            
            String idServico = auxServico[1].substring(0, 6);
            
            lista.add(codServicoPostagem);
            lista.add(idServico);
            
            if(retornarCodigoServicoPostagem(codServicoPostagem).equals("null")) {
            	manager.createQuery("update Correios set codigoservicopostagem = '"+codServicoPostagem+"' where escolhido = 'SIM'").executeUpdate();	
            }
                                
        } else {        	             
             lista.add("0");             
        }

        return lista;        
    }
	
	public File gerarXmlConsulta(ArrayList<String> dados, Correios correio) throws IOException {
		
		System.out.println("entrou para gerar o XML para buscar rastreio");
		
		File file;								
		File dir = new File("/opt/pedidos/correios/xml");
		
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		file = new File(dir, "consultaRastreio.xml");					
		file.createNewFile();
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		
		bw.write("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cli=\"http://cliente.bean.master.sigep.bsb.correios.com.br/\">");		
		bw.newLine();				
		bw.write("	<soapenv:Header/>");
		bw.newLine();		
		bw.write("	<soapenv:Body>");
		bw.newLine();		
		bw.write("		<cli:solicitaEtiquetas>");
		bw.newLine();		
		bw.write("			<tipoDestinatario>C</tipoDestinatario>");
		bw.newLine();		
		bw.write("			<identificador>"+correio.getIdentificador()+"</identificador>");
		bw.newLine();		
		bw.write("			<idServico>"+dados.get(1)+"</idServico>");
		bw.newLine();		
		bw.write("			<qtdEtiquetas>1</qtdEtiquetas>");
		bw.newLine();		
		bw.write("			<usuario>"+correio.getUsuario()+"</usuario>");
		bw.newLine();		
		bw.write("			<senha>"+correio.getSenha()+"</senha>");
		bw.newLine();		
		bw.write("		</cli:solicitaEtiquetas>");
		bw.newLine();		
		bw.write("	</soapenv:Body>");
		bw.newLine();		
		bw.write("</soapenv:Envelope>");
		bw.close();		
		
		return file;		
	}
	
	public String buscarEtiqueta(File input, String id) throws ClientProtocolException, IOException{
        
        System.out.println("entrou para buscar Etiquetas!!!");		
        
        //String url = "https://apphom.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl"; //HOMOLOGAÇÃO        
        String url = "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente?wsdl";//PRODUÇÃO 
        
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(url);

        String dadosNFe = "";
        BufferedReader br = new BufferedReader(new InputStreamReader(
		new FileInputStream(input), "UTF-8"));

        String line = br.readLine();
        while (line != null) {
        	dadosNFe += line;
        	line = br.readLine();
        }
        br.close();

        String teste = Normalizer.normalize(dadosNFe, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");

        StringEntity stringEntity = new StringEntity(teste);
        stringEntity.setContentEncoding("UTF-8");
        stringEntity.setContentType("text/xml");

        post.setEntity(stringEntity);
        post.addHeader("cache-control", "no-cache");
        post.addHeader("User-Agent", "Jakarta Commons-HttpClient/3.0.1");		
        post.addHeader("Connection", "close");
        
        HttpResponse response = httpClient.execute(post);

        System.out.println("\nSending 'POST' request to URL: " + url);
        System.out.println("Post parameters: " + post.getEntity());

        int responseCode = response.getStatusLine().getStatusCode();

        System.out.println("Response Code: " + responseCode);

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        line = "";
        while ((line = rd.readLine()) != null) {
        	result.append(line);
        }

        httpClient.close();
        
        String[] aux = (result.toString()).split("return");
        String etiqueta = aux[1].substring(1,11)+"BR";
        
        String numEtiquetaFinal = geraEtiquetaComDigitoVerificador(etiqueta);
        
        manager.createQuery("update Pedido set rastreio = '"+numEtiquetaFinal+"' where id_order = '"+id+"'").executeUpdate();
        
        return numEtiquetaFinal;                
        
    }
	
	public void gerarTxtEtiqueta(List<Cliente> cliente, List<Empresa> empresa, String numeroNota, String id_order, 
			String cod_rastreio, String pesoTotal) throws Exception {
		
		System.out.println("entrou aqui para gerar etiqueta em txt");
		
		String nome = "";
		String nomeRecebedor = "";
		String endereco = "";
		String complemento = "";
		String numero = "";
		String bairro = "";
		String cidade = "";
		String estado = "";		
		String cep = "";
		String ref = "";
		
		String nomeEmpresa = "";		
		String logradouro = "";
		String numeroEmpresa = "";
		String bairroEmpresa = "";
		String municipio = "";
		String ufEmpresa = "";
		String cepEmpresa = "";
				
		for (Cliente cli : cliente) {
			nome = cli.getNome();
			nomeRecebedor = cli.getNome_recebedor();
			endereco = cli.getEndereco();
			complemento = cli.getComplemento();
			numero = cli.getNumero();
			bairro = cli.getBairro();
			cidade = cli.getCidade();
			estado = cli.getEstado();
			cep = cli.getCep();	
			ref = cli.getReference();
		}
		
		for (Empresa emp : empresa) {
			nomeEmpresa = emp.getNomeFantasia() + " MAGAZINE";
			logradouro = emp.getLogradouro();
			numeroEmpresa = emp.getNumero();
			bairroEmpresa = emp.getBairro();
			municipio = emp.getMunicipio();
			ufEmpresa = emp.getUf();
			cepEmpresa = emp.getCep();			
		}
		
		//Verifica o nome do Recebedor:
		if(!nome.equals(nomeRecebedor)) {
			nome = nomeRecebedor;
		}
		//
		
		//valida abreviaturas:
		if(endereco.contains("DESEMBARGADOR")){
			endereco = endereco.replaceAll("DESEMBARGADOR", "DES.");	
		}
		if(endereco.contains("Desembargador")) {
			endereco = endereco.replaceAll("Desembargador", "Des.");
		}
		if(endereco.contains("CONSELHEIRO")){
			endereco = endereco.replaceAll("CONSELHEIRO", "CONS.");	
		}
		if(endereco.contains("Conselheiro")) {
			endereco = endereco.replaceAll("Conselheiro", "Cons.");
		}
		if(endereco.contains("BRIGADEIRO")){
			endereco = endereco.replaceAll("BRIGADEIRO", "BRIG.");	
		}
		if(endereco.contains("Brigadeiro")) {
			endereco = endereco.replaceAll("Brigadeiro", "Brig.");
		}
		if(endereco.contains("EVANGELISTA")){
			endereco = endereco.replaceAll("EVANGELISTA", "EVG.");	
		}
		if(endereco.contains("Evangelista")) {
			endereco = endereco.replaceAll("Evangelista", "Evg.");
		}
		if(endereco.contains("AVENIDA")) {
			endereco = endereco.replaceAll("AVENIDA", "AV.");			
		}
		if(endereco.contains("Avenida")) {
			endereco = endereco.replaceAll("Avenida", "Av.");
		}					
		if(endereco.contains("RODOVIA")) {
			endereco = endereco.replaceAll("RODOVIA", "ROD.");			
		}
		if(endereco.contains("Rodovia")) {
			endereco = endereco.replaceAll("Rodovia", "Rod.");
		}
		if(endereco.contains("TRAVESSA")) { 
			endereco = endereco.replaceAll("TRAVESSA", "TRV.");			
		}
		if(endereco.contains("Travessa")) {
			endereco = endereco.replaceAll("Travessa", "Trv.");
		}
		if(endereco.contains("PRESIDENTE")) { 
			endereco = endereco.replaceAll("PRESIDENTE", "PRES.");			
		}
		if(endereco.contains("Presidente")) {
			endereco = endereco.replaceAll("Presidente", "Pres.");
		}
		if(endereco.contains("GOVERNADOR")) { 
			endereco = endereco.replaceAll("GOVERNADOR", "GOV.");			
		}
		if(endereco.contains("Governador")) {
			endereco = endereco.replaceAll("Governador", "Gov.");
		}
		if(endereco.contains("PREFEITO")) { 
			endereco = endereco.replaceAll("PREFEITO", "PREF.");			
		}
		if(endereco.contains("Prefeito")) {
			endereco = endereco.replaceAll("Prefeito", "Pref.");
		}
		if(endereco.contains("ENGENHEIRO")) {
			endereco = endereco.replaceAll("ENGENHEIRO", "ENG.");
		}
		if(endereco.contains("Engenheiro")) {
			endereco = endereco.replaceAll("Engenheiro", "Eng.");
		}
		if(endereco.contains("DOUTOR")) {
			endereco = endereco.replaceAll("DOUTOR", "DR.");
		}
		if(endereco.contains("Doutor")) {
			endereco = endereco.replaceAll("Doutor", "Dr.");
		}
		if(endereco.contains("SARGENTO")) {
			endereco = endereco.replaceAll("SARGENTO", "SGTO.");
		}
		if(endereco.contains("Sargento")) {
			endereco = endereco.replaceAll("Sargento", "Sgto.");
		}
		if(endereco.contains("GENERAL")) {
			endereco = endereco.replaceAll("GENERAL", "GEN.");
		}
		if(endereco.contains("General")) {
			endereco = endereco.replaceAll("General", "Gen.");
		}
		if(endereco.contains("COMENDADOR")) {
			endereco = endereco.replaceAll("COMENDADOR", "COM.");
		}
		if(endereco.contains("Comendador")) {
			endereco = endereco.replaceAll("Comendador", "Com.");
		}
		if(endereco.contains("TRAVESSA")) {
			endereco = endereco.replaceAll("TRAVESSA", "TRV.");
		}
		if(endereco.contains("Travessa")) {
			endereco = endereco.replaceAll("Travessa", "Trv.");
		}		
		if(complemento.contains("CONDOMÍNIO")) {
			complemento = complemento.replaceAll("CONDOMÍNIO", "COND.");
		}
		if(complemento.contains("Condomínio")) {
			complemento = complemento.replaceAll("Condomínio", "Cond.");
		}
		if(complemento.contains("EDIFÍCIO")) {
			complemento = complemento.replaceAll("EDIFÍCIO", "ED.");
		}
		if(complemento.contains("EDIFICIO")) {
			complemento = complemento.replaceAll("EDIFICIO", "ED.");
		}
		if(complemento.contains("Edifício")) {
			complemento = complemento.replaceAll("Edifício", "Ed.");
		}
		if(complemento.contains("Edificio")) {
			complemento = complemento.replaceAll("Edificio", "Ed.");
		}
		if(complemento.contains("BLOCO")) {
			complemento = complemento.replaceAll("BLOCO", "BL.");
		}
		if(complemento.contains("Bloco")) {
			complemento = complemento.replaceAll("Bloco", "Bl.");
		}
		if(endereco.contains("PROFESSORA")) {
			endereco = endereco.replaceAll("PROFESSORA", "PROF.");
		}
		if(endereco.contains("Professora")) {
			endereco = endereco.replaceAll("Professora", "Prof.");
		}
		if(endereco.contains("PROFESSOR")) {
			endereco = endereco.replaceAll("PROFESSOR", "PROF.");
		}
		if(endereco.contains("Professor")) {
			endereco = endereco.replaceAll("Professor", "Prof.");
		}
		
		if(cep.contains(".")) 
			cep = cep.replace(".", "");
		
		if (cep.contains("-")) 
			cep = cep.replace("-", "");		
		
		String logoAmerica = "";
		BufferedReader lerArq;									  
		lerArq = new BufferedReader(new InputStreamReader(new FileInputStream("/opt/pedidos/correios/logo.txt"), "ISO-8859-1"));			          	  			  
      	while (lerArq.ready()) {
      		logoAmerica = logoAmerica + lerArq.readLine();					
		}
		lerArq.close();		  
		
		File dir = new File("/opt/pedidos/correios/etiquetas");
		
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		BufferedWriter etiqueta = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("/opt/pedidos/correios/etiquetas/etiqueta_"+id_order+".txt"),"Cp1252"));
		
		etiqueta.append("I8,A,001" + "\r\n");
	  	etiqueta.append("" + "\r\n");
    	etiqueta.append("" + "\r\n");
    	etiqueta.append("Q1200,024" + "\r\n");
    	etiqueta.append("q831" + "\r\n");
    	etiqueta.append("rN" + "\r\n");
    	etiqueta.append("S3" + "\r\n");
    	etiqueta.append("D7" + "\r\n");
    	etiqueta.append("ZT" + "\r\n");
    	etiqueta.append("JF" + "\r\n");
    	etiqueta.append("O" + "\r\n");
    	etiqueta.append("R8,0" + "\r\n");
    	etiqueta.append("f100" + "\r\n");
    	etiqueta.append("N" + "\r\n");
    	etiqueta.append(logoAmerica + "\r\n");
    	etiqueta.append("LO23,538,749,2" + "\r\n");
    	etiqueta.append("LO25,896,749,2" + "\r\n");
    	etiqueta.append("A46,234,0,3,1,1,N,\"NF:\"" + "\r\n");
    	etiqueta.append("A321,234,0,3,1,1,N,\""+id_order+"\"" + "\r\n");
    	etiqueta.append("A90,234,0,3,1,1,N,\""+numeroNota+"\"" + "\r\n");
    	etiqueta.append("A50,725,0,4,1,1,N,\""+cep+" "+cidade+"/"+estado+"\" " + "\r\n");
    	etiqueta.append("A37,997,0,4,1,1,N,\""+logradouro+", "+numeroEmpresa+"\"" + "\r\n");
    	etiqueta.append("A36,1057,0,4,1,1,N,\""+cepEmpresa+" "+municipio+"/"+ufEmpresa+"\" " + "\r\n");
    	etiqueta.append("A571,553,0,3,1,1,N,\"Volume:1/1\" " + "\r\n");
    	etiqueta.append("A51,505,0,3,1,1,N,\"Documento:\" " + "\r\n");
    	etiqueta.append("A52,473,0,3,1,1,N,\"Nome Legivel:\" " + "\r\n");
    	etiqueta.append("A223,234,0,3,1,1,N,\"Pedido:\" " + "\r\n");
    	etiqueta.append("A304,280,0,3,1,1,N,\""+cod_rastreio+"\"" + "\r\n");
    	etiqueta.append("A610,234,0,3,1,1,N,\""+pesoTotal+"\"" + "\r\n");
    	etiqueta.append("A49,636,0,4,1,1,N,\""+bairro+"\"" + "\r\n");
    	etiqueta.append("A50,696,0,4,1,1,N,\""+ref+"\"" + "\r\n");
    	etiqueta.append("A49,666,0,4,1,1,N,\""+complemento+"\"" + "\r\n");
    	etiqueta.append("A49,609,0,4,1,1,N,\""+endereco+", "+numero+"\"" + "\r\n");
    	etiqueta.append("A36,1025,0,4,1,1,N,\""+bairroEmpresa+"\"" + "\r\n");
    	etiqueta.append("A37,941,0,4,1,1,N,\"Remetente:\" " + "\r\n");
    	etiqueta.append("A52,550,0,4,1,1,N,\"Destinatario:\" " + "\r\n");
    	etiqueta.append("A38,969,0,4,1,1,N,\""+nomeEmpresa+"\"" + "\r\n");
    	etiqueta.append("A51,582,0,4,1,1,N,\""+nome+"\"" + "\r\n");
    	etiqueta.append("A377,763,0,3,1,1,N,\"Obs:\" " + "\r\n");
    	etiqueta.append("A495,234,0,3,1,1,N,\"Peso(g):\"" + "\r\n");
    	etiqueta.append("B23,308,0,1,5,15,145,N,\""+cod_rastreio+"\"" + "\r\n");
    	etiqueta.append("A118,28,0,3,3,3,N,\"PAC\"" + "\r\n");
    	etiqueta.append("A56,111,0,2,1,1,N,\"9912407401/2017/DR-MG\"" + "\r\n");
    	etiqueta.append("A85,131,0,4,1,1,N,\"CASA AMERICA\"" + "\r\n");
    	etiqueta.append("A86,176,0,2,2,2,N,\"CORREIOS\"" + "\r\n");
    	etiqueta.append("LO311,16,2,174" + "\r\n");
    	etiqueta.append("LO47,16,5,174" + "\r\n");
    	etiqueta.append("LO50,15,262,3" + "\r\n");
    	etiqueta.append("LO277,192,37,1" + "\r\n");
    	etiqueta.append("LO49,190,35,2" + "\r\n");
    	etiqueta.append("LO770,539,3,358" + "\r\n");
    	etiqueta.append("LO254,491,427,1" + "\r\n");
    	etiqueta.append("LO23,538,3,359" + "\r\n");		
    	etiqueta.append("B60,761,0,1,3,9,118,N,\""+cep+"\"" + "\r\n");
    	etiqueta.append("P1" + "\r\n");			            	    	
    	etiqueta.close();
    	
    	manager.createQuery("update Pedido set etiqueta_impressa = 'SIM' where id_order = '"+id_order+"'").executeUpdate();
    	    	    	
	}
	
	public void geraEtiquetaPdf(List<Cliente> cliente, List<Empresa> empresa, String numeroNota, String id_order, 
			String cod_rastreio, String pesoTotal) throws Exception {
		
		String nome = "";
		String nomeRecebedor = "";
		String endereco = "";
		String complemento = "";
		String numero = "";
		String bairro = "";
		String cidade = "";
		String estado = "";		
		String cep = "";
		String ref = "";
		
		String nomeEmpresa = "";		
		String logradouro = "";
		String numeroEmpresa = "";
		String bairroEmpresa = "";
		String municipio = "";
		String ufEmpresa = "";
		String cepEmpresa = "";
				
		for (Cliente cli : cliente) {
			nome = cli.getNome();
			nomeRecebedor = cli.getNome_recebedor();
			endereco = cli.getEndereco();
			complemento = cli.getComplemento();
			numero = cli.getNumero();
			bairro = cli.getBairro();
			cidade = cli.getCidade();
			estado = cli.getEstado();
			cep = cli.getCep();	
			ref = cli.getReference();
		}
		
		for (Empresa emp : empresa) {
			nomeEmpresa = emp.getNomeFantasia() + " MAGAZINE";
			logradouro = emp.getLogradouro();
			numeroEmpresa = emp.getNumero();
			bairroEmpresa = emp.getBairro();
			municipio = emp.getMunicipio();
			ufEmpresa = emp.getUf();
			cepEmpresa = emp.getCep();			
		}
		
		//Verifica o nome do Recebedor:
		if(!nome.equals(nomeRecebedor)) {
			nome = nomeRecebedor;
		}
		//
		
		//valida abreviaturas:
		if(endereco.contains("DESEMBARGADOR")){
			endereco = endereco.replaceAll("DESEMBARGADOR", "DES.");	
		}
		if(endereco.contains("Desembargador")) {
			endereco = endereco.replaceAll("Desembargador", "Des.");
		}
		if(endereco.contains("CONSELHEIRO")){
			endereco = endereco.replaceAll("CONSELHEIRO", "CONS.");	
		}
		if(endereco.contains("Conselheiro")) {
			endereco = endereco.replaceAll("Conselheiro", "Cons.");
		}
		if(endereco.contains("BRIGADEIRO")){
			endereco = endereco.replaceAll("BRIGADEIRO", "BRIG.");	
		}
		if(endereco.contains("Brigadeiro")) {
			endereco = endereco.replaceAll("Brigadeiro", "Brig.");
		}
		if(endereco.contains("EVANGELISTA")){
			endereco = endereco.replaceAll("EVANGELISTA", "EVG.");	
		}
		if(endereco.contains("Evangelista")) {
			endereco = endereco.replaceAll("Evangelista", "Evg.");
		}
		if(endereco.contains("AVENIDA")) {
			endereco = endereco.replaceAll("AVENIDA", "AV.");			
		}
		if(endereco.contains("Avenida")) {
			endereco = endereco.replaceAll("Avenida", "Av.");
		}					
		if(endereco.contains("RODOVIA")) {
			endereco = endereco.replaceAll("RODOVIA", "ROD.");			
		}
		if(endereco.contains("Rodovia")) {
			endereco = endereco.replaceAll("Rodovia", "Rod.");
		}
		if(endereco.contains("TRAVESSA")) { 
			endereco = endereco.replaceAll("TRAVESSA", "TRV.");			
		}
		if(endereco.contains("Travessa")) {
			endereco = endereco.replaceAll("Travessa", "Trv.");
		}
		if(endereco.contains("PRESIDENTE")) { 
			endereco = endereco.replaceAll("PRESIDENTE", "PRES.");			
		}
		if(endereco.contains("Presidente")) {
			endereco = endereco.replaceAll("Presidente", "Pres.");
		}
		if(endereco.contains("GOVERNADOR")) { 
			endereco = endereco.replaceAll("GOVERNADOR", "GOV.");			
		}
		if(endereco.contains("Governador")) {
			endereco = endereco.replaceAll("Governador", "Gov.");
		}
		if(endereco.contains("PREFEITO")) { 
			endereco = endereco.replaceAll("PREFEITO", "PREF.");			
		}
		if(endereco.contains("Prefeito")) {
			endereco = endereco.replaceAll("Prefeito", "Pref.");
		}
		if(endereco.contains("ENGENHEIRO")) {
			endereco = endereco.replaceAll("ENGENHEIRO", "ENG.");
		}
		if(endereco.contains("Engenheiro")) {
			endereco = endereco.replaceAll("Engenheiro", "Eng.");
		}
		if(endereco.contains("DOUTOR")) {
			endereco = endereco.replaceAll("DOUTOR", "DR.");
		}
		if(endereco.contains("Doutor")) {
			endereco = endereco.replaceAll("Doutor", "Dr.");
		}
		if(endereco.contains("SARGENTO")) {
			endereco = endereco.replaceAll("SARGENTO", "SGTO.");
		}
		if(endereco.contains("Sargento")) {
			endereco = endereco.replaceAll("Sargento", "Sgto.");
		}
		if(endereco.contains("GENERAL")) {
			endereco = endereco.replaceAll("GENERAL", "GEN.");
		}
		if(endereco.contains("General")) {
			endereco = endereco.replaceAll("General", "Gen.");
		}
		if(endereco.contains("COMENDADOR")) {
			endereco = endereco.replaceAll("COMENDADOR", "COM.");
		}
		if(endereco.contains("Comendador")) {
			endereco = endereco.replaceAll("Comendador", "Com.");
		}
		if(endereco.contains("TRAVESSA")) {
			endereco = endereco.replaceAll("TRAVESSA", "TRV.");
		}
		if(endereco.contains("Travessa")) {
			endereco = endereco.replaceAll("Travessa", "Trv.");
		}
		if(complemento.contains("CONDOMÍNIO")) {
			endereco = endereco.replaceAll("CONDOMÍNIO", "COND.");
		}
		if(complemento.contains("Condomínio")) {
			complemento = complemento.replaceAll("Condomínio", "Cond.");
		}
		if(complemento.contains("EDIFÍCIO")) {
			complemento = complemento.replaceAll("EDIFÍCIO", "ED.");
		}
		if(complemento.contains("EDIFICIO")) {
			complemento = complemento.replaceAll("EDIFICIO", "ED.");
		}
		if(complemento.contains("Edifício")) {
			complemento = complemento.replaceAll("Edifício", "Ed.");
		}
		if(complemento.contains("Edificio")) {
			complemento = complemento.replaceAll("Edificio", "Ed.");
		}
		if(complemento.contains("BLOCO")) {
			complemento = complemento.replaceAll("BLOCO", "BL.");
		}
		if(complemento.contains("Bloco")) {
			complemento = complemento.replaceAll("Bloco", "Bl.");
		}
		if(endereco.contains("PROFESSORA")) {
			endereco = endereco.replaceAll("PROFESSORA", "PROF.");
		}
		if(endereco.contains("Professora")) {
			endereco = endereco.replaceAll("Professora", "Prof.");
		}
		if(endereco.contains("PROFESSOR")) {
			endereco = endereco.replaceAll("PROFESSOR", "PROF.");
		}
		if(endereco.contains("Professor")) {
			endereco = endereco.replaceAll("Professor", "Prof.");
		}
		
		if(cep.contains(".")) 
			cep = cep.replace(".", "");
		
		if (cep.contains("-")) 
			cep = cep.replace("-", "");	
		
		String caminho = "/opt/pedidos/correios/etiquetas/reports/";
		
		Map<String, String> param = new HashMap();
		
		ArrayList<Pedido> pedidos = (ArrayList<Pedido>) ped.findPedido(id_order);
		
		param.put("numeroNota", numeroNota);
		param.put("numeroPedido", id_order);
		param.put("valorPeso", pesoTotal);
		param.put("cepDestinatario", cep);
		param.put("rastreio", cod_rastreio);
		param.put("nomeDestinatario", nome);
		param.put("endDestinatario", endereco+", "+numero);
		param.put("bairroDestinatario", bairro);
		param.put("complementoDestinatario", complemento);
		param.put("refDestinatario", ref);
		param.put("cepDestinatario", cep);
		param.put("refDestinatario", ref);
		param.put("municipioDestinatario", cidade);
		param.put("ufDestinatario", estado);
		param.put("nomeRemetente", nomeEmpresa);
		param.put("endRemetente", logradouro+", "+numeroEmpresa);
		param.put("bairroRemetente", bairroEmpresa);
		param.put("cepRemetente", cepEmpresa);
		param.put("nomeRemetente", nomeEmpresa);
		param.put("municipioRemetente", municipio);
		param.put("ufRemetente", ufEmpresa);
		
		geraEtiqueta(id_order, "Etiqueta PDF", pedidos, caminho, "etiquetaPdf.jasper", param, true);
		
		manager.createQuery("update Pedido set etiqueta_impressa = 'SIM' where id_order = '"+id_order+"'").executeUpdate();
				
	}
		
	public void geraEtiqueta(String id_order, String titulo, Collection dados, String caminho_arquivo_jasper, String nome_arquivo_jasper, Map parametros, boolean mensagem) {		
				
		System.out.println("entrou aqui para gerar a Etiqueta em PDF!");
		
		String pathPdf = "/home/correios/"+id_order+".pdf";
		
		DataSource ds = new DataSource(dados);
		
		try {		
			
			JasperReport jasperReport = (JasperReport)JRLoader.loadObject(new File(caminho_arquivo_jasper + nome_arquivo_jasper));				
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametros, ds);
			
			JRPdfExporter exporter = new JRPdfExporter();
			 
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pathPdf));
			 
			SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
			reportConfig.setSizePageToContent(true);
			reportConfig.setForceLineBreakPolicy(false);
			 
			SimplePdfExporterConfiguration exportConfig = new SimplePdfExporterConfiguration();
			exportConfig.setMetadataAuthor("infat");
			exportConfig.setEncrypted(true);
			exportConfig.setAllowedPermissionsHint("PRINTING");
			 
			exporter.setConfiguration(reportConfig);
			exporter.setConfiguration(exportConfig);
			 
			exporter.exportReport();
			
		} catch (JRException e) {
			e.printStackTrace(); 
		}						
		
	}

	public String geraEtiquetaComDigitoVerificador(String numeroEtiqueta) {

		String prefixo = numeroEtiqueta.substring(0, 2);
        String numero = numeroEtiqueta.substring(2, 10);
        String sufixo = numeroEtiqueta.substring(10).trim();
        String retorno = numero;
        String dv;
        Integer[] multiplicadores = {8, 6, 4, 2, 3, 5, 9, 7};
        Integer soma = 0;

        if (numeroEtiqueta.length() < 12){
            retorno = "Error...";
        } else if(numero.length () < 8 && numeroEtiqueta.length() == 12){
            String zeros = "";
            int diferenca = 8 - numero.length();
            
            for (int i = 0; i < diferenca; i++) {
                zeros += "0";
            }
            retorno = zeros + numero;
        } else {
            retorno = numero.substring(0, 8);
        }
        for (int i = 0; i < 8; i++) {
            soma += new Integer(retorno.substring(i, (i + 1))) * multiplicadores[i];
        }
        Integer resto = soma % 11;
        if (resto == 0) {
            dv = "5";
        } else if (resto == 1) {
            dv = "0";
        } else {
            dv = new Integer(11 - resto).toString();
        }
        retorno += dv;
        retorno = prefixo + retorno + sufixo;
        return retorno;
    }	
	
	public String retornarPesoPedido(String id) {

		ArrayList<Produto> produtos = (ArrayList<Produto>) manager.createQuery("select e from Produto e where id_order = '"+id+"'", Produto.class).getResultList();
		
		double pesoTotal = 0.0;
		
		for (Produto produto : produtos) {
			String peso = produto.getPeso().replaceAll(",", ".");
			double pesoDouble = Double.parseDouble(peso);
			double pesoQuantidade = pesoDouble * produto.getQuantidade();
			pesoTotal += pesoQuantidade;
		}
		
		String pesoString = Double.toString(pesoTotal);
		String pesoFinal = pesoString.substring(0, pesoString.length()-2);
		
		return pesoFinal;
				
	}
	
	public String retornarCodigoServicoPostagem(String cod) {
		
		Correios correio = manager.createQuery("select c from Correios c where escolhido = 'SIM'", Correios.class).getSingleResult();
		
		if(correio.getCodigoServicoPostagem() != null) {
			if(!correio.getCodigoServicoPostagem().equals(cod)) {
				return cod; 
			} else {
				return correio.getCodigoServicoPostagem();	
			}			
		} else {
			return "null";
		}		
	}
	
	public String retornarValorTotalNota(String ref) throws Exception {
		
		System.out.println("entrou aqui pra retornar o valor total da nota fiscal!");
		
		String token = "R40tF7pr29P7tUhTJIj708ej5dTyDi7m";//AUTORIZAÇÃO
		//String token = "KD0QdUDKXiElEnd1mvalgVGeeyZ0SC8U";                

        String url = "https://api.focusnfe.com.br/v2/nfe/"+ref+"?completa=1";//AUTORIZAÇÃO 
        //String url = "http://homologacao.acrasnfe.acras.com.br/v2/nfe/"+ref+"?completa=1";
        
        Object config = new DefaultClientConfig();
        Client client = Client.create((ClientConfig) config);
        client.addFilter(new HTTPBasicAuthFilter(token, ""));

        WebResource request = client.resource(url);

        ClientResponse resposta = request.get(ClientResponse.class);

        String result = resposta.getEntity(String.class);
        
        JSONParser parser = new JSONParser();        		
		JSONArray results = (JSONArray) parser.parse("[" + result + "]");
		
		String valor_total = "";
		
		for (int i = 0; i < results.size(); i++) {
			JSONObject jsonObject = (JSONObject) results.get(i);        									
			JSONArray results2 = (JSONArray) parser.parse("[" + jsonObject.get("requisicao_nota_fiscal") + "]");			
			for(int j = 0;j < results2.size(); j++) {
				JSONObject jsonObject2 = (JSONObject) results2.get(j);
				valor_total = (String) (jsonObject2.get("valor_total"));				
			}			
		}						
		
		return valor_total;
        
	}
	
	public void montarUnidades() throws IOException {		
		Runtime run = Runtime.getRuntime();
		run.exec("mount -a");				
	}
	
	public void atualizaDadosCaixa(Integer id, String altura, String largura, String comprimento) {
		
		manager.createQuery("update Pedido set altura = '"+altura+"' where id_order = '"+id+"'").executeUpdate();
        manager.createQuery("update Pedido set largura = '"+largura+"' where id_order = '"+id+"'").executeUpdate();
        manager.createQuery("update Pedido set comprimento = '"+comprimento+"' where id_order = '"+id+"'").executeUpdate();
        
	}

}
