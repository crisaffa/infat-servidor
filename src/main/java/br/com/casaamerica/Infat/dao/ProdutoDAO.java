package br.com.casaamerica.Infat.dao;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.list.ListaTotal;
import br.com.casaamerica.Infat.model.Pedido;
import br.com.casaamerica.Infat.model.Produto;

@Repository
@Transactional(noRollbackFor = Exception.class)
public class ProdutoDAO {

	@PersistenceContext
	EntityManager manager;	

	public List<Produto> selectProduto(List<Pedido> pedidos) throws Exception {
				
		List<Produto> prod = manager.createQuery("select e from Produto e ", Produto.class).getResultList();

		return prod;

	}

	public List<ListaTotal> total(String id_order) {

		Locale meuLocal = new Locale( "pt", "BR" );
		NumberFormat nfVal = NumberFormat.getCurrencyInstance( meuLocal );
		List<Pedido> p = manager.createQuery("Select p from Pedido p where id_order= '"+id_order+"'", Pedido.class).getResultList();
			
		List<Produto> total2 = manager
				.createQuery("select p from Produto p where id_order = '" + id_order + "'", Produto.class)
				.getResultList();
		Double h = 0.00;
		for(int a = 0; a < p.size(); a++) {
			 h= p.get(a).getTaxa_total();
			
		}

		ArrayList<ListaTotal> resultado = new ArrayList<>();
		for (int i = 0; i < (total2.size()/total2.size()); i++) {

			ListaTotal t = new ListaTotal();

			t.setValortotal(total2.get(i).getValortotal());
			t.setFretetotal(total2.get(i).getFretetotal());	
			t.setDescontototal(total2.get(i).getDescontoTotal());
			t.setTaxa_total(String.format("%.2f", h));
			
			

			double valor = Double.parseDouble(total2.get(i).getValortotal().replaceAll(",", "."));
			double valor2 = Double.parseDouble(total2.get(i).getFretetotal().replaceAll(",", "."));
			double valor3 = Double.parseDouble(total2.get(i).getDescontoTotal().replaceAll(",", "."));

			double res = (valor + valor2) - valor3 + h;
			
			manager.createQuery("update Pedido set produtos_total ='"+valor+"' where id_order = '"+id_order+"'").executeUpdate();
			manager.createQuery("update Pedido set frete_total ='"+valor2+"' where id_order = '"+id_order+"'").executeUpdate();
			manager.createQuery("update Pedido set desconto_total ='"+valor3+"' where id_order = '"+id_order+"'").executeUpdate();

			String value = (String.format("%.2f", res)).replace(".",",");
			
			t.setValorNF(value);

			resultado.add(t);
		}

		return resultado;
	}

	public int quantidadeProdutos(String id) {

		List<Produto> total2 = manager
				.createQuery("select p from Produto p where id_order = '" + id + "'", Produto.class).getResultList();

		int q = 0;

		for (int i = 0; i < total2.size(); i++) {

			q += total2.get(i).getQuantidade();

		}

		return q;

	}

	public List<Produto> findProduto(String id_order) {

		List<Produto> total = manager
				.createQuery("select p from Produto p where id_order = '" + id_order + "'", Produto.class)
				.getResultList();

		return total;
	}
}
