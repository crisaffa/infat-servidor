package br.com.casaamerica.Infat.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.connLoja.ConnectionFactoryPostgresql;
import br.com.casaamerica.Infat.model.Produto;

@Repository
@Transactional
public class RemovePedidoBancoDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public void removePedido(Integer id) throws Exception {
		
		System.out.println("entrou aqui para remover o pedido do banco da loja!");
		
		adicionarEstoque(id);		
	}
	
	public void adicionarEstoque(int id_order) throws Exception { 
		
		System.out.println("entrou para adicionar ao estoque");
		
		Connection connection = ConnectionFactoryPostgresql.getConnection();
		
		Statement st = connection.createStatement();
		Statement st2 = connection.createStatement();
		
		List<Produto> produtos = manager.createQuery("select e from Produto e where e.id_order = '" + id_order + "'", Produto.class).getResultList();

		System.out.println("tamanho :" + produtos.size());
		
		for (Produto produto : produtos) {
			System.out.println("produto: "+produto.getDescricao());
			
			boolean sql = st.execute("select * from produtos_estoques where id_produto = "+produto.getId_produto()+"");
			ResultSet rst = st.getResultSet();
			
			while(rst.next()) {
				
				int estoque =  rst.getInt("estoque_pos0") + rst.getInt("estoque_pos1") + rst.getInt("estoque_pos2") + rst.getInt("estoque_pos3") + rst.getInt("estoque_pos4") + rst.getInt("estoque_pos5") + rst.getInt("estoque_pos6") + rst.getInt("estoque_pos7") + rst.getInt("estoque_pos8") + rst.getInt("estoque_pos9") +rst.getInt("estoque_pos10") + rst.getInt("estoque_pos11")+rst.getInt("estoque_pos12")+rst.getInt("estoque_pos13");
				Integer valor = Integer.valueOf(produto.getQuantidade().toString());
				int resultado = estoque + valor;
				System.out.println("total estoque : " + resultado);
				
				boolean update = st2.execute("update produtos_estoques set estoque_pos0 = "+resultado+" where id_produto = '"+produto.getId_produto()+"'");
			}
		}
		
		removerVenda(id_order);
	}
	
	
	public void removerVenda(int id_order) throws Exception {
		
		System.out.println("entrou para remover a venda");
		
		Connection connection = ConnectionFactoryPostgresql.getConnection();
		Statement st = connection.createStatement();
		Statement st2 = connection.createStatement();
		Statement st3 = connection.createStatement();
		
		System.out.println("id_order: "+id_order);
		
		manager.createQuery("update Produto set id_venda_caixa = '' where id_order = '" + id_order + "'").executeUpdate();
		
		boolean sql = st.execute("select id_venda from vendas_vendas where id_order = '" + id_order + "'");
		ResultSet rs = st.getResultSet();
		
		while (rs.next()) {				
			int id_venda = rs.getInt("id_venda");	
			
			boolean sql2 = st2.execute("update vendas_itens_venda set discriminador = 'itemCancelado',frete = null where id_venda = '"+id_venda+"'");
			
			boolean sql3 = st3.execute("update vendas_vendas set observacao = '-',cancelada = true,id_usuario_cancelamento = 1 where id_venda = '"+id_venda+"'");
		}
		
		System.out.println("terminou de remover a venda!");		
		
	}
		
}
