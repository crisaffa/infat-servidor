package br.com.casaamerica.Infat.dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.text.MaskFormatter;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.list.ListaClientes;
import br.com.casaamerica.Infat.model.Cliente;
import br.com.casaamerica.Infat.model.Pedido;

@Repository
@Transactional
public class ClienteDAO {

	@PersistenceContext
	EntityManager manager;
	
	@Autowired
	PedidosDAO ped;
	
	public ArrayList<ListaClientes> insertCliente(List<Pedido> pedidos) throws Exception {

		ArrayList<ListaClientes> cliente = new ArrayList<>();

		for (int i = 0; i < pedidos.size(); i++) {

			System.out.println("" + pedidos.get(i).getOrderId());

			String url = "https://casaamerica.vtexcommercestable.com.br/api/oms/pvt/orders/"+ pedidos.get(i).getOrderId();

			HttpGet get = new HttpGet(url);
			CloseableHttpClient client = HttpClientBuilder.create().build();
			get.addHeader("cache-control", "no-cache");
			get.addHeader("content-type", "application/json");
			get.addHeader("X-VTEX-API-AppKey", "vtexappkey-casaamerica-ABHYEZ");
			get.addHeader("X-VTEX-API-AppToken",
					"QAXGZRVYBWXDEERBUCNUPRYDLHLXIVRAXPQESTYXSKGGCJLWQUXTAGMZCUUSFXPFEXGPONIBEPCEKRACWPHOYYZRCRBXYGSUVBCFCWHCMWPKEIOKUSWQPBHGXPMXLKCY");

			HttpResponse response = client.execute(get);
			System.out.println("[sendGET COMTELE] REQUEST: ");
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

			cliente = tratarResposta(response);

		}
		
		return cliente;
	}
	
	private ArrayList<ListaClientes> tratarResposta(HttpResponse response) throws Exception {

		ArrayList<ListaClientes> lista = new ArrayList<>();

		if (response.getStatusLine().getStatusCode() != 200) {
			String resposta = "[tratarResposta COMTELE] Response Code : " + response.getStatusLine().getStatusCode();
			resposta += "\nReason : " + response.getStatusLine().getReasonPhrase();
		}
		
		// Captura resposta		
		StringBuffer result = new StringBuffer();
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		try {

			JSONParser parser = new JSONParser();
			JSONArray results = (JSONArray) parser.parse("[" + result.toString() + "]");
			String telefone = "";
			String cpf = "";
			String nome = "";
			String corporateName = "";
			String inscricaoEstadual = "";
			String nomeRecebedor = "";
			String rua = "";
			String numero = "";
			String bairro = "";
			String cidade = "";
			String estado = "";
			String cep = "";
			String complemento = "";
			String referencia = "";
			String pedido = "";
			String status= "";
			Double taxa = 0.00;
			
		
			for (int i = 0; i < results.size(); i++) {

				JSONObject jsonObject = (JSONObject) results.get(i);
				JSONArray results2 = (JSONArray) parser.parse("" + jsonObject.get("items") + "");
				JSONArray results3 = (JSONArray) parser.parse("[" + jsonObject.get("clientProfileData") + "]");
				JSONArray results4 = (JSONArray) parser.parse("[" + jsonObject.get("shippingData") + "]");
				JSONArray r = (JSONArray) parser.parse("" + jsonObject.get("totals") + "");
			
				pedido = (String) jsonObject.get("sequence");
				status = (String) jsonObject.get("status");				
					
			
				for(int sm =0 ; sm < r.size(); sm ++) {
					
					JSONObject jsonObject6 = (JSONObject) r.get(sm);
					
					NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
					String t = (String) jsonObject6.get("id");
			
					if(t.equals("Tax") ) {
					
						String v = numberFormat.format(jsonObject6.get("value"));
						
						String []t1 = v.split(" ");
						taxa = Double.parseDouble(t1[1].replace(",", "."))/100;
						
						
					}
					
				}
				
				for (int a = 0; a < results3.size(); a++) {

					JSONObject jsonObject3 = (JSONObject) results3.get(a);
					
					corporateName = (String) jsonObject3.get("corporateName");
					
					if(corporateName != null) {									
						nome = corporateName;
						inscricaoEstadual = (String) jsonObject3.get("stateInscription");
						telefone = (String) jsonObject3.get("corporatePhone");
						cpf = (String) jsonObject3.get("corporateDocument");
						try {
							if (cpf.endsWith("null")) {

							} else if (cpf.length() == 14) {								
								MaskFormatter mask = new MaskFormatter("##.###.###/####-##");
								mask.setValueContainsLiteralCharacters(false);
								cpf = mask.valueToString(cpf);											
							}
						} catch (Exception e) {
							e.getMessage();
						}
					} else {
						
						if(((String) jsonObject3.get("lastName")).contains("--")) {
							nome = (String) jsonObject3.get("firstName");
						} else {
							nome = (String) jsonObject3.get("firstName") + " " + (String) jsonObject3.get("lastName");	
						}
						telefone = (String) jsonObject3.get("phone");	
						cpf = (String) jsonObject3.get("document");						
						try {

							if (cpf.endsWith("null")) {

							} else if (cpf.length() != 14) {
								MaskFormatter mask = new MaskFormatter("###.###.###-##");
								mask.setValueContainsLiteralCharacters(false);
								cpf = mask.valueToString(cpf);
							}
						} catch (Exception e) {
							e.getMessage();
						}
					}															
				}
				
				for (int b = 0; b < results4.size(); b++) {
					
					JSONObject jsonObject4 = (JSONObject) results4.get(b);

					JSONArray results5 = (JSONArray) parser.parse("[" + jsonObject4.get("address") + "]");
					JSONArray results6 = (JSONArray) parser.parse("" + jsonObject4.get("logisticsInfo") + "");

					for (int c = 0; c < results5.size(); c++) {
						
						JSONObject jsonObject5 = (JSONObject) results5.get(c);
						
						nomeRecebedor = (String) jsonObject5.get("receiverName");							
						rua = (String) jsonObject5.get("street");
						numero = (String) jsonObject5.get("number");
						bairro = (String) jsonObject5.get("neighborhood");
						cidade = (String) jsonObject5.get("city");
						estado = (String) jsonObject5.get("state");
						
						cep = (String) jsonObject5.get("postalCode");
						if(!cep.contains("-")) {
							MaskFormatter mask = new MaskFormatter("##.###-###");
							mask.setValueContainsLiteralCharacters(false);
							cep = mask.valueToString(cep);	
						}
						
						if(jsonObject5.get("complement")!= null ) {
							complemento = (String) jsonObject5.get("complement");
						}
						
						if(jsonObject5.get("reference")!= null ) {
							referencia = (String) jsonObject5.get("reference");	
						}	
						
					}
										
					if(telefone.contains("+55")) {
						if (telefone.contains(" ")) {
							String tel = telefone.replaceAll(" ", "");
							telefone = tel.substring(3, tel.length());	
						} else {
							telefone = telefone.substring(3, telefone.length());
						}												
					}
					
					if(telefone.length() <= 6) {						
						StringBuilder strBuilder = new StringBuilder(telefone);				        
						for(int k=telefone.length();k<11;k++) {
							strBuilder.append("0");							
						}												
						telefone = strBuilder.toString();												
					}
					
					if(telefone.contains("/")) {
						String tel[] = new String[2];
						tel = telefone.split("/");
						if(tel[0].contains("000000")) {
							telefone = tel[1];
						} else {
							telefone = tel[0];
						}											
					}
					
					System.out.println("Dados: " + cpf + " / " + nome + " / " + rua + " / " + numero + " / " + bairro + " / " + cidade + " / " + estado + " / " + cep + " / " + referencia + " / " + complemento + " / " + telefone );

					if(status.equals("cancel") || status.equals("CANCEL")){
						status = "CANCELANDO";
						
					}else if (status.equals("handling") || status.equals("HANDLING")){
						status = "PREPARANDO ENTREGA";
						
					}else if(status.equals("payment-pending") || status.equals("PAYMENT-PENDING")){
						status = "AGUARDANDO PAGAMENTO";
						
					}else if(status.equals("canceled") || status.equals("CANCELED")){
						status = "CANCELADO";
						
					}else if(status.equals("devolução") || status.equals("DEVOLUÇÃO")){
						status = "DEVOLUÇÃO DE PEDIDO";
						
					}else if (status.equals("ready-for-handling") || status.equals("READY-FOR-HANDLING")){
						status = "PROCESSANDO";
						
					}else if (status.equals("invoiced") || status.equals("INVOICED")){
						status = "NFE EMITIDA";
						
					}else if (status.equals("payment-approved") || status.equals("PAYMENT-APPROVED")){
						status = "PAGAMENTO APROVADO";
					}					
					
					if(!ped.retornarStatusPedido(pedido).equals("DEVOLUÇÃO DE VENDA")){
						if(ped.retornarValorRastreio(pedido) != null || status.equals("CANCELADO")) {
							manager.createQuery("update Pedido set status ='"+status+"' where id_order = '"+pedido+"'").executeUpdate();
							manager.createQuery("update Produto set status ='"+status+"' where id_order = '"+pedido+"'").executeUpdate();
							System.out.println("UPDATE : " + status);	
						}													
					}
					
					Cliente c = new Cliente();

					c.setCpf(cpf);
					c.setNome(nome);
					c.setEndereco(rua);
					c.setNumero(numero);
					c.setBairro(bairro);
					c.setCidade(cidade);
					c.setEstado(estado);
					c.setCep(cep);
					c.setReference(referencia);
					c.setComplemento(complemento);
					c.setTelefone(telefone);
					c.setNome_recebedor(nomeRecebedor);
					c.setInscricao_estadual(inscricaoEstadual);
					c.setIdOrder(pedido);

					ListaClientes ci = new ListaClientes();
					ci.setCpf(cpf);
					ci.setNome(nome);
					ci.setEndereco(rua);
					ci.setNumero(numero);
					ci.setBairro(bairro);
					ci.setCidade(cidade);
					ci.setEstado(estado);
					ci.setCep(cep);
					ci.setReference(referencia);
					ci.setComplemento(complemento);					
					ci.setTelefone(telefone);
					ci.setNome_recebedor(nomeRecebedor);
					ci.setInscricao_estadual(inscricaoEstadual);
					ci.setIdOrder(pedido);
					lista.add(ci);

					manager.createQuery("update Pedido set taxa_total = "+taxa+" where id_order = '"+pedido+"'").executeUpdate();
					
					try {
						c = manager.createQuery("select e from Cliente e where e.idOrder = :idOrder", Cliente.class).setParameter("idOrder", pedido).getSingleResult();

					} catch (Exception e) {
						e.printStackTrace();
					}

					if (c != null) {
						manager.persist(c);
					} 
					
				
						
					
					
										
				}

			}

		} catch (Exception e) {
			e.getMessage();
		}

		return lista;
	}
	
	public void updateCliente(Cliente cliente, Integer id) {
		cliente.setIdOrder(id.toString());
		manager.merge(cliente);		
	}

}
