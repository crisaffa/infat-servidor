package br.com.casaamerica.Infat.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.model.Historico;

@Repository
@Transactional
public class HistoricoDAO {
	
	@PersistenceContext
	EntityManager manager;

	public List<Historico> listarHistorico(){
		
		List<Historico> historicos = manager.createQuery("select e from Historico e", Historico.class).getResultList();
		
		return historicos;
		
	}
	
	
}
