package br.com.casaamerica.Infat.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional; 
import br.com.casaamerica.Infat.model.Usuario;
import br.com.casaamerica.Infat.model.Usuario_Role;

@Repository
@Transactional
public class CadastroUsuarioDAO {
	
	@PersistenceContext
	EntityManager manager;

	public void insereUsuario(Usuario usuario) {

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(usuario.getSenha());
		System.out.println("senha : " + usuario.getSenha());
		System.out.println(encodedPassword);

		usuario.setSenha(encodedPassword);

		manager.persist(usuario);

		Usuario_Role u = new Usuario_Role();
				
		if (usuario.getTipoUsuario().equals("user")) {
			
			u.setRoles_nome("ROLE_USER");
			u.setUsuario_usuario(usuario.getUsuario());
			manager.persist(u);
			
		} else if (usuario.getTipoUsuario().equals("admin")) {
			
			u.setRoles_nome("ROLE_ADMIN");
			u.setUsuario_usuario(usuario.getUsuario());
			manager.persist(u);
		}

		System.out.println("usuario : " + usuario.getEmail());

	}
	
	public List<Usuario> findUsuarios() {		
		List<Usuario> usuarios = manager.createQuery("select e from Usuario e ", Usuario.class).getResultList();		
		return usuarios;
	}

	public List<Usuario> findUnicoUsuario(String usuario) {
		List<Usuario> user = manager.createQuery("select e from Usuario e where usuario = '"+usuario+"'", Usuario.class).getResultList();		
		return user;		
	}

	public void update(Usuario us) {

		System.out.println("entrou no update!");
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(us.getSenha());
		System.out.println("senha : " + us.getSenha());
		System.out.println(encodedPassword);
		us.setSenha(encodedPassword);
		
		Usuario_Role role = new Usuario_Role();
		System.out.println("email : " + us.getEmail());
		role.setUsuario_usuario(us.getUsuario());
		
		if(us.getTipoUsuario().equals("user")) {
			role.setRoles_nome("ROLE_USER");
		} else {
			role.setRoles_nome("ROLE_ADMIN");
		}		
		manager.merge(us);
		manager.merge(role);		
	}


	public void removeUsuario(String usuario) {
		manager.createQuery("delete from Usuario where usuario = '"+usuario+"'").executeUpdate();
		manager.createQuery("delete from Usuario_Role where usuario_usuario = '"+usuario+"'").executeUpdate();		
	}
	
	

}
