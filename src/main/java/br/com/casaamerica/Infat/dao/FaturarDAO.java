package br.com.casaamerica.Infat.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import br.com.casaamerica.Infat.connLoja.ConnectionFactoryPostgresql;
import br.com.casaamerica.Infat.model.Cliente;
import br.com.casaamerica.Infat.model.Empresa;
import br.com.casaamerica.Infat.model.Pedido;
import br.com.casaamerica.Infat.model.Produto;
import br.com.casaamerica.Infat.model.Usuario;

@Repository
@Transactional
public class FaturarDAO {

	@PersistenceContext
	EntityManager manager;
	
	HttpURLConnection con;
	
	@Autowired
	EtiquetaDAO etq;
	
	@Autowired
	CorreiosDAO correios;
	
	@Autowired
	PedidosDAO pedido;
	
	@Autowired
	EmpresaDAO emp;
	
	public void atualizaStatusDevolucao(String id) {
		manager.createQuery("update Pedido set status = 'DEVOLUÇÃO DE VENDA' where id_order = '"+id+"'").executeUpdate();		
	}
	
	public int cancelarNota(String ref, String just, String user) throws Exception{
		
		System.out.println("entrou aqui para cancelar a nota fiscal!");
		
		String token = "R40tF7pr29P7tUhTJIj708ej5dTyDi7m";//AUTORIZAÇÃO
		//String token = "KD0QdUDKXiElEnd1mvalgVGeeyZ0SC8U";

        String url = "https://api.focusnfe.com.br/v2/nfe/"+ref;//AUTORIZAÇÃO 
        //String url = "http://homologacao.acrasnfe.acras.com.br/v2/nfe/"+ref;
        
        File fileJson;        				
		File dir = new File("/opt/pedidos/json-can");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		fileJson = new File(dir, "can_" + ref + ".json");
		fileJson.createNewFile();
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileJson), "UTF-8"));
		
		bw.write("{");		
		bw.newLine();		
		bw.write("  \"justificativa\":\""+just+"\"");
		bw.newLine();
		bw.write("}");
		bw.close();
		
		Object config = new DefaultClientConfig();        
		Client client = Client.create((ClientConfig) config);
        client.addFilter(new HTTPBasicAuthFilter(token, ""));
        
        WebResource request = client.resource(url);

        ClientResponse resposta = request.delete(ClientResponse.class, fileJson);
        
        int httpCode = resposta.getStatus();

        String result = resposta.getEntity(String.class);
        
        System.out.println("HTTP Code: "+httpCode);
        System.out.println("result:" +result);  
        
        String pathXml = "";
        String chaveNfe = "";
        
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();  
		Date dataNew = calendar.getTime(); 
		String dataAtual = sdf.format(dataNew);	
		String ano = dataAtual.substring(0, 4);
		String mes = dataAtual.substring(5, 7);
        
        if((httpCode == 200)) {
    		JSONParser parser = new JSONParser();        		
    		JSONArray results = (JSONArray) parser.parse("[" + result + "]");    		    		
    		for (int i = 0; i < results.size(); i++) {
    			JSONObject jsonObject = (JSONObject) results.get(i);        			    			
    			pathXml = (String) jsonObject.get("caminho_xml_cancelamento");
    			chaveNfe = (String) jsonObject.get("chave_nfe");
    		}
    		
    		//String urlXml = "http://homologacao.acrasnfe.acras.com.br"+pathXml+"";
    		String urlXml = "https://api.focusnfe.com.br"+pathXml+"";//AUTORIZAÇÃO
    		
    		WebResource requestXml = client.resource(urlXml);
    		
    		ClientResponse respostaxml = requestXml.get(ClientResponse.class);
    		InputStream bodyXml = respostaxml.getEntityInputStream();
    		
    		File diretorio = new File("/opt/pedidos/xml/"+ano+"/"+mes+"");
        	if (!diretorio.exists()) {
        		diretorio.mkdirs();
        	}
        		
        	OutputStream outputStream = new FileOutputStream(new File("/opt/pedidos/xml/"+ano+"/"+mes+"/"+chaveNfe+"-can.xml"));
        		
        	int read = 0;
        	byte[] bytes = new byte[1024];

        	while ((read = bodyXml.read(bytes)) != -1) {
        		outputStream.write(bytes, 0, read);
        	}
        		
        	bodyXml.close();
        	outputStream.close();
        	
        	manager.createQuery("update Pedido set user_can ='"+user+"' where ref_nota = '"+ref+"'").executeUpdate();        	
    		   		
    		return 200;
        }
        
        return 0;        
			
	}
	
	public void atualizaStatusNota(Usuario user, String ref, String id) throws Exception {
		
		System.out.println("entrou aqui pra verificar o Status da Nota Fiscal!");
		
		String token = "R40tF7pr29P7tUhTJIj708ej5dTyDi7m";//AUTORIZAÇÃO
		//String token = "KD0QdUDKXiElEnd1mvalgVGeeyZ0SC8U";                

        String url = "https://api.focusnfe.com.br/v2/nfe/"+ref+"?completa=1";//AUTORIZAÇÃO 
        //String url = "http://homologacao.acrasnfe.acras.com.br/v2/nfe/"+ref+"?completa=1";
        
        Object config = new DefaultClientConfig();
        Client client = Client.create((ClientConfig) config);
        client.addFilter(new HTTPBasicAuthFilter(token, ""));

        WebResource request = client.resource(url);

        ClientResponse resposta = request.get(ClientResponse.class);

        String result = resposta.getEntity(String.class);
        
        ArrayList<String> dados = null;
        
        String status = "";
        if(result.contains("nao_encontrado")) {
        	status = "AGUARDANDO_ENVIO";        	
    	} else if(result.contains("autorizado")) {
    		String data_emissao = retornaDataEmissao(result);
        	DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    		Calendar calendar = Calendar.getInstance();  
    		Date dataNew = calendar.getTime(); 
    		String dataAtual = sdf.format(dataNew);	
    		Date dataHoraAtual = sdf.parse(dataAtual);
    		Date dataHoraEmissao = sdf.parse(data_emissao);    		
    		DateTime dateTimeAtual = new DateTime(dataHoraAtual);
    		DateTime dateTimeEmissao = new DateTime(dataHoraEmissao);
    		Hours h = Hours.hoursBetween(dateTimeEmissao, dateTimeAtual);    		
    		if(h.getHours() >= 24) {
    			manager.createQuery("update Pedido set periodo_vencido = 'TRUE' where id_order = '"+id+"'").executeUpdate();	
    		} else {
    			manager.createQuery("update Pedido set periodo_vencido = 'FALSE' where id_order = '"+id+"'").executeUpdate();
    		}
        	status = "AUTORIZADA";    
        	
        	//status ="PREPARANDO ENTREGA";
        	
       	if(!(retornaStatusPedido(Integer.parseInt(id)).equals("NFE EMITIDA"))) {
        		
       	 		String codigoRastreio = "";
        		
        		if(!retornaAlturaPedido(id).equals("") && !retornaLarguraPedido(id).equals("") && !retornaComprimentoPedido(id).equals("")) {
        			
        			File xmlBuscaCliente = etq.gerarXmlBuscaCliente(correios.retornaContaCorreioEscolhido());
					
					dados = etq.buscarCliente(xmlBuscaCliente);
					
					if(dados.size() == 1) {						
						System.out.println("ERRO NA API DOS CORREIOS!");						
					} else {				
						File xmlConsulta = etq.gerarXmlConsulta(dados, correios.retornaContaCorreioEscolhido());
			
						codigoRastreio = etq.buscarEtiqueta(xmlConsulta, id.toString());
				
						etq.gerarTxtEtiqueta(pedido.findClientes(id.toString()), emp.findCnpjEscolhido(), retornaNumeroNota(id.toString()), id.toString(), 
								codigoRastreio, etq.retornarPesoPedido(id.toString()));
					}					
        		}
        		
        		if(dados != null) {
        			String data = data_emissao.substring(0,10);
            		
            		JSONParser parser = new JSONParser();        		
            		JSONArray results = (JSONArray) parser.parse("[" + result + "]");
            		String numero = "";
            		String chaveNfe = "";
            		String pathPdf = "";
            		int count = results.size();	        		
            		for (int i = 0; i < count; i++) {
            			JSONObject jsonObject = (JSONObject) results.get(i);        			
            			numero = (String) jsonObject.get("numero");      
            			chaveNfe = (String) jsonObject.get("chave_nfe");
            			pathPdf = (String) jsonObject.get("caminho_danfe");
            		}
            		
            		String chave = chaveNfe.substring(3, chaveNfe.length());
            		
            		String urlPdf = "https://api.focusnfe.com.br"+pathPdf+"";//AUTORIZAÇÃO
            		//String urlPdf = "http://homologacao.acrasnfe.acras.com.br"+pathPdf+"";
            		
            		String desconto = "";
            		if((retornoValorDescontoTotal(Integer.parseInt(id)).equals("0"))) {        			
            			desconto = "0.00";
            		} else {        			
            			desconto = (retornoValorDescontoTotal(Integer.parseInt(id))).replace(",", ".");
            		}        		
            		String valorProdutos = (retornoValorProdutos(Integer.parseInt(id))).replace(",", ".");
            		String valorFrete = (retornoValorFreteTotal(Integer.parseInt(id))).replace(",", "."); 
            		double produtosDouble = Double.parseDouble(valorProdutos);
            		double freteDouble = Double.parseDouble(valorFrete);
            		double descontoDouble = Double.parseDouble(desconto);
            		
            		List<Pedido> p = manager.createQuery("select p from Pedido p where id_order='"+id+"'", Pedido.class).getResultList();
            		
            		Double taxa = 0.0;
            		
            		for(int i =0; i < p.size(); i++) {
            			
            			taxa = p.get(i).getTaxa_total();
            		}
            		
            		double valorTotal = (produtosDouble + freteDouble) - descontoDouble;
            		
            		valorTotal = valorTotal + taxa;
            		
            		System.out.println(valorTotal);
            		
            		String value = String.format("%.2f", valorTotal);
            		
            		System.out.println(value);
            		//tem que ser com "." no servidor:
            		//value = value.replace(".", "");        		
            		//tem que ser com "," se for local:
            		value = value.replace(",", "");
            		
            		String urlRastreio = "https://www2.correios.com.br/sistemas/rastreamento/default.cfm/";
            		
            		salvaDataHoraEmissaoNota(dateTimeEmissao,ref);
            		
            		atualizaStatusVtex(id, ref, data, numero, value, chave, urlPdf, "000000", urlRastreio);              		            		
        		}        		      		        	
       	}        		
        	
    	} else if(result.contains("processando_autorizacao")){
    		status = "PROCESSANDO_AUTORIZAÇÃO";    		
    	} else if(result.contains("erro_autorizacao")) {
    		status = "ERRO_AUTORIZAÇÃO";
    		String msg = retornaMsgSefaz(result);
    		manager.createQuery("update Pedido set status ='PREPARANDO ENTREGA' where id_order = '"+id+"'").executeUpdate();    		
    		manager.createQuery("update Pedido set msg_sefaz ='"+msg+"' where id_order = '"+id+"'").executeUpdate();    		
    	} else if(result.contains("cancelado")) {    		
    		status = "CANCELADA";    		    		
    	} else if(result.contains("denegado")) {
    		status = "DENEGADA";    		
    	}
        
        manager.createQuery("update Pedido set status_nota ='"+status+"' where id_order = '"+id+"'").executeUpdate();                                
	}
	
	public void atualizaStatusVtex(String id, String ref, String data, String numero, String valor, String chave, String link, 
			String codRastreio, String urlRastreio) throws IOException {
	
		System.out.println("entrou para atualizar status na vtex! " + valor);
		
		File jsonFatura = geraJsonVtex(id, ref, data, numero, valor, chave, link, codRastreio, urlRastreio);
				
		String url = "https://casaamerica.vtexcommercestable.com.br/api/oms/pvt/orders/"+retornaOrderIdPedido(Integer.parseInt(id))+"/invoice";
		
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		
		String dadosNFe = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(
				new FileInputStream(jsonFatura), "UTF-8"));

		String line = br.readLine();
		while (line != null) {
			dadosNFe += line;
			line = br.readLine();
		}
		br.close();

		String temp = Normalizer.normalize(dadosNFe, Normalizer.Form.NFD)
				.replaceAll("[^\\p{ASCII}]", "");
		
		StringEntity stringEntity = new StringEntity(temp);
		stringEntity.setContentEncoding("UTF-8");
		stringEntity.setContentType("application/json");

		post.setEntity(stringEntity);
		post.addHeader("cache-control", "no-cache");
		post.addHeader("content-type", "application/json");
		post.addHeader("X-VTEX-API-AppKey", "vtexappkey-casaamerica-ABHYEZ");
		post.addHeader("X-VTEX-API-AppToken",
				"QAXGZRVYBWXDEERBUCNUPRYDLHLXIVRAXPQESTYXSKGGCJLWQUXTAGMZCUUSFXPFEXGPONIBEPCEKRACWPHOYYZRCRBXYGSUVBCFCWHCMWPKEIOKUSWQPBHGXPMXLKCY");
		
		HttpResponse response = httpClient.execute(post);

		System.out.println("\nSending 'POST' request to URL: " + url);
		System.out.println("Post parameters: " + post.getEntity());

		int responseCode = response.getStatusLine().getStatusCode();
		
		System.out.println("Response Code: " + responseCode);

		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		httpClient.close();

		if(!pedido.retornarStatusPedido(id).equals("DEVOLUÇÃO DE VENDA")) {
			manager.createQuery("update Pedido set status = 'NFE EMITIDA' where id_order = '"+id+"'").executeUpdate();
			manager.createQuery("update Produto set status = 'NFE EMITIDA' where id_order = '"+id+"'").executeUpdate();
		}
		
		manager.createQuery("update Pedido set etiqueta_impressa = 'NÃO' where id_order = '"+id+"'").executeUpdate();
	
		System.out.println("enviou pra Vtex dados do pedido!");	
		
	}
	
	public File geraJsonVtex(String id, String refNota, String data, String numero, String valor, String chaveNfe, String link, 
			String rastreio, String urlRastreio) throws IOException {
		
		File file;								
		File dir = new File("/opt/pedidos/json");
		
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		file = new File(dir, "fatura_" + refNota + ".json");					
		file.createNewFile();
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		
		bw.write("{");		
		bw.newLine();
		
		bw.write("  \"type\":\"Output\",");
		bw.newLine();
		
		bw.write("  \"issuanceDate\":");
		bw.write("\"" + data+ "\",");
		bw.newLine();
		
		bw.write("  \"invoiceNumber\":");
		bw.write("\"" + numero+ "\",");
		bw.newLine();
		System.out.println("valor de invoice : " + valor);
		
		bw.write("  \"invoiceValue\":");
		bw.write("\"" + valor+ "\",");
		bw.newLine();
		
		bw.write("  \"invoiceKey\":");
		bw.write("\"" + chaveNfe+ "\",");
		bw.newLine();
		
		bw.write("  \"invoiceUrl\":");
		bw.write("\"" + link+ "\",");
		bw.newLine();
		
		bw.write("  \"courier\": null,");
		bw.newLine();
		
		bw.write("  \"trackingNumber\":");
		bw.write("\"" + rastreio+ "\",");
		bw.newLine();
		
		bw.write("  \"trackingUrl\":");
		bw.write("\"" + urlRastreio+ "\",");
		bw.newLine();
		
		bw.write("  \"items\": [");
		
		int num = 1;
		ArrayList<Produto> produtos = listaProdutos(Integer.parseInt(id));
		for (Produto produto : produtos) {
			bw.newLine();
			
			bw.write("    {");
			bw.newLine();
			
			bw.write("      \"id\":\"3"+produto.getId_produto()+"\",");
			bw.newLine();
			
			String str = (produto.getPreco()).replace(",", "");
			
			bw.write("      \"price\": "+str+",");
			bw.newLine();
			
			bw.write("      \"quantity\": "+produto.getQuantidade()+"");
			bw.newLine();
						
			if(produtos.size() == num) {
				bw.write("    }");
			} else {
				bw.write("    },");	
			}
			
			num++;
		}
		
		bw.newLine();
		bw.write("  ]");
		bw.newLine();
		
		bw.write("}");
		bw.close();
				
		return file;		
		
	}
	
	public String retornaStatusPedido(Integer id) {
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		
		return pedido.getStatus();
	}
	
	public String retornaStatusDevolucao(Integer id) {
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		
		return pedido.getDevolucao();
	}
	
	public String retornaOrderIdPedido(Integer id) {
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		
		return pedido.getOrderId();
	}
	
	public String retornaDataEmissao(String result) throws Exception{
		
		JSONParser parser = new JSONParser();        		
		JSONArray results = (JSONArray) parser.parse("[" + result + "]");
		
		String data_emissao = "";
		
		for (int i = 0; i < results.size(); i++) {
			JSONObject jsonObject = (JSONObject) results.get(i);        									
			JSONArray results2 = (JSONArray) parser.parse("[" + jsonObject.get("requisicao_nota_fiscal") + "]");			
			for(int j = 0;j < results2.size(); j++) {
				JSONObject jsonObject2 = (JSONObject) results2.get(j);
				String data = (String) (jsonObject2.get("data_emissao"));
				data_emissao = data.substring(0,19);
			}			
		}						
		
		return data_emissao;
	}
	
	public String retornaNumeroNota(String id) throws Exception{
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		String ref = pedido.getRef_nota();
		
		String token = "R40tF7pr29P7tUhTJIj708ej5dTyDi7m";//AUTORIZAÇÃO
		//String token = "KD0QdUDKXiElEnd1mvalgVGeeyZ0SC8U";                

        String url = "https://api.focusnfe.com.br/v2/nfe/"+ref+"?completa=1";//AUTORIZAÇÃO 
        //String url = "http://homologacao.acrasnfe.acras.com.br/v2/nfe/"+ref+"?completa=1";
        
        Object config = new DefaultClientConfig();
        Client client = Client.create((ClientConfig) config);
        client.addFilter(new HTTPBasicAuthFilter(token, ""));

        WebResource request = client.resource(url);

        ClientResponse resposta = request.get(ClientResponse.class);

        String result = resposta.getEntity(String.class);
		
		JSONParser parser = new JSONParser();        		
		JSONArray results = (JSONArray) parser.parse("[" + result + "]");
		String numero = "";
		int count = results.size();	        		
		for (int i = 0; i < count; i++) {
			JSONObject jsonObject = (JSONObject) results.get(i);        			
			numero = (String) jsonObject.get("numero");        			        				        				        				
		}				
		
		return numero;
	}
	
	public String retornaMsgSefaz(String result) throws Exception{
		
		JSONParser parser = new JSONParser();        		
		JSONArray results = (JSONArray) parser.parse("[" + result + "]");
		int count = results.size();
		
		String msgSefaz = "";
		
		for (int i = 0; i < count; i++) {
			JSONObject jsonObject = (JSONObject) results.get(i);        			
			msgSefaz = (String) jsonObject.get("mensagem_sefaz");				        				
		}
		
		return msgSefaz;
	}
		
	public String sendGet(String ref, Integer id) throws Exception {
		
		System.out.println("entrou aqui para baixar a nota fiscal!");
		
		String token = "R40tF7pr29P7tUhTJIj708ej5dTyDi7m";//AUTORIZAÇÃO
		//String token = "KD0QdUDKXiElEnd1mvalgVGeeyZ0SC8U";
		
        String url = "https://api.focusnfe.com.br/v2/nfe/"+ref+"?completa=0";//AUTORIZAÇÃO
        //String url = "http://homologacao.acrasnfe.acras.com.br/v2/nfe/"+ref+"?completa=0";
        
        Object config = new DefaultClientConfig();
        Client client = Client.create((ClientConfig) config);
        client.addFilter(new HTTPBasicAuthFilter(token, ""));

        WebResource request = client.resource(url);

        ClientResponse resposta = request.get(ClientResponse.class);

        int HttpCode = resposta.getStatus();

        String result = resposta.getEntity(String.class);
        
        String pathPdf = "";
        String pathXml = "";
        String chaveNfe = "";
        
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();  
		Date dataNew = calendar.getTime(); 
		String dataAtual = sdf.format(dataNew);	
		String ano = dataAtual.substring(0, 4);
		String mes = dataAtual.substring(5, 7);
		
		String urlPdf = "";
		
        if((HttpCode == 200) && (result.contains("autorizado"))) {
        		JSONParser parser = new JSONParser();        		
        		JSONArray results = (JSONArray) parser.parse("[" + result + "]");
        		int count = results.size();	
        		
        		for (int i = 0; i < count; i++) {

        			JSONObject jsonObject = (JSONObject) results.get(i);        			
        			chaveNfe = (String) jsonObject.get("chave_nfe");
        			pathPdf = (String) jsonObject.get("caminho_danfe");
        			pathXml = (String) jsonObject.get("caminho_xml_nota_fiscal");        				
        				        				
        		}
        		
        		urlPdf = "https://api.focusnfe.com.br"+pathPdf+"";//AUTORIZAÇÃO
        		//urlPdf = "http://homologacao.acrasnfe.acras.com.br"+pathPdf+"";
        		
        		String urlXml = "https://api.focusnfe.com.br"+pathXml+"";//AUTORIZAÇÃO  
        		//String urlXml = "http://homologacao.acrasnfe.acras.com.br"+pathXml+"";        		        
        		
        		WebResource requestXml = client.resource(urlXml);
        		
        		ClientResponse respostaxml = requestXml.get(ClientResponse.class);
        		InputStream bodyXml = respostaxml.getEntityInputStream();
        		        		        		
        		File dirPdf = new File("/opt/pedidos/notas-pdf/"+ano+"/"+mes+"");
            	if (!dirPdf.exists()) {
            		dirPdf.mkdirs();
            	}
        			        			
            	String caminhoPdf = "/opt/pedidos/notas-pdf/"+ano+"/"+mes+"/";
        			
        		try {
        			URL urlTeste = new URL(urlPdf);
        	        InputStream in = urlTeste.openStream();
        	        File file = new File(caminhoPdf,""+chaveNfe+".pdf");
        	        FileOutputStream os = new FileOutputStream(file);
        	        byte[] buf = new byte[1024];
        	        int len;
        	        while( (len = in.read(buf)) > 0 ){
        	        	for(int i = 0; i < len; i++){
        	        		os.write(buf[i]);
        	            }
        	        }
        	        os.flush();
        	        os.close();
        	        in.close();
        	     }catch (Exception e){
        	    	 System.out.println(e);
        	     }finally{
        	         System.out.println("tudo ok");
        	     }        			        			
        		        			
        		File dir = new File("/opt/pedidos/xml/"+ano+"/"+mes+"");
            	if (!dir.exists()) {
            		dir.mkdirs();
            	}
            		
            	OutputStream outputStream = new FileOutputStream(new File("/opt/pedidos/xml/"+ano+"/"+mes+"/"+chaveNfe+".xml"));
            		
            	int read = 0;
            	byte[] bytes = new byte[1024];

            	while ((read = bodyXml.read(bytes)) != -1) {
            		outputStream.write(bytes, 0, read);
            	}
            		
            	bodyXml.close();
            	outputStream.close();
            		            	
        		String status = "NFE EMITIDA";
        		manager.createQuery("update Pedido set link ='"+urlPdf+"' where id_order = '"+id+"'").executeUpdate();
        		manager.createQuery("update Pedido set status ='"+status+"' where id_order = '"+id+"'").executeUpdate();        		
        		        		
        	}   
        
        return urlPdf;
		
	}	
	
	public String sendPost(File fileJson, String ref, Integer id, String user) throws Exception {
		
		System.out.println("entrou para gerar a nota fiscal!");
		
		String token = "R40tF7pr29P7tUhTJIj708ej5dTyDi7m";//AUTORIZAÇÃO
	//	String token = "KD0QdUDKXiElEnd1mvalgVGeeyZ0SC8U";
		
        String url = "https://api.focusnfe.com.br/v2/nfe?ref="+ref;//AUTORIZAÇÃO 
        //String url = "http://homologacao.acrasnfe.acras.com.br/v2/nfe?ref="+ref;
        
        Object config = new DefaultClientConfig();
        Client client = Client.create((ClientConfig) config);
        client.addFilter(new HTTPBasicAuthFilter(token, ""));        

        WebResource request = client.resource(url);

        ClientResponse resposta = request.post(ClientResponse.class, fileJson);

        int st = resposta.getStatus();
        
        String body = resposta.getEntity(String.class);
        
        System.out.println("body do sendPost: "+body);
        System.out.println("st: "+st);
        
        String status = "";
		
        if(st == 202) {
        	if(body.contains("processando_autorizacao")) {
        		status = "PROCESSANDO_AUTORIZAÇÃO";
        		manager.createQuery("update Pedido set status_nota ='"+status+"' where id_order = '"+id+"'").executeUpdate();
        		manager.createQuery("update Pedido set user_fat ='"+user+"' where id_order = '"+id+"'").executeUpdate();        		
        		System.out.println("UPDATE STATUS_NOTA: "+status);
        	}
        } else if(st == 422) {
        		String mensagem = "";
        		
        		JSONParser parser = new JSONParser();        		
        		JSONArray results = (JSONArray) parser.parse("[" + body.toString() + "]");
        		
        		for (int i = 0; i < results.size(); i++) {
        			JSONObject jsonObject = (JSONObject) results.get(i);
        			mensagem = (String) jsonObject.get("mensagem");
        		}		        		
        		
        		return mensagem;        	
        }
        
        return Integer.toString(st);
                
	}
	
	public String sendPostDev(File fileJson, String ref, Integer id, String user) throws Exception {
		
		System.out.println("entrou para gerar a nota fiscal!");
		
		String token = "R40tF7pr29P7tUhTJIj708ej5dTyDi7m";//AUTORIZAÇÃO
		//String token = "KD0QdUDKXiElEnd1mvalgVGeeyZ0SC8U";
		
        String url = "https://api.focusnfe.com.br/v2/nfe?ref="+ref;//AUTORIZAÇÃO 
        //String url = "http://homologacao.acrasnfe.acras.com.br/v2/nfe?ref="+ref;
        
        Object config = new DefaultClientConfig();
        Client client = Client.create((ClientConfig) config);
        client.addFilter(new HTTPBasicAuthFilter(token, ""));        

        WebResource request = client.resource(url);

        ClientResponse resposta = request.post(ClientResponse.class, fileJson);

        int st = resposta.getStatus();
        
        String body = resposta.getEntity(String.class);
        
        System.out.println("body do sendPost: "+body);
        System.out.println("st: "+st);
        
        String status = "";
		
        if(st == 202) {
        	if(body.contains("processando_autorizacao")) {
        		status = "PROCESSANDO_AUTORIZAÇÃO";
        		manager.createQuery("update Pedido set status_nota ='"+status+"' where id_order = '"+id+"'").executeUpdate();
        		manager.createQuery("update Pedido set user_dev ='"+user+"' where id_order = '"+id+"'").executeUpdate();        		
        		System.out.println("UPDATE STATUS_NOTA: "+status);
        	}
        } else if(st == 422 && body.contains("erros")) {
        		String mensagem = "";
        		
        		JSONParser parser = new JSONParser();        		
        		JSONArray results = (JSONArray) parser.parse("[" + body.toString() + "]");
        		
        		for (int i = 0; i < results.size(); i++) {
        			JSONObject jsonObject = (JSONObject) results.get(i);
    				JSONArray results2 = (JSONArray) parser.parse("" + jsonObject.get("erros") + "");
    				
    				for (int a = 0; a < results2.size(); a++) {
    					JSONObject jsonObject3 = (JSONObject) results2.get(a);
    					mensagem = (String) jsonObject3.get("mensagem");	
    				}
        		}        		        		        		
        		return mensagem;        	
        }
        
        return Integer.toString(st);
                
	}	
	
	public File gerarJson(String refNota, Integer id_order, List<Cliente> cliente, List<Empresa> empresa) throws IOException {
		
		System.out.println("entrou para gerar o JSON!");
		
		System.out.println("id_order: "+id_order);		
		
		
		
				
		String nome = "";
		String cpf = "";
		String endereco = "";
		String numero = "";
		String bairro = "";
		String cidade = "";
		String estado = "";		
		String cep = "";
		String telefone = "";
		String inscricaoEstadual = "";
		
		String nomeEmpresa = "";
		String nomeFantasia = "";
		String insEstEmpresa = "";
		String logradouro = "";
		String numeroEmpresa = "";
		String bairroEmpresa = "";
		String municipio = "";
		String ufEmpresa = "";
		String cepEmpresa = "";
		String cnpj = "";
				
		for (Cliente cli : cliente) {
			nome = cli.getNome();
			cpf = cli.getCpf();
			endereco = cli.getEndereco();
			numero = cli.getNumero();
			bairro = cli.getBairro();
			cidade = cli.getCidade();
			estado = cli.getEstado();
			cep = cli.getCep();
			telefone = cli.getTelefone();
			inscricaoEstadual = cli.getInscricao_estadual();
		}
		
		for (Empresa emp : empresa) {
			nomeEmpresa = emp.getNome();
			nomeFantasia = emp.getNomeFantasia();
			insEstEmpresa = emp.getInscricaoEstadual();
			logradouro = emp.getLogradouro();
			numeroEmpresa = emp.getNumero();
			bairroEmpresa = emp.getBairro();
			municipio = emp.getMunicipio();
			ufEmpresa = emp.getUf();
			cepEmpresa = emp.getCep();
			cnpj = emp.getCnpj();			
		}
		
		File file;								
		File dir = new File("/opt/pedidos/json");
		
		if (!dir.exists()) {
			dir.mkdirs();
		}
		



		
		file = new File(dir, "nFe_" + refNota + ".json");	
				
		file.createNewFile();
			
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		
		bw.write("{");		
		bw.newLine();
		
		bw.write("  \"natureza_operacao\":\"Venda\",");
		bw.newLine();
		
		bw.write("  \"forma_pagamento\":1,");
		bw.newLine();
		
		bw.write("  \"data_emissao\":");
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Calendar calendar = Calendar.getInstance();  
		Date dataNew = calendar.getTime(); 
		String dataAtual = sdf.format(dataNew);	
		
		bw.write("\"" + dataAtual+ "\",");
		bw.newLine();
		
		bw.write("  \"tipo_documento\":1,");
		bw.newLine();
		
		bw.write("  \"local_destino\":");
		if(estado.equals("MG")) {
			bw.write("1,");
		} else {
			bw.write("2,");
		}
		bw.newLine();
		
		bw.write("  \"finalidade_emissao\":1,");		
		bw.newLine();
		
		bw.write("  \"consumidor_final\":1,");
		bw.newLine();
		
		bw.write("  \"presenca_comprador\":1,");
		bw.newLine();
		
		bw.write("  \"cnpj_emitente\":\""+cnpj+"\",");
		bw.newLine();
		
		bw.write("  \"nome_emitente\":\""+nomeEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"nome_fantasia_emitente\":\""+nomeFantasia+"\",");
		bw.newLine();
		
		bw.write("  \"logradouro_emitente\":\""+logradouro+"\",");
		bw.newLine();
		
		bw.write("  \"numero_emitente\":\""+numeroEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"bairro_emitente\":\""+bairroEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"municipio_emitente\":\""+municipio+"\",");
		bw.newLine();
		
		bw.write("  \"uf_emitente\":\""+ufEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"cep_emitente\":\""+cepEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"inscricao_estadual_emitente\":\""+insEstEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"regime_tributario_emitente\":1,");
		bw.newLine();
		
		if(cpf.contains("/")) {
			bw.write("  \"cnpj_destinatario\":\""+cpf+"\",");						
		} else {
			bw.write("  \"cpf_destinatario\":\""+cpf+"\",");
		}		
		bw.newLine();
		
		//teste-homologacao:
		//nome = "NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL";
		
		bw.write("  \"nome_destinatario\":\""+nome+"\",");
		bw.newLine();
		
		bw.write("  \"logradouro_destinatario\":\""+endereco+"\",");
		bw.newLine();
		
		bw.write("  \"numero_destinatario\":\""+numero+"\",");
		bw.newLine();
		
		bw.write("  \"bairro_destinatario\":\""+bairro+"\",");
		bw.newLine();
		
		bw.write("  \"municipio_destinatario\":\""+cidade+"\",");
		bw.newLine();
		
		bw.write("  \"uf_destinatario\":\""+estado+"\",");
		bw.newLine();
		
		bw.write("  \"cep_destinatario\":");
		String cepFinal = "";		
		if (cep.contains(".") && !cep.contains("-")) {
			cepFinal = cep.replaceAll(".", "");	
		}
		if (cep.contains("-") && !cep.contains(".")) {
			cepFinal = cep.replaceAll("-", "");
		}
		if (cep.contains("-") && cep.contains(".")) { 
			String cepAux = cep.replace("-", "");
			cepFinal = cepAux.replace(".", "");			
		}
		bw.write("\""+cepFinal+"\",");
		bw.newLine();
		
		bw.write("  \"telefone_destinatario\":\""+telefone+"\",");
		bw.newLine();
		
		bw.write("  \"indicador_inscricao_estadual_destinatario\":");
		if(inscricaoEstadual.equals("")) {
			bw.write("9,");			
		} else {
			bw.write("1,");
			bw.write("  \"inscricao_estadual_destinatario\":\""+inscricaoEstadual+"\",");
		}
		bw.newLine();
		
		bw.write("  \"valor_produtos\":");
		bw.write("\""+(retornoValorProdutos(id_order)).replace(",", ".")+"\",");
		bw.newLine();
		
		
		bw.write("  \"valor_desconto\":");
		String desconto = "";
		if((retornoValorDescontoTotal(id_order).equals("0"))) {
			bw.write("\"0.00\",");
			desconto = "0.00";
		} else {
			bw.write("\""+(retornoValorDescontoTotal(id_order)).replace(",", ".")+"\",");
			desconto = (retornoValorDescontoTotal(id_order)).replace(",", ".");
		}
		bw.newLine();
		
		
		
		List<Pedido> p = manager.createQuery("select p from Pedido p where id_order='"+id_order+"'", Pedido.class).getResultList();
		
		Double taxa = 0.0;
		
		for(int i =0; i < p.size(); i++) {
			
			taxa = p.get(i).getTaxa_total();
		}
		
		String valorProdutos = (retornoValorProdutos(id_order)).replace(",", ".");
		String valorFrete = (retornoValorFreteTotal(id_order)).replace(",", "."); 
		double produtosDouble = Double.parseDouble(valorProdutos);
		double freteDouble = Double.parseDouble(valorFrete) ;
		double descontoDouble = Double.parseDouble(desconto);
		double valorTotal = (produtosDouble + freteDouble) - descontoDouble ;
		
		
		valorTotal = valorTotal+taxa;
		freteDouble = freteDouble+taxa;
			
		

		bw.write("  \"valor_frete\":");
		bw.write("\""+(freteDouble)+"\",");
		bw.newLine();
		bw.write("  \"valor_total\":");
		bw.write("\""+(String.format("%.2f",valorTotal)).replace(",", ".")+"\",");
		bw.newLine();
		bw.write("  \"modalidade_frete\":1,");
		bw.newLine();
		
		bw.write("  \"items\": [");
				
		int num = 1;
		ArrayList<Produto> produtos = listaProdutos(id_order);
		for (Produto produto : produtos) {
			bw.newLine();
			
			bw.write("    {");
			bw.newLine();
			
			bw.write("      \"numero_item\":"+num+",");
			bw.newLine();
			
			bw.write("      \"codigo_produto\":\""+produto.getId_produto()+"\",");
			bw.newLine();
			
			bw.write("      \"descricao\":\""+produto.getDescricao()+"\",");
			bw.newLine();
			
			int cfop = 0;
			if(estado.equals("MG")) {
				cfop = 5102;
			} else {
				cfop = 6102;
			}			
			bw.write("      \"cfop\":\""+cfop+"\",");
			bw.newLine();
			
			bw.write("      \"unidade_comercial\":\"UN\",");
			bw.newLine();
			
			bw.write("      \"quantidade_comercial\":\""+produto.getQuantidade()+".0\",");
			bw.newLine();
			
			bw.write("      \"valor_unitario_comercial\":\""+(produto.getPreco()).replace(",", ".")+"\",");
			bw.newLine();
			
			bw.write("      \"unidade_tributavel\":\"UN\",");
			bw.newLine();
			
			bw.write("      \"quantidade_tributavel\":\""+produto.getQuantidade()+".0\",");
			bw.newLine();
			
			bw.write("      \"valor_unitario_tributavel\":\""+(produto.getPreco()).replace(",", ".")+"\",");
			bw.newLine();
			
			String valor = produto.getPreco().replaceAll(",", ".");
			double valorDouble = Double.parseDouble(valor);
			double calculo = valorDouble * produto.getQuantidade();

			Double t = taxa / produtos.size();
		
			Double f = Double.parseDouble(produto.getFrete().replace(",",".")) + t;
			
			bw.write("      \"valor_bruto\":\""+(String.format("%.2f",calculo)).replace(",", ".")+"\",");
			bw.newLine();
			
			bw.write("      \"valor_frete\":\""+ f +"\"," );
			
			bw.newLine();
			
			bw.write("      \"valor_desconto\":");			
			String descontoItem = produto.getDesconto();
			if(descontoItem.equals("0")) {
				bw.write("\"0.00\",");
			} else {
				String desc = descontoItem.replace(",", ".");
				bw.write("\""+desc+"\",");
			}
			bw.newLine();
			              
			bw.write("      \"inclui_no_total\":1,");			
			bw.newLine();
			
			bw.write("      \"icms_origem\":0,");
			bw.newLine();		
			
			bw.write("      \"codigo_ncm\":");
			String ncm = "";
			try {
				ncm = retornaNCM(id_order, produto.getId_produto());
			} catch (Exception e) {
				e.printStackTrace();
			}
			bw.write("\""+ncm+"\",");
			bw.newLine();
			
			if(cpf.contains("/")) {
				bw.write("      \"icms_situacao_tributaria\":\"900\",");						
			} else {
				bw.write("      \"icms_situacao_tributaria\":\"102\",");
			}		
			bw.newLine();
			
			bw.write("      \"icms_modalidade_base_calculo\":0,");			
			bw.newLine();
			
			bw.write("      \"pis_situacao_tributaria\":\"98\",");			
			bw.newLine();
			
			bw.write("      \"cofins_situacao_tributaria\":\"98\"");			
			bw.newLine();
			
			if(produtos.size() == num) {
				bw.write("    }");
			} else {
				bw.write("    },");	
			}
			
			num++;
		}
		
		bw.newLine();
		bw.write("  ],");
		bw.newLine();
		
		bw.write("  \"volumes\": [");
		bw.newLine();
		
		bw.write("    {");
		bw.newLine();
		
		bw.write("      \"quantidade\":\"1\",");
		bw.newLine();
		
		bw.write("      \"especie\":\"volume\",");
		bw.newLine();
		
		double pesoTotal = 0.0;
		
		for (Produto produto : produtos) {
			String peso = produto.getPeso().replaceAll(",", ".");
			double pesoDouble = Double.parseDouble(peso);
			pesoTotal += pesoDouble;
		}
		
		pesoTotal = pesoTotal/1000;
		
		bw.write("  	  \"peso_liquido\":\""+pesoTotal+"\",");
		bw.newLine();
		
		bw.write("  	  \"peso_bruto\":\""+pesoTotal+"\"");
		bw.newLine();
		
		bw.write("    }");
		bw.newLine();
		
		bw.write("  ]");
		bw.newLine();
		
		bw.write("}");
		bw.close();
		
		return file;
	}
	
	
	public File gerarJsonDevolucao(String refNota, Integer id_order, List<Cliente> cliente, List<Empresa> empresa) throws Exception {
		
		System.out.println("entrou para gerar o JSON de Devolucao!");
		
		System.out.println("id_order: "+id_order);
		
		String token = "R40tF7pr29P7tUhTJIj708ej5dTyDi7m";//AUTORIZAÇÃO
		//String token = "KD0QdUDKXiElEnd1mvalgVGeeyZ0SC8U";                

		String url = "https://api.focusnfe.com.br/v2/nfe/"+refNota+"?completa=1";//AUTORIZAÇÃO 
		//String url = "http://homologacao.acrasnfe.acras.com.br/v2/nfe/"+refNota+"?completa=1";
		        
		Object config = new DefaultClientConfig();
		Client client = Client.create((ClientConfig) config);
		client.addFilter(new HTTPBasicAuthFilter(token, ""));

		WebResource request = client.resource(url);

		ClientResponse resposta = request.get(ClientResponse.class);

		String result = resposta.getEntity(String.class);
		
		JSONParser parser = new JSONParser();        		
		JSONArray results = (JSONArray) parser.parse("[" + result + "]");
		
		String data_emissao = "";
		String numeroNota = "";
		String chaveNfe = "";
		
		for (int i = 0; i < results.size(); i++) {
			JSONObject jsonObject = (JSONObject) results.get(i);        									
			JSONArray results2 = (JSONArray) parser.parse("[" + jsonObject.get("requisicao_nota_fiscal") + "]");			
			for(int j = 0;j < results2.size(); j++) {
				JSONObject jsonObject2 = (JSONObject) results2.get(j);
				String data = (String) (jsonObject2.get("data_emissao"));
				data_emissao = data.substring(0,19);
				numeroNota = (String) jsonObject2.get("numero");
				chaveNfe = (String) jsonObject2.get("chave_nfe");
			}			
		}	
		
		data_emissao = (retornaDataEmissao(result)).substring(0,10);
		
		String nome = "";
		String cpf = "";
		String endereco = "";
		String numero = "";
		String bairro = "";
		String cidade = "";
		String estado = "";		
		String cep = "";
		String telefone = "";
		String inscricaoEstadual = "";
		
		String nomeEmpresa = "";
		String nomeFantasia = "";
		String insEstEmpresa = "";
		String logradouro = "";
		String numeroEmpresa = "";
		String bairroEmpresa = "";
		String municipio = "";
		String ufEmpresa = "";
		String cepEmpresa = "";
		String cnpj = "";
		
		
		for (Cliente cli : cliente) {
			nome = cli.getNome();
			cpf = cli.getCpf();
			endereco = cli.getEndereco();
			numero = cli.getNumero();
			bairro = cli.getBairro();
			cidade = cli.getCidade();
			estado = cli.getEstado();
			cep = cli.getCep();
			telefone = cli.getTelefone();
			inscricaoEstadual = cli.getInscricao_estadual();
		}
		
		for (Empresa emp : empresa) {
			nomeEmpresa = emp.getNome();
			nomeFantasia = emp.getNomeFantasia();
			insEstEmpresa = emp.getInscricaoEstadual();
			logradouro = emp.getLogradouro();
			numeroEmpresa = emp.getNumero();
			bairroEmpresa = emp.getBairro();
			municipio = emp.getMunicipio();
			ufEmpresa = emp.getUf();
			cepEmpresa = emp.getCep();
			cnpj = emp.getCnpj();			
		}
		
		File file;								
		File dir = new File("/opt/pedidos/json-dev");
		
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		file = new File(dir, "dev_" + refNota + ".json");	
				
		file.createNewFile();
		
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		
		bw.write("{");		
		bw.newLine();
		
		bw.write("  \"natureza_operacao\":\"Devolução de Venda\",");
		bw.newLine();
		
		bw.write("  \"forma_pagamento\":1,");
		bw.newLine();
		
		bw.write("  \"data_emissao\":");
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Calendar calendar = Calendar.getInstance();  
		Date dataNew = calendar.getTime(); 
		String dataAtual = sdf.format(dataNew);	
		
		bw.write("\"" + dataAtual+ "\",");
		bw.newLine();
		
		bw.write("  \"tipo_documento\":0,");
		bw.newLine();
		
		bw.write("  \"local_destino\":");
		if(estado.equals("MG")) {
			bw.write("1,");
		} else {
			bw.write("2,");
		}
		bw.newLine();
		
		bw.write("  \"finalidade_emissao\":4,");		
		bw.newLine();
		
		bw.write("  \"consumidor_final\":1,");
		bw.newLine();
		
		bw.write("  \"presenca_comprador\":1,");
		bw.newLine();
		
		bw.write("  \"cnpj_emitente\":\""+cnpj+"\",");
		bw.newLine();
		
		bw.write("  \"nome_emitente\":\""+nomeEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"nome_fantasia_emitente\":\""+nomeFantasia+"\",");
		bw.newLine();
		
		bw.write("  \"logradouro_emitente\":\""+logradouro+"\",");
		bw.newLine();
		
		bw.write("  \"numero_emitente\":\""+numeroEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"bairro_emitente\":\""+bairroEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"municipio_emitente\":\""+municipio+"\",");
		bw.newLine();
		
		bw.write("  \"uf_emitente\":\""+ufEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"cep_emitente\":\""+cepEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"inscricao_estadual_emitente\":\""+insEstEmpresa+"\",");
		bw.newLine();
		
		bw.write("  \"regime_tributario_emitente\":1,");
		bw.newLine();
		
		if(cpf.contains("/")) {
			bw.write("  \"cnpj_destinatario\":\""+cpf+"\",");						
		} else {
			bw.write("  \"cpf_destinatario\":\""+cpf+"\",");
		}		
		bw.newLine();
				
		bw.write("  \"nome_destinatario\":\""+nome+"\",");
		bw.newLine();
				
		bw.write("  \"logradouro_destinatario\":\""+endereco+"\",");
		bw.newLine();
				
		bw.write("  \"numero_destinatario\":\""+numero+"\",");
		bw.newLine();
				
		bw.write("  \"bairro_destinatario\":\""+bairro+"\",");
		bw.newLine();
				
		bw.write("  \"municipio_destinatario\":\""+cidade+"\",");
		bw.newLine();
				
		bw.write("  \"uf_destinatario\":\""+estado+"\",");
		bw.newLine();
				
		bw.write("  \"cep_destinatario\":");
		String cepFinal = "";		
		if (cep.contains(".") && !cep.contains("-")) {
			cepFinal = cep.replaceAll(".", "");	
		}
		if (cep.contains("-") && !cep.contains(".")) {
			cepFinal = cep.replaceAll("-", "");
		}
		if (cep.contains("-") && cep.contains(".")) { 
			String cepAux = cep.replace("-", "");
			cepFinal = cepAux.replace(".", "");			
		}
		bw.write("\""+cepFinal+"\",");
		bw.newLine();
				
		bw.write("  \"telefone_destinatario\":\""+telefone+"\",");
		bw.newLine();
				
		bw.write("  \"indicador_inscricao_estadual_destinatario\":");
		if(inscricaoEstadual.equals("")) {
			bw.write("9,");			
		} else {
			bw.write("1,");
			bw.write("  \"inscricao_estadual_destinatario\":\""+inscricaoEstadual+"\",");
		}
		bw.newLine();
				
		bw.write("  \"valor_produtos\":");
		bw.write("\""+(retornoValorProdutos(id_order)).replace(",", ".")+"\",");
		bw.newLine();
				
		bw.write("  \"valor_frete\":");
		bw.write("\""+(retornoValorFreteTotal(id_order)).replace(",", ".")+"\",");
		bw.newLine();
				
		bw.write("  \"valor_desconto\":");
		String desconto = "";
		if((retornoValorDescontoTotal(id_order).equals("0"))) {
			bw.write("\"0.00\",");
			desconto = "0.00";
		} else {
			bw.write("\""+(retornoValorDescontoTotal(id_order)).replace(",", ".")+"\",");
			desconto = (retornoValorDescontoTotal(id_order)).replace(",", ".");
		}
		bw.newLine();
			
		bw.write("  \"valor_total\":");
		String valorProdutos = (retornoValorProdutos(id_order)).replace(",", ".");
		String valorFrete = (retornoValorFreteTotal(id_order)).replace(",", "."); 
		double produtosDouble = Double.parseDouble(valorProdutos);
		double freteDouble = Double.parseDouble(valorFrete);
		double descontoDouble = Double.parseDouble(desconto);
		double valorTotal = (produtosDouble + freteDouble) - descontoDouble;
		bw.write("\""+(String.format("%.2f",valorTotal)).replace(",", ".")+"\",");
		bw.newLine();
				
		bw.write("  \"modalidade_frete\":1,");
		bw.newLine();
		
		String ano = data_emissao.substring(0, 4);
		String mes = data_emissao.substring(5, 7);
		String dia = data_emissao.substring(8, 10);
		String data = dia+"/"+mes+"/"+ano;
		
		String chave = chaveNfe.substring(3, 47);
		
		bw.write("  \"informacoes_adicionais_contribuinte\":\"Devolução total referente a nossa NFe "+numeroNota+" de "+data+", chave de Acesso "+chave+" por desacordo comercial\",");
		bw.newLine();
		
		bw.write("  \"items\": [");
		
		int num = 1;
		ArrayList<Produto> produtos = listaProdutos(id_order);
		for (Produto produto : produtos) {
			bw.newLine();
			
			bw.write("    {");
			bw.newLine();
			
			bw.write("      \"numero_item\":"+num+",");
			bw.newLine();
			
			bw.write("      \"codigo_produto\":\""+produto.getId_produto()+"\",");
			bw.newLine();
			
			bw.write("      \"descricao\":\""+produto.getDescricao()+"\",");
			bw.newLine();
			
			int cfop = 0;
			if(estado.equals("MG")) {
				cfop = 1202;
			} else {
				cfop = 2202;
			}			
			bw.write("      \"cfop\":\""+cfop+"\",");
			bw.newLine();
			
			bw.write("      \"unidade_comercial\":\"UN\",");
			bw.newLine();
			
			bw.write("      \"quantidade_comercial\":\""+produto.getQuantidade()+".0\",");
			bw.newLine();
			
			bw.write("      \"valor_unitario_comercial\":\""+(produto.getPreco()).replace(",", ".")+"\",");
			bw.newLine();
			
			bw.write("      \"unidade_tributavel\":\"UN\",");
			bw.newLine();
			
			bw.write("      \"quantidade_tributavel\":\""+produto.getQuantidade()+".0\",");
			bw.newLine();
			
			bw.write("      \"valor_unitario_tributavel\":\""+(produto.getPreco()).replace(",", ".")+"\",");
			bw.newLine();
			
			String valor = produto.getPreco().replaceAll(",", ".");
			double valorDouble = Double.parseDouble(valor);
			double calculo = valorDouble * produto.getQuantidade();
			
			bw.write("      \"valor_bruto\":\""+(String.format("%.2f",calculo)).replace(",", ".")+"\",");
			bw.newLine();
			
			bw.write("      \"valor_frete\":\""+(produto.getFrete()).replace(",", ".")+"\",");
			bw.newLine();
			
			bw.write("      \"valor_desconto\":");			
			String descontoItem = produto.getDesconto();
			if(descontoItem.equals("0")) {
				bw.write("\"0.00\",");
			} else {
				String desc = descontoItem.replace(",", ".");
				bw.write("\""+desc+"\",");
			}
			bw.newLine();
			              
			bw.write("      \"inclui_no_total\":1,");			
			bw.newLine();
			
			bw.write("      \"icms_origem\":0,");
			bw.newLine();		
			
			bw.write("      \"codigo_ncm\":");
			String ncm = "";
			try {
				ncm = retornaNCM(id_order, produto.getId_produto());
			} catch (Exception e) {
				e.printStackTrace();
			}
			bw.write("\""+ncm+"\",");
			bw.newLine();
			
			bw.write("      \"icms_situacao_tributaria\":\"900\",");						
			bw.newLine();
			
			bw.write("      \"icms_modalidade_base_calculo\":0,");			
			bw.newLine();
			
			bw.write("      \"pis_situacao_tributaria\":\"98\",");			
			bw.newLine();
			
			bw.write("      \"cofins_situacao_tributaria\":\"98\"");			
			bw.newLine();
			
			if(produtos.size() == num) {
				bw.write("    }");
			} else {
				bw.write("    },");	
			}
			
			num++;
		}
		
		bw.newLine();
		bw.write("  ],");
		bw.newLine();
		
		bw.write("  \"volumes\": [");
		bw.newLine();
		
		bw.write("    {");
		bw.newLine();
		
		bw.write("      \"quantidade\":\"1\",");
		bw.newLine();
		
		bw.write("      \"especie\":\"volume\",");
		bw.newLine();
		
		double pesoTotal = 0.0;
		
		for (Produto produto : produtos) {
			String peso = produto.getPeso().replaceAll(",", ".");
			double pesoDouble = Double.parseDouble(peso);
			pesoTotal += pesoDouble;
		}
		
		pesoTotal = pesoTotal/1000;
		
		bw.write("  	  \"peso_liquido\":\""+pesoTotal+"\",");
		bw.newLine();
		
		bw.write("  	  \"peso_bruto\":\""+pesoTotal+"\"");
		bw.newLine();
		
		bw.write("    }");
		bw.newLine();
		
		bw.write("  ],");
		bw.newLine();
		
		bw.write("  \"notas_referenciadas\": [");
		bw.newLine();
		
		bw.write("    {");
		bw.newLine();
		
		bw.write("      \"chave_nfe\":\""+chave+"\"");
		bw.newLine();
		
		bw.write("    }");
		bw.newLine();
		
		bw.write("  ]");
		bw.newLine();
		
		bw.write("}");
		bw.close();
		
		manager.createQuery("update Pedido set devolucao = 'TRUE' where id_order = '"+id_order+"'").executeUpdate();
				
		return file;
		
	}
	
	public String retornaNCM(Integer id, String id_produto) throws Exception {
		
		Connection connection = ConnectionFactoryPostgresql.getConnection();
		Statement st = connection.createStatement();
		
		boolean sql = st.execute("select * from" + 
				" produtos p" + 
				" inner join fiscal_ncm_mva f on f.id_ncm = p.id_ncm" + 
				" where p.id_produto = "+id_produto+" ");
				
		ResultSet rs = st.getResultSet();
		String codigo = "";
		while (rs.next()){
			codigo = rs.getString("codigo_ncm");
		}
		
		return codigo; 
	}
		
	public String retornaAlturaPedido(String id) {
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		
		return pedido.getAltura();
	}
	
	public String retornaLarguraPedido(String id) {
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		
		return pedido.getLargura();
	}

	public String retornaComprimentoPedido(String id) {
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		
		return pedido.getComprimento();
	}
	
	
	
	public String retornaRefNota(Integer id) {
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		
		return pedido.getRef_nota();
	}
	
	public void atualizaRefNota(Integer ref, Integer id) {
		
		manager.createQuery("update Pedido set ref_nota = '"+ref+"' where id_order = '"+id+"'").executeUpdate();	
	}
	
	public String retornaStatusNota(Integer id) {
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		
		return pedido.getStatus_nota();
	}
	
	public String retornoValorProdutos(Integer id) {

		List<Produto> produtos = manager.createQuery("select e from Produto e where id_order = '"+id+"'", Produto.class).getResultList();

		String valorProdutos = "";
		
		for (Produto produto : produtos) {
			valorProdutos = produto.getValortotal();
		}
		
		return valorProdutos;
				
	}
	
	public String retornoValorFreteTotal(Integer id) {

		List<Produto> produtos = manager.createQuery("select e from Produto e where id_order = '"+id+"'", Produto.class).getResultList();

		String valorFrete = "";
		
		for (Produto produto : produtos) {
			valorFrete = produto.getFretetotal();
		}
		
		return valorFrete;
				
	}
	
	public String retornoValorDescontoTotal(Integer id) {

		List<Produto> produtos = manager.createQuery("select e from Produto e where id_order = '"+id+"'", Produto.class).getResultList();

		String valorDesconto = "";
		
		for (Produto produto : produtos) {
			valorDesconto = produto.getDescontoTotal();
		}				
		
		return valorDesconto;
				
	}
	
	public ArrayList<Produto> listaProdutos(Integer id) {

		ArrayList<Produto> produtos = (ArrayList<Produto>) manager.createQuery("select e from Produto e where id_order = '"+id+"'", Produto.class).getResultList();

		return produtos;
				
	}
	
	public String retornaCodigoRastreio(String id) {
		
		Pedido pedido = manager.createQuery("select p from Pedido p where id_order = '"+id+"'", Pedido.class).getSingleResult();
		
		return pedido.getRastreio();
		
	}
	
	public void salvaDataHoraEmissaoNota(DateTime data, String ref) {
		
		String dia = (data.toString()).substring(8, 10);
		String mes = (data.toString()).substring(5, 7);
		String ano = (data.toString()).substring(0, 4);
		String hora = (data.toString()).substring(11, 13);
		String min = (data.toString()).substring(14, 16);
		String seg = (data.toString()).substring(17, 19);
		
		String emissao = ""+dia+"/"+mes+"/"+ano+" "+hora+":"+min+":"+seg+""; 
		
		manager.createQuery("update Pedido set data_emissao = '"+emissao+"' where ref_nota = '"+ref+"'").executeUpdate(); 
		
	}
	
	
}
