package br.com.casaamerica.Infat.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.connLoja.ConnectionFactoryPostgresql;
import br.com.casaamerica.Infat.model.Cliente;
import br.com.casaamerica.Infat.model.Pedido;
import br.com.casaamerica.Infat.model.Produto;

@Repository
@Transactional
public class EnviaPedidoBancoDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public void enviaPedido(Integer id) throws Exception {

		System.out.println("Enviando o pedido: " + id);
		
		Connection connection = ConnectionFactoryPostgresql.getConnection();
		Statement statement = connection.createStatement();
		
		System.out.println("passou aqui!");
		
		String cpf = "";

		try {

			List<Produto> p = manager
					.createQuery("select e from Produto e where e.id_order = '" + id + "'", Produto.class)
					.getResultList();

			for (int i = 0; i < p.size(); i++) {

				cpf = p.get(i).getCpfCliente();

			}

			System.out.println(cpf);

			boolean sql = statement.execute("select * from pessoas p inner join pessoas_fones pf on p.id_pessoa = pf.id_pessoa inner join pessoas_enderecos pe on p.id_pessoa = pe.id_pessoa where cpf = '"+ cpf + "'");
			ResultSet rs = statement.getResultSet();

			if (rs.next()) {
				System.out.println("nomes: " + rs.getString("nome"));
				System.out.println("telefone: " + rs.getString("fone"));
				System.out.println("end: " + rs.getString("cidade"));
				String id_pessoa = rs.getString("id_pessoa");
				insereVenda(id, id_pessoa);
				financeiroPedido(id);
			} else {
				System.out.println("cadastrar cliente");
				cadastrarCliente(cpf, id);
				financeiroPedido(id);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void cadastrarCliente(String cpfCliente, int id_order) throws Exception {

		System.out.println("cadastrando cliente : " + cpfCliente);

		Connection connection = ConnectionFactoryPostgresql.getConnection();
		Statement statement = connection.createStatement();

		Cliente c = new Cliente();
		
		Timestamp dataUltimaCompra = new Timestamp(System.currentTimeMillis());

		try {

			c = manager.createQuery("select c from Cliente c where c.cpf = '" + cpfCliente + "' and c.idOrder = '"+id_order+"' ", Cliente.class).getSingleResult();
			System.out.println(c.getNome());

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			String data = dateFormat.format(date);

			boolean findPessoa = statement.execute("select * from pessoas where cpf = '" + c.getCpf() + "'");
			ResultSet rsp = statement.getResultSet();
			String id_pessoa = "";

			if (rsp.next()) {
				System.out.println("ja tem cadastro");
			} else {

				boolean sql = statement.execute(
						"insert into pessoas (nome, cpf, sexo, estado_civil,naturalidade, discriminador, id_foto, id_tipo_ficha, salario, data_abertura_ficha, id_assinatura, valor_limite, data_ultima_compra) "
								+ "values ('" + c.getNome() + "', '" + c.getCpf() + "','Masculino', 'Solteiro','"
								+ c.getCidade() + "', 'PessoaFisica', 2017601, 5, 1500, '" + data
								+ "', 1936201, 2.53164556962025, '"+dataUltimaCompra+"')");

			}

			boolean findPessoa2 = statement.execute("select * from pessoas where cpf = '" + c.getCpf() + "'");
			ResultSet rsp2 = statement.getResultSet();

			if (rsp2.next()) {

				id_pessoa = rsp2.getString("id_pessoa");
			}

			boolean findEnd = statement.execute("select * from pessoas_enderecos where id_pessoa = " + id_pessoa + " ");
			ResultSet rs = statement.getResultSet();

			if (rs.next()) {

				System.out.println("pessoa : " + rs.getString("endereco"));
			} else {

				boolean insertEnd = statement.execute(
						"insert into pessoas_enderecos (id_pessoa, logradouro, numero,complemento, bairro, cidade, estado, cep) "
								+ "values (" + id_pessoa + ", '" + c.getEndereco() + "', '" + c.getNumero() + "','"
								+ c.getComplemento() + "','" + c.getBairro() + "', '" + c.getCidade() + "', '"
								+ c.getEstado() + "','" + c.getCep() + "' )");
			}

			boolean findTel = statement.execute("select * from pessoas_fones where id_pessoa = " + id_pessoa + "");
			ResultSet rst = statement.getResultSet();

			if (rst.next()) {
				System.out.println("telefone : " + rst.getString("fone"));
			} else {
				boolean insertEnd = statement.execute("insert into pessoas_fones (id_pessoa, fone,referencia)"
						+ "values (" + id_pessoa + ", '035000000000', ' telefone cliente site' )");
			}

			insereVenda(id_order, id_pessoa);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void insereVenda(int id_order, String id_pessoa) throws Exception {

		System.out.println("insere venda no caixa");
		
		Connection connection = ConnectionFactoryPostgresql.getConnection();
		Statement statement = connection.createStatement();

		Statement st = connection.createStatement();
		Statement s = connection.createStatement();

		List<Produto> p = manager
				.createQuery("select e from Produto e where e.id_order = '" + id_order + "'", Produto.class)
				.getResultList();

		System.out.println("tamanho :" + p.size());

		for (int i = 0; i < p.size() / p.size(); i++) {

			String id_caixa = p.get(i).getId_venda_caixa();
			System.out.println("id_venda_caixa:"+p.get(i).getId_venda_caixa());

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar calendar = Calendar.getInstance();
			Date dataNew = calendar.getTime();
			String data = sdf.format(dataNew);

			calendar.add(Calendar.DATE, 30);
			Date dataPagamento = calendar.getTime();
			String dataP = sdf.format(dataPagamento);

			if (id_caixa.equals("")) {
				System.out.println("incluir pedido no caixa");

				System.out.println(data + " - " + dataP + " - " + id_pessoa + " - " + id_order + " - "
						+ p.get(i).getFretetotal() + " - " + p.get(i).getValortotal());

				String descontoTotal = p.get(i).getDescontoTotal().replaceAll(",", ".");
				String precoTotal = p.get(i).getValortotal().replaceAll(",", ".");
				
				double descontoDouble = Double.parseDouble(descontoTotal);
				double precoDouble = Double.parseDouble(precoTotal);
				double precoFinal = precoDouble - descontoDouble;
				
				String freteTotal = p.get(i).getFretetotal().replaceAll(",", ".");

				boolean sql = statement.execute(
						"insert into vendas_vendas (data, confirmada,forma_pagamento, tipo, valor_total, numero_parcelas, data_primeira_parcela,id_loja, id_vendedor,id_caixa, id_cliente, id_order, frete) "
								+ "values ('" + data + "',TRUE, 0, 0, " + precoFinal + ", 1, '"+dataP+"', 18908, 265 ,265, "+id_pessoa+",'"+ id_order + "', '" + freteTotal + "')");

				System.out.println("tamanho 2 : " + p.size());
				int q = 0;
				int id_venda = 0;
				for (int j = 0; j < p.size(); j++) {

					q += p.get(j).getQuantidade();					

					boolean sql2 = statement.execute("select id_venda from vendas_vendas where id_order = '" + id_order + "'");

					ResultSet rs = statement.getResultSet();

					if (rs.next()) {

						System.out.println(rs.getInt("id_venda"));
						String preco = p.get(j).getPreco().replaceAll(",", ".");
						String frete = p.get(j).getFrete().replaceAll(",", ".");

						id_venda = rs.getInt("id_venda");

						System.out.println(preco + " - " + rs.getInt("id_venda") + " - " + p.get(j).getQuantidade()
								+ " - " + p.get(j).getDescricao() + " - " + frete);

						boolean sql3 = st.execute("insert into vendas_itens_venda (discriminador, valor, id_produto, id_venda, pos0, descricao_produto, valor_aliquota_item, tipo_aliquota_item, frete )"
										+ "values ('ItemVendido'," + preco + ","+p.get(j).getId_produto()+",  " + rs.getInt("id_venda")
										+ ", " + p.get(j).getQuantidade() + ",'" + p.get(j).getDescricao()
										+ "', 18, 0, '" + frete + "' )");

						boolean up = st.execute("select * from produtos_estoques where id_produto = "+p.get(j).getId_produto()+"");
						ResultSet rst = st.getResultSet();
						
						while(rst.next()) {
							
							int estoque =  rst.getInt("estoque_pos0") + rst.getInt("estoque_pos1") + rst.getInt("estoque_pos2") + rst.getInt("estoque_pos3") + rst.getInt("estoque_pos4") + rst.getInt("estoque_pos5") + rst.getInt("estoque_pos6") + rst.getInt("estoque_pos7") + rst.getInt("estoque_pos8") + rst.getInt("estoque_pos9") +rst.getInt("estoque_pos10") + rst.getInt("estoque_pos11")+rst.getInt("estoque_pos12")+rst.getInt("estoque_pos13");
							Integer valor = Integer.valueOf(p.get(i).getQuantidade().toString());
							int resultado = estoque - valor;
							System.out.println("total estoque : " + resultado);
							
							boolean update = s.execute("update produtos_estoques set estoque_pos0 = "+resultado+" where id_produto = "+rst.getInt("id_produto")+"");
							
						}												
						
						System.out.println("insere : " + j);
					}

					boolean sql4 = st.execute("update vendas_vendas set itens_total = " + q + " where id_venda = " + id_venda + "");

				}
				
				manager.createQuery("update Produto set id_venda_caixa ='" + id_venda + "' where id_order = '" + id_order + "'").executeUpdate();
				System.out.println(id_order);
			
			} else {
				System.out.println("pedido já foi incluido");
			}
		}
	}

	
	public void financeiroPedido(Integer id) throws Exception {
				
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String data = dateFormat.format(date);               
        
        DateFormat dateFormat2 = new SimpleDateFormat("yyMMdd");
        Date date2 = new Date();
        String rv = dateFormat2.format(date2);              
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        String dataMov = dateFormat.format(cal.getTime());
        
        Pedido p = manager.createQuery("select e from Pedido e where e.id_order = '" + id + "'", Pedido.class).getSingleResult();
        
        double preco = p.getProdutos_total() - p.getDesconto_total();
        
        System.out.println("valorTotal : " + preco);
        
        Connection connection = ConnectionFactoryPostgresql.getConnection();
        Statement statement = connection.createStatement();
        
        boolean sql = statement.execute("select * from vendas_vendas where id_order = '"+id+"'");
        
        ResultSet rs = statement.getResultSet();
        int id_venda = 0;
        int id_cliente =0;
        if(rs.next()) {            
            id_venda = rs.getInt("id_venda");
            id_cliente =    rs.getInt("id_cliente");
            System.out.println("venda  : " + id_venda);            
        }
        
        Statement st = connection.createStatement();
        Statement st3 = connection.createStatement();

        boolean insertFinanceiro = st.execute("insert into financeiro_movimentacoes(id_centro_financeiro, descricao, duplicata,situacao,id_venda,discriminador,valor,id_cliente,valor_pago,data_lancamento,data_vencimento,data_pagamento, auditado) "
                + "values (338,'VENDA FEITA PELO SITE','"+id_venda+"-00000',6,"+id_venda+",'EntradaVenda',"+preco+","+id_cliente+","+preco+", '"+data+"', '"+data+"', '"+data+"',TRUE)");

        
        System.out.println("venda  : " + id_venda);
        
        boolean insertCarteira = st.execute("insert into financeiro_carteiras_movimentacoes(id_carteira, valor, id_tipo,aut,data_movimentacao,status_movimentacao_carteira,discriminador,auditada,numero_cartao,rv,numero_cheque)"
                + "values(801,"+preco+",2601,'99','"+dataMov+"',2,'Entrada',TRUE,99999-9999,'"+rv+"',"+id_venda+")");
        
        System.out.println("venda  : " + id_venda);
        Statement s= connection.createStatement();
        
        boolean findMov = s.execute("select * from financeiro_movimentacoes fm inner join financeiro_carteiras_movimentacoes fcm on fm.id_venda = fcm.numero_cheque where id_venda = "+id_venda+"");
        ResultSet rst = s.getResultSet();
        
        int id_movimentacao = 0;
        int id_movimentacao_carteira = 0;
        System.out.println("venda  : " + id_venda);
        
        if(rst.next()) {
            
            id_movimentacao = rst.getInt("id_movimentacao");
            id_movimentacao_carteira = rst.getInt("id_movimentacao_carteira");
            
        }
        System.out.println("venda  : " + id_movimentacao);
        
        Statement st2 = connection.createStatement();
        boolean rateios = st2.execute("insert into financeiro_movimentacoes_rateios (id_movimentacao,id_loja,valor) values("+id_movimentacao+",18908,100)");
        boolean insertMovCart = st2.execute("insert into movimentacoes_carteiras (id_movimentacao_carteira,id_movimentacao) values ("+id_movimentacao_carteira+", "+id_movimentacao+")");
    
        System.out.println("Finalizou entrada de dados no financeiro");
		
	}

}
