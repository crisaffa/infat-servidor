package br.com.casaamerica.Infat.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.casaamerica.Infat.model.Impressora;

@Repository
@Transactional
public class ConfiguracaoDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public ArrayList<Impressora> listarImpressoras(){
		
		ArrayList<Impressora> lista = new ArrayList<Impressora>();
		
		PrintService[] services = null;        
        services = PrintServiceLookup.lookupPrintServices(null, null);
 		for(PrintService ps : services){ 			
 			Impressora imp = new Impressora();
 			imp.setNome(ps.getName());
 			lista.add(imp); 					
 		}
 		
 		return lista;
				
	}
		
	public int verificaSelecaoImpressora() {
		
		List<Impressora> impressoras = manager.createQuery("select e from Impressora e", Impressora.class).getResultList();
			
			System.out.println("tamanho : " + impressoras.size());
			
			if(impressoras.size() <= 0 ) {
				return 0;
			} else {
				return 1;
			}		
	}
	
	public ArrayList<Impressora> retornaImpressoras(){
		
		Impressora impressora = manager.createQuery("select e from Impressora e", Impressora.class).getSingleResult();
		
		ArrayList<Impressora> lista = new ArrayList<Impressora>();
		
		PrintService[] services = null;
        services = PrintServiceLookup.lookupPrintServices(null, null);
 		for(PrintService ps : services){ 			 			
 			if(!(impressora.getNome()).equals(ps.getName())) {
 				Impressora imp = new Impressora();
 				imp.setNome(ps.getName());
 				lista.add(imp); 					
 			}
 		}
		
 		lista.add(impressora);
		
 		return lista;
			
	}
	
	public void salvarImpressora(String imp) {
		
		List<Impressora> printers = manager.createQuery("select e from Impressora e where nome ='"+imp+"'", Impressora.class).getResultList();
		
		if(printers.size() == 0 ) {
		
			List<Impressora> impressoras = manager.createQuery("select e from Impressora e", Impressora.class).getResultList();
		
			if(impressoras.size() != 0 ) {
				manager.createQuery("delete from Impressora ").executeUpdate();	
			}
		
			Impressora impressora = new Impressora();
			impressora.setEscolhido("TRUE");
			impressora.setNome(imp);
			manager.persist(impressora);
			
		} 
	}
	

}
