package br.com.casaamerica.Infat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.casaamerica.Infat.dao.CadastroProdutoDAO;
import br.com.casaamerica.Infat.dao.ConvertCharsetUTF8DAO;
import br.com.casaamerica.Infat.dao.PedidosDAO;
import br.com.casaamerica.Infat.model.CadastroProduto;

@Controller
public class ProdutoController {
	
	@Autowired
	PedidosDAO p;

	@Autowired
	CadastroProdutoDAO dao;
	
	@Autowired
	ConvertCharsetUTF8DAO c;	

	@RequestMapping("produtos/gestaoProdutos")
	public ModelAndView gestao() throws Exception {
		
		System.out.println("entrou na gestão de produtos!");
		
		List<CadastroProduto> produtos = dao.listaProdutos();
		Object totalInativo = dao.totalProdutosInativos();
		Object totalAtivo = dao.totalProdutosAtivos();
		Object totalPendente = dao.totalProdutosPendentes();
		String atualiza = p.atualiza();
		String bucardo = p.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/produtos/gestaoProdutos");
		
		modelAndView.addObject("produtos", produtos);
		modelAndView.addObject("totalInativo", totalInativo);
		modelAndView.addObject("totalAtivo", totalAtivo);
		modelAndView.addObject("totalPendente", totalPendente);
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		return modelAndView;
	}

	@RequestMapping("/produtos/detalhes/{id_produto}")
	public ModelAndView detalhesProduto(@PathVariable("id_produto") String id) throws Exception {

		System.out.println("entrou na página de detalhes do produto: "+id);
		
		dao.loadDadosVtex(id);
		
		CadastroProduto produto = dao.retornaProdutoEspecifico(id);
		String atualiza = p.atualiza();
		String bucardo = p.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/produtos/produto");
		modelAndView.addObject("produto", produto);
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		return modelAndView;
	}

	@RequestMapping("/update/{id_produto}")
	public ModelAndView updateProduto(CadastroProduto cad,	@PathVariable("id_produto") String id) {
		
		System.out.println("id: " + id);
		System.out.println("entrou no botão update: " + cad.getMarca());
		cad.setImagem("http://localhost:8080/resources/img/img_evento_placeholder.png");
		
		cad.setNome(c.converter(cad.getNome()));
		cad.setDescricao(c.converter(cad.getDescricao()));
		
		dao.updateProduto(cad);

		ModelAndView modelAndView = new ModelAndView("redirect:/produtos/detalhes/" + id + "");

		return modelAndView;
	}

}
