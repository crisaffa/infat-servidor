package br.com.casaamerica.Infat.controller;

import java.io.File;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casaamerica.Infat.dao.ClienteDAO;
import br.com.casaamerica.Infat.dao.ConvertCharsetUTF8DAO;
import br.com.casaamerica.Infat.dao.CorreiosDAO;
import br.com.casaamerica.Infat.dao.EmpresaDAO;
import br.com.casaamerica.Infat.dao.EnviaPedidoBancoDAO;
import br.com.casaamerica.Infat.dao.EtiquetaDAO;
import br.com.casaamerica.Infat.dao.FaturarDAO;
import br.com.casaamerica.Infat.dao.PedidosDAO;
import br.com.casaamerica.Infat.dao.ProdutoDAO;
import br.com.casaamerica.Infat.dao.RemovePedidoBancoDAO;
import br.com.casaamerica.Infat.model.Cliente;
import br.com.casaamerica.Infat.model.Usuario;

@Controller
public class FaturarController {
	
	@Autowired
	FaturarDAO dao;
	
	@Autowired
	ProdutoDAO produto;
	
	@Autowired
	PedidosDAO pedido;
	
	@Autowired
	EnviaPedidoBancoDAO env;
	
	@Autowired
	RemovePedidoBancoDAO remove;
	
	@Autowired
	ConvertCharsetUTF8DAO c;
	
	@Autowired
	ClienteDAO cli;
	
	@Autowired
	EmpresaDAO emp;
	
	@Autowired
	CorreiosDAO correios;
	
	@Autowired
	EtiquetaDAO etq;
	
	@RequestMapping("faturar/faturar")
	public ModelAndView faturar() {
			
		ModelAndView modelAndView = new ModelAndView("/pedidos/faturar");
		return modelAndView;
	}
	
	@RequestMapping("faturar/{id_order}")
	public ModelAndView faturar(@AuthenticationPrincipal Usuario user, Cliente cliente, @PathVariable("id_order") Integer id, @RequestParam("altura") String altura, 
			@RequestParam("largura") String largura, @RequestParam("comprimento") String comprimento, RedirectAttributes redirectAttributes) throws Exception {
		
		System.out.println("entrou para faturar a Nota Fiscal - "+id+"");
		
		atualizaDados(cliente, id);
						
		if(correios.findContaCorreiosEscolhido() == 0) {
			
			redirectAttributes.addFlashAttribute("faturando",
					"<div class=\"alert alert-danger alert-dismissible fade show m-0\" role=\"alert\">\r\n"
							+ "                              Não foi possível faturar a Nota Fiscal.\r\n"
							+ "                              É necessário primeiro configurar uma conta de Transportadora.\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");
		} else {
		
			if(emp.findCnpjEscolhido().isEmpty()) {
				redirectAttributes.addFlashAttribute("faturando",
						"<div class=\"alert alert-danger alert-dismissible fade show m-0\" role=\"alert\">\r\n"
								+ "                              Favor cadastrar os dados da Empresa antes de Faturar Nota Fiscal.\r\n"
								+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
								+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
								+ "                              </button>\r\n" + "                            </div>");			
			} else {
			
				if(dao.retornaStatusNota(id).equals("CANCELADA")) {			
					Random random = new Random();
					int num = random.nextInt(101);
					String concat = id + "" + num;
					dao.atualizaRefNota(Integer.parseInt(concat),id);
				}
			
				File fileJson = dao.gerarJson(dao.retornaRefNota(id), id, pedido.findClientes(id.toString()), emp.findCnpjEscolhido());								
		
				String status = dao.sendPost(fileJson, dao.retornaRefNota(id), id, user.getUsername());					
			
				if(status.equals("202")) {	
					
					//env.enviaPedido(id);
				
					etq.atualizaDadosCaixa(id, altura, largura, comprimento);
					
					redirectAttributes.addFlashAttribute("faturando",
							"<div class=\"alert alert-success alert-dismissible fade show m-0\" role=\"alert\">\r\n"									
									+ "                              Aguardando autorização da SEFAZ-MG sobre a Nota Fiscal.\r\n"									
									+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
									+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
									+ "                              </button>\r\n" + "                            </div>");
				
				} else if(status.contains("Erro")) {
					redirectAttributes.addFlashAttribute("faturando",
							"<div class=\"alert alert-danger alert-dismissible fade show m-0\" role=\"alert\">\r\n"
									+ "                              "+status+" \r\n"
									+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
									+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
									+ "                              </button>\r\n" + "                            </div>");
				} else {
					redirectAttributes.addFlashAttribute("faturando",
							"<div class=\"alert alert-success alert-dismissible fade show m-0\" role=\"alert\">\r\n"
									+ "                              Nota fiscal do Pedido "+id+" já foi cancelada!\r\n"
									+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
									+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
									+ "                              </button>\r\n" + "                            </div>");
				}		
					
			}
			
		 }				
		
		ModelAndView modelAndView = new ModelAndView("redirect:/pedidos/detalhes/"+id+"");
		return modelAndView;
	}
	
	public void atualizaDados(Cliente cliente, Integer id) {
		
		System.out.println("Entrou aqui para salvar o endereço");
		
		cliente.setEndereco(c.converter(cliente.getEndereco()));		
		cliente.setComplemento(c.converter(cliente.getComplemento()));
		cliente.setReference(c.converter(cliente.getReference()));
		cliente.setBairro(c.converter(cliente.getBairro()));
		cliente.setCidade(c.converter(cliente.getCidade()));		
		
		cli.updateCliente(cliente, id);
	}
		
	@RequestMapping("/baixar/{id_order}")
	public ResponseEntity<String> baixar(@PathVariable("id_order") Integer id) throws Exception {		
		
		System.out.println("baixar nota do Pedido: " + id);	
		
		String link = dao.sendGet(dao.retornaRefNota(id), id);
				
		if(!link.equals("")) {
			return new ResponseEntity<String>(link, HttpStatus.OK);		
		} else {			
			return new ResponseEntity<String>(link, HttpStatus.FORBIDDEN);	
		}				
		
	}
		
	@RequestMapping(value="cancelar/{id_order}")
	public ModelAndView cancelar(@AuthenticationPrincipal Usuario user, RedirectAttributes redirectAttributes,@PathVariable("id_order") Integer id, @RequestParam("customRadio") String param) throws Exception {
		
		System.out.println("entrei para cancelar a nota fiscal!");
		
		String justificativa = "";
		if(param.equals("1")) {
			justificativa = "Desistência do comprador";
		} else if(param.equals("2")) {
			justificativa = "Cancelamento das vendas";
		} else if(param.equals("3")) {
			justificativa = "Cálculo de preços e impostos incorretos";
		} else if(param.equals("4")) {
			justificativa = "Erros cadastrais, como no CNPJ ou na Razão Social";
		} 
			
		int status = dao.cancelarNota(dao.retornaRefNota(id), justificativa, user.getUsername());		
		
		if(status == 200) {
			redirectAttributes.addFlashAttribute("faturando",
					"<div class=\"alert alert-success alert-dismissible fade show m-0\" role=\"alert\">\r\n"
							+ "                              Nota Fiscal cancelada com sucesso.\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");
			
			remove.removePedido(id);
			dao.atualizaStatusNota(user, dao.retornaRefNota(id),id.toString());
		} 
		
		ModelAndView modelAndView = new ModelAndView("redirect:/pedidos/detalhes/"+id+"");
		return modelAndView;
				
	}
	
	@RequestMapping(value="devolver/{id_order}")
	public ModelAndView devolver(@AuthenticationPrincipal Usuario user, RedirectAttributes redirectAttributes, @PathVariable("id_order") Integer id) throws Exception {
		
		System.out.println("entrou no devolver!!!");
		
		if(emp.findCnpjEscolhido().isEmpty()) {
			redirectAttributes.addFlashAttribute("faturando",
					"<div class=\"alert alert-danger alert-dismissible fade show m-0\" role=\"alert\">\r\n"
							+ "                              Favor cadastrar os dados da Empresa antes de Gerar a Nota Fiscal de Devolução.\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");			
		} else {
			
			File fileJsonDevolucao = dao.gerarJsonDevolucao(dao.retornaRefNota(id), id, pedido.findClientes(id.toString()), emp.findCnpjEscolhido());	
			
			Random random = new Random();
			int num = random.nextInt(101);
			String concat = id + "" + num;
			dao.atualizaRefNota(Integer.parseInt(concat),id);
			
			String status = dao.sendPostDev(fileJsonDevolucao, dao.retornaRefNota(id), id, user.getUsername());
			
		if(status.equals("202")) {			
				redirectAttributes.addFlashAttribute("faturando",
						"<div class=\"alert alert-success alert-dismissible fade show m-0\" role=\"alert\">\r\n"
								+ "                              Nota de devolução gerada com sucesso.\r\n"
								+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
								+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
								+ "                              </button>\r\n" + "                            </div>");	
				
				remove.removePedido(id);
				dao.atualizaStatusDevolucao(id.toString());
				dao.atualizaStatusNota(user,dao.retornaRefNota(id),id.toString());
				
			} else if(status.contains(" ")) {
				redirectAttributes.addFlashAttribute("faturando",
						"<div class=\"alert alert-danger alert-dismissible fade show m-0\" role=\"alert\">\r\n"
								+ "                              "+status+" \r\n"
								+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
								+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
								+ "                              </button>\r\n" + "                            </div>");
				
			} else if(status.equals("422")) {
				redirectAttributes.addFlashAttribute("faturando",
						"<div class=\"alert alert-danger alert-dismissible fade show m-0\" role=\"alert\">\r\n"
								+ "                              Não foi possível gerar a nota fiscal de devolução.\r\n"
								+ "                              A nota já foi gerada e autorizada anteriormente.\r\n"
								+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
								+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
								+ "                              </button>\r\n" + "                            </div>");				
			}
			
		}		
		
		ModelAndView modelAndView = new ModelAndView("redirect:/pedidos/detalhes/"+id+"");
		return modelAndView;
		
	}
	

}
