package br.com.casaamerica.Infat.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casaamerica.Infat.dao.CadastroUsuarioDAO;
import br.com.casaamerica.Infat.dao.PedidosDAO;
import br.com.casaamerica.Infat.dao.UsuarioDAO;
import br.com.casaamerica.Infat.model.Usuario;
import br.com.casaamerica.Infat.validation.UsuarioValidation;

@Controller
public class UsuarioController {

	@Autowired
	UsuarioDAO dao;

	@Autowired
	CadastroUsuarioDAO us;
	
	@Autowired
	PedidosDAO ped;

	@RequestMapping("/usuario/usuario")
	public ModelAndView ususario() throws Exception {

		List<Usuario> usuarios = us.findUsuarios();

		ModelAndView modelAndView = new ModelAndView("/usuario/usuario");
		String atualiza = ped.atualiza();
		String bucardo = ped.bucardo();
		
		modelAndView.addObject("usuarios", usuarios);
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);

		return modelAndView;
	}

	@RequestMapping("/usuario/cadastroUsuario")
	public ModelAndView cadastro() {

		ModelAndView modelAndView = new ModelAndView("/usuario/cadastroUsuario");

		return modelAndView;
	}

	@RequestMapping("/usuario/cadastro")
	public ModelAndView cadastroUsuario(@Valid Usuario usuario, BindingResult result, RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return cadastro();
		}
		
		us.insereUsuario(usuario);

		System.out.println("entrou em cadastro : " + usuario.getUsuario());
		System.out.println("entrou em cadastro : " + usuario.getSenha());
		System.out.println("entrou em cadastro : " + usuario.getEmail());
		System.out.println("tipo de usuario : " + usuario.getTipoUsuario());

		ModelAndView modelAndView = new ModelAndView("redirect:/usuario/usuario");

		redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                              Usuário criado com sucesso! \r\n"
						+ "                                                          </div>");

		return modelAndView;
	}

	@RequestMapping("/usuario/editarUsuario/{usuario}")
	public ModelAndView editarUsuario(@PathVariable("usuario") String usuario) {

		System.out.println("entrou em editar usuario: " + usuario);

		List<Usuario> usuarios = us.findUnicoUsuario(usuario);

		ModelAndView modelAndView = new ModelAndView("usuario/editarUsuario");

		modelAndView.addObject("usuarios", usuarios);

		return modelAndView;

	}

	@RequestMapping("/usuarioEditado")
	public ModelAndView updateUsuario(@Valid Usuario usuario, BindingResult result, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return editarUsuario(usuario.getUsuario());
		}
		us.update(usuario);
		ModelAndView modelAndView = new ModelAndView("redirect:/usuario/editarUsuario/"+usuario.getUsuario()+"");
		
		redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                              Usuário "+usuario.getUsername()+" editado com sucesso! \r\n"
						+ "                                                          </div>");
		return modelAndView;
	}
	
	@RequestMapping("removerUsuario/{usuario}")
	public ModelAndView removeUsuario(@PathVariable("usuario") String usuario, RedirectAttributes redirectAttributes) {
		System.out.println("entrou em remover usuario: " +usuario);
		us.removeUsuario(usuario);
		ModelAndView modelAndView = new ModelAndView("redirect:/usuario/usuario");
		
		redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                              Usuário "+usuario+" removido com sucesso!! \r\n"
						+ "                                                          </div>");
		return modelAndView;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new UsuarioValidation());
	}

}
