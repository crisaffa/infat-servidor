package br.com.casaamerica.Infat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.casaamerica.Infat.dao.HistoricoDAO;
import br.com.casaamerica.Infat.dao.PedidosDAO;
import br.com.casaamerica.Infat.model.Historico;

@Controller
public class HistoricoController {

	@Autowired
	private HistoricoDAO h;
	
	@Autowired
	PedidosDAO dao;
	
	@RequestMapping("/historico/listahistorico")
	public ModelAndView listar() throws Exception {
				
		System.out.println("entrou na pagina de listar atividades");
		
		List<Historico> historicos = h.listarHistorico();
	
		String atualiza = dao.atualiza();
		String bucardo = dao.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/historico/listahistorico");
		
		modelAndView.addObject("historicos", historicos);
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		return modelAndView;

	}
	
	
	
	
}
