package br.com.casaamerica.Infat.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.casaamerica.Infat.dao.HomeDAO;
import br.com.casaamerica.Infat.dao.PedidosDAO;

@Controller
public class HomeController {

	@Autowired
	private HomeDAO dao;
	
	@Autowired
	private PedidosDAO ped;
	
	@RequestMapping("/")
	public ModelAndView home() throws Exception {

		ModelAndView modelAndView = new ModelAndView("home");
		
		//dao.preencherbanco();
		//dao.atualizarFinanceiro();
		//dao.somaFrete();
		
		Long vendas = dao.vendas();
		
		//anual:		
		
		Long qtdPedidosAnuais = dao.vendasAnuais();
		String receitaAnual = dao.receitaAnual();
		String ticketMedioAnual = dao.ticketMedioAnual(qtdPedidosAnuais);
		Long qtdPedidosCanceladosAno = dao.retornaQtdPedidosCanceladosAno();
		Long qtdPedidosFaturadosAno = dao.retornaQtdPedidosFaturadosAno();
		Long qtdPedidosPrepEntregaAno = dao.retornaQtdPedidosPrepEntregaAno();
		Long qtdPedidosAgPgtoAno = dao.retornaQtdPedidosAgPgtoAno();
				
		//mensal:
		
		Long qtdPedidosMensaisAtual = dao.vendasMensais();		
		String ticketMedioMensal = dao.ticketMedioMensal(qtdPedidosMensaisAtual);
		Long qtdPedidosCanceladosMesAtual = dao.retornaQtdPedidosCanceladosMesAtual();
		Long qtdPedidosFaturadosMesAtual = dao.retornaQtdPedidosFaturadosMesAtual();
		Long qtdPedidosPrepEntregaMesAtual = dao.retornaQtdPedidosPrepEntregaMesAtual();
		Long qtdPedidosAgPgtoMesAtual = dao.retornaQtdPedidosAgPgtoMesAtual();
		
		////////////grafico:		
		String receitaVetorMensalAtual = dao.receitaVetorMensalAtual();
		String receitaVetorMensalPassado = dao.receitaVetorMensalPassado();
		
		//Valor do pedido(R$):
		String receitaMensal = dao.receitaMensal();
		String receitaMesPassado = dao.receitaMensalPassado();
		String receitaHoje = dao.receitaHoje();
		String receitaSemanal = dao.receitaSemanal();
		
		
		//Quantidade de pedidos:		
		Long qtdPedidosHoje = dao.vendasHoje();
		Long qtdPedidosSemana = dao.vendasSemana();
		Long qtdPedidosMensaisPassado = dao.vendasMensaisPassado();
		
		String atualiza = ped.atualiza();
		String bucardo = ped.bucardo();
		
		modelAndView.addObject("vendas", vendas);
		modelAndView.addObject("vendasAnuais", qtdPedidosAnuais);
		
		modelAndView.addObject("ticketMedioAnual", ticketMedioAnual);
		modelAndView.addObject("ticketMedioMensal", ticketMedioMensal);
		
		modelAndView.addObject("canceladosMes", qtdPedidosCanceladosMesAtual);
		modelAndView.addObject("canceladosAno", qtdPedidosCanceladosAno);
		modelAndView.addObject("faturadosMes", qtdPedidosFaturadosMesAtual);
		modelAndView.addObject("faturadosAno", qtdPedidosFaturadosAno);		
		modelAndView.addObject("prepEntregaMes", qtdPedidosPrepEntregaMesAtual);
		modelAndView.addObject("prepEntregaAno", qtdPedidosPrepEntregaAno);		
		modelAndView.addObject("agPgtoAno", qtdPedidosAgPgtoAno);
		modelAndView.addObject("agPgtoMes", qtdPedidosAgPgtoMesAtual);
		
		modelAndView.addObject("qtdPedidosHoje", qtdPedidosHoje);
		modelAndView.addObject("qtdPedidosSemana", qtdPedidosSemana);
		modelAndView.addObject("qtdPedidosMensaisPassado", qtdPedidosMensaisPassado);
		modelAndView.addObject("qtdPedidosMensaisAtual", qtdPedidosMensaisAtual);
		
		modelAndView.addObject("receitaAnual", receitaAnual);
		modelAndView.addObject("receitaMensal", receitaMensal);
		modelAndView.addObject("receitaMesPassado", receitaMesPassado);
		modelAndView.addObject("receitaHoje", receitaHoje);
		modelAndView.addObject("receitaSemanal", receitaSemanal);
		
		modelAndView.addObject("receitaVetorMensalAtual", receitaVetorMensalAtual);
		modelAndView.addObject("receitaVetorMensalPassado", receitaVetorMensalPassado);
				
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		
		return modelAndView;
	}

}
