package br.com.casaamerica.Infat.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casaamerica.Infat.dao.ConfiguracaoDAO;
import br.com.casaamerica.Infat.model.Impressora;

@Controller
public class ConfiguracaoController {
	
	@Autowired
	ConfiguracaoDAO conf;
	
	@RequestMapping("/configuracao")
	public ModelAndView configuracao() {
		
		ArrayList<Impressora> impressoras = new ArrayList<Impressora>();
		
		if(conf.verificaSelecaoImpressora() == 0) {
			impressoras = conf.listarImpressoras();
		} else {
			impressoras = (ArrayList<Impressora>) conf.retornaImpressoras();
		}
		
		ModelAndView modelAndView = new ModelAndView("/configuracao");
		
		modelAndView.addObject("impressoras", impressoras);
		
		return modelAndView;
	}
		
	@RequestMapping("configurar/{nome}")
	public ModelAndView configurar(RedirectAttributes redirectAttributes, @PathVariable("nome") String nome) throws Exception {		
		
		System.out.println("Salvar nome da Impressora: " + nome);
		
		if(nome.equals("Selecionar")) {
			redirectAttributes.addFlashAttribute("salvando",
					"<div class=\"alert alert-danger alert-dismissible fade show m-0\" role=\"alert\">\r\n"
							+ "                              Por favor selecione uma impressora antes de salvar.\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");
		} else {
		
			conf.salvarImpressora(nome);
		
			redirectAttributes.addFlashAttribute("salvando",
				"<div class=\"alert alert-success alert-dismissible fade show m-0\" role=\"alert\">\r\n"
						+ "                              Impressora salva com sucesso.\r\n"
						+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
						+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
						+ "                              </button>\r\n" + "                            </div>");
		
		}
		
		ModelAndView modelAndView = new ModelAndView("redirect:/configuracao");
		
		return modelAndView;
		
	}
	
	

}
