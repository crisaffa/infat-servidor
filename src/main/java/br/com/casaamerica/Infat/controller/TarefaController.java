package br.com.casaamerica.Infat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casaamerica.Infat.dao.PedidosDAO;
import br.com.casaamerica.Infat.dao.TarefaDAO;
import br.com.casaamerica.Infat.model.Tarefa;
import br.com.casaamerica.Infat.validation.TarefaValidation;

@Controller
public class TarefaController {

	@Autowired
	TarefaDAO dao;
	
	@Autowired
	PedidosDAO ped;
	
	@RequestMapping("tarefa/ativar/{id}")
	public ModelAndView ativarTarefa (@PathVariable("id") int id, RedirectAttributes redirectAttributes) throws Exception{
		
		ModelAndView modelAndView = new ModelAndView("redirect:/tarefa/agendamentoTarefa");		
		
		if(dao.validarEmail(id)) {
			
			String nome = dao.ativar(id);
			String n = nome.substring(3, nome.length());

			if(nome.contains("sim")) {				
				redirectAttributes.addFlashAttribute("sucesso",
						"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
								+ "                              Tarefa "+n+" foi ativada.\r\n"
		                        + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"
								+ "                                                          </div>");
			} else {
				redirectAttributes.addFlashAttribute("sucesso",
						"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
								+ "                              Tarefa "+n+" foi desativada.\r\n"
		                        + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"
								+ "                                                          </div>");
			}
							
		} else {
			redirectAttributes.addFlashAttribute("sucesso",
					"<div class=\"alert alert-danger alert-dismissible fade show m-0\" role=\"alert\">\r\n"
							+ "                              Não foi possível ativar a tarefa!\r\n"
							+ "                              É necessário cadastrar pelo menos 1 conta de e-mail.\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");
		}
		
		return modelAndView;
		
	}
	
	@RequestMapping("tarefa/agendamentoTarefa")
	public ModelAndView tarefas() throws Exception {
		
		List<Tarefa> tarefas = dao.listarTarefas();
		
		String atualiza = ped.atualiza();
		String bucardo = ped.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/tarefa/agendamentoTarefa");
				
		modelAndView.addObject("tarefas", tarefas);		
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		return modelAndView;
	}
	
	
	@RequestMapping("/updateTarefa")
	public ModelAndView updateTarefa (@RequestParam("emailsDest") String emails, @RequestParam("emailsCc") String emailsCc, @RequestParam("idTarefa") int idTarefa,
			RedirectAttributes redirectAttributes) throws Exception{
		
		ModelAndView modelAndView = new ModelAndView("redirect:/tarefa/agendamentoTarefa");
		
		if(emails.equals("") || emails.equals("")) {
			redirectAttributes.addFlashAttribute("sucesso",
					"<div class=\"alert alert-danger alert-dismissible fade show m-0\" role=\"alert\">\r\n"
							+ "                              Não foi possível salvar a tarefa!\r\n"
							+ "                              É necessário cadastrar pelo menos 1 conta de e-mail.\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");
		} else {
			dao.updateTarefa(idTarefa,emails,emailsCc);
			
			redirectAttributes.addFlashAttribute("sucesso",
					"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
							+ "                              Dados salvos com sucesso \r\n"
							+ "                                                          </div>");				
		}
				
		return modelAndView;
		
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    binder.addValidators(new TarefaValidation());
	}
	
	
}
