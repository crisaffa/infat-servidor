package br.com.casaamerica.Infat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casaamerica.Infat.dao.ClienteDAO;
import br.com.casaamerica.Infat.dao.CorreiosDAO;
import br.com.casaamerica.Infat.dao.EmpresaDAO;
import br.com.casaamerica.Infat.dao.EtiquetaDAO;
import br.com.casaamerica.Infat.dao.FaturarDAO;
import br.com.casaamerica.Infat.dao.PedidosDAO;
import br.com.casaamerica.Infat.model.Pedido;

@Controller
public class EtiquetasController {
		
	@Autowired
	PedidosDAO dao;
	
	@Autowired
	EtiquetaDAO etq;
	
	@Autowired
	FaturarDAO fat;
	
	@Autowired
	ClienteDAO c;
	
	@Autowired
	EmpresaDAO emp;
	
	@Autowired
	CorreiosDAO correios;
	
	final String servidor = "35.198.33.138";
	
	@RequestMapping("/etiquetas/listarEtiquetas")
	public ModelAndView listar() throws Exception {
				
		System.out.println("entrou na pagina de listar etiquetas");
		
		List<Pedido> pedidos = dao.listarPedidosEmitidos();
	
		String atualiza = dao.atualiza();
		String bucardo = dao.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/etiquetas/listarEtiquetas");
		
		modelAndView.addObject("pedidos", pedidos);
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		return modelAndView;

	}
	
	@RequestMapping("etiquetas/imprimir/{id_order}")
	public ModelAndView imprimir(RedirectAttributes redirectAttributes ,@PathVariable("id_order") String id) throws Exception {
		
		System.out.println("entrou para imprimir etiqueta ZEBRA: "+id);
		
		etq.montarUnidades();
		
		etq.gerarTxtEtiqueta(dao.findClientes(id), emp.findCnpjEscolhido(), fat.retornaNumeroNota(id), id, dao.retornarCodigoRastreio(id), etq.retornarPesoPedido(id));		
		
		redirectAttributes.addFlashAttribute("gerando",
				"<div class=\"alert alert-success alert-dismissible fade show m-0\" role=\"alert\">\r\n"
						+ "                              Etiqueta impressa com sucesso.\r\n"
						+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
						+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
						+ "                              </button>\r\n" + "                            </div>");
		
		ModelAndView modelAndView = new ModelAndView("redirect:/etiquetas/listarEtiquetas");
		return modelAndView;
		
	}
	
	@RequestMapping("etiquetas/salvar/{id_order}")
	public ResponseEntity<String> salvar(@PathVariable("id_order") String id) throws Exception {
		
		System.out.println("entrou para salvar em PDF a etiqueta: "+id);
		
		etq.geraEtiquetaPdf(dao.findClientes(id), emp.findCnpjEscolhido(), fat.retornaNumeroNota(id), id, dao.retornarCodigoRastreio(id), etq.retornarPesoPedido(id));

		String arqPdf = "http://"+servidor+"/correios/"+id+".pdf";
		
		return new ResponseEntity<String>(arqPdf, HttpStatus.OK);
	}
	
	
	
	
	

}
