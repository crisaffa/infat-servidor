package br.com.casaamerica.Infat.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casaamerica.Infat.dao.CorreiosDAO;
import br.com.casaamerica.Infat.dao.PedidosDAO;
import br.com.casaamerica.Infat.model.Correios;
import br.com.casaamerica.Infat.validation.CorreiosValidation;

@Controller
public class CorreiosController {
	
	@Autowired
	CorreiosDAO dao;
	
	@Autowired
	PedidosDAO ped;
	
	@RequestMapping("/correios/config")
	public ModelAndView config() throws Exception {
		
		System.out.println("entrou na página de configurações dos correios");

		String atualiza = ped.atualiza();
		
		ModelAndView modelAndView = new ModelAndView("/correios/config");
		modelAndView.addObject("atualiza", atualiza);
			
		return modelAndView;
		
	}
	
	@RequestMapping("/correios/configuracoes")
	public ModelAndView correio(@Valid Correios correio, BindingResult result, RedirectAttributes redirectAttributes) throws Exception {

		if(result.hasErrors()) {
			
		     return config();
		}
		
		String resposta = dao.configuracao(correio);
		
		ModelAndView modelAndView = new ModelAndView("redirect:/correios/contas");
		
		if(resposta.equals("SIM")) {
			
			redirectAttributes.addFlashAttribute("sucesso",
					"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
							+ "                 CNPJ já cadastrado!\r\n"
							+ "                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                    <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                  </button>\r\n" + "                </div>");
			
		}else {
			redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                  Dados salvos com sucesso!\r\n"
						+ "                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
						+ "                    <span aria-hidden=\"true\">&times;</span>\r\n"
						+ "                  </button>\r\n" + "                </div>");
		}	
		
		return modelAndView;
	}
	
	@RequestMapping("/correios/contas")
	public ModelAndView findContaCorreios() throws Exception {
		
		List<Correios> correios = dao.findContasCorreios();
		List<Correios> contaCorreiosEscolhido = dao.findContaCorreioEscolhido();
		String atualiza = ped.atualiza();
		String bucardo = ped.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/correios/contas");
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		modelAndView.addObject("correios", correios);
		modelAndView.addObject("contaCorreiosEscolhido", contaCorreiosEscolhido);
		
		return modelAndView;
		
	}
		
	@RequestMapping("/correios/cadastroCnpj/{cnpj}")
	public ModelAndView cadastroCnpj (@PathVariable("cnpj") String cnpj, RedirectAttributes redirectAttributes) throws Exception{
		
		String c  = dao.escolhido(cnpj);
		String transportadora = c.substring(3, c.length());		
		
		ModelAndView modelAndView = new ModelAndView("redirect:/correios/contas");
		
		if(c.contains("nao")) {
			redirectAttributes.addFlashAttribute("sucesso",
					"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
							+ "                              Transportadora "+transportadora+" foi inativada!\r\n"
	                        + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"
							+ "                                                          </div>");
		
		} else {		
			redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                              Transportadora "+transportadora+" foi ativada!\r\n"
                        + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"
						+ "                                                          </div>");
		}
	
		return modelAndView;
		
	}
		
	@RequestMapping("/correios/removerCnpj/{cnpj}")
	public ModelAndView removerCnpj (@PathVariable("cnpj") String cnpj, RedirectAttributes redirectAttributes) throws Exception{
		
		String c  = dao.removerCnpj(cnpj);
		
		ModelAndView modelAndView = new ModelAndView("redirect:/correios/contas");
		
		redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                              Transportadora removida com sucesso : "+c+"\r\n"
						+ "                                                          </div>");
		
		return modelAndView;
		
	}
	
	@RequestMapping("/updateCorreios")
	public ModelAndView atualizarContaCorreios (@Valid Correios correio, BindingResult result, RedirectAttributes redirectAttributes) throws Exception{
		
		if(result.hasErrors()) {
			
		     return findContaCorreios();
		}		
		
		dao.updateCorreios(correio);
				
		ModelAndView modelAndView = new ModelAndView("redirect:/correios/contas");
				
		redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                              Dados salvos com sucesso \r\n"
						+ "                                                          </div>");
				
		return modelAndView;
		
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    binder.addValidators(new CorreiosValidation());
	}
}
