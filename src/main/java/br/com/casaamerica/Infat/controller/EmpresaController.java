package br.com.casaamerica.Infat.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casaamerica.Infat.dao.ConvertCharsetUTF8DAO;
import br.com.casaamerica.Infat.dao.EmpresaDAO;
import br.com.casaamerica.Infat.dao.PedidosDAO;
import br.com.casaamerica.Infat.model.Empresa;
import br.com.casaamerica.Infat.validation.EmpresaValidation;

@Controller
public class EmpresaController {
	
	@Autowired
	EmpresaDAO dao;
	
	@Autowired
	ConvertCharsetUTF8DAO c;
	
	@Autowired
	PedidosDAO ped;
	
	@RequestMapping("/empresa/configuracao")
	public ModelAndView config() throws Exception {

		System.out.println("entrou na página de configurações da empresa");
		
		String atualiza = ped.atualiza();
		String bucardo = ped.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/empresa/configuracao");
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		return modelAndView;
	}
	
	@RequestMapping("empresa/configuracoes")
	public ModelAndView empresa(@Valid Empresa empresa, BindingResult result, RedirectAttributes redirectAttributes) throws Exception {

		if(result.hasErrors()) {
			
		     return config();
		}
		
		empresa.setNome(c.converter(empresa.getNome()));
		empresa.setNomeFantasia(c.converter(empresa.getNomeFantasia()));
		empresa.setLogradouro(c.converter(empresa.getLogradouro()));
		empresa.setBairro(c.converter(empresa.getBairro()));
		empresa.setMunicipio(c.converter(empresa.getMunicipio()));
		
		String resposta = dao.configuracao(empresa);
		
		ModelAndView modelAndView = new ModelAndView("redirect:/empresa/cnpj");
		
		if(resposta.equals("SIM")) {
			
			redirectAttributes.addFlashAttribute("sucesso",
					"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
							+ "                 CNPJ já cadastrado!\r\n"
							+ "                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                    <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                  </button>\r\n" + "                </div>");
			
		}else {
			redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                  Dados salvos com sucesso!\r\n"
						+ "                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
						+ "                    <span aria-hidden=\"true\">&times;</span>\r\n"
						+ "                  </button>\r\n" + "                </div>");
		}
		return modelAndView;
	}
	
	@RequestMapping("/empresa/cnpj")
	public ModelAndView findCnpj() throws Exception {
		
		List<Empresa> empresa = dao.findCnpj();
		List<Empresa> cnpjEscolhido = dao.findCnpjEscolhido();
		String atualiza = ped.atualiza();
		String bucardo = ped.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/empresa/cnpj");
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		modelAndView.addObject("empresa", empresa);
		modelAndView.addObject("cnpjEscolhido", cnpjEscolhido);
		
		return modelAndView;
		
	}
	
	@RequestMapping("/cadastroCnpj/{cnpj}")
	public ModelAndView cadastroCnpj (@PathVariable("cnpj") String cnpj, RedirectAttributes redirectAttributes) throws Exception{
		
		System.out.println("cnpj: "+cnpj);
		
		String c  = dao.escolhido(cnpj);
		String empresa = c.substring(3, c.length());	
		ModelAndView modelAndView = new ModelAndView("redirect:/empresa/cnpj");		
		
		if(c.contains("nao")) {
			redirectAttributes.addFlashAttribute("sucesso",
					"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
							+ "                              CNPJ "+empresa+" foi inativado!\r\n"
	                        + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"
							+ "                                                          </div>");	
		} else {
			redirectAttributes.addFlashAttribute("sucesso",
					"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
							+ "                              CNPJ "+empresa+" foi ativado!\r\n"
	                        + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>"
							+ "                                                          </div>");	
		}
		
		return modelAndView;
		
	}
	

	@RequestMapping("/removerCnpj/{cnpj}")
	public ModelAndView removerCnpj (@PathVariable("cnpj") String cnpj, RedirectAttributes redirectAttributes) throws Exception{		
		
		String c  = dao.removerCnpj(cnpj);
		
		ModelAndView modelAndView = new ModelAndView("redirect:/empresa/cnpj");		
		
		redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                              CNPJ removido com sucesso : "+c+"\r\n"
						+ "                                                          </div>");
		
		return modelAndView;
		
	}
	
	@RequestMapping("/updateEmpresa")
	public ModelAndView updateEmpresa (@Valid Empresa empresa, BindingResult result, RedirectAttributes redirectAttributes) throws Exception{
		
		if(result.hasErrors()) {
			
		     return findCnpj();
		}		
		
		empresa.setNome(c.converter(empresa.getNome()));
		empresa.setNomeFantasia(c.converter(empresa.getNomeFantasia()));
		empresa.setLogradouro(c.converter(empresa.getLogradouro()));
		empresa.setBairro(c.converter(empresa.getBairro()));
		empresa.setMunicipio(c.converter(empresa.getMunicipio()));
		
		dao.updateEmpresa(empresa);
		
		ModelAndView modelAndView = new ModelAndView("redirect:/empresa/cnpj");
				
		redirectAttributes.addFlashAttribute("sucesso",
				"<div class=\"alert alert-success alert-dismissible fade show\" role=\"alert\">\r\n"
						+ "                              Dados salvos com sucesso \r\n"
						+ "                                                          </div>");
				
		return modelAndView;
		
	}
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    binder.addValidators(new EmpresaValidation());
	}
}
