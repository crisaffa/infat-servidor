package br.com.casaamerica.Infat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.casaamerica.Infat.dao.ClienteDAO;
import br.com.casaamerica.Infat.dao.EmpresaDAO;
import br.com.casaamerica.Infat.dao.FaturarDAO;
import br.com.casaamerica.Infat.dao.PedidosDAO;
import br.com.casaamerica.Infat.dao.ProdutoDAO;
import br.com.casaamerica.Infat.dao.UsuarioDAO;
import br.com.casaamerica.Infat.list.ListaTotal;
import br.com.casaamerica.Infat.model.Cliente;
import br.com.casaamerica.Infat.model.Empresa;
import br.com.casaamerica.Infat.model.Pedido;
import br.com.casaamerica.Infat.model.Produto;
import br.com.casaamerica.Infat.model.Usuario;

@Controller
public class PedidosController {

	@Autowired
	PedidosDAO dao;

	@Autowired
	ProdutoDAO prod;
	
	@Autowired
	FaturarDAO fat;

	@Autowired
	ClienteDAO c;
	
	@Autowired
	EmpresaDAO emp;
	
	@Autowired
	UsuarioDAO user;

	@RequestMapping("/pedidos/pedidos")
	public ModelAndView pedidos() throws Exception {
		
		ModelAndView modelAndView = new ModelAndView("pedidos/pedidos");
		return modelAndView;
	}

	@RequestMapping("pedidos/detalhes/{id_order}")
	public ModelAndView detalhes(@AuthenticationPrincipal Usuario user, @PathVariable("id_order") String id) throws Exception {

		System.out.println("página de detalhes do pedido : " + id);
		
		List<Pedido> pedidos = dao.findPedido(id);
		c.insertCliente(pedidos);
		prod.selectProduto(pedidos);
		
		//atualiza status da nota:
		fat.atualizaStatusNota(user, fat.retornaRefNota(Integer.parseInt(id)), id);
		
		List<Pedido> pedidosAtual = dao.findPedido(id);		
		List<Cliente> cliente = dao.findClientes(id);
		List<Produto> produtos = prod.findProduto(id);
		List<ListaTotal> total = prod.total(id);
		int quantidade = prod.quantidadeProdutos(id);
		String atualiza = dao.atualiza();
		String bucardo = dao.bucardo();
		List<Empresa> empresas = emp.findCnpjEscolhido();
		
		ModelAndView modelAndView = new ModelAndView("/pedidos/pedidos");		
		modelAndView.addObject("cliente", cliente);
		modelAndView.addObject("pedidos", pedidosAtual);
		modelAndView.addObject("produtos", produtos);
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		modelAndView.addObject("total", total);
		modelAndView.addObject("quantidade", quantidade);
		modelAndView.addObject("empresas",empresas);
			
		return modelAndView;
		
	}
	
	@RequestMapping("pedidos/gestaoPedidos")
	public ModelAndView gestao() throws Exception {

		List<Pedido> pedidos = dao.listaPedidos();
		String atualiza = dao.atualiza();
		String bucardo = dao.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/pedidos/gestaoPedidos");
		
		modelAndView.addObject("pedidos", pedidos);
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		return modelAndView;

	}
	
	@RequestMapping("/pedidos/separarPedidos")
	public ModelAndView separar() throws Exception {
				
		System.out.println("entrou na pagina de separar pedidos");
		
		List<Produto> produtos = dao.separarPedidos();
		String atualiza = dao.atualiza();
		String bucardo = dao.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/pedidos/separarPedidos");
		
		modelAndView.addObject("produtos", produtos);
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		return modelAndView;

	}
	
	
	@RequestMapping("/pedidos/relatorioPedidos")
	public ModelAndView relatorio() throws Exception {
				
		String atualiza = dao.atualiza();
		String bucardo = dao.bucardo();
		
		ModelAndView modelAndView = new ModelAndView("/pedidos/relatorioPedidos");
		
		modelAndView.addObject("atualiza", atualiza);
		modelAndView.addObject("bucardo", bucardo);
		
		return modelAndView;
	}	
	
	@RequestMapping("pedidos/cancelar/{id_order}/{orderId}/{status}")
	public ModelAndView cancelar(@AuthenticationPrincipal Usuario user, RedirectAttributes redirectAttributes, @PathVariable("id_order") String id, @PathVariable("orderId") String orderid,
			@PathVariable("status") String status, @RequestParam("textAreaMotivoCancelamento") String param) throws Exception {
		
		System.out.println("entrou para cancelar o pedido: "+id);
				
		int st = dao.cancelaPedido(id, orderid, status, param, user.getUsername());
		
		if(st == 200) {
			redirectAttributes.addFlashAttribute("faturando",
				"<div class=\"alert alert-success alert-dismissible fade show m-0\" role=\"alert\">\r\n"
						+ "                              Pedido cancelado com sucesso.\r\n"
						+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
						+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
						+ "                              </button>\r\n" + "                            </div>");
		}		
		
		ModelAndView modelAndView = new ModelAndView("redirect:/pedidos/detalhes/"+id+"");
		return modelAndView;
	}
	
	
	@RequestMapping("pedidos/exportar/{id_order}")
	public ModelAndView exportar(@PathVariable("id_order") String id) throws Exception {
		
		System.out.println("entrou aqui para exportar o pedido!");
		
		System.out.println("id_order: "+id);
		
		ModelAndView modelAndView = new ModelAndView("redirect:/pedidos/detalhes/"+id+"");
		return modelAndView;
		
	}
	
	@RequestMapping("relatorio/marketplaces")
	public ModelAndView gerarEnviarRelatorioMarket(@AuthenticationPrincipal Usuario user, RedirectAttributes redirectAttributes, @RequestParam("customRadio") String param, 
			@RequestParam("dataInicial") String dataInicial, @RequestParam("dataFinal") String dataFinal)throws Exception{
		
	  System.out.println("entrou na pagina de relatório de pedidos por marketplaces");
		
	  if(dataInicial.equals("") || dataFinal.equals("")) {
			redirectAttributes.addFlashAttribute("gerando",
					"<div class=\"alert alert-danger alert-dismissible fade show \" role=\"alert\">\r\n"
							+ "                              ERRO: Datas inválidas\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");
	  } else {
		
		
		String diaInicial = dataInicial.substring(8,10);
		String mesInicial = dataInicial.substring(5,7);
		String anoInicial = dataInicial.substring(0,4);
		
		String diaFinal = dataFinal.substring(8,10);
		String mesFinal = dataFinal.substring(5,7);
		String anoFinal = dataFinal.substring(0,4);
		
		if(Integer.parseInt(anoFinal) < Integer.parseInt(anoInicial)) {
			redirectAttributes.addFlashAttribute("gerando",
					"<div class=\"alert alert-danger alert-dismissible fade show \" role=\"alert\">\r\n"
							+ "                              ERRO: Ano da Data Final menor que o ano da Data Inicial\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");
		} else {
			if(Integer.parseInt(anoFinal) == Integer.parseInt(anoInicial)) {
				if(Integer.parseInt(mesFinal) < Integer.parseInt(mesInicial)) {
					redirectAttributes.addFlashAttribute("gerando",
							"<div class=\"alert alert-danger alert-dismissible fade show \" role=\"alert\">\r\n"
									+ "                              ERRO: Mês da Data Final menor que o mês da Data Inicial\r\n"
									+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
									+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
									+ "                              </button>\r\n" + "                            </div>");				
				} else {
					if(Integer.parseInt(mesFinal) == Integer.parseInt(mesInicial)) {
						if(Integer.parseInt(diaFinal) < Integer.parseInt(diaInicial)){
							redirectAttributes.addFlashAttribute("gerando",
									"<div class=\"alert alert-danger alert-dismissible fade show \" role=\"alert\">\r\n"
											+ "                              ERRO: Dia da Data Final menor que o Dia da Data Inicial\r\n"
											+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
											+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
											+ "                              </button>\r\n" + "                            </div>");												
						} else {
							
							String data1 = ""+diaInicial+"/"+mesInicial+"/"+anoInicial+"";
							String data2 = ""+diaFinal+"/"+mesFinal+"/"+anoFinal+""; 
							
							dao.gerarRelatorioPedidosMarket(data1, data2, param, user.getEmail());
							String atualiza = dao.atualiza();
							String bucardo = dao.bucardo();
							
							redirectAttributes.addFlashAttribute("gerando",
									"<div class=\"alert alert-success alert-dismissible fade show \" role=\"alert\">\r\n"
											+ "                              Relatório enviado por e-mail com sucesso!\r\n"
											+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
											+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
											+ "                              </button>\r\n" + "                            </div>");
						}
					} else {
						String data1 = ""+diaInicial+"/"+mesInicial+"/"+anoInicial+"";
						String data2 = ""+diaFinal+"/"+mesFinal+"/"+anoFinal+""; 
						
						dao.gerarRelatorioPedidosMarket(data1, data2, param, user.getEmail());
						String atualiza = dao.atualiza();
						String bucardo = dao.bucardo();
						
						redirectAttributes.addFlashAttribute("gerando",
								"<div class=\"alert alert-success alert-dismissible fade show \" role=\"alert\">\r\n"
										+ "                              Relatório enviado por e-mail com sucesso!\r\n"
										+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
										+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
										+ "                              </button>\r\n" + "                            </div>");												
					}
				}
			} else {
				String data1 = ""+diaInicial+"/"+mesInicial+"/"+anoInicial+"";
				String data2 = ""+diaFinal+"/"+mesFinal+"/"+anoFinal+""; 
				
				dao.gerarRelatorioPedidosMarket(data1, data2, param, user.getEmail());
				String atualiza = dao.atualiza();
				String bucardo = dao.bucardo();
				
				redirectAttributes.addFlashAttribute("gerando",
						"<div class=\"alert alert-success alert-dismissible fade show \" role=\"alert\">\r\n"
								+ "                              Relatório enviado por e-mail com sucesso!\r\n"
								+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
								+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
								+ "                              </button>\r\n" + "                            </div>");
			}			
		}			
	  }
		ModelAndView modelAndView = new ModelAndView("redirect:/pedidos/relatorioPedidos");
		
		return modelAndView;
		
	}
	
	
	@RequestMapping("relatorio/tipoPagamento")
	public ModelAndView gerarEnviarRelatorioPagamento(@AuthenticationPrincipal Usuario user, RedirectAttributes redirectAttributes, @RequestParam("customRadio") String param, 
			@RequestParam("dataInicial") String dataInicial, @RequestParam("dataFinal") String dataFinal)throws Exception{
		
		System.out.println("entrou na pagina de relatório de pedidos por tipo de Pagamento");
		
		if(dataInicial.equals("") || dataFinal.equals("")) {
			redirectAttributes.addFlashAttribute("gerando",
					"<div class=\"alert alert-danger alert-dismissible fade show \" role=\"alert\">\r\n"
							+ "                              ERRO: Datas inválidas\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");
	  } else {
		
		String diaInicial = dataInicial.substring(8,10);
		String mesInicial = dataInicial.substring(5,7);
		String anoInicial = dataInicial.substring(0,4);
		
		String diaFinal = dataFinal.substring(8,10);
		String mesFinal = dataFinal.substring(5,7);
		String anoFinal = dataFinal.substring(0,4);
		
		if(Integer.parseInt(anoFinal) < Integer.parseInt(anoInicial)) {
			redirectAttributes.addFlashAttribute("gerando",
					"<div class=\"alert alert-danger alert-dismissible fade show \" role=\"alert\">\r\n"
							+ "                              ERRO: Ano da Data Final menor que o ano da Data Inicial\r\n"
							+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
							+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
							+ "                              </button>\r\n" + "                            </div>");
		} else {
			if(Integer.parseInt(anoFinal) == Integer.parseInt(anoInicial)) {
				if(Integer.parseInt(mesFinal) < Integer.parseInt(mesInicial)) {
					redirectAttributes.addFlashAttribute("gerando",
							"<div class=\"alert alert-danger alert-dismissible fade show \" role=\"alert\">\r\n"
									+ "                              ERRO: Mês da Data Final menor que o mês da Data Inicial\r\n"
									+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
									+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
									+ "                              </button>\r\n" + "                            </div>");				
				} else {
					if(Integer.parseInt(mesFinal) == Integer.parseInt(mesInicial)) {
						if(Integer.parseInt(diaFinal) < Integer.parseInt(diaInicial)){
							redirectAttributes.addFlashAttribute("gerando",
									"<div class=\"alert alert-danger alert-dismissible fade show \" role=\"alert\">\r\n"
											+ "                              ERRO: Dia da Data Final menor que o Dia da Data Inicial\r\n"
											+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
											+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
											+ "                              </button>\r\n" + "                            </div>");												
						} else {
							
							String data1 = ""+diaInicial+"/"+mesInicial+"/"+anoInicial+"";
							String data2 = ""+diaFinal+"/"+mesFinal+"/"+anoFinal+""; 
							
							dao.gerarRelatorioPedidosTipoPgto(data1, data2, param, user.getEmail());
							String atualiza = dao.atualiza();
							String bucardo = dao.bucardo();
							
							redirectAttributes.addFlashAttribute("gerando",
									"<div class=\"alert alert-success alert-dismissible fade show \" role=\"alert\">\r\n"
											+ "                              Relatório enviado por e-mail com sucesso!\r\n"
											+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
											+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
											+ "                              </button>\r\n" + "                            </div>");
						}
					} else {
						String data1 = ""+diaInicial+"/"+mesInicial+"/"+anoInicial+"";
						String data2 = ""+diaFinal+"/"+mesFinal+"/"+anoFinal+""; 
						
						dao.gerarRelatorioPedidosTipoPgto(data1, data2, param, user.getEmail());
						String atualiza = dao.atualiza();
						String bucardo = dao.bucardo();
						
						redirectAttributes.addFlashAttribute("gerando",
								"<div class=\"alert alert-success alert-dismissible fade show \" role=\"alert\">\r\n"
										+ "                              Relatório enviado por e-mail com sucesso!\r\n"
										+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
										+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
										+ "                              </button>\r\n" + "                            </div>");												
					}
				}
			} else {
				String data1 = ""+diaInicial+"/"+mesInicial+"/"+anoInicial+"";
				String data2 = ""+diaFinal+"/"+mesFinal+"/"+anoFinal+""; 
				
				dao.gerarRelatorioPedidosTipoPgto(data1, data2, param, user.getEmail());
				String atualiza = dao.atualiza();
				String bucardo = dao.bucardo();
				
				redirectAttributes.addFlashAttribute("gerando",
						"<div class=\"alert alert-success alert-dismissible fade show \" role=\"alert\">\r\n"
								+ "                              Relatório enviado por e-mail com sucesso!\r\n"
								+ "                              <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">\r\n"
								+ "                                <span aria-hidden=\"true\">&times;</span>\r\n"
								+ "                              </button>\r\n" + "                            </div>");
			}			
		}		
	  }
		ModelAndView modelAndView = new ModelAndView("redirect:/pedidos/relatorioPedidos");
		
		return modelAndView;
	}
	
	
}