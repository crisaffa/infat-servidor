package br.com.casaamerica.Infat.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import br.com.casaamerica.Infat.dao.UsuarioDAO;



@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	@Autowired
	UsuarioDAO usuarioDAO;	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {		
		http.csrf().disable();
	    http.authorizeRequests()	    
	    .antMatchers("/resources/**").permitAll()
	    .antMatchers("/home").permitAll()
	    .antMatchers("/usuario/**").hasRole("ADMIN")
	    .anyRequest().authenticated()
	    .and().formLogin().loginPage("/login").permitAll()
	    .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).permitAll().logoutSuccessUrl("/login");
	}
	
	 @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	        auth.userDetailsService(usuarioDAO)
	        .passwordEncoder(new BCryptPasswordEncoder());
	    }	 

}
