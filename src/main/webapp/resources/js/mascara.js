var behavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
options = {
    onKeyPress: function (val, e, field, options) {
        field.mask(behavior.apply({}, arguments), options);
    }
};

$('.phone').mask(behavior, options);
$('.cnpj').mask('00.000.000/0000-00', {reverse: true});
$('.cep').mask('00000-000');

$(function($){
   $(".dataPedido").mask("99/99/9999");
});