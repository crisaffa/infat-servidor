/* abre o menu dropdown com hover quando o menu sidebar está no modo fechado */
$('ul.sidebar.navbar-nav.toggled').on('mouseenter mouseleave','.dropdown',function(e){
  var _d=$(e.target).closest('.dropdown');_d.addClass('show');
  setTimeout(function(){
    _d[_d.is(':hover')?'addClass':'removeClass']('show');
    $('[data-toggle="dropdown"]', _d).attr('aria-expanded',_d.is(':hover'));
  },300);
});

/*previne fechar o dropdown da sidebar quando clica em um sub-item*/
$('.sidebar .dropdown-menu').on({
	"click":function(e){
      e.stopPropagation();
    }
});

/*previne fechar o dropdown da sidebar quando clica em qualquer lugar da página*/
//$(document).on('click', ':not(.do-not-close)', function(e) {
//  if ($(this).closest('.do-not-close').length == 0) {
//      //
//      // if on doc and not on a dropdown....
//      //
//      e.stopPropagation();
//  }
//});