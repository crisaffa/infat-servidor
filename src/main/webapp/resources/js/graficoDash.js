var ctx = document.getElementById("graficoDash").getContext('2d');

function graficoGeral(receitaVetorMensalAtual, receitaVetorMensalPassado){
    var graficoDash = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"], //dias do mes
            datasets: [{
                label: 'Mês Atual',
                data: receitaVetorMensalAtual, // valores para cada dia do mes
                fill: true,
                borderColor: '#419480', // Add custom color border (Line)
                backgroundColor: 'rgba(135,212,193,.4)', // Add custom color background (Points and Fill)
                borderWidth: 1 // Specify bar border width
            },
            {
                label: 'Mês Passado',
                data: receitaVetorMensalPassado, // valores para cada dia do mes
                fill: true,
                borderColor: '#717171', // Add custom color border (Line)
                backgroundColor: '#D9D9D9', // Add custom color background (Points and Fill)
                borderWidth: 1 // Specify bar border width
            }]
        },
        options: {
          responsive: true, // Instruct chart js to respond nicely.
          maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
          tooltips: {
            mode: 'index'
          }
        }
    });
}
