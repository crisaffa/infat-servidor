<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c"%> <%@ taglib uri="http://www.springframework.org/tags/form"
prefix="form"%> <%@ taglib uri="http://www.springframework.org/tags"
prefix="s"%>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="charset=UTF-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <c:url value="/resources/img" var="imgPath" />
    <c:url value="/resources/css" var="cssPath" />
    <c:url value="/resources/js" var="jsPath" />
    <c:url value="/resources/vendor" var="vendorPath" />

    <link rel="icon" href="${imgPath}/favicon.ico" />
    <link
      rel="apple-touch-icon"
      sizes="144x144"
      href="${imgPath}/apple-touch-icon.png"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="32x32"
      href="${imgPath}/favicon-32x32.png"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="16x16"
      href="${imgPath}/favicon-16x16.png"
    />
    <link rel="manifest" href="${imgPath}/site.webmanifest" />
    <link
      rel="mask-icon"
      href="${imgPath}/safari-pinned-tab.svg"
      color="#5bbad5"
    />
    <meta name="msapplication-TileColor" content="#da532c" />
    <meta name="theme-color" content="#ffffff" />
    <title>Usuários Cadastrados - Casa América</title>
    <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
    <link rel="stylesheet" href="${cssPath}/style.css" />
    <link
      href="https://fonts.googleapis.com/css?family=Roboto:400,700"
      rel="stylesheet"
    />

    <!-- Custom fonts for this template-->
    <link
      href="${vendorPath}/fontawesome-free/css/all.min.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      rel="stylesheet"
      href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css"
    />
    <!-- Page level plugin CSS-->
    <link
      href="${vendorPath}/datatables/dataTables.bootstrap4.css"
      rel="stylesheet"
    />
    <!-- Custom styles for this template-->
    <link href="${cssPath}/sb-admin.css" rel="stylesheet" />
    <link href="${cssPath}/jquery-ui.css" rel="stylesheet" />

    <!-- jPList Core -->
    <script src="${vendorPath}/jquery/jquery.min.js"></script>
    <link
      href="${cssPath}/jplist.demo-pages.min.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="${cssPath}/jplist.core.min.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="${cssPath}/jplist.filter-toggle-bundle.min.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="${cssPath}/jplist.pagination-bundle.min.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="${cssPath}/jplist.history-bundle.min.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="${cssPath}/jplist.textbox-filter.min.css"
      rel="stylesheet"
      type="text/css"
    />
    <link
      href="${cssPath}/jplist.jquery-ui-bundle.min.css"
      rel="stylesheet"
      type="text/css"
    />

    <script src="${jsPath}/jquery-ui.js"></script>
    <script src="${jsPath}/jplist.core.min.js"></script>
    <script src="${jsPath}/jplist.filter-toggle-bundle.min.js"></script>
    <script src="${jsPath}/jplist.history-bundle.min.js"></script>
    <script src="${jsPath}/jplist.jquery-ui-bundle.min.js"></script>
    <script src="${jsPath}/jplist.pagination-bundle.min.js"></script>
    <script src="${jsPath}/jplist.sort-bundle.min.js"></script>
    <script src="${jsPath}/jplist.textbox-filter.min.js"></script>

    <script>
      $("document").ready(function() {
        /**
         * jPList
         */

        $("#tabelaProdutos").jplist({
          itemsBox: ".demo-tbl tbody",
          itemPath: ".tbl-item",
          panelPath: ".jplist-panel",

          //save plugin state
          storage: "localstorage", //'', 'cookies', 'localstorage'
          storageName: "jplist-table-3",
          cookiesExpiration: -1,

          redrawCallback: function() {
            $(".tbl-item").each(function(index, el) {
              if (index % 2 === 0) {
                $(el)
                  .removeClass("odd")
                  .addClass("even");
              } else {
                $(el)
                  .removeClass("even")
                  .addClass("odd");
              }
            });
          }
        });

        //resetar filtros quando carrega a página
        $(".jplist-reset-btn").click();
      });
    </script>
  </head>

  <body id="page-top" class="usuariosCadastrados">
    <!-- Menu Topo -->
    <%@include file="/WEB-INF/views/modulos/menu.jsp" %>

    <div id="wrapper">
      <!-- Sidebar -->
      <%@include file="/WEB-INF/views/modulos/sidebar.jsp" %>
      <div id="content-wrapper">
        <div class="container-fluid">
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item active">
              <a href="/">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Usuário</li>
          </ol>

          <!-- TABELA DE PEDIDOS -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i> Lista de Usuários Cadastrados
              <!-- Button trigger modal -->
              <button
                type="button"
                class="btn btn-sm btn-success float-right"
                data-toggle="modal"
                data-target="#modalAddUsuario"
              >
                <i class="fas fa-plus-square"></i>
              </button>
            </div>
            <div class="card-body">
              ${sucesso}
              <!-- demo -->
              <div id="tabelaProdutos" class="box jplist table-layout-2">
                <!-- ios button: show/hide panel -->
                <div class="jplist-ios-button">
                  <i class="fa fa-sort"></i>
                  Filtros
                </div>

                <!-- panel -->
                <div class="jplist-panel box panel-top">
                  <!-- reset button -->
                  <button
                    type="button"
                    class="jplist-reset-btn"
                    data-control-type="reset"
                    data-control-name="reset"
                    data-control-action="reset"
                  >
                    Resetar &nbsp;<i class="fa fa-share"></i>
                  </button>

                  <!-- items per page dropdown -->
                  <div
                    class="jplist-drop-down"
                    data-control-type="items-per-page-drop-down"
                    data-control-name="paging"
                    data-control-action="paging"
                  >
                    <ul>
                      <li><span data-number="10"> 10 por página </span></li>
                      <li>
                        <span data-number="25" data-default="true">
                          25 por página
                        </span>
                      </li>
                      <li><span data-number="50"> 50 por página </span></li>
                      <li><span data-number="all"> Ver todos </span></li>
                    </ul>
                  </div>

                  <!-- sort dropdown -->
                  <div
                    class="jplist-drop-down"
                    data-control-type="sort-drop-down"
                    data-control-name="sort"
                    data-control-action="sort"
                    data-datetime-format="{day}/{month}/{year}"
                  >
                    <!-- {year}, {month}, {day}, {hour}, {min}, {sec} -->

                    <ul>
                      <li><span data-path="default">Ordenar por</span></li>
                      <li>
                        <span
                          data-path=".title"
                          data-order="asc"
                          data-type="text"
                          data-default="true"
                          >Usuário asc</span
                        >
                      </li>
                      <li>
                        <span
                          data-path=".title"
                          data-order="desc"
                          data-type="text"
                          >Usuário desc</span
                        >
                      </li>
                      <li>
                        <span
                          data-path=".desc"
                          data-order="asc"
                          data-type="text"
                          >E-mail A-Z</span
                        >
                      </li>
                      <li>
                        <span
                          data-path=".desc"
                          data-order="desc"
                          data-type="text"
                          >E-mail Z-A</span
                        >
                      </li>
                    </ul>
                  </div>

                  <!-- filter by title -->
                  <div class="text-filter-box">
                    <i class="fa fa-search  jplist-icon"></i>

                    <!--[if lt IE 10]>
                      <div class="jplist-label">Filter by Title:</div>
                    <![endif]-->

                    <input
                      data-path=".title"
                      type="text"
                      value=""
                      placeholder="Buscar Usuário"
                      data-control-type="textbox"
                      data-control-name="title-filter"
                      data-control-action="filter"
                    />
                  </div>

                  <!-- filter by description -->
                  <div class="text-filter-box">
                    <i class="fa fa-search  jplist-icon"></i>

                    <!--[if lt IE 10]>
                      <div class="jplist-label">Filter by Description:</div>
                    <![endif]-->

                    <input
                      data-path=".desc"
                      type="text"
                      value=""
                      placeholder="Buscar E-mail"
                      data-control-type="textbox"
                      data-control-name="desc-filter"
                      data-control-action="filter"
                    />
                  </div>
                </div>

                <!-- data -->
                <div class="box text-shadow">
                  <table class="demo-tbl">
                    <thead>
                      <tr>
                        <th>Usuário</th>
                        <th>E-mail</th>
                        <th style="width:100px">Editar</th>
                        <th style="width:100px">Remover</th>
                      </tr>
                    </thead>
                    <tbody>
                      <!-- item 1 -->
                      <c:forEach items="${usuarios}" var="usuarios">
                        <tr class="tbl-item">
                          <!-- data -->
                          <td class="td-block">
                            <p class="title">${usuarios.usuario}</p>
                          </td>

                          <td class="td-block">
                            <p class="desc">${usuarios.email}</p>
                          </td>

                          <td class="td-block">
                            <p class="like"
                              ><a
                                href="/usuario/editarUsuario/${usuarios.usuario}"
                                ><i class="far fa-edit"></i></a
                            ></p>
                          </td>

                          <td class="td-block">
                            <p class="like"
                              ><a
                                href="/removerUsuario/${usuarios.usuario}"
                                ><i class="far fa-trash-alt"></i></a
                            ></p>
                          </td>
                        </tr>
                      </c:forEach>
                    </tbody>
                  </table>
                </div>
                <!-- end of data -->

                <div class="box jplist-no-results text-shadow align-center">
                  <p>Nenhum resultado encontrado.</p>
                </div>

                <!-- ios button: show/hide panel -->
                <div class="jplist-ios-button">
                  <i class="fa fa-sort"></i>
                  Paginação
                </div>

                <!-- panel -->
                <div class="jplist-panel box panel-bottom">
                  <div class="paginacaoBase float-right">
                    <!-- pagination results -->
                    <div
                      class="jplist-label"
                      data-type="{start} - {end} de {all}"
                      data-control-type="pagination-info"
                      data-control-name="paging"
                      data-control-action="paging"
                    ></div>

                    <!-- pagination -->
                    <div
                      class="jplist-pagination"
                      data-control-type="pagination"
                      data-control-name="paging"
                      data-control-action="paging"
                      data-control-animate-to-top="false"
                    ></div>
                  </div>
                </div>
              </div>
              <!-- end of demo -->
            </div>

            <div class="card-footer small text-muted">
              Atualizado em ${atualiza}
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
        <!-- Rodape -->
        <%@include file="/WEB-INF/views/modulos/rodape.jsp" %>
      </div>
      <!-- /.content-wrapper -->

      <!-- Modal -->
      <div
        class="modal fade"
        id="modalAddUsuario"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div
          class="modal-dialog modal-dialog-centered modal-lg"
          role="document"
        >
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">
                Cadastrar Usuário
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form
                action="/usuario/cadastro"
                method="post"
                commandName="usuario"
              >
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label>E-mail do Usuário</label>
                    <input
                      type="email"
                      class="form-control email"
                      name="email"
                      id="inputEmail"
                      placeholder="Digite seu email"
                    />
                    <form:errors
                      path="email"
                      style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;"
                    />
                  </div>
                  <div class="form-group col-md-6">
                    <label>Nome do Usuário</label>
                    <input
                      type="text"
                      class="form-control"
                      name="usuario"
                      id="inputUsuario"
                      placeholder="Escolha seu usuário"                      
                      title="Deve conter apenas letras com no mínimo 5 caracteres"
                      required
                    />
                    <form:errors
                      path="usuario"
                      style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;"
                    />
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label>Senha</label>
                    <input
                      type="password"
                      class="form-control"
                      id="inputSenha"
                      placeholder="Escolha a sua senha"
                      name="senha"
                      required
                    />
                    <form:errors
                      path="senha"
                      style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;"
                    />
                  </div>

                  <div class="form-group col-md-6">
                    <label>Confirmar Senha</label>
                    <input
                      type="password"
                      class="form-control"
                      id="inputConfirmarSenha"
                      placeholder="Confirmar senha"
                      name="confirmarSenha"
                      required
                    />
                    <form:errors
                      path="senha"
                      style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;"
                    />
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label>Tipo de Usuário</label>
                    <select class="form-control" name="tipoUsuario">
                      <option value="user">USUARIO</option>
                      <option value="admin">ADMIN</option>
                    </select>
                  </div>
                </div>

                <button
                  type="submit"
                  class="btn btn-success btn-sm w-100 mt-3 sombraVerde"
                >
                  <i class="fas fa-save mr-2"></i>Cadastrar Usuário
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /#wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- abre submenu recolhido quando hover-->
    <script src="${jsPath}/abreMenu.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <!-- <script src="${vendorPath}/chart.js/Chart.min.js"></script> -->
    <script src="${vendorPath}/datatables/jquery.dataTables.js"></script>
    <script src="${vendorPath}/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="${jsPath}/sb-admin.min.js"></script>
    <!-- Demo scripts for this page-->
    <script src="${jsPath}/demo/datatables-demo.js"></script>
    <!-- <script src="${jsPath}/demo/chart-area-demo.js"></script> -->

    <script>
      var password = document.getElementById("inputSenha"),
        confirm_password = document.getElementById("inputConfirmarSenha");

      function validatePassword() {
        if (password.value != confirm_password.value) {
          confirm_password.setCustomValidity("As senhas não conferem");
        } else {
          confirm_password.setCustomValidity("");
        }
      }

      password.onchange = validatePassword;
      confirm_password.onkeyup = validatePassword;
    </script>

    <!-- colorindo itens ativos do menu -->
    <script>
      $(document).ready(function() {
        $(".sidebar li .dropdown-toggle:contains('Usuário')").addClass(
          "dropdownAtivo"
        );
        $(
          ".sidebar li .dropdown-menu .dropdown-item:contains('Usuários')"
        ).addClass("itemAtivo");
      });
    </script>
  </body>
</html>
