<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <c:url value="/resources/img" var="imgPath" />
    <c:url value="/resources/css" var="cssPath" />
    <c:url value="/resources/js" var="jsPath" />
    <c:url value="/resources/vendor" var="vendorPath" />

    <link rel="icon" href="${imgPath}/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
    <link rel="manifest" href="${imgPath}/site.webmanifest">
    <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Configurações - Casa América</title>
    <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
    <link rel="stylesheet" href="${cssPath}/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css">
    <!-- Page level plugin CSS-->
    <link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="${cssPath}/sb-admin.css" rel="stylesheet">
    <link href="${cssPath}/jquery-ui.css" rel="stylesheet">

    <!-- jPList Core -->
    <script src="${vendorPath}/jquery/jquery.min.js"></script>


</head>

<body id="page-top" class="configuracao">
    <!-- Menu Topo -->
    <%@include file="/WEB-INF/views/modulos/menu.jsp" %>

    <div id="wrapper">
        <!-- Sidebar -->
        <%@include file="/WEB-INF/views/modulos/sidebar.jsp" %>
        <div id="content-wrapper">
            <div class="container-fluid">

                <div class="msgFaturando">${salvando}</div>

                <!-- TABELA DE PEDIDOS -->
                <form:form id="salvarConfigImpressora">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-table"></i> Configurações</div>
                        <div class="card-body">
                            <label class="my-1 mr-2" for="selectImpressora">Escolher impressora</label>
                            <select class="custom-select my-1 mr-sm-2" id="selectImpressora">
                                <option selected>Selecionar</option>
                                <c:forEach items="${impressoras}" var="impressoras">
                                    <c:choose>
                                        <c:when test="${(impressoras.escolhido=='TRUE')}">
                                            <option selected value="${impressoras.nome}">${impressoras.nome}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${impressoras.nome}">${impressoras.nome}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select>
                            <button type="submit" onClick="valorSelecionado();"
                                class="btn btn-primary btn-sm w-100 mt-3 sombraAzul">Salvar</button>
                        </div>
                    </div>
                </form:form>

            </div>
            <!-- /.container-fluid -->
            <!-- Rodape -->
            <%@include file="/WEB-INF/views/modulos/rodape.jsp" %>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- abre submenu recolhido quando hover-->
    <script src="${jsPath}/abreMenu.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="${vendorPath}/datatables/jquery.dataTables.js"></script>
    <script src="${vendorPath}/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="${jsPath}/sb-admin.min.js"></script>

    <!-- filtro por data de inicio e data final-->
    <script src="${jsPath}/dateRange.js"></script>

    <!-- Plugin para aplicação de máscaras -->
    <script src="${jsPath}/jquery.mask.js"></script>
    <!-- <script src="${jsPath}/mascara.js"></script> -->

    <script>
        function valorSelecionado() {
            var id = document.getElementById('salvarConfigImpressora');
            id.action = "/configurar/" + document.getElementById('selectImpressora').value;
        }

    </script>

    

</body>

</html>