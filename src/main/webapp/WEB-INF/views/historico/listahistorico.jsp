<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <c:url value="/resources/img" var="imgPath" />
    <c:url value="/resources/css" var="cssPath" />
    <c:url value="/resources/js" var="jsPath" />
    <c:url value="/resources/vendor" var="vendorPath" />

    <link rel="icon" href="${imgPath}/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
    <link rel="manifest" href="${imgPath}/site.webmanifest">
    <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Histórico de Atividades - Casa América</title>
    <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
    <link rel="stylesheet" href="${cssPath}/style.css" />
    <link rel="stylesheet" href="${cssPath}/imprimirModal.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css">
    <!-- Page level plugin CSS-->
    <link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="${cssPath}/sb-admin.css" rel="stylesheet">
    <link href="${cssPath}/jquery-ui.css" rel="stylesheet">
    <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	<link href="${cssPath}/sb-admin.css" rel="stylesheet">

    <!-- jPList Core -->
    <script src="${vendorPath}/jquery/jquery.min.js"></script>
    <link href="${cssPath}/jplist.demo-pages.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.core.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.filter-toggle-bundle.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.pagination-bundle.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.history-bundle.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.textbox-filter.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.jquery-ui-bundle.min.css" rel="stylesheet" type="text/css" />

    <script src="${jsPath}/jquery-ui.js"></script>
    <script src="${jsPath}/jplist.core.min.js"></script>
    <script src="${jsPath}/jplist.filter-toggle-bundle.min.js"></script>
    <script src="${jsPath}/jplist.history-bundle.min.js"></script>
    <script src="${jsPath}/jplist.jquery-ui-bundle.min.js"></script>
    <script src="${jsPath}/jplist.pagination-bundle.min.js"></script>
    <script src="${jsPath}/jplist.sort-bundle.min.js"></script>
    <script src="${jsPath}/jplist.textbox-filter.min.js"></script>

    <script>        
        $('document').ready(function () {
        /**
        * user defined functions
        */
        jQuery.fn.jplist.settings = {

            /**
            * jQuery UI date picker
            */
            datepicker: function (input, options) {

                //set options
                options.dateFormat = 'dd/mm/yy';
                options.changeMonth = true;
                options.changeYear = true;

                //start datepicker
                input.datepicker(options);
            }

        };

        /**
        * jPList
        */

        $('#tabelaPedidos').jplist({

            itemsBox: '.demo-tbl tbody'
            , itemPath: '.tbl-item'
            , panelPath: '.jplist-panel'

            //save plugin state
            , storage: 'localstorage' //'', 'cookies', 'localstorage'			
            , storageName: 'jplist-table-2'
            , cookiesExpiration: -1

            , redrawCallback: function () {

                $('.tbl-item').each(function (index, el) {
                    if (index % 2 === 0) {
                        $(el).removeClass('odd').addClass('even');
                    }
                    else {
                        $(el).removeClass('even').addClass('odd');
                    }
                });
                                
                $(".like:contains('NÃO')").addClass("vermelho");                
                $(".like:contains('SIM')").addClass("verde");
                
            }
        });
            
        //resetar filtros quando carrega a página
        $('.jplist-reset-btn').click();

        //limpa inputs do modal depois que fecha
        $('#gerarEtiquetaModal').on('hidden.bs.modal', function (e) {  
            $(this)
                .find("input,textarea,select")
                .val('')
                .end()
                .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end();
            document.getElementById('btnGerarEtiqueta').setAttribute("disabled", "disabled");
        });
        
    });
	</script>

</head>

<body id="page-top" class="listarEtiquetas">
    <!-- Menu Topo -->
    <%@include file="/WEB-INF/views/modulos/menu.jsp" %>

    <div id="wrapper">
        <!-- Sidebar -->
        <%@include file="/WEB-INF/views/modulos/sidebar.jsp" %>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        <a href="/Infat">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Histórico de Atividades</li>
                </ol>


                <!-- TABELA DE PEDIDOS -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i> Histórico</div>
                    <div class="card-body">

					<div class="msgFaturando">${gerando}</div>
					
                        <!-- demo -->
                        <div id="tabelaPedidos" class="box jplist table-layout-2">

                            <!-- ios button: show/hide panel -->
                            <div class="jplist-ios-button">
                                <i class="fa fa-sort"></i>
                                Filtros
                            </div>

                            <!-- panel -->
                            <div class="jplist-panel box panel-top">

                                <!-- reset button -->
                                <button type="button" class="jplist-reset-btn" data-control-type="reset" data-control-name="reset" data-control-action="reset">
                                    Resetar &nbsp;<i class="fa fa-share"></i>
                                </button>

                                <!-- items per page dropdown -->
                                <div class="jplist-drop-down" data-control-type="items-per-page-drop-down" data-control-name="paging" data-control-action="paging">

                                    <ul>
                                        <li><span data-number="10"> 10 por página </span></li>
                                        <li><span data-number="25" data-default="true"> 25 por página </span></li>
                                        <li><span data-number="50"> 50 por página </span></li>
                                        <li><span data-number="all"> Ver todos </span></li>
                                    </ul>
                                </div>

                                <!-- sort dropdown -->
                                <div class="jplist-drop-down" data-control-type="sort-drop-down" data-control-name="sort" data-control-action="sort" data-datetime-format="{day}/{month}/{year}">
                                    <!-- {year}, {month}, {day}, {hour}, {min}, {sec} -->

                                    <ul>
                                        <li><span data-path="default">Ordenar por</span></li>
                                        <li><span data-path=".date" data-order="asc" data-type="datetime">Data cresc</span></li>
                                        <li><span data-path=".date" data-order="desc" data-type="datetime" data-default="true">Data decres</span></li>
                                    </ul>
                                </div>

                                <!-- date picker range filter -->
                                <!-- data-datepicker-func is a user function defined in jQuery.fn.jplist.settings -->
                                <div data-control-type="date-picker-range-filter" data-control-name="date-picker-range-filter" data-control-action="filter" data-path=".date" data-datetime-format="{day}/{month}/{year}" data-datepicker-func="datepicker" class="jplist-date-picker-range">

                                    <i class="fa fa-calendar jplist-icon" aria-hidden="true"></i>
                                    <input type="text" class="date-picker" placeholder="Data inicial" data-type="prev" />

                                    <i class="fa fa-minus gap" aria-hidden="true"></i>

                                    <i class="fa fa-calendar jplist-icon" aria-hidden="true"></i>
                                    <input type="text" class="date-picker" placeholder="Data final" data-type="next" />
                                </div>                              
                            </div>                                                       

							<!-- data -->
                            <div class="box text-shadow">
                            
                                <table class="demo-tbl">
                                    <thead>
                                        <tr>                                        	
                                            <th>Data de Processamento</th>
                                            <th>Nome da Ação</th>                                            
                                            <th>Mensagem</th>
                                            <th>Status</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- item 1 -->
                                        <c:forEach items="${historicos}" var="historicos">
                                            <tr class="tbl-item">
                                            
                                                <!-- data -->
                                                <td class="td-block align-middle">
                                                    <p class="date">${historicos.data}</p>
                                                </td>

                                                <td class="td-block align-middle">
                                                    <p class="title">${historicos.nome}</p>
                                                </td>
                                                
                                                <td class="td-block align-middle">
                                                    <p class="plp">${historicos.mensagem}</p>
                                                </td>

                                                <td class="td-block align-middle">
                                                    <p class="desc">${historicos.status}</p>
                                                </td>
                                                
                                            </tr>
                                        </c:forEach>

                                    </tbody>
                                </table>
                            </div>
                            <!-- end of data -->

                            <div class="box jplist-no-results text-shadow align-center">
                                <p>Nenhum resultado encontrado.</p>
                            </div>

                            <!-- ios button: show/hide panel -->
                            <div class="jplist-ios-button">
                                <i class="fa fa-sort"></i>
                                Paginação
                            </div>

                            <!-- panel -->
                            <div class="jplist-panel box panel-bottom">

                                <div class="paginacaoBase float-right">
                                    <!-- pagination results -->
                                    <div class="jplist-label" data-type="{start} - {end} de {all}" data-control-type="pagination-info" data-control-name="paging" data-control-action="paging">
                                    </div>

                                    <!-- pagination -->
                                    <div class="jplist-pagination" data-control-type="pagination" data-control-name="paging" data-control-action="paging" data-control-animate-to-top="false">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- end of demo -->

                    </div>

                    <div class="card-footer small text-muted">Atualizado em ${atualiza}</div>

                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- Rodape -->
            <%@include file="/WEB-INF/views/modulos/rodape.jsp" %>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    
    	<!-- gerar Etiqueta Modal -->
		<div class="modal fade bd-example-modal-sm" id="gerarEtiquetaModal" tabindex="-1"
			role="dialog" aria-labelledby="exampleModalCenterTitle"
			aria-hidden="true">
			<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Dimensões da caixa:</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>					
						<form:form id="medidas" onsubmit="return spinGerarEtiqueta();">
							<div class="modal-body">
                                <div class="form-group">
                                    <label for="altura">Altura (cm)</label>
                                	<input type="number" class="form-control" name="altura" id="altura" min="2" max="105" required placeholder="Altura da caixa">
                                	<label for="largura" class="mt-3">Largura (cm)</label>
                                	<input type="number" class="form-control" name="largura" id="largura" min="11" max="105" required placeholder="Largura da caixa">
                                	<label for="comprimento"  class="mt-3">Comprimento (cm)</label>
                                	<input type="number" class="form-control" name="comprimento" id="comprimento" min="16" max="105" required placeholder="Comprimento da caixa">                                    
                                </div>
							</div>
							<div class="modal-footer">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="checkRastreio">
                                    <label class="form-check-label" for="checkRastreio">Os campos foram preenchidos corretamente.</label>
                                </div>
								<button class="btn btn-success btn-sm w-100 mt-3 sombraVerde"
									id="btnGerarEtiqueta" type="submit" disabled>
									<i class="fa fa-file mr-2"></i>Gerar Rastreio
								</button>
							</div>
						</form:form>					
				</div>
			</div>
		</div>
		
		<!-- salvar etiqueta em pdf -->
		<c:forEach items="${pedidos}" var="pedidos">
            <form:form action="/etiquetas/salvar/${pedidos.id_order}" >
                <div class="modal fade" id="salvarNfModal" tabindex="-1"
                    role="dialog" aria-labelledby="exampleModalCenterTitle"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">						
                                <iframe id="framePdf" src="" width="800" height="600" style="border: none;" ></iframe>
                                <button type="button" type="submit" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
		</c:forEach>
    
    
    <!-- Bootstrap core JavaScript-->
    <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- abre submenu recolhido quando hover-->
    <script src="${jsPath}/abreMenu.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="${vendorPath}/datatables/jquery.dataTables.js"></script>
    <script src="${vendorPath}/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="${jsPath}/sb-admin.min.js"></script>

    <!-- filtro por data de inicio e data final-->
    <script src="${jsPath}/dateRange.js"></script>

    <!-- Plugin para aplicação de máscaras -->
    <script src="${jsPath}/jquery.mask.js"></script>
    <!-- <script src="${jsPath}/mascara.js"></script> -->
    <script src="${jsPath}/print.min.js"></script>
    
    <script>
    function enviaOrder(id_order){
        var id = document.getElementById('medidas');    	
       	id.action = "/Infat/etiquetas/gerar/" +id_order;    	
    }
    
    function spinImprimirEtiqueta(id_order){
        var id = id_order;
		document.getElementById(id+"btnGerarZebra").innerHTML = "<i class='fas fa-spinner'></i>";
	}      
    
    function spinGerarEtiqueta(){
		document.getElementById("btnGerarEtiqueta").innerHTML = "<i class='fas fa-spinner'></i> Gerando";
	}  

	function salvarPdf(id_order){
		document.getElementById(id_order+"salvarButton").innerHTML = "<i class='fas fa-spinner'></i>";
		var id = id_order;
		$.ajax({
		    type: 'POST',
		    url: '/Infat/etiquetas/salvar/'+id,				    			   
		    success: function(data){
		    	$('#framePdf').attr('src', data)
		    	$('#salvarNfModal').modal('show')
		    	document.getElementById(id+"salvarButton").innerHTML = "<i class='fas fa-file-pdf'></i> PDF";    	
		    },
		    error: function(data){				    	
		    	alert('Nota não está disponível para download!');
		    }
		})								
	}      
    
    //habilita botao gerar rastreio depois de marcar o checkbox
    $("input[type=checkbox]").change(function(){
        if($('#checkRastreio').is(':checked')) {
            document.getElementById('btnGerarEtiqueta').removeAttribute("disabled");
        }
        else if(!$('#checkRastreio').is(':checked')) {
            document.getElementById('btnGerarEtiqueta').setAttribute("disabled", "disabled");
        }
    });
    </script>

    <!-- colorindo itens ativos do menu -->
    <script>
        $(document).ready(function() {
            $(".sidebar li .dropdown-toggle:contains('Histórico')").addClass("dropdownAtivo");
            $(".sidebar li .dropdown-menu .dropdown-item:contains('Listar Atividades')").addClass("itemAtivo");
        });
    </script>
    
</body>

</html>