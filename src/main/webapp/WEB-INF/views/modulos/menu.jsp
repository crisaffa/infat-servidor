<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<nav class="navbar navbar-expand navbar-dark bg-vinho static-top pr-3">

    <a href="/" id="logoMenu">
        <!-- <img src="${imgPath}/logo-b-sm.png"> -->
    </a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#" onclick="resizeLogo();">
        <i class="fas fa-bars"></i>
    </button>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="infoBucardo" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-clock"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <p class="dropdown-item m-0 text-center">Última atualização do Bucardo em<span class="d-block font-weight-bold">${bucardo}</span></p>                
            </div>            
        </li>
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle fa-fw"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">Log de Atividade</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="/logout">Logout</a>
            </div>
        </li>        
    </ul>


</nav>

<c:url value="/resources/img" var="imgPath" />
<script>
    function resizeLogo() {
        if ($('.sidebar').width() == 225) {
            $('#logoMenu').width(60);
            // $('#logoMenu').html('<img src="${imgPath}/icone.png">');
            $('#logoMenu').css('background-image', 'url(${imgPath}/icone_menu-b.png)');
        } else if ($('.sidebar').width() == 90) {
            $('#logoMenu').width(0);
            // $('#logoMenu').html('');
            $('#logoMenu').css('background-image', 'none');
        } else if ($('.sidebar').width() == 0) {
            $('#logoMenu').width(90);
            $('#logoMenu').css('background-image', 'url(${imgPath}/icone_menu-b.png)');
        } else {
            $('#logoMenu').width(225);
            $('#logoMenu').css('background-image', 'url(${imgPath}/logo_menu-b.png)');
        }
    }
</script>