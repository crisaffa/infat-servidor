<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ taglib
uri="http://www.springframework.org/security/tags" prefix="security"%>

<ul class="sidebar navbar-nav">
  <!-- <li class="mx-auto mt-4 mb-3">
        <a class="navbar-brand" href="/"><img src="${imgPath}/logo-b-sm.png" alt="" height="36"></a>
    </li> -->

  <li class="nav-item dropdown do-not-close">
    <a
      class="nav-link dropdown-toggle"
      href="#"
      id="pedidosDropdown"
      role="button"
      data-toggle="dropdown"
      aria-haspopup="true"
      aria-expanded="false"
    >
      <i class="fas fa-book"></i> <span>Pedidos</span></a
    >
    <div
      class="dropdown-menu dropdown-menu-pedidos py-0"
      aria-labelledby="pedidosDropdown"
    >
      <a class="dropdown-item gestaoPed" href="/pedidos/gestaoPedidos"
        ><i class="fas fa-caret-right"></i> Gestão de Pedidos</a
      >
      <a class="dropdown-item separar" href="/pedidos/separarPedidos"
        ><i class="fas fa-caret-right"></i> Separação de Pedidos</a
      >
      <!-- <a
        class="dropdown-item relatorioPed"
        href="/pedidos/relatorioPedidos"
        ><i class="fas fa-caret-right"></i> Relatório de Pedidos</a
      > -->
      <a class="dropdown-item etiquetas" href="/etiquetas/listarEtiquetas"
        ><i class="fas fa-caret-right"></i> Etiquetas</a
      >
    </div>
  </li>

  <li class="nav-item dropdown do-not-close">
    <a
      class="nav-link dropdown-toggle"
      href="#"
      id="produtosDropdown"
      role="button"
      data-toggle="dropdown"
      aria-haspopup="true"
      aria-expanded="false"
    >
      <i class="fas fa-box"></i> <span>Produtos</span></a
    >
    <div
      class="dropdown-menu dropdown-menu-produtos py-0"
      aria-labelledby="produtosDropdown"
    >
      <a class="dropdown-item gestaoProd" href="/produtos/gestaoProdutos"
        ><i class="fas fa-caret-right"></i> Gestão de Produtos</a
      >
      <!-- <a class="dropdown-item" href="#"><i class="fas fa-caret-right"></i> Categorias</a>
            <a class="dropdown-item" href="#"><i class="fas fa-caret-right"></i> Atributos</a>
			<a class="dropdown-item" href="#"><i class="fas fa-caret-right"></i> Preços</a> -->
    </div>
  </li>

  <security:authorize access="hasRole('ROLE_ADMIN')">
    <li class="nav-item dropdown do-not-close">
      <a
        class="nav-link dropdown-toggle"
        href="#"
        id="empresaDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        <i class="fas fa-building"></i> <span>Empresa</span></a
      >
      <div
        class="dropdown-menu dropdown-menu-empresa py-0"
        aria-labelledby="empresaDropdown"
      >
        <!-- <a class="dropdown-item config" href="/empresa/configuracao"><i class="fas fa-caret-right"></i> Cadastrar CNPJ</a> -->
        <a class="dropdown-item cadast" href="/empresa/cnpj"
          ><i class="fas fa-caret-right"></i> Listar Empresas</a
        >
      </div>
    </li>

    <li class="nav-item dropdown do-not-close">
      <a
        class="nav-link dropdown-toggle"
        href="#"
        id="transportadoraDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        <i class="fas fa-truck"></i> <span>Transportadora</span></a
      >
      <div
        class="dropdown-menu dropdown-menu-transportadora py-0"
        aria-labelledby="transportadoraDropdown"
      >
        <!-- <a class="dropdown-item corr" href="/correios/config"><i class="fas fa-caret-right"></i> Cadastrar Transportadora</a> -->
        <a class="dropdown-item contas" href="/correios/contas"
          ><i class="fas fa-caret-right"></i> Listar Transportadoras</a
        >
      </div>
    </li>

    <li class="nav-item dropdown do-not-close">
      <a
        class="nav-link dropdown-toggle"
        href="#"
        id="usuarioDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        <i class="fas fa-users"></i> <span>Usuário</span></a
      >
      <div
        class="dropdown-menu dropdown-menu-usuario py-0"
        aria-labelledby="usuarioDropdown"
      >
        <a class="dropdown-item usuario" href="/usuario/usuario"
          ><i class="fas fa-caret-right"></i> Listar Usuários</a
        >
        <!-- <a class="dropdown-item cadUsuario" href="/usuario/cadastroUsuario"><i class="fas fa-caret-right"></i> Cadastrar Usuário</a> -->
      </div>
    </li>

    <li class="nav-item dropdown do-not-close">
      <a
        class="nav-link dropdown-toggle"
        href="#"
        id="tarefaDropdown"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        <i class="fas fa-tasks"></i> <span>Tarefas</span></a
      >
      <div
        class="dropdown-menu dropdown-menu-tarefa py-0"
        aria-labelledby="tarefaDropdown"
      >
        <a class="dropdown-item tarefa" href="/tarefa/agendamentoTarefa"
          ><i class="fas fa-caret-right"></i> Listar Tarefas</a
        >
      </div>
    </li>

    <li class="nav-item dropdown do-not-close">
      <a class="nav-link dropdown-toggle" href="#" id="tarefaDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-history"></i> <span>Histórico</span></a>
      <div class="dropdown-menu dropdown-menu-tarefa py-0" aria-labelledby="tarefaDropdown">
        <a class="dropdown-item tarefa" href="/historico/listahistorico"><i class="fas fa-caret-right"></i> Listar Atividades</a>
      </div>
    </li>

    <li class="nav-item dropdown do-not-close">
      <a class="nav-link dropdown-toggle" href="/pedidos/relatorioPedidos" id="relatorioDropdown" role="button" aria-expanded="false">
        <i class="fas fa-clipboard"></i> <span>Relatórios</span></a>
    </li>

    <li class="nav-item dropdown do-not-close">
      <!-- MENU DE RELATÓRIOS -->
    </li>
  </security:authorize>
</ul>
