<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<!doctype html>
<html lang="pt-br">
  <head>
   <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <c:url value="/resources/img" var="imgPath" />
    <c:url value="/resources/css" var="cssPath" />
    <c:url value="/resources/vendor" var="vendorPath" />
    
    <link rel="icon" href="${imgPath}/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
    <link rel="manifest" href="${imgPath}/site.webmanifest">
    <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>Login - Integração de Faturamento Casa América</title>
    <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
    <link rel="stylesheet" href="${cssPath}/signin.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  </head>

  <body class="text-center">
    <form:form servletRelativeAction="/login" method="post" class="form-signin">
      <!-- <img class="mb-4 rounded img-fluid" src="${imgPath}/logo.png" alt=""> -->
      <img class="mb-4 rounded logo" src="${imgPath}/logo-sm.png" alt="">
      <!-- <h1 class="mb-3 font-weight-normal">Faça login em sua conta.</h1> -->
      
      <label for="inputUsuario" class="sr-only">Usuário</label>
      <input type="text" id="inputUsuario" class="form-control mb-2" name="username" placeholder="Usuário" required autofocus>
      <label for="inputPassword" class="sr-only">Senha</label>
      <input type="password" id="inputPassword" class="form-control mb-3" name="password" placeholder="Senha" required>
      <!-- <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Lembrar-me
        </label>
      </div> -->
      <button class="btn btn-vinho btn-block" type="submit"><i class="fas fa-sign-in-alt mr-2"></i>Acessar</button>
      <p class="mt-4 mb-2 text-muted copyright">&copy; Casa América - 2018</p>
    </form:form>
  </body>
</html>