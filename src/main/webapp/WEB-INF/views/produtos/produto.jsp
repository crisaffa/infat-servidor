<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <c:url value="/resources/img" var="imgPath" />
    <c:url value="/resources/css" var="cssPath" />
    <c:url value="/resources/js" var="jsPath" />
    <c:url value="/resources/vendor" var="vendorPath" />

    <link rel="icon" href="${imgPath}/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
    <link rel="manifest" href="${imgPath}/site.webmanifest">
    <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Visualização de Produto - Casa América</title>
    <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
    <link rel="stylesheet" href="${cssPath}/style.css" />
    <link rel="stylesheet" href="${cssPath}/imprimirModal.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="${cssPath}/sb-admin.css" rel="stylesheet">
</head>

<body id="page-top" class="produtos">
    <!-- Menu Topo -->
    <%@include file="/WEB-INF/views/modulos/menu.jsp"%>

    <div id="wrapper">
        <!-- Sidebar -->
        <%@include file="/WEB-INF/views/modulos/sidebar.jsp"%>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/produtos/gestaoProdutos">Gestão de Produtos</a></li>
                    <li class="breadcrumb-item active">Visualização de Produto</li>
                </ol>
                <!-- Vizualização de Produto -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i> Visualização de Produto
                        <!--<a class="btn btn-sm btn-primary float-right sombraAzul" href="#" data-toggle="modal" data-target="#imprimirModal"><i class="fas fa-print mr-2"></i>Imprimir</a>-->
                            <p class="float-right m-0 font-weight-bold">
                                Status: <span class="status font-weight-normal vermelho">${produto.status}</span>
                            </p>                        
                    </div>
                    <div class="msgFaturando">${faturando}</div>
                    <div class="card-body">
                        <!-- ABAS DE ITENS -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item"><a class="nav-link active" id="home-tab" data-toggle="tab" href="#informacao" role="tab" aria-controls="home" aria-selected="true">Dados do Produto</a></li>
                            <!-- <li class="nav-item"><a class="nav-link" id="contact-tab" data-toggle="tab" href="#detalhe" role="tab" aria-controls="contact" aria-selected="false">Imagens</a></li>
                            <li class="nav-item"><a class="nav-link" id="status-tab" data-toggle="tab" href="#status" role="tab" aria-controls="status" aria-selected="false">Info</a></li> -->
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content p-3" id="myTabContent">
                            <!-- ABA DADOS DO PRODUTO -->
                            <div class="tab-pane fade show active" id="informacao" role="tabpanel" aria-labelledby="informacao-tab">
                                <div class="row">
                                    <div class="col-12">                                        
                                            <form:form action="/update/${produto.id_produto}" method="post" accept-charset="UTF-8">
                                                <div class="card-columns">


                                                    <div class="card">
                                                        <div class="card-header font-weight-bold">Código do
                                                            Produto</div>
                                                        <div class="card-body">
                                                            <p class="card-text">
                                                                <b>Código:</b> ${produto.id_produto}
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="card">
                                                        <div class="card-header font-weight-bold">Produto</div>
                                                        <div class="card-body">
                                                            <p class="card-text">
                                                                <b>Nome:</b><br /> <input type="text" class="form-control" value="${produto.nome}" name="nome" readonly>
                                                            </p>
                                                            <p class="card-text">
                                                                <b>Descrição:</b><br />
                                                                <textarea type="text" rows="3" class="form-control" name="descricao" readonly>${produto.descricao}</textarea>
                                                            </p>
                                                            <p class="card-text">
                                                                <b>Marca:</b><br /> <input type="text" class="form-control" value="${produto.marca}" name="marca" readonly>
                                                            </p>
                                                            <p class="card-text">
                                                                <b>NCM:</b><br /> <input type="text" class="form-control" value="${produto.id_ncm}" name="id_ncm" readonly>
                                                            </p>
                                                            <p class="card-text">
                                                                <b>EAN:</b><br /> <input type="text" class="form-control" value="${produto.codigo_gtin}" name="codigo_gtin" readonly>
                                                            </p>
                                                            <p class="card-text">
                                                                <b>Cor:</b><br /> <input type="text" class="form-control" value="${produto.cor}" name="cor" readonly>
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="card">
                                                        <div class="card-header font-weight-bold">Medidas</div>
                                                        <div class="card-body">
                                                            <p class="card-text">
                                                                <b>Altura (cm):</b><br /> <input type="text" class="form-control" value="${produto.altura} " name="altura" readonly>
                                                            </p>
                                                            <p class="card-text">
                                                                <b>Largura (cm):</b><br /> <input type="text" class="form-control" value="${produto.largura}" name="largura" readonly>
                                                            </p>
                                                            <p class="card-text">
                                                                <b>Comprimento (cm):</b><br /> <input type="text" class="form-control" value="${produto.comprimento}" name="comprimento" readonly>
                                                            </p>
                                                            <p class="card-text">
                                                                <b>Peso (g):</b><br /> <input type="text" class="form-control" value="${produto.peso}" name="peso" readonly>
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="card">
                                                        <div class="card-header font-weight-bold">Estoque</div>
                                                        <div class="card-body">
                                                            <p class="card-text">
                                                                <b>Estoque:</b><br /> <input type="text" class="form-control" value="${produto.estoque}" name="estoque" readonly>
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="card">
                                                        <div class="card-header font-weight-bold">Preços</div>
                                                        <div class="card-body">
                                                            <p class="card-text">
                                                                <b>Promoção (R$):</b><br /> <input type="text" class="form-control" value="${produto.preco_promocao}" name="preco_promocao" readonly>
                                                            </p>
                                                            <p class="card-text">
                                                                <b>Preço (R$):</b><br /> <input type="text" class="form-control" value="${produto.preco_venda}" name="preco_venda" readonly>
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="card">
                                                        <div class="card-header font-weight-bold">Imagem</div>
                                                        <div class="card-body">
                                                            <p class="card-text">
                                                                <img class="w-25" src="${produto.imagem}">
                                                                <!-- <div class="form-group col-md-6 arquivo">
                                                                    <input type="file" name="sumario" class="form-control-file" id="formControlFile">
                                                                </div> -->
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--  <button type="submit" class="btn btn-success btn-sm w-100 mt-3 sombraVerde">
                                                    <i class="fa fa-file mr-2"></i>Atualizar Dados</button>-->
                                            </form:form>                                        
                                    </div>
                                </div>
                                <!-- <button type="submit" class="btn btn-success btn-sm w-100 mt-3 sombraVerde"><i class="fa fa-file mr-2"></i>Atualizar Dados</button> -->

                            </div>

                            <!-- ABA IMAGENS -->
                            <!-- <div class="tab-pane fade" id="detalhe" role="tabpanel" aria-labelledby="detalhe-tab">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card-columns">
                                            <div class="card">
                                                IMAGEM
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>-->

                            <!-- ABA INFO -->
                            <!-- <div class="tab-pane fade" id="status" role="tabpanel" aria-labelledby="status-tab">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card-columns">
                                            <div class="card">
                                                INFO
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->                             
                        </div>
                        <!-- Fim Tab panes -->

                    </div>
                    	<div class="card-footer small text-muted">Atualizado em ${atualiza}</div>	                    
                </div>
                <!-- /.container-fluid -->
                <!-- Rodape -->
                <%@include file="/WEB-INF/views/modulos/rodape.jsp"%>
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /#wrapper -->
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top"> <i class="fas fa-angle-up"></i></a>

        <!-- Bootstrap core JavaScript-->
        <script src="${vendorPath}/jquery/jquery.min.js"></script>
        <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- abre submenu recolhido quando hover-->
        <script src="${jsPath}/abreMenu.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
        <!-- Page level plugin JavaScript-->
        <!-- <script src="${vendorPath}/chart.js/Chart.min.js"></script> -->
        <script src="${vendorPath}/datatables/jquery.dataTables.js"></script>
        <script src="${vendorPath}/datatables/dataTables.bootstrap4.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="${jsPath}/sb-admin.min.js"></script>
        <!-- Demo scripts for this page-->
        <script src="${jsPath}/demo/datatables-demo.js"></script>
        <!-- <script src="${jsPath}/demo/chart-area-demo.js"></script> -->
        <!-- Plugin para aplicação de máscaras -->
        <script src="${jsPath}/jquery.mask.js"></script>
        <script src="${jsPath}/mascara.js"></script>

        <script>
            $('document').ready(function() {
                $('.status').each(function() {
                    if ($(this).text() === "DESATIVADO") {
                        $(this).addClass('vermelho');
                    } else if ($(this).text() === "PENDENTE") {
                        $(this).addClass('amarelo');
                    } else {
                        $(this).addClass('verde');
                    }
                })
            });
        </script>

</body>

</html>