<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <c:url value="/resources/img" var="imgPath" />
    <c:url value="/resources/css" var="cssPath" />
    <c:url value="/resources/js" var="jsPath" />
    <c:url value="/resources/vendor" var="vendorPath" />

    <link rel="icon" href="${imgPath}/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
    <link rel="manifest" href="${imgPath}/site.webmanifest">
    <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Gestão de Produtos - Casa América</title>
    <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
    <link rel="stylesheet" href="${cssPath}/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css">
    <!-- Page level plugin CSS-->
    <link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="${cssPath}/sb-admin.css" rel="stylesheet">
    <link href="${cssPath}/jquery-ui.css" rel="stylesheet">

    <!-- jPList Core -->
    <script src="${vendorPath}/jquery/jquery.min.js"></script>
    <link href="${cssPath}/jplist.demo-pages.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.core.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.filter-toggle-bundle.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.pagination-bundle.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.history-bundle.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.textbox-filter.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.jquery-ui-bundle.min.css" rel="stylesheet" type="text/css" />

    <script src="${jsPath}/jquery-ui.js"></script>
    <script src="${jsPath}/jplist.core.min.js"></script>
    <script src="${jsPath}/jplist.filter-toggle-bundle.min.js"></script>
    <script src="${jsPath}/jplist.history-bundle.min.js"></script>
    <script src="${jsPath}/jplist.jquery-ui-bundle.min.js"></script>
    <script src="${jsPath}/jplist.pagination-bundle.min.js"></script>
    <script src="${jsPath}/jplist.sort-bundle.min.js"></script>
    <script src="${jsPath}/jplist.textbox-filter.min.js"></script>

    <script>
        $('document').ready(function () {
        /**
        * jPList
        */

        $('#tabelaProdutos').jplist({

            itemsBox: '.demo-tbl tbody'
            , itemPath: '.tbl-item'
            , panelPath: '.jplist-panel'

            //save plugin state
            , storage: 'localstorage' //'', 'cookies', 'localstorage'			
            , storageName: 'jplist-table-3'
            , cookiesExpiration: -1

            , redrawCallback: function () {

                $('.tbl-item').each(function (index, el) {
                    if (index % 2 === 0) {
                        $(el).removeClass('odd').addClass('even');
                    }
                    else {
                        $(el).removeClass('even').addClass('odd');
                    }
                });
                
                $('.status').each(function(){
                    if ($(this).text() === "DESATIVADO") {
                        $(this).addClass('vermelho');
                    } else  if ($(this).text() === "PENDENTE") {
                    	$(this).addClass('amarelo');
                    } else if ($(this).text() === "ATIVO") {
                        $(this).addClass('verde');
                    }
                })
                
                $(".marketplace:contains('MARKETPLACE')").addClass("verde");

                //adiciona classe com a sigla dos marketplaces
                // $('.mktplace').each(function(){
                //     $(this).addClass($(this).text().substring(0, 3));
                //     $(".mktplace:contains('csmr')").addClass("CSMR");
                // }); 

            }
        });
        
        //resetar filtros quando carrega a página
        $('.jplist-reset-btn').click();
    });
	</script>

</head>

<body id="page-top" class="gestaoProdutos">
    <!-- Menu Topo -->
    <%@include file="/WEB-INF/views/modulos/menu.jsp" %>

    <div id="wrapper">
        <!-- Sidebar -->
        <%@include file="/WEB-INF/views/modulos/sidebar.jsp" %>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        <a href="/">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Gestão de Produtos</li>
                </ol>

                <!-- TABELA DE PEDIDOS -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i> Produtos
                        <p class="float-right m-0 font-weight-bold px-2">Desativados: <span class="font-weight-normal">${totalInativo}</span></p>
                        <p class="float-right m-0 font-weight-bold px-2">Ativos: <span class="font-weight-normal">${totalAtivo}</span></p>
                        <p class="float-right m-0 font-weight-bold px-2">Pendentes: <span class="font-weight-normal">${totalPendente}</span></p>
                    </div>
                        
                    <div class="card-body">

                        <!-- demo -->
                        <div id="tabelaProdutos" class="box jplist table-layout-2">

                            <!-- ios button: show/hide panel -->
                            <div class="jplist-ios-button">
                                <i class="fa fa-sort"></i>
                                Filtros
                            </div>

                            <!-- panel -->
                            <div class="jplist-panel box panel-top">

                                <!-- reset button -->
                                <button type="button" class="jplist-reset-btn" data-control-type="reset" data-control-name="reset" data-control-action="reset">
                                    Resetar &nbsp;<i class="fa fa-share"></i>
                                </button>

                                <!-- items per page dropdown -->
                                <div class="jplist-drop-down" data-control-type="items-per-page-drop-down" data-control-name="paging" data-control-action="paging">

                                    <ul>
                                        <li><span data-number="10"> 10 por página </span></li>
                                        <li><span data-number="25" data-default="true"> 25 por página </span></li>
                                        <li><span data-number="50"> 50 por página </span></li>
                                        <li><span data-number="all"> Ver todos </span></li>
                                    </ul>
                                </div>

                                <!-- sort dropdown -->
                                <div class="jplist-drop-down" data-control-type="sort-drop-down" data-control-name="sort" data-control-action="sort" data-datetime-format="{day}/{month}/{year}">
                                    <!-- {year}, {month}, {day}, {hour}, {min}, {sec} -->

                                    <ul>
                                        <li><span data-path="default">Ordenar por</span></li>
                                        <li><span data-path=".title" data-order="asc" data-type="number">Cód. Prod. cresc</span></li>
                                        <li><span data-path=".title" data-order="desc" data-type="number" data-default="true">Cód. Prod. decres</span></li>
                                        <li><span data-path=".desc" data-order="asc" data-type="text">Produto A-Z</span></li>
                                        <li><span data-path=".desc" data-order="desc" data-type="text">Produto Z-A</span></li>
                                    </ul>
                                </div>

                                <!-- filter by title -->
                                <div class="text-filter-box">

                                    <i class="fa fa-search  jplist-icon"></i>

                                    <!--[if lt IE 10]>
                                <div class="jplist-label">Filter by Title:</div>
                                <![endif]-->

                                    <input data-path=".title" type="text" value="" placeholder="Buscar código" data-control-type="textbox" data-control-name="title-filter" data-control-action="filter" />
                                </div>

                                <!-- filter by description -->
                                <div class="text-filter-box">

                                    <i class="fa fa-search  jplist-icon"></i>

                                    <!--[if lt IE 10]>
                                <div class="jplist-label">Filter by Description:</div>
                                <![endif]-->

                                    <input data-path=".desc" type="text" value="" placeholder="Buscar produto" data-control-type="textbox" data-control-name="desc-filter" data-control-action="filter" />
                                </div>


                                <!-- checkbox text filter -->
                                <div class="jplist-group filtroStatus" data-control-type="checkbox-text-filter" data-control-action="filter" data-control-name="like" data-path=".like" data-logic="or"><span>Filtrar por status:</span>

                                    <input value="ativo" id="ativo" type="checkbox" class="ml-2" />
                                    <label for="ATIVO">Ativo</label>

                                    <input value="desativado" id="inativo" type="checkbox" class="ml-2" />
                                    <label for="Desativado">Desativado</label>
                                    
                                    <input value="pendente" id="pendente" type="checkbox" class="ml-2" />
                                    <label for="PENDENTE">Pendente</label>

                                </div>


                            </div>

                            <!-- data -->
                            <div class="box text-shadow">
                                <table class="demo-tbl">
                                    <thead>
                                        <tr>
                                            <th>Imagem do Produto</th>
                                            <th>Código do Produto</th>
                                            <th>Nome do Produto</th>
                                            <th>Marketplace</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- item 1 -->
                                        <c:forEach items="${produtos}" var="produtos">
                                            <tr class="tbl-item">

                                                <!-- data -->
                                                <td class="td-block">
                                                	<!-- <img src="<c:url value="${produtos.imagem}" />" /> -->
                                                    <p><img class="imgProduto" src="<c:url value='${produtos.imagem}'/>"></p>
                                                </td>

                                                <td class="td-block">
                                                    <p class="title">${produtos.id_produto}</p>
                                                </td>

                                                <td class="td-block">
                                                    <p class="desc"><a href="<c:url value='detalhes'/>/${produtos.id_produto}">${produtos.nome}</a></p>
                                                </td>

                                                <td class="td-block">
                                                    <p class="mktplace vtexLogo">aaaaa</p>
                                                </td>

                                                <td class="td-block">
                                                    <p class="like"><span class="status">${produtos.status}</span></p>
                                                </td>
                                            </tr>
                                        </c:forEach>

                                    </tbody>
                                </table>
                            </div>
                            <!-- end of data -->

                            <div class="box jplist-no-results text-shadow align-center">
                                <p>Nenhum resultado encontrado.</p>
                            </div>

                            <!-- ios button: show/hide panel -->
                            <div class="jplist-ios-button">
                                <i class="fa fa-sort"></i>
                                Paginação
                            </div>

                            <!-- panel -->
                            <div class="jplist-panel box panel-bottom">

                                <div class="paginacaoBase float-right">
                                    <!-- pagination results -->
                                    <div class="jplist-label" data-type="{start} - {end} de {all}" data-control-type="pagination-info" data-control-name="paging" data-control-action="paging">
                                    </div>

                                    <!-- pagination -->
                                    <div class="jplist-pagination" data-control-type="pagination" data-control-name="paging" data-control-action="paging" data-control-animate-to-top="false">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- end of demo -->

                    </div>

                    <div class="card-footer small text-muted">Atualizado em ${atualiza}</div>

                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- Rodape -->
            <%@include file="/WEB-INF/views/modulos/rodape.jsp" %>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- abre submenu recolhido quando hover-->
    <script src="${jsPath}/abreMenu.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <!-- <script src="${vendorPath}/chart.js/Chart.min.js"></script> -->
    <script src="${vendorPath}/datatables/jquery.dataTables.js"></script>
    <script src="${vendorPath}/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="${jsPath}/sb-admin.min.js"></script>
    <!-- Demo scripts for this page-->
    <script src="${jsPath}/demo/datatables-demo.js"></script>
    <!-- <script src="${jsPath}/demo/chart-area-demo.js"></script> -->

    <!-- filtro por data de inicio e data final-->
    <!-- <script src="${jsPath}/dateRange.js"></script> -->

    <!-- adiciona classe de status-->
    <!-- <script src="${jsPath}/status.js"></script> -->

    <!-- Plugin para aplicação de máscaras -->
    <script src="${jsPath}/jquery.mask.js"></script>
    <!-- <script src="${jsPath}/mascara.js"></script> -->

    <!-- colorindo itens ativos do menu -->
    <script>
        $(document).ready(function() {
            $(".sidebar li .dropdown-toggle:contains('Produtos')").addClass("dropdownAtivo");
            $(".sidebar li .dropdown-menu .dropdown-item:contains('Gestão de Produtos')").addClass("itemAtivo");
        });
    </script>

</body>

</html>