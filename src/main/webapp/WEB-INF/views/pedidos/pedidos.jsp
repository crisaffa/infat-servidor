<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="charset=UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<c:url value="/resources/img" var="imgPath" />
<c:url value="/resources/css" var="cssPath" />
<c:url value="/resources/js" var="jsPath" />
<c:url value="/resources/vendor" var="vendorPath" />

<link rel="icon" href="${imgPath}/favicon.ico">
<link rel="apple-touch-icon" sizes="144x144"
	href="${imgPath}/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32"
	href="${imgPath}/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16"
	href="${imgPath}/favicon-16x16.png">
<link rel="manifest" href="${imgPath}/site.webmanifest">
<link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg"
	color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<title>Visualização de Pedido - Casa América</title>
<link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
<link rel="stylesheet" href="${cssPath}/style.css" />
<link rel="stylesheet" href="${cssPath}/imprimirModal.css" />
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700"
	rel="stylesheet">

<!-- Custom fonts for this template-->
<link href="${vendorPath}/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="${vendorPath}/datatables/dataTables.bootstrap4.css"
	rel="stylesheet">
<!-- Custom styles for this template-->
<link href="${cssPath}/sb-admin.css" rel="stylesheet">
</head>

<body id="page-top" class="pedidos">
	<!-- Menu Topo -->
	<%@include file="/WEB-INF/views/modulos/menu.jsp"%>

	<div id="wrapper">
		<!-- Sidebar -->
		<%@include file="/WEB-INF/views/modulos/sidebar.jsp"%>
		<div id="content-wrapper">
			<div class="container-fluid">
				<!-- Breadcrumbs-->
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a
						href="/pedidos/gestaoPedidos">Gestão de Pedidos</a></li>
					<li class="breadcrumb-item active">Visualização de Pedido</li>
				</ol>
				<!-- Vizualização de Pedido -->
				<div class="card mb-3">
					<div class="card-header">
						<i class="fas fa-table"></i> Visualização de Pedido
						<!-- <a class="btn btn-sm btn-primary float-right" href="#" role="button" onclick="window.print();return false;"><i class="fas fa-print"></i> Imprimir</a> -->
						<a class="btn btn-sm btn-primary float-right sombraAzul" href="#"
							data-toggle="modal" data-target="#imprimirModal"><i
							class="fas fa-print mr-2"></i>Imprimir Pedido</a>
					</div>
					<div class="msgFaturando">${faturando}</div>
					<div class="card-body">
						<!-- ABAS DE ITENS -->
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item"><a class="nav-link active"
								id="home-tab" data-toggle="tab" href="#informacao" role="tab"
								aria-controls="home" aria-selected="true">Informações</a></li>
							<li class="nav-item"><a class="nav-link" id="contact-tab"
								data-toggle="tab" href="#detalhe" role="tab"
								aria-controls="contact" aria-selected="false">Detalhes do
									Faturamento</a></li>
							<li class="nav-item"><a class="nav-link" id="status-tab"
								data-toggle="tab" href="#status" role="tab"
								aria-controls="status" aria-selected="false">Nota Fiscal</a></li>
							<li class="nav-item"><a class="nav-link" id="devolucao-tab"
								data-toggle="tab" href="#devolucao" role="tab"
								aria-controls="devolucao" aria-selected="false">Nota Fiscal de Devolução</a></li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content p-3" id="myTabContent">
							<!-- ABA INFORMAÇÕES -->
							<div class="tab-pane fade show active" id="informacao"
								role="tabpanel" aria-labelledby="informacao-tab">
								<div class="row">
									<div class="col-12">
										<div class="card-columns">
											<div class="card">
												<div class="card-header font-weight-bold">Dados do
													Pedido</div>
												<div class="card-body">
													<c:forEach items="${pedidos}" var="pedidos">
														<p class="card-text">
															<b>Código do pedido:</b> ${pedidos.id_order}
														</p>
														<p class="card-text">
															<b>Data/Hora de criação:</b> ${pedidos.data}
														</p>
														<p class="card-text">
															<b>Código do marketplace:</b> ${pedidos.orderId}
														</p>
													</c:forEach>
												</div>
											</div>

											<div class="card">
												<div class="card-header font-weight-bold">Dados da
													Entrega</div>
												<div class="card-body">
													<c:forEach items="${cliente}" var="cliente">
														<p class="card-text">
															<b>Nome do Recebedor:</b> ${cliente.nome_recebedor}
														</p>
														<p class="card-text">
															<b>CEP:</b> ${cliente.cep}
														</p>
														<p class="card-text">
															<b>Endereço:</b> ${cliente.endereco}
														</p>
														<p class="card-text">
															<b>Número:</b> ${cliente.numero}
														</p>
														<p class="card-text">
															<b>Bairro:</b> ${cliente.bairro}
														</p>
														<p class="card-text">
															<b>Cidade:</b> ${cliente.cidade}
														</p>
														<p class="card-text">
															<b>Estado:</b> ${cliente.estado}
														</p>
														<p class="card-text">
															<b>Complemento:</b> ${cliente.complemento}
														</p>
														<p class="card-text">
															<b>Ponto de referência:</b> ${cliente.reference}
														</p>
													</c:forEach>
												</div>
											</div>
											<div class="card">
												<div class="card-header font-weight-bold">Dados do
													Cliente</div>
												<div class="card-body">
													<c:forEach items="${cliente}" var="cliente">
														<p class="card-text">
															<b>Nome do cliente/empresa:</b> ${cliente.nome}
														</p>
														<p class="card-text">
															<b>CPF do cliente/CNPJ da empresa:</b> ${cliente.cpf}
														</p>
														<p class="card-text">
															<b>Telefone:</b> <span class="phone">${cliente.telefone}</span>
														</p>
													</c:forEach>
												</div>
											</div>

											<div class="card">
												<div class="card-header font-weight-bold">Total do
													Pedido</div>
												<div class="card-body">
													<c:forEach items="${total}" var="total">
														<p class="card-text">
															<b>Valor total:</b> R$ ${total.valortotal}
														</p>
														<p class="card-text">
															<b>Frete total:</b> R$ ${total.fretetotal}
														</p>
														<p class="card-text">
															<b>Desconto total:</b> R$ ${total.descontototal}
														</p>
														<p class="card-text">
															<b>Taxa total:</b> R$ ${total.taxa_total}
														</p>
														<p class="card-text">
															<b>Total nota fiscal:</b> R$ ${total.valorNF}
														</p>
													</c:forEach>
												</div>
											</div>
										</div>
									</div>

									<div class="col-12 produtosPedido">
										<div class="card">
											<div class="card-header font-weight-bold">Produtos do Pedido (${quantidade})
											
<!--                                                <a class="btn btn-sm btn-primary float-right sombraAzul" href="#" data-target="#imprimirProdutos"><i class="fas fa-print mr-2"></i>Imprimir Produtos</a>-->
                                                
                                                 <!--  <button type="button" class="btn btn-sm btn-primary float-right sombraAzul" onclick="printJS('imprimirProdutos', 'html')">
                                                    <i class="fas fa-print mr-2"></i>Imprimir Produtos
                                                 </button>-->	
								            </div>
											<div class="card-body">
												<div class="row">
													<c:forEach items="${produtos}" var="produtos">
														<div class="col-3">
															<img src="<c:url value=" ${produtos.imagem}" />" />
															<p class="card-text">
																<b>Nome do produto:</b> ${produtos.descricao}
															</p>
															<p class="card-text">
																<b>Id do SKU: </b>${produtos.id_produto}</p>
															<p class="card-text">
																<b>Preço:</b> R$ ${produtos.preco}
															</p>
															<p class="card-text">
																<b>Frete:</b> ${produtos.frete}
															</p>
															<p class="card-text">
																<b>Quantidade:</b> ${produtos.quantidade}
															</p>
															<p class="card-text">
																<b>Peso (g):</b> ${produtos.peso}
															</p>															
														</div>
													</c:forEach>
												</div>
											</div>
										</div>
									</div>
								</div>
								<c:forEach items="${pedidos}" var="pedidos">
									<c:choose>
										<c:when test="${(pedidos.status=='CANCELANDO')||(pedidos.status=='CANCELADO')||(pedidos.status=='NFE EMITIDA')||(pedidos.status=='PROCESSANDO')}">
											<button class="btn btn-sm btn-danger mt-3 w-100 sombraVermelho" disabled><i class="fa fa-ban mr-2"></i>Cancelar Pedido</button>
										</c:when>
										<c:otherwise>
										    <button class="btn btn-danger btn-sm w-100 mt-3 sombraVermelho" data-toggle="modal" data-target="#cancelarPedidoModal">
										    <i class="fa fa-ban mr-2"></i>Cancelar Pedido
                                            </button>																					
										</c:otherwise>
									</c:choose>
								</c:forEach>
								
								<!-- Imprimir produtos do pedido -->
                                <div class="row hide">
                                   <div class="col-12">
                                       <div id="imprimirProdutos">
                                            <div class="row">
                                                <div class="col-12">
                                                    <c:forEach items="${cliente}" var="cliente">
                                                        <p>
                                                            <b>Nome do cliente/empresa:</b> ${cliente.nome}
                                                        </p>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-12">
                                                    <c:forEach items="${pedidos}" var="pedidos">
                                                        <p class="card-text">
                                                            <b>Código do pedido:</b> ${pedidos.id_order}
                                                        </p>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                            
                                           <hr>                                  
                                            
                                           <c:forEach items="${produtos}" var="produtos">
                                               <div class="row">
                                                    <div class="col-12">
                                                        <p>
                                                            <b>Nome do produto:</b> ${produtos.descricao}
                                                        </p>
                                                        <p>
                                                            <b>Id do SKU: </b>${produtos.id_produto}</p>
                                                        <p>
                                                            <b>Preço:</b> ${produtos.preco}
                                                        </p>
                                                        <p>
                                                            <b>Frete:</b> ${produtos.frete}
                                                        </p>
                                                        <p>
                                                            <b>Quantidade:</b> ${produtos.quantidade}
                                                        </p>
                                                    </div>
                                                </div>
                                                <hr>
                                            </c:forEach>
                                        </div>
                                   </div>
                                </div>
							</div>

							<!-- ABA DETALHES DO FATURAMENTO -->
							<div class="tab-pane fade" id="detalhe" role="tabpanel" aria-labelledby="detalhe-tab">
								<form:form id="formDetalhesFaturamento">
								<div class="row">
									<div class="col">
										<div id="accordion">

											<!-- INFORMAÇOES GERAIS -->
											<div class="card">
												<div class="card-header" id="headingInformGeral">
													<h5 class="mb-0">
														<a role="button" class="btn btn-link collapsed"
															data-toggle="collapse" data-target="#collapseInformGeral"
															aria-expanded="false" aria-controls="collapseInformGeral">
															<i class="fa fa-plus mr-2" aria-hidden="true"></i>Informações
															Gerais</a>														
													</h5>
												</div>

												<div id="collapseInformGeral" class="collapse"
													aria-labelledby="headingInformGeral"
													data-parent="#accordion">
													<div class="card-body">
														<div class="row">
															<c:forEach items="${pedidos}" var="pedidos">
																<div class="col-4">
																	<p class="card-text">
																		<b>Número do pedido:</b> ${pedidos.id_order}
																	</p>
																	<p class="card-text">
																		<b>Data:</b> ${pedidos.data}
																	</p>
																	<p class="card-text">
																		<b>Tipo de Local de Destino:</b> Operação
																		Interestadual
																	</p>
																</div>
																<div class="col-4">
																	<p class="card-text">
																		<b>Tipo da Nota Fiscal: </b> Nota de Saída
																	</p>
																	<p class="card-text">
																		<b>Finalidade da Nota Fiscal:</b> Nota Normal
																	</p>
																	<p class="card-text">
																		<b>Presença do Comprador:</b> Não presencial, compra
																		pela Internet
																	</p>
																</div>
																<div class="col-4">
																	<p class="card-text">
																		<b>Natureza da Operação:</b> Venda de Produtos pela
																		Internet
																	</p>
																	<p class="card-text">
																		<b>Forma de Pagamento:</b> À Vista
																	</p>
																</div>
															</c:forEach>
														</div>
													</div>
												</div>
											</div>
											<!-- FIM DAS INFORMAÇOES GERAIS -->

											<!-- EMISSOR -->
											<div class="card">
												<div class="card-header" id="headingEmissor">
													<h5 class="mb-0">
														<a role="button" class="btn btn-link collapsed"	data-toggle="collapse" href="#collapseEmissor" aria-expanded="false" aria-controls="collapseEmissor"><i	class="fa fa-plus mr-2" aria-hidden="true"></i>Emissor</a>
													</h5>
												</div>
												<div id="collapseEmissor" class="collapse" data-parent="#accordion" aria-labelledby="headingEmissor">
													<div class="card-body">
														<c:forEach items="${empresas}" var="empresas">
															<div class="row">
																<div class="col-4">
																	<p class="card-text">
																		<b>CNPJ:</b> ${empresas.cnpj}
																	</p>
																	<p class="card-text">
																		<b>Inscrição Estadual:</b>
																		${empresas.inscricaoEstadual}
																	</p>
																	<p class="card-text">
																		<b>Razão Social:</b> ${empresas.nome}
																	</p>
																</div>
																<div class="col-4">
																	<p class="card-text">
																		<b>Nome fantasia:</b> ${empresas.nomeFantasia}
																	</p>
																	<p class="card-text">
																		<b>Status da empresa:</b> Ativa
																	</p>
																</div>
																<div class="col-4">
																	<p class="card-text">
																		<b>Natureza jurídica:</b> 126-0 - Fundação Pública de
																		Direito Privado Estdual ou do Distrito Federal
																	</p>
																	<p class="card-text">
																		<b>Regime de Tributação:</b> Normal
																	</p>
																</div>
															</div>

															<br />
															<!-- COMEÇO DO ENDEREÇO -->
															<div id="accordion-1">
																<div class="card">
																	<div class="card-header" id="headingEmissorEnd">
																		<h5 class="mb-0">
																			<a class="collapsed btn btn-link" role="button"	data-toggle="collapse" href="#collapseEmissorEnd"
																				aria-expanded="false" aria-controls="collapseEmissorEnd"> <i class="fa fa-plus mr-2" aria-hidden="true"></i>Endereço
																			</a>
																		</h5>
																	</div>
																	<div id="collapseEmissorEnd" class="collapse" data-parent="#accordion-1" aria-labelledby="headingEmissorEnd">
																		<div class="card-body">
																			<div class="row">
																				<div class="col-6">
																					<p class="card-text">
																						<b>Logradouro: </b> ${empresas.logradouro}
																					</p>
																					<p class="card-text">
																						<b>Número:</b> ${empresas.numero}
																					</p>
																					<p class="card-text">
																						<b>Bairro:</b> ${empresas.bairro}
																					</p>
																				</div>
																				<div class="col-6">
																					<p class="card-text">
																						<b>Cidade:</b> ${empresas.municipio}
																					</p>
																					<p class="card-text">
																						<b>Estado:</b> ${empresas.uf}
																					</p>
																					<p class="card-text">
																						<b>CEP:</b> ${empresas.cep}
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<!-- FIM DO ENDEREÇO -->
														</c:forEach>
													</div>
												</div>
											</div>
											<!-- FIM DO EMISSOR -->

											<!-- COMPRADOR -->
											<div class="card">
												<div class="card-header" id="headingComprador">
													<h5 class="mb-0">
														<a role="button" class="btn btn-link collapsed"
															data-toggle="collapse" href="#collapseComprador"
															aria-expanded="false" aria-controls="collapseComprador"><i
															class="fa fa-plus mr-2" aria-hidden="true"></i>Comprador
														</a>
													</h5>
												</div>
												<div id="collapseComprador" class="collapse"
													data-parent="#accordion" aria-labelledby="headingComprador">
													<div class="card-body">
														<div class="row">
															<c:forEach items="${cliente}" var="cliente">
																<div class="col">
																	<p class="card-text">
																		<b>Nome:</b> ${cliente.nome}
																	</p>
																	<p class="card-text">
																		<b>CPF/CNPJ:</b> ${cliente.cpf}
																	</p>
																	<p class="card-text">
																		<b>Telefone:</b> <span class="phone">${cliente.telefone}</span>
																	</p>
																</div>
															</c:forEach>
														</div>
														<br />
														<!-- COMEÇO DO ENDEREÇO DO COMPRADOR -->
														<div id="accordion-1">
															<div class="card">
																<div class="card-header" id="headingCompradorEnd">
																	<h5 class="mb-0">
																		<a class="collapsed btn btn-link" role="button" data-toggle="collapse" href="#collapseCompradorEnd"	aria-expanded="false" aria-controls="collapseCompradorEnd"> <i class="fa fa-plus mr-2" aria-hidden="true"></i>Endereço</a>
																	</h5>
																</div>
																<div id="collapseCompradorEnd" class="collapse" data-parent="#accordion-1" aria-labelledby="headingCompradorEnd">
																	<div class="card-body">
																		<c:forEach items="${pedidos}" var="pedidos">																			
																				<div class="row">
                                                                                    <div class="col">
                                                                                        <c:forEach items="${cliente}" var="cliente">
                                                                                            <input type="hidden" class="form-control" value="${cliente.cpf}" name="cpf" id="inputCpf" >
                                                                                            <input type="hidden" class="form-control" value="${cliente.nome}" name="nome" id="inputNome" >
                                                                                            <input type="hidden" class="form-control" value="${cliente.telefone}" name="telefone" id="inputTelefone" >
                                                                                            <input type="hidden" class="form-control" value="${cliente.inscricao_estadual}" name="inscricao_estadual" id="inputInscricaoEstadual" >
                                                                                            <input type="hidden" class="form-control" value="${cliente.nome_recebedor}" name="nome_recebedor" id="inputNomeRecebedor" >
                                                                                            <div class="form-row">
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label for="inputEndereco">Logradouro</label>
                                                                                                    <input type="text" class="form-control" value="${cliente.endereco}" name="endereco" id="inputEndereco" placeholder="Logradouro">
                                                                                                </div>
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label for="inputNumero">Número</label>                                                                                                    
                                                                                                    <input type="text" class="form-control" value="${cliente.numero}" name="numero" id="inputNumero" placeholder="Número">
                                                                                                </div>
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label for="inputBairro">Bairro</label>
                                                                                                    <input type="text" class="form-control" value="${cliente.bairro}" name="bairro" id="inputBairro" placeholder="Bairro" required>
                                                                                                </div>
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label for="inputComplemento">Complemento</label>
                                                                                                    <input type="text" class="form-control" value="${cliente.complemento}" name="complemento" id="inputComplemento" placeholder="Complemento">
                                                                                                </div>
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label for="inputReferencia">Ponto de referência</label>
                                                                                                    <input type="text" class="form-control" value="${cliente.reference}" name="reference" id="inputReferencia" placeholder="Ponto de Referência">
                                                                                                </div>
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label for="inputCidade">Cidade</label>
                                                                                                    <input type="text" class="form-control" value="${cliente.cidade}" name="cidade" id="inputCidade" placeholder="Cidade" required>
                                                                                                </div>
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label for="inputEstado">Estado</label>
                                                                                                        <select class="custom-select" name="estado" id="inputEstado">
                                                                                                            <option value="AC">Acre</option>
                                                                                                            <option value="AL">Alagoas</option>
                                                                                                            <option value="AP">Amapá</option>
                                                                                                            <option value="AM">Amazonas</option>
                                                                                                            <option value="BA">Bahia</option>
                                                                                                            <option value="CE">Ceará</option>
                                                                                                            <option value="DF">Distrito Federal</option>
                                                                                                            <option value="ES">Espirito Santo</option>
                                                                                                            <option value="GO">Goiás</option>
                                                                                                            <option value="MA">Maranhão</option>
                                                                                                            <option value="MS">Mato Grosso do Sul</option>
                                                                                                            <option value="MT">Mato Grosso</option>
                                                                                                            <option value="MG">Minas Gerais</option>
                                                                                                            <option value="PA">Pará</option>
                                                                                                            <option value="PB">Paraíba</option>
                                                                                                            <option value="PR">Paraná</option>
                                                                                                            <option value="PE">Pernambuco</option>
                                                                                                            <option value="PI">Piauí</option>
                                                                                                            <option value="RJ">Rio de Janeiro</option>
                                                                                                            <option value="RN">Rio Grande do Norte</option>
                                                                                                            <option value="RS">Rio Grande do Sul</option>
                                                                                                            <option value="RO">Rondônia</option>
                                                                                                            <option value="RR">Roraima</option>
                                                                                                            <option value="SC">Santa Catarina</option>
                                                                                                            <option value="SP">São Paulo</option>
                                                                                                            <option value="SE">Sergipe</option>
                                                                                                            <option value="TO">Tocantins</option>
                                                                                                        </select>	 
                                                                                                        <script>
                                                                                                        (function() {
                                                                                            				document.getElementById('inputEstado').value = '${cliente.estado}';
                                                                                            			})();
                                                                                                        </script>                                                                                                                                                                                                              
                                                                                                </div>
                                                                                                <div class="form-group col-md-6">
                                                                                                    <label for="inputCep">CEP</label>
                                                                                                    <input type="text" class="form-control cep" value="${cliente.cep}" name="cep" id="inputCep" placeholder="CEP" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode))) return true; else return false;">
																								</div>
																								<div class="form-group col-md-12">
																									<p class="text-uppercase">Forma de Envio</p>
																									<div class="form-check form-check-inline">
																										<input class="form-check-input" type="radio" name="inlineRadioOptions" id="radioTransportadora" value="transportadora" checked>
																										<label class="form-check-label" for="radioTransportadora">Transportadora</label>
																									</div>
																									<div class="form-check form-check-inline">
																										<input class="form-check-input" type="radio" name="inlineRadioOptions" id="radioLoja" value="">
																										<label class="form-check-label" for="radioLoja">Retirar na Loja</label>
																									</div>
																								</div>
																								<div class="form-row col-12 px-0" id="inputsTransportadora">
																								<c:choose>
																									<c:when test="${(pedidos.status=='NFE EMITIDA')}">
																										<div class="form-group col-md-4">																										
																											<label for="inputAltura">Altura (cm)</label>
																											<input type="number" class="form-control" name="altura" id="altura" value="${pedidos.altura}" placeholder="Altura da caixa" min="2" max="105" readonly>
																										</div>
																										<div class="form-group col-md-4">
																											<label for="inputLargura">Largura (cm)</label>
																											<input type="number" class="form-control" name="largura" id="largura" value="${pedidos.largura}" min="11" max="105" placeholder="Largura da caixa" readonly>
																										</div>
																										<div class="form-group col-md-4">
																											<label for="inputComprimento">Comprimento (cm)</label>
																											<input type="number" class="form-control" name="comprimento" id="comprimento" value="${pedidos.comprimento}" min="16" max="105" placeholder="Comprimento da caixa" readonly>
																										</div>
																									</c:when>	
																									<c:otherwise>
																										<div class="form-group col-md-4">																										
																											<label for="inputAltura">Altura (cm)</label>
																											<input type="number" class="form-control" name="altura" id="altura" value="${pedidos.altura}" placeholder="Altura da caixa" min="2" max="105">
																										</div>
																										<div class="form-group col-md-4">
																											<label for="inputLargura">Largura (cm)</label>
																											<input type="number" class="form-control" name="largura" id="largura"  value="${pedidos.largura}"min="11" max="105" placeholder="Largura da caixa">
																										</div>
																										<div class="form-group col-md-4">
																											<label for="inputComprimento">Comprimento (cm)</label>
																											<input type="number" class="form-control" name="comprimento" id="comprimento" value="${pedidos.comprimento}" min="16" max="105" placeholder="Comprimento da caixa">
																										</div>
																									</c:otherwise>
																								</c:choose>				
																								</div>
                                                                                            </div>

                                                                                        </c:forEach>
                                                                                    </div>
																				</div>
																				<div class="form-check text-center checkConferido">																					
																					<c:choose>
																						<c:when test="${(pedidos.status=='PREPARANDO ENTREGA')}">
  																							<label class="form-check-label font-weight-bold">
    																							<input class="form-check-input" type="checkbox" name="habi" id="habi" onClick="HabiDsabi()">
    																								Atesto que os dados de Endereço acima foram conferidos.
  																							</label>
  																						</c:when> 
  																						<c:otherwise>
  																							<label class="form-check-label font-weight-bold">
    																							<input class="form-check-input" type="checkbox" name="habi" id="habi" onClick="HabiDsabi()" disabled>
    																								Atesto que os dados de Endereço acima foram conferidos.
  																							</label>
  																						</c:otherwise> 																							
  																					</c:choose>  																					
																				</div>																			
																		</c:forEach>
																	</div>
																</div>
															</div>
														</div>
														<!-- FIM DO ENDEREÇO DO COMPRADOR -->
													</div>
												</div>
											</div>
											<!-- FIM DO COMPRADOR -->

											<!-- NOVO ITENS DA NOTA -->
											<div class="card">
												<div class="card-header" id="headingItensNota">
													<h5 class="mb-0">
														<a role="button" class="btn btn-link collapsed"
															data-toggle="collapse" href="#collapseItensNota"
															aria-expanded="false" aria-controls="collapseItensNota"><i
															class="fa fa-plus mr-2" aria-hidden="true"></i>Itens da
															nota </a>
													</h5>
												</div>
												<div id="collapseItensNota" class="collapse"
													data-parent="#accordion" aria-labelledby="headingItensNota">
													<div class="card-body">
														<!-- COMEÇO DO PRODUTO -->
														<div id="accordion-1">
															<div class="card">
																<c:forEach items="${produtos}" var="produtos">
																	<div class="card-header"
																		id="headingItensNotaProd${produtos.id}">
																		<h5 class="mb-0">
																			<a class="collapsed btn btn-link" role="button"
																				data-toggle="collapse"
																				href="#collapseItensNotaProd${produtos.id}"
																				aria-expanded="false"
																				aria-controls="collapseItensNotaProd${produtos.id}">
																				<i class="fa fa-plus mr-2" aria-hidden="true"></i>${produtos.descricao}
																			</a>
																		</h5>
																	</div>
																	<div id="collapseItensNotaProd${produtos.id}"
																		class="collapse" data-parent="#accordion-1"
																		aria-labelledby="headingItensNotaProd${produtos.id}">
																		<div class="card-body">
																			<div class="row">

																				<div class="col-6">
																					<p class="card-text">
																						<b>Código do Produto: </b>${produtos.id_produto}
																					</p>
																					<p class="card-text">
																						<b>Preço:</b> R$ ${produtos.preco}
																					</p>
																					<p class="card-text">
																						<b>Frete:</b> R$ ${produtos.frete}
																					</p>
																					<p class="card-text">
																						<b>Desconto:</b> R$ ${produtos.desconto}
																					</p>
																					<p class="card-text">
																						<b>Quantidade:</b> ${produtos.quantidade}
																					</p>
																					<p class="card-text">
																						<b>Peso (g):</b> ${produtos.peso}
																					</p>
																				</div>
																			</div>
																		</div>
																	</div>
																</c:forEach>
															</div>
														</div>
														<!-- FIM DO PRODUTO -->
													</div>
												</div>
											</div>
											<!-- FIM DO NOVO ITENS DA NOTA -->

											<!-- VALORES TOTAIS -->
											<div class="card">
												<div class="card-header" id="headingValoresTotais">
													<h5 class="mb-0">
														<a role="button" class="btn btn-link collapsed"
															data-toggle="collapse"
															data-target="#collapseValoresTotais"
															aria-expanded="false"
															aria-controls="collapseValoresTotais">
															<i class="fa fa-plus mr-2" aria-hidden="true"></i>Valores
															Totais</a>														
													</h5>
												</div>

												<div id="collapseValoresTotais" class="collapse"
													aria-labelledby="headingValoresTotais"
													data-parent="#accordion">
													<div class="card-body">
														<div class="row">
															<c:forEach items="${total}" var="total">
																<div class="col">
																	<p class="card-text">
																		<b>Valor total:</b> R$ ${total.valortotal}
																	</p>
																	<p class="card-text">
																		<b>Frete total:</b> R$ ${total.fretetotal}
																	</p>
																	<p class="card-text">
																		<b>Desconto total:</b> R$ ${total.descontototal}
																	</p>
																	<p class="card-text">
																		<b>Desconto total:</b> R$ ${total.taxa_total}
																	</p>
																	<p class="card-text">
																		<b>Total nota fiscal:</b> ${total.valorNF}
																	</p>
																</div>
															</c:forEach>
														</div>
													</div>
												</div>
											</div>
											<!-- FIM DO VALORES TOTAIS -->
										</div>
									</div>
								</div>

								<c:forEach items="${pedidos}" var="pedidos">
									<input type="hidden" class="form-control" value="${pedidos.id_order}" name="id_order" id="inputIdOrder" >
										<c:choose>											
											<c:when test="${(pedidos.status!='PREPARANDO ENTREGA')}">
												<button
													class="btn btn-success btn-sm w-100 mt-3 sombraVerde" disabled><i class="fa fa-file mr-2"></i>Faturar Nota Fiscal
												</button>
											</c:when>
											<c:when test="${(pedidos.status_nota=='AUTORIZADA')||(pedidos.status_nota=='PROCESSANDO_AUTORIZAÇÃO')}">
												<button class="btn btn-success btn-sm w-100 mt-3 sombraVerde" disabled><i class="fa fa-file mr-2"></i>Faturar Nota Fiscal</button>
											</c:when>
											<c:otherwise>
												<button type="submit" class="btn btn-success btn-sm w-100 mt-3 sombraVerde" name="envia" id="envia" disabled><i class="fa fa-file mr-2"></i>Faturar Nota Fiscal</button>
											</c:otherwise>
										</c:choose>									
								</c:forEach>
								</form:form>
							</div>

							<!-- ABA STATUS DA NOTA FISCAL -->
							<div class="tab-pane fade" id="status" role="tabpanel"
								aria-labelledby="status-tab">
								<c:forEach items="${pedidos}" var="pedidos">
									<c:choose>
										<c:when test="${(pedidos.status_nota=='ERRO_AUTORIZAÇÃO')}">
											<div class="alert alert-danger alert-dismissible fade show m-0" role="alert">
												Foram encontrados dados errados na nota.<br />
												${pedidos.msg_sefaz}
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											</div>
										</c:when>
										<c:otherwise>
										</c:otherwise>
									</c:choose>
									<div>
										<br />
										<b>Status da Nota Fiscal:</b> ${pedidos.status_nota}
										<c:choose>
											<c:when test="${(pedidos.status_nota=='AUTORIZADA')}">
												<br />
												<b>Data e Hora da Emissão da Nota Fiscal:</b> ${pedidos.data_emissao}
											</c:when>											
										</c:choose>
									</div>									 
										<c:choose>
											<c:when
												test="${(pedidos.status_nota!='AUTORIZADA')||(pedidos.status=='CANCELANDO')||(pedidos.status=='CANCELADO')||(pedidos.status=='PROCESSANDO')}">
												<button class="btn btn-primary btn-sm w-100 mt-3 sombraAzul" disabled>
													<i class="fa fa-file mr-2"></i>Salvar Nota Fiscal
												</button>
											</c:when>
											<c:otherwise>																																		
												<button id="salvarButton" class="btn btn-primary btn-sm w-100 mt-3 sombraAzul" data-idorder="${pedidos.id_order}" onclick="return salvarPdf()">
													<i class="fa fa-file mr-2"></i>Salvar Nota Fiscal													
												</button> 
											</c:otherwise>
										</c:choose>																																																		
									<c:choose>
										<c:when
											test="${(pedidos.status_nota!='AUTORIZADA')||(pedidos.status=='CANCELANDO')||(pedidos.status=='CANCELADO')||(pedidos.periodo_vencido=='TRUE')||(pedidos.devolucao=='TRUE')}">
											<button
												class="btn btn-danger btn-sm w-100 mt-3 sombraVermelho" disabled>
												<i class="fa fa-ban mr-2"></i>Cancelar Nota Fiscal
											</button>
										</c:when>
										<c:otherwise>										
											<button
												class="btn btn-danger btn-sm w-100 mt-3 sombraVermelho"
												data-toggle="modal" data-target="#cancelarNfModal">
												<i class="fa fa-ban mr-2"></i>Cancelar Nota Fiscal
											</button>
										</c:otherwise>
									</c:choose>									
								</c:forEach>
							</div>
							<!-- Fim da aba de status da nota fiscal -->
							
							<!-- ABA NOTA FISCAL DE DEVOLUÇÃO -->
							<div class="tab-pane fade" id="devolucao" role="tabpanel"
								aria-labelledby="devolucao-tab">
								<c:forEach items="${pedidos}" var="pedidos">																											 
										<c:choose>
										<c:when
											test="${(pedidos.status_nota!='AUTORIZADA')||(pedidos.status=='CANCELADO')||(pedidos.periodo_vencido=='FALSE')||(pedidos.devolucao=='TRUE')}">
											<button
											class="btn btn-success btn-sm w-100 mt-3 sombraVerde" disabled>
											<i class="fa fa-file mr-2"></i>Gerar Nota Fiscal de Devolução
										</button>
									</c:when>
									<c:otherwise>
										<!-- <form:form action="/devolver/${pedidos.id_order}/">										 -->
										<button class="btn btn-success btn-sm w-100 mt-3 sombraVerde"
										onClick="showDevolution()"
											data-toggle="modal" data-target="#devolutionModal">
												<i class="fa fa-file mr-2"></i>Gerar Nota Fiscal de Devolução
											</button>
											<!-- </form:form> -->
										</c:otherwise>
									</c:choose>				
								</c:forEach>
							</div>
							<!-- Fim da aba nota fiscal de devolução -->
						</div>
						<!-- Fim Tab panes -->
					</div>					
						<div class="card-footer small text-muted">Atualizado em ${atualiza}</div>					
				</div>
				<!-- /.container-fluid -->
				<!-- Rodape -->
				<%@include file="/WEB-INF/views/modulos/rodape.jsp"%>
			</div>
			<!-- /.content-wrapper -->
		</div>
		<!-- /#wrapper -->
		<!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top"> <i class="fas fa-angle-up"></i></a>
			
		<!-- salvar nota fiscal -->
		<c:forEach items="${pedidos}" var="pedidos">
            <form:form action="/baixar/${pedidos.id_order}" >
                <div class="modal fade" id="salvarNfModal" tabindex="-1"
                    role="dialog" aria-labelledby="exampleModalCenterTitle"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">						
                                <iframe id="framePdf" src="" width="800" height="600" style="border: none;"></iframe>
                                <button type="button" type="submit" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
		</c:forEach>
		
		
		<!-- cancelar nota fiscal Modal -->
		<div class="modal fade" id="cancelarNfModal" tabindex="-1"
			role="dialog" aria-labelledby="exampleModalCenterTitle"
			aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Escolha o
							motivo do cancelamento</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<c:forEach items="${pedidos}" var="pedidos">
						<form:form action="/cancelar/${pedidos.id_order}/">
							<div class="modal-body">

								<div
									class="custom-control custom-radio custom-radio-cancelamento">
									<input type="radio" id="customRadio1" name="customRadio"
										value="1" class="custom-control-input"
										onclick="showCancelarNF()"> <label
										class="custom-control-label" for="customRadio1">Desistência
										do comprador</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio2" name="customRadio"
										value="2" class="custom-control-input"
										onclick="showCancelarNF()"> <label
										class="custom-control-label" for="customRadio2">Cancelamento
										das vendas</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio3" name="customRadio"
										value="3" class="custom-control-input"
										onclick="showCancelarNF()"> <label
										class="custom-control-label" for="customRadio3">Cálculo
										de preços e impostos incorretos</label>
								</div>
								<div class="custom-control custom-radio">
									<input type="radio" id="customRadio4" name="customRadio"
										value="4" class="custom-control-input"
										onclick="showCancelarNF()"> <label
										class="custom-control-label" for="customRadio4">Erros
										cadastrais, como no CNPJ ou na Razão Social</label>
								</div>
							</div>
							<div class="modal-footer">
								<button class="btn btn-danger btn-sm w-100 mt-3 sombraVermelho"
									id="btnCancelarNF" type="submit" disabled>
									<i class="fa fa-ban mr-2"></i>Confirmar Cancelamento
								</button>
							</div>
						</form:form>
					</c:forEach>
				</div>
			</div>
		</div>
		
		<!-- cancelar Pedido Modal -->
		<div class="modal fade" id="cancelarPedidoModal" tabindex="-1"
			role="dialog" aria-labelledby="exampleModalCenterTitle"
			aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header border-0">
						<h5 class="modal-title" id="exampleModalLongTitle">Confirmar cancelamento</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<c:forEach items="${pedidos}" var="pedidos">
						<form:form action="/pedidos/cancelar/${pedidos.id_order}/${pedidos.orderId}/${pedidos.status}">
							<div class="modal-body">
                                <p class="text-center"><i class="fas fa-exclamation-triangle"></i></p>
                                <p class="text-center font-weight-bold">Atenção: estas ações interferem diretamente no fluxo do pedido e não é possível desfazê-las.</p>

                                <div class="form-check my-1">
                                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                  <label class="form-check-label" for="defaultCheck1">
                                    Eu entendo que vou interferir no andamento do pedido.
                                  </label>
                                </div>
                                <div class="form-check my-1">
                                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                                  <label class="form-check-label" for="defaultCheck2">
                                    Eu entendo que esta ação não pode ser desfeita.
                                  </label>
                                </div>
                                <div class="form-check my-1">
                                  <input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
                                  <label class="form-check-label" for="defaultCheck3">
                                    Eu entendo as consequências dessa ação no fluxo do pedido.
                                  </label>
                                </div>
                                <div class="form-group mt-3">
                                    <textarea class="form-control" name="textAreaMotivoCancelamento" id="exampleFormControlTextarea1" rows="3" placeholder="Motivo de cancelamento"></textarea>
                                </div>
							</div>
							<div class="modal-footer border-0">
								<button class="btn btn-danger btn-sm w-100 mt-3 sombraVermelho"
									id="btnCancelarPedido" type="submit" disabled>
									<i class="fa fa-ban mr-2"></i>Confirmar Cancelamento
								</button>
							</div>
						</form:form>
					</c:forEach>
				</div>
			</div>
		</div>

		<!--Devolução Modal-->
		<div class="modal fade" id="devolutionModal" tabindex="-1"
			role="dialog" aria-labelledby="exampleModalCenterTitle"
			aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header border-0">
						<h5 class="modal-title" id="exampleModalLongTitle">Confirmar Devolução</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<c:forEach items="${pedidos}" var="pedidos">
						<form:form action="/devolver/${pedidos.id_order}/">
					<div class="modal-body">Deseja realmente solicitar devolução do pedido?</div>
					<div class="modal-footer border-0">
						<button class="btn btn-success btn-sm w-100 mt-3 sombraVerde"
							 type="submit"
							 id="btnDevolution"
							 onClick="spinDevolucao()"
							 disabled
							>
							<i class="fa fa-file mr-2"></i>Confirmar Nota Fiscal de devolução
						</button>
					</div>
						</form:form>
					</c:forEach>
				</div>
			</div>
		</div>

		<!--onClick="spinDevolucao();" id="enviaDevolucao"-->
		<!--Fim Devolução Modal-->

		<!-- Imprimir Modal-->
		<div class="modal fade bd-example-modal-lg" id="imprimirModal"
			tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
			aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							<button id="btnPrint" type="button"
								class="btn btn-sm btn-primary">
								<i class="fas fa-print"></i>
							</button>
						</h5>
						<button class="close" type="button" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<table class="table table-bordered table-sm">
							<tr>
								<td colspan="5" class="infoCliente"><c:forEach
										items="${pedidos}" var="pedidos">
                                        ${pedidos.id_order}
                                    </c:forEach> - <c:forEach items="${cliente}"
										var="cliente">
                                        ${cliente.nome}
                                    </c:forEach></td>
							</tr>
							<c:forEach items="${produtos}" var="produtos">
								<tr>
									<!-- <td><img src="<c:url value="${produtos.imagem}"/>" /></td> -->
									<!-- <td class="text-center align-middle" colspan="4"><b>${produtos.descricao}</b></td> -->
									<td colspan="5"><b>${produtos.descricao}</b></td>
								</tr>
								<tr>
									<td><b>Sku Id</b></td>
									<td><b>Código ERP</b></td>
									<td><b>Quantidade</b></td>
									<td><b>Preço Total</b></td>
									<td><b>Frete</b></td>
								</tr>
								<tr>
									<td>${produtos.id_produto}</td>
									<td>${produtos.id_produto}</td>
									<td>${produtos.quantidade}</td>
									<td>${produtos.preco}</td>
									<td>${produtos.frete}</td>
								</tr>
							</c:forEach>

							<tbody>
								<!-- DADOS DO CLIENTE -->
								<tr>
									<td colspan="5" class="py-4 info">Dados do Cliente</td>
								</tr>
								<tr>
									<td colspan="2"><b>Nome do Cliente</b></td>
									<td><b>Cidade</b></td>
									<td><b>Estado</b></td>
									<td><b>País</b></td>
								</tr>
								<c:forEach items="${cliente}" var="cliente">
									<tr>
										<td colspan="2">${cliente.nome}</td>
										<td>${cliente.cidade}</td>
										<td>${cliente.estado}</td>
										<td>BRA</td>
									</tr>
								</c:forEach>
								<tr>
									<td colspan="2"><b>Rua</b></td>
									<td colspan="2"><b>Bairro</b></td>
									<td><b>CEP</b></td>
								</tr>
								<c:forEach items="${cliente}" var="cliente">
									<tr>
										<td colspan="2">${cliente.endereco}</td>
										<td colspan="2">${cliente.bairro}</td>
										<td>${cliente.cep}</td>
									</tr>
								</c:forEach>
								<tr>
									<td colspan="3"><b>Ponto de Referência</b></td>
									<td><b>CPF do Cliente</b></td>
									<td><b>Telefone</b></td>
								</tr>
								<c:forEach items="${cliente}" var="cliente">
									<tr>
										<td colspan="3">${cliente.reference}</td>
										<td>${cliente.cpf}</td>
										<td class="phone">${cliente.telefone}</td>
									</tr>
								</c:forEach>

								<!-- DADOS DO PEDIDO -->
								<tr>
									<td colspan="5" class="py-4 info">Dados do Pedido</td>
								</tr>
								<tr>
									<td><b>Número do Pedido</b></td>
									<td><b>Data de Compra</b></td>
									<td><b>Previsão de Entrega</b></td>
									<td><b>Nº Pedido MKP</b></td>
									<td><b>Frete</b></td>
								</tr>
								<c:forEach items="${pedidos}" var="pedidos">
									<tr>
										<td>${pedidos.id_order}</td>
										<td>xxxxx</td>
										<td>xxxxx</td>
										<td>xxxxx</td>
										<td>xxxxx</td>
									</tr>
								</c:forEach>

								<!-- STATUS DO PEDIDO -->
								<tr>
									<td colspan="5" class="py-4 info">Status do Pedido</td>
								</tr>
								<tr>
									<td><b>Status</b></td>
									<td><b>Nº da Nota Fiscal</b></td>
									<td><b>Série da Nota Fiscal</b></td>
									<td><b>Data Emissão da Nota</b></td>
									<td><b>Data de Envio</b></td>
								</tr>
								<c:forEach items="${pedidos}" var="pedidos">
									<tr>
										<td>xxxxx</td>
										<td>xxxxx</td>
										<td>xxxxx</td>
										<td>xxxxx</td>
										<td>xxxxx</td>
									</tr>
								</c:forEach>
								<tr>
									<td colspan="2"><b>Chave da Nota Fiscal</b></td>
									<td><b>Data Estimada da Entrega</b></td>
									<td><b>Transportadora</b></td>
									<td><b>Rastreio</b></td>
								</tr>
								<c:forEach items="${pedidos}" var="pedidos">
									<tr>
										<td colspan="2">xxxxx</td>
										<td>xxxxx</td>
										<td>xxxxx</td>
										<td>xxxxx</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button"
							data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Bootstrap core JavaScript-->
		<script src="${vendorPath}/jquery/jquery.min.js"></script>
		<script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

		<!-- abre submenu recolhido quando hover-->
		<script src="${jsPath}/abreMenu.js"></script>
		<!-- Core plugin JavaScript-->
		<script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
		<!-- Page level plugin JavaScript-->
		<!-- <script src="${vendorPath}/chart.js/Chart.min.js"></script> -->
		<script src="${vendorPath}/datatables/jquery.dataTables.js"></script>
		<script src="${vendorPath}/datatables/dataTables.bootstrap4.js"></script>
		<!-- Custom scripts for all pages-->
		<script src="${jsPath}/sb-admin.min.js"></script>
		<!-- Demo scripts for this page-->
		<script src="${jsPath}/demo/datatables-demo.js"></script>
		<!-- <script src="${jsPath}/demo/chart-area-demo.js"></script> -->
		<!-- Imprimir detalhes do pedido do modal-->
		<script src="${jsPath}/imprimirModal.js"></script>
		<!-- Plugin para aplicação de máscaras -->
		<script src="${jsPath}/jquery.mask.js"></script>
		<script src="${jsPath}/mascara.js"></script>
		<script src="${jsPath}/print.min.js"></script>

		<script>
		$('#envia').on('click', function (e) {
			e.preventDefault();
			validate();
		});
        function validate() {	

            testEnd = (document.getElementById('inputEndereco').value).replace(/\s/g, "");
            if(testEnd == "" || (document.getElementById("inputEndereco").value).length < 2){
                alert('O campo Logradouro é obrigatório e deve ter no mínimo 2 caracteres');
                document.getElementById("inputEndereco").focus();
                return false;
            }
            testBairro = (document.getElementById('inputBairro').value).replace(/\s/g, "");
            if(testBairro == ""){
                alert('Por favor, preencha o campo Bairro');
                document.getElementById("inputBairro").focus();
                return false;
            }
            testCidade = (document.getElementById('inputCidade').value).replace(/\s/g, "");
            if(testCidade == ""){
                alert('Por favor, preencha o campo Cidade');
                document.getElementById("inputCidade").focus();
                return false;
            }
            testNumero = (document.getElementById('inputNumero').value).replace(/\s/g, "");
            if(testNumero == ""){
                alert('Por favor, preencha o campo Número');
                document.getElementById("inputNumero").focus();
                return false;
            }
            testCep = (document.getElementById('inputCep').value).replace(/\s/g, "");
            if(testCep == ""){
                alert('Por favor, preencha o campo CEP');
                document.getElementById("inputCep").focus();
                return false;
            }

			if($('input[name=inlineRadioOptions]:checked').val() == 'transportadora') {
				testAltura = (document.getElementById('altura').value);
				if(testAltura == "" || testAltura < 2 || testAltura > 105){					
					alert('Por favor, preencha o campo Altura com valor entre 2 e 105cm');
					document.getElementById("altura").focus();
					return false;
				}
				testLargura = (document.getElementById('largura').value);
				if(testLargura == "" || testLargura < 11 || testLargura > 105){
					alert('Por favor, preencha o campo Largura com valor entre 11 e 105cm');
					document.getElementById("largura").focus();
					return false;
				}
				testComprimento = (document.getElementById('comprimento').value);
				if(testComprimento == "" || testComprimento < 16 || testComprimento > 105){
					alert('Por favor, preencha o campo Comprimento com valor entre 16 e 105cm');
					document.getElementById("comprimento").focus();
					console.log(document.getElementById("comprimento").focus())
					return false;
				}
			} 
			
            document.getElementById("envia").innerHTML = "<i class='fas fa-spinner'></i> Faturando";
			
			$('#formDetalhesFaturamento').submit();
        }

		function spinDevolucao(){
			document.getElementById("btnDevolution").innerHTML = "<i class='fas fa-spinner'></i> Gerando";
		}
        
        (function() {
            var faturar = document.getElementById('formDetalhesFaturamento');
            faturar.action = "/faturar/"+document.getElementById('inputIdOrder').value;
            $('#headingComprador').removeClass("bgDanger");
            $('#headingComprador h5 a').removeClass("text-white font-weight-normal");
        })();

        function HabiDsabi(){  
            if(document.getElementById('habi').checked == true){
                document.getElementById('envia').disabled = "";
            }  
            if(document.getElementById('habi').checked == false){
                    document.getElementById('envia').disabled = "disabled"  
            }	
        }
        </script>
        
        <script>
            //checar se os check box do modal de cancelar pedido estao todos marcados para ativar o botao cancelar
		    $("input[type=checkbox]").change(function(){
                if($('#defaultCheck1').is(':checked') && $('#defaultCheck2').is(':checked') && $('#defaultCheck3').is(':checked')) {
                    document.getElementById('btnCancelarPedido').removeAttribute("disabled");
                }
                  else if(!$('#defaultCheck1').is(':checked') || !$('#defaultCheck2').is(':checked') || !$('#defaultCheck3').is(':checked')) {
                    document.getElementById('btnCancelarPedido').setAttribute("disabled", "disabled");
                  }
                
                $('#cancelarPedidoModal').on('hidden.bs.modal', function() {
                    $(this).find('form').trigger('reset');
                    $('#btnCancelarPedido').attr('disabled', 'disabled');

                });
            });
            
			function showCancelarNF() {
				document.getElementById('btnCancelarNF').removeAttribute("disabled");
			}
			$('#cancelarNfModal').on('hidden.bs.modal', function() {
				$(this).find('form').trigger('reset');
				$('#btnCancelarNF').attr('disabled', 'disabled');

			});
			
			function showDevolution() {
				document.getElementById('btnDevolution').removeAttribute("disabled");
			}
			$('#devolutionModal').on('hidden.bs.modal', function() {
				$(this).find('form').trigger('reset');
				$('#devolutionModal').attr('disabled', 'disabled');

			});
            
            //funcao pra pintar de vermelho o bg o accordion Comprador
            var e = $('.checkConferido label input');
            //alert(e.is(':disabled'));
            if (e.is(':disabled')) {
                
            } else {
                $('#headingComprador').addClass("bgDanger");
                $('#headingComprador h5 a').addClass("text-white font-weight-normal");
            }
			
			function salvarPdf(){
				document.getElementById("salvarButton").innerHTML = "<i class='fas fa-spinner'></i> Salvando";
				var id = $('#salvarButton').data('idorder');
				$.ajax({
				    type: 'POST',
				    url: '/baixar/'+id,				    			   
				    success: function(data){
				    	$('#framePdf').attr('src', data)
				    	$('#salvarNfModal').modal('show')
				    	document.getElementById("salvarButton").innerHTML = "<i class='fa fa-file mr-2'></i>Salvar Nota Fiscal";    	
				    },
				    error: function(data){				    	
				    	alert('Nota não está disponível para download!');
				    }
				})								
			}

			//RADIO BUTTON ATIVO NO TRANSPORTADORA MOSTRA O INPUT DE DIMENSÃO DO PACOTE
			$("input[type=radio]").on('change', function(){
				if($(this).val() == '') {
					$("#inputsTransportadora").hide();
					$("#altura").val('');
					$("#largura").val('');
					$("#comprimento").val('');
				} else {
					$("#inputsTransportadora").show();
				}
			});
            
		</script>

		<!-- colorindo itens ativos do menu -->
		<script>
			$(document).ready(function() {
				$(".sidebar li .dropdown-toggle:contains('Pedidos')").addClass("dropdownAtivo");
				$(".sidebar li .dropdown-menu .dropdown-item:contains('Gestão de Pedidos')").addClass("itemAtivo");
			});
		</script>
</body>

</html>
