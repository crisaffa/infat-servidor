<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <c:url value="/resources/img" var="imgPath" />
    <c:url value="/resources/css" var="cssPath" />
    <c:url value="/resources/js" var="jsPath" />
    <c:url value="/resources/vendor" var="vendorPath" />

    <link rel="icon" href="${imgPath}/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
    <link rel="manifest" href="${imgPath}/site.webmanifest">
    <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Relatório Pedidos - Casa América</title>
    <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
    <link rel="stylesheet" href="${cssPath}/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css">
    <!-- Page level plugin CSS-->
    <link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="${cssPath}/sb-admin.css" rel="stylesheet">
    <link href="${cssPath}/jquery-ui.css" rel="stylesheet">

    <!-- jPList Core -->
    <script src="${vendorPath}/jquery/jquery.min.js"></script>
    <script src="${jsPath}/jquery-ui.js"></script>

</head>

<body id="page-top" class="separarPedidos">
    <!-- Menu Topo -->
    <%@include file="/WEB-INF/views/modulos/menu.jsp" %>

    <div id="wrapper">
        <!-- Sidebar -->
        <%@include file="/WEB-INF/views/modulos/sidebar.jsp" %>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        <a href="/">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Relatório de Pedidos</li>
                </ol>

                <!-- TABELA DE PEDIDOS -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i> Relatório
                    </div>

					<div class="card-body">
                    
                    <div class="msgFaturando">${gerando}</div>
                    
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item"><a class="nav-link active" id="home-tab" data-toggle="tab"
                                    href="#relatorio1" role="tab" aria-controls="home" aria-selected="true">Marketplace</a></li>
                            <li class="nav-item"><a class="nav-link" id="r2-tab" data-toggle="tab" href="#relatorio2"
                                    role="tab" aria-controls="r2">Tipo de Pagamento</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content p-3" id="myTabContent">
                            <!-- ABA Relatório 1 -->
                            <div class="tab-pane fade show active" id="relatorio1" role="tabpanel"
                                aria-labelledby="relatorio1-tab">
                                <div class="row">
                                    <div class="col-12">

                                        <form:form action="/relatorio/marketplaces">
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label>Data de Emissão Inicial:</label>
                                                    <input type="date" class="form-control" name="dataInicial"
                                                        id="inputDataInicial">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Data de Emissão Final:</label>
                                                    <input type="date" class="form-control" name="dataFinal"
                                                        id="inputDataFinal">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="inputMarketplace">Marketplace:</label>
                                                    <select class="custom-select mr-sm-2" id="customRadio"
                                                        name="customRadio">
                                                        <option value="Todos" selected>Todos</option>
                                                        <option value="CSU">CSU</option>
                                                        <option value="America">Casa América</option>
                                                        <option value="Dia">Dia</option>
                                                        <option value="MadeiraMadeira">MadeiraMadeira</option>
                                                        <option value="Zoom">Zoom</option>
                                                        <option value="Carrefour">Carrefour</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <button type="submit" id="btnGerar"
                                                class="btn btn-success btn-sm w-100 mt-3 sombraVerde"
                                                onClick="spinner();">
                                                <i class="fa fa-file mr-2"></i>Gerar e enviar por e-mail
                                            </button>
                                        </form:form>

                                    </div>
                                </div>
                            </div>

                            <!-- ABA Relatório 2 -->
                            <div class="tab-pane fade show" id="relatorio2" role="tabpanel"
                                aria-labelledby="relatorio2-tab">
                                <div class="row">
                                    <div class="col-12">

                                        <form:form action="/relatorio/tipoPagamento">
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label>Data de Emissão Inicial:</label>
                                                    <input type="date" class="form-control" name="dataInicial"
                                                        id="inputDataInicial">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Data de Emissão Final:</label>
                                                    <input type="date" class="form-control" name="dataFinal"
                                                        id="inputDataFinal">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="inputMarketplace">Tipo de Pagamento:</label>
                                                    <select class="custom-select mr-sm-2" id="customRadio"
                                                        name="customRadio">
                                                        <option value="Todos" selected>Todos</option>
                                                        <option value="Boleto">Boleto Bancário</option>
                                                        <option value="Mercado">Mercado Pago</option>
                                                        <option value="PayPal">PayPal</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <button type="submit" id="btnGerar"
                                                class="btn btn-success btn-sm w-100 mt-3 sombraVerde"
                                                onClick="spinner();">
                                                <i class="fa fa-file mr-2"></i>Gerar e enviar por e-mail
                                            </button>
                                        </form:form>

                                    </div>
                                </div>
                            </div>
                        </div>                        

                    </div>
                    <div class="card-footer small text-muted">Atualizado em ${atualiza}</div>
                </div>

            </div>
        </div>
        <!-- /.container-fluid -->
        <!-- Rodape -->
        <%@include file="/WEB-INF/views/modulos/rodape.jsp" %>
    </div>
    <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>


    <!-- Bootstrap core JavaScript-->
    <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- abre submenu recolhido quando hover-->
    <script src="${jsPath}/abreMenu.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <!-- <script src="${vendorPath}/chart.js/Chart.min.js"></script> -->
    <script src="${vendorPath}/datatables/jquery.dataTables.js"></script>
    <script src="${vendorPath}/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="${jsPath}/sb-admin.min.js"></script>
    <!-- Demo scripts for this page-->
    <script src="${jsPath}/demo/datatables-demo.js"></script>
    <!-- <script src="${jsPath}/demo/chart-area-demo.js"></script> -->

    <!-- colorindo itens ativos do menu -->
    <script>
        $(document).ready(function () {
            $(".sidebar li .dropdown-toggle:contains('Relatórios')").addClass("dropdownAtivo");
            // $(".sidebar li .dropdown-menu .dropdown-item:contains('Relatório de Pedidos')").addClass("itemAtivo");
        });

        function spinner() {
            document.getElementById("btnGerar").innerHTML = "<i class='fas fa-spinner'></i> Gerando";
        }
    </script>

</body>

</html>