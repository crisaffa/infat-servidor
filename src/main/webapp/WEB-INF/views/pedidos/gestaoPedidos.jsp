<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%> <%@ taglib uri="http://java.sun.com/jsp/jstl/core"
prefix="c"%> <%@ taglib uri="http://www.springframework.org/tags/form"
prefix="form"%> <%@ taglib uri="http://www.springframework.org/tags"
prefix="s"%>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <c:url value="/resources/img" var="imgPath" />
  <c:url value="/resources/css" var="cssPath" />
  <c:url value="/resources/js" var="jsPath" />
  <c:url value="/resources/vendor" var="vendorPath" />

  <link rel="icon" href="${imgPath}/favicon.ico" />
  <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png" />
  <link rel="manifest" href="${imgPath}/site.webmanifest" />
  <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5" />
  <meta name="msapplication-TileColor" content="#da532c" />
  <meta name="theme-color" content="#ffffff" />
  <title>Gestão de Pedidos - Casa América</title>
  <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
  <link rel="stylesheet" href="${cssPath}/style.css" />
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" />

  <!-- Custom fonts for this template-->
  <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
  <!-- Page level plugin CSS-->
  <link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
  <!-- Custom styles for this template-->
  <link href="${cssPath}/sb-admin.css" rel="stylesheet" />
  <link href="${cssPath}/jquery-ui.css" rel="stylesheet" />

  <!-- jPList Core -->
  <script src="${vendorPath}/jquery/jquery.min.js"></script>
  <link href="${cssPath}/jplist.demo-pages.min.css" rel="stylesheet" type="text/css" />
  <link href="${cssPath}/jplist.core.min.css" rel="stylesheet" type="text/css" />
  <link href="${cssPath}/jplist.filter-toggle-bundle.min.css" rel="stylesheet" type="text/css" />
  <link href="${cssPath}/jplist.pagination-bundle.min.css" rel="stylesheet" type="text/css" />
  <link href="${cssPath}/jplist.history-bundle.min.css" rel="stylesheet" type="text/css" />
  <link href="${cssPath}/jplist.textbox-filter.min.css" rel="stylesheet" type="text/css" />
  <link href="${cssPath}/jplist.jquery-ui-bundle.min.css" rel="stylesheet" type="text/css" />

  <script src="${jsPath}/jquery-ui.js"></script>
  <script src="${jsPath}/jplist.core.min.js"></script>
  <script src="${jsPath}/jplist.filter-toggle-bundle.min.js"></script>
  <script src="${jsPath}/jplist.history-bundle.min.js"></script>
  <script src="${jsPath}/jplist.jquery-ui-bundle.min.js"></script>
  <script src="${jsPath}/jplist.pagination-bundle.min.js"></script>
  <script src="${jsPath}/jplist.sort-bundle.min.js"></script>
  <script src="${jsPath}/jplist.textbox-filter.min.js"></script>

  <script>
    $("document").ready(function () {
      /**
       * user defined functions
       */
      jQuery.fn.jplist.settings = {
        /**
         * jQuery UI date picker
         */
        datepicker: function (input, options) {
          //set options
          options.dateFormat = "dd/mm/yy";
          options.changeMonth = true;
          options.changeYear = true;

          //start datepicker
          input.datepicker(options);
        }
      };

      /**
       * jPList
       */

      $("#tabelaPedidos").jplist({
        itemsBox: ".demo-tbl tbody",
        itemPath: ".tbl-item",
        panelPath: ".jplist-panel",

        //save plugin state
        storage: "localstorage", //'', 'cookies', 'localstorage'
        storageName: "jplist-table-2",
        cookiesExpiration: -1,

        redrawCallback: function () {
          $(".tbl-item").each(function (index, el) {
            if (index % 2 === 0) {
              $(el)
                .removeClass("odd")
                .addClass("even");
            } else {
              $(el)
                .removeClass("even")
                .addClass("odd");
            }
          });

          $(".status:contains('CANCELADO')").addClass("vermelho");
          $(".status:contains('CANCELANDO')").addClass("vermelho");
          $(".status:contains('PREPARANDO ENTREGA')").addClass("azul");
          $(".status:contains('PROCESSANDO')").addClass("laranjaCl");
          $(".status:contains('NFE EMITIDA')").addClass("verde");
          $(".status:contains('DEVOLUÇÃO DE VENDA')").addClass("roxo");
          $(".status:contains('AGUARDANDO PAGAMENTO')").addClass("laranja");
          $(".status:contains('PAGAMENTO APROVADO')").addClass("laranja");

          //adiciona classe com a sigla dos marketplaces
          $(".mktplace").each(function () {
            $(this).addClass(
              $(this)
                .text()
                .substring(0, 3)
            );
            $(".mktplace:contains('csmr')").addClass("CSMR");
          });

          
        }
      });

      //resetar filtros quando carrega a página
      $(".jplist-reset-btn").click();
    });
  </script>
</head>

<body id="page-top" class="gestaoPedidos">
  <!-- Menu Topo -->
  <%@include file="/WEB-INF/views/modulos/menu.jsp" %>

  <div id="wrapper">
    <!-- Sidebar -->
    <%@include file="/WEB-INF/views/modulos/sidebar.jsp" %>
    <div id="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item active">
            <a href="/">Dashboard</a>
          </li>
          <li class="breadcrumb-item active">Gestão de Pedidos</li>
        </ol>

        <!-- TABELA DE PEDIDOS -->
        <div class="card mb-3">
          <div class="card-header"><i class="fas fa-table"></i> Pedidos</div>
          <div class="card-body">
            <!-- demo -->
            <div id="tabelaPedidos" class="box jplist table-layout-2">
              <!-- ios button: show/hide panel -->
              <div class="jplist-ios-button">
                <i class="fa fa-sort"></i>
                Filtros
              </div>

              <!-- panel -->
              <div class="jplist-panel box panel-top">
                <!-- reset button -->
                <button type="button" class="jplist-reset-btn" data-control-type="reset" data-control-name="reset"
                  data-control-action="reset">
                  Resetar &nbsp;<i class="fa fa-share"></i>
                </button>

                <!-- items per page dropdown -->
                <div class="jplist-drop-down" data-control-type="items-per-page-drop-down" data-control-name="paging"
                  data-control-action="paging">
                  <ul>
                    <li><span data-number="10"> 10 por página </span></li>
                    <li>
                      <span data-number="25" data-default="true">
                        25 por página
                      </span>
                    </li>
                    <li><span data-number="50"> 50 por página </span></li>
                    <li><span data-number="all"> Ver todos </span></li>
                  </ul>
                </div>

                <!-- sort dropdown -->
                <div class="jplist-drop-down" data-control-type="sort-drop-down" data-control-name="sort"
                  data-control-action="sort" data-datetime-format="{day}/{month}/{year}">
                  <!-- {year}, {month}, {day}, {hour}, {min}, {sec} -->

                  <ul>
                    <li><span data-path="default">Ordenar por</span></li>
                    <li>
                      <span data-path=".title" data-order="asc" data-type="number">Nº Pedido cresc</span>
                    </li>
                    <li>
                      <span data-path=".title" data-order="desc" data-type="number" data-default="true">Nº Pedido
                        decres</span>
                    </li>
                    <li>
                      <span data-path=".desc" data-order="asc" data-type="text">Cliente A-Z</span>
                    </li>
                    <li>
                      <span data-path=".desc" data-order="desc" data-type="text">Cliente Z-A</span>
                    </li>
                    <li>
                      <span data-path=".like" data-order="asc" data-type="text">Status A-Z</span>
                    </li>
                    <li>
                      <span data-path=".like" data-order="desc" data-type="text">Status Z-A</span>
                    </li>
                    <li>
                      <span data-path=".date" data-order="asc" data-type="datetime">Data cresc</span>
                    </li>
                    <li>
                      <span data-path=".date" data-order="desc" data-type="datetime">Data decres</span>
                    </li>
                  </ul>
                </div>

                <!-- filter by title -->
                <div class="text-filter-box">
                  <i class="fa fa-search  jplist-icon"></i>

                  <!--[if lt IE 10]>
                      <div class="jplist-label">Filter by Title:</div>
                    <![endif]-->

                  <input data-path=".title" type="text" value="" placeholder="Buscar pedido" data-control-type="textbox"
                    data-control-name="title-filter" data-control-action="filter" />
                </div>

                <!-- filter by description -->
                <div class="text-filter-box">
                  <i class="fa fa-search  jplist-icon"></i>

                  <!--[if lt IE 10]>
                      <div class="jplist-label">Filter by Description:</div>
                    <![endif]-->

                  <input data-path=".desc" type="text" value="" placeholder="Buscar cliente" data-control-type="textbox"
                    data-control-name="desc-filter" data-control-action="filter" />
                </div>

                <!-- date picker range filter -->
                <!-- data-datepicker-func is a user function defined in jQuery.fn.jplist.settings -->
                <div data-control-type="date-picker-range-filter" data-control-name="date-picker-range-filter"
                  data-control-action="filter" data-path=".date" data-datetime-format="{day}/{month}/{year}"
                  data-datepicker-func="datepicker" class="jplist-date-picker-range">
                  <i class="fa fa-calendar jplist-icon" aria-hidden="true"></i>
                  <input type="text" class="date-picker" placeholder="Data inicial" data-type="prev" />

                  <i class="fa fa-minus gap" aria-hidden="true"></i>

                  <i class="fa fa-calendar jplist-icon" aria-hidden="true"></i>
                  <input type="text" class="date-picker" placeholder="Data final" data-type="next" />
                </div>

                <!-- checkbox text filter -->
                <div class="jplist-group w-100 filtroStatus" data-control-type="checkbox-text-filter"
                  data-control-action="filter" data-control-name="like" data-path=".like" data-logic="or">
                  <span>Filtrar por status:</span>

                  <input value="Aguardando Pagamento" id="aguardandoPg" type="checkbox" class="ml-2" />
                  <label for="AGUARDANDO PAGAMENTO">Aguardando Pagamento</label>

                  <input value="cancelado" id="cancelado" type="checkbox" class="ml-2" />
                  <label for="CANCELADO">Cancelado</label>

                  <input value="cancelando" id="cancelando" type="checkbox" class="ml-2" />
                  <label for="CANCELANDO">Cancelando</label>

                  <input value="NFe Emitida" id="nfeEm" type="checkbox" class="ml-2" />
                  <label for="NFE EMITIDA">NFe Emitida</label>

                  <input value="Devolução de Venda" id="devolução" type="checkbox" class="ml-2" />
                  <label for="DEVOLUÇÃO DE VENDA">Devolução</label>

                  <input value="Preparando Entrega" id="preparando" type="checkbox" class="ml-2" />
                  <label for="PREPARANDO ENTREGA">Preparando Entrega</label>

                  <input value="processando" id="processando" type="checkbox" class="ml-2" />
                  <label for="PROCESSANDO">Processando</label>
                </div>

                <!-- checkbox text filter -->
                <div class="jplist-group w-100 filtroStatus" data-control-type="checkbox-text-filter"
                  data-control-action="filter" data-control-name="mktplace" data-path=".mktplace" data-logic="or">
                  <span>Filtrar por marketplace:</span>

                  <!-- <input value="brastemp" id="brastemp" type="checkbox" class="ml-2" />
                  <label for="brastemp">Brastemp</label> -->

                  <!-- <input value="CSR" id="CSR" type="checkbox" class="ml-2" />
                  <label for="CSR">Consul</label> -->

                  <!-- <input value="SPF" id="SPF" type="checkbox" class="ml-2" />
                  <label for="SPF">Shopfácil</label> -->

                  <!-- <input value="vtexLogo" id="vtexLogo" type="checkbox" class="ml-2" />
                  <label for="vtexLogo">Vtex</label> -->

                  <input value="casaamerica" id="casaamerica" type="checkbox" class="ml-2" />
                  <label for="casaamerica">Casa América</label>

                  <input value="CSU" id="CSU" type="checkbox" class="ml-2" />
                  <label for="CSU">CSU</label>

                  <input value="MDM" id="MDM" type="checkbox" class="ml-2" />
                  <label for="MDM">Madeira Madeira</label>

                  <input value="DLD" id="DLD" type="checkbox" class="ml-2" />
                  <label for="DLD">Dia Delivery</label>

                  <input value="ZZZ" id="ZZZ" type="checkbox" class="ml-2" />
                  <label for="ZZZ">Zoom</label>
                  
                  <input value="CRF" id="CRF" type="checkbox" class="ml-2" />
                  <label for="CRF">Carrefour</label>
                </div>
              </div>

              <!-- data -->
              <div class="box text-shadow">
                <table class="demo-tbl">
                  <thead>
                    <tr>
                      <th>Data do Pedido</th>
                      <th>Pedido Nº</th>
                      <th>Cliente</th>
                      <th>Marketplace</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!-- item 1 -->
                    <c:forEach items="${pedidos}" var="pedidos">
                      <tr class="tbl-item">
                        <!-- data -->
                        <td class="td-block">
                          <p class="date">${pedidos.data}</p>
                        </td>

                        <td class="td-block">
                          <p class="title"><a
                              href="<c:url value='detalhes'/>/${pedidos.id_order}">${pedidos.id_order}</a></p>
                        </td>

                        <td class="td-block">
                          <p class="desc">${pedidos.nome}</p>
                        </td>

                        <td class="td-block">
                          <p class="mktplace">${pedidos.orderId}</p>
                        </td>

                        <td class="td-block">
                          <p class="like"><span class="status">${pedidos.status}</span></p>
                        </td>
                      </tr>
                    </c:forEach>
                  </tbody>
                </table>
              </div>
              <!-- end of data -->

              <div class="box jplist-no-results text-shadow align-center">
                <p>Nenhum resultado encontrado.</p>
              </div>

              <!-- ios button: show/hide panel -->
              <div class="jplist-ios-button">
                <i class="fa fa-sort"></i>
                Paginação
              </div>

              <!-- panel -->
              <div class="jplist-panel box panel-bottom">
                <div class="paginacaoBase float-right">
                  <!-- pagination results -->
                  <div class="jplist-label" data-type="{start} - {end} de {all}" data-control-type="pagination-info"
                    data-control-name="paging" data-control-action="paging"></div>

                  <!-- pagination -->
                  <div class="jplist-pagination" data-control-type="pagination" data-control-name="paging"
                    data-control-action="paging" data-control-animate-to-top="false"></div>
                </div>
              </div>
            </div>
            <!-- end of demo -->
          </div>

          <div class="card-footer small text-muted">
            Atualizado em ${atualiza}
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
      <!-- Rodape -->
      <%@include file="/WEB-INF/views/modulos/rodape.jsp" %>
    </div>
    <!-- /.content-wrapper -->
  </div>
  <!-- /#wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- abre submenu recolhido quando hover-->
  <script src="${jsPath}/abreMenu.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
  <!-- Page level plugin JavaScript-->
  <script src="${vendorPath}/datatables/jquery.dataTables.js"></script>
  <script src="${vendorPath}/datatables/dataTables.bootstrap4.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="${jsPath}/sb-admin.min.js"></script>

  <!-- filtro por data de inicio e data final-->
  <script src="${jsPath}/dateRange.js"></script>

  <!-- Plugin para aplicação de máscaras -->
  <script src="${jsPath}/jquery.mask.js"></script>
  <!-- <script src="${jsPath}/mascara.js"></script> -->

  <!-- colorindo itens ativos do menu -->
  <script>
    $(document).ready(function () {
      $(".sidebar li .dropdown-toggle:contains('Pedidos')").addClass("dropdownAtivo");
      $(".sidebar li .dropdown-menu .dropdown-item:contains('Gestão de Pedidos')").addClass("itemAtivo");
    });
  </script>

  <script>
  $(".mktplace:contains('csmr')").html("casaamerica").addClass('CSMR');
  $(".mktplace:contains('LBS')").html("CSU").addClass('LBS');
  $(".mktplace:contains('LBG')").html("CSU").addClass('LBG');
  $(".mktplace:contains('LBC')").html("CSU").addClass('LBC');
  $(".mktplace:contains('VVV')").html("CSU").addClass('VVV');
  $(".mktplace:contains('CLL')").html("CSU").addClass('CLL');
  $(".mktplace:contains('BCC')").html("CSU").addClass('BCC');
  $(".mktplace:contains('CSN')").html("CSU").addClass('CSN');
  $(".mktplace:contains('CSP')").html("CSU").addClass('CSP');
  $(".mktplace:contains('CPS')").html("CSU").addClass('CPS');
  $(".mktplace:contains('CBC')").html("CSU").addClass('CBC');
  $(".mktplace:contains('CBT')").html("CSU").addClass('CBT');
  $(".mktplace:contains('CLB')").html("CSU").addClass('CLB');
  $(".mktplace:contains('CVS')").html("CSU").addClass('CVS');
  $(".mktplace:contains('CSL')").html("CSU").addClass('CSL');
  $(".mktplace:contains('CSW')").html("CSU").addClass('CSW');
  $(".mktplace:contains('csw')").html("CSU").addClass('CSW');
  $(".mktplace:contains('CSM')").html("brastemp").addClass('CSM');
  $(".mktplace:contains('CRF')").html("CRF").addClass('CRF');
  </script>
</body>

</html>