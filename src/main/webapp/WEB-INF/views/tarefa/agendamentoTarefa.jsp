<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<c:url value="/resources/img" var="imgPath" />
	<c:url value="/resources/css" var="cssPath" />
	<c:url value="/resources/js" var="jsPath" />
	<c:url value="/resources/vendor" var="vendorPath" />

	<link rel="icon" href="${imgPath}/favicon.ico">
	<link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
	<link rel="manifest" href="${imgPath}/site.webmanifest">
	<link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<title>Tarefas Cadastradas - Casa América</title>
	<link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
	<link rel="stylesheet" href="${cssPath}/style.css" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

	<!-- Custom fonts for this template-->
	<link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css">
	<!-- Page level plugin CSS-->
	<link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="${cssPath}/sb-admin.css" rel="stylesheet">
	<link href="${cssPath}/jquery-ui.css" rel="stylesheet">

	<!-- jPList Core -->
	<script src="${vendorPath}/jquery/jquery.min.js"></script>

</head>

<body id="page-top" class="agendamentoTarefa">
	<!-- Menu Topo -->
	<%@include file="/WEB-INF/views/modulos/menu.jsp"%>

	<div id="wrapper">
		<!-- Sidebar -->
		<%@include file="/WEB-INF/views/modulos/sidebar.jsp"%>
		<div id="content-wrapper">
			<div class="container-fluid">
				<!-- Breadcrumbs-->
				<ol class="breadcrumb">
					<li class="breadcrumb-item active"><a href="/">Dashboard</a>
					</li>
					<li class="breadcrumb-item active">Tarefas</li>
				</ol>

				<!-- TABELA DE PEDIDOS -->
				<div class="card mb-3">
					<div class="card-header">
						<i class="fas fa-table"></i> Lista de Tarefas Cadastradas
						<!-- Button trigger modal -->
						<!-- <button type="button" class="btn btn-sm btn-success float-right" data-toggle="modal"
							data-target="#modalAddTarefa"><i class="fas fa-plus-square"></i></button> -->
					</div>

					<div class="card-body p-0">
						${sucesso}

						<c:forEach items="${tarefas}" var="tarefas" varStatus="status">

							<div class="row">
								<div class="col">
									<div class="card card${status.index} px-2 py-1">
										<p class="d-none">${tarefas.id}</p>
										<script type="text/javascript">
											$(document).ready(function () {

												var id = "${tarefas.id}";
												var layout = '<form class="formBtn" action="/tarefa/ativar/' + id + '">' +
													'<button type="submit"  class="escolherTarefa" id="checkTarefa${status.index}"><i class="far fa-check-square mr-2"></i> Inativo</button> <span><b>${tarefas.nome}</b>: ${tarefas.periodo}</span>' +
													'</form>' +
													'<button type="submit"  class="btn btn-primary detalhesTarefa sombraAzul" data-toggle="collapse" data-target="#collapse${status.index}" aria-expanded="false" aria-controls="collapse${status.index}"><i class="fas fa-angle-down"></i></button>'+
													'<form class="formBtn d-none" action="/tarefa/ativar/' + id + '">' +
													'<button type="submit"  class="removerTarefa sombraVermelho"><i class="far fa-trash-alt"></i></button>' +
													'</form>';
												$('.card${status.index}').append(layout);

												if ('true' == '${tarefas.ativo}') {
													$('.card${status.index}').addClass('ativo');
													$('#checkTarefa${status.index}').addClass('escolherTarefaAtivo');
													document.getElementById("checkTarefa${status.index}").innerHTML = "<i class=\"fas fa-check-square mr-2\"></i> Ativo";
												}

											});
										</script>

										<div class="collapse" id="collapse${status.index}">
											<form:form action="/updateTarefa" method="post" class="mt-2 formTarefa">
											
												<div class="form-row">
													<div class="form-group col-md-6">
														<label>E-mails destinatário (separado por vírgula)</label>
														<input type="text" class="form-control" name="emailsDest"
															value="${tarefas.emails}" placeholder="contato1@email.com, contato2@email.com">
														<input type="hidden" class="form-control" value="${tarefas.id}" name="idTarefa" id="inputIdTarefa" >														
													</div>
													<div class="form-group col-md-6">
														<label>E-mails cópia (separado por vírgula)</label>
														<input type="text" class="form-control" name="emailsCc"
															value="${tarefas.emailscc}" placeholder="contato3@email.com, contato4@email.com">														
													</div>
												</div>

												<div class="form-row">
													<div class="form-group col-md-12">
														<label>Descrição</label>
														<input type="text" class="form-control"
															value="${tarefas.descricao}" name="tarefaDesc" readonly>														
													</div>
												</div>

												<button type="submit"
													class="btn btn-success btn-sm w-100 mt-3 sombraVerde">
													<i class="fas fa-save mr-2"></i>Salvar Alterações
												</button>

											</form:form>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
						<!-- fim do conteudo -->

					</div>
					<div class="card-footer small text-muted">Atualizado em ${atualiza}</div>
				</div>
			</div>
			<!-- /.container-fluid -->
			<!-- Rodape -->
			<%@include file="/WEB-INF/views/modulos/rodape.jsp"%>
		</div>

		<!-- /.content-wrapper -->
	</div>

	<!-- Modal -->
	<!-- <div class="modal fade" id="modalAddTarefa" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
		aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Cadastrar Tarefa</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="/tarefa/agendar" method="post" commandName="tarefas">
						<div class="form-row">
							<div class="form-group col-md-12">
								<label class="mr-3">Ativo?</label>
								<div class="form-check-inline">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optAtivo" checked>Sim
									</label>
								</div>
								<div class="form-check-inline">
									<label class="form-check-label">
										<input type="radio" class="form-check-input" name="optAtivo">Não
									</label>
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label class="mt-3">E-mail dos destinatários (separados por
									vírgula)</label>
								<input type="text" class="form-control"	placeholder="contato1@email.com, contato2@email.com">
							</div>

							<div class="form-group col-md-6">
								<label class="mt-3">E-mails de cópia (separados por
									vírgula)</label>
								<input type="text" class="form-control"	placeholder="contato1@email.com, contato2@email.com">
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-12">
								<label for="descricaoInput">Descrição</label>
								<textarea class="form-control" id="descricaoInput" rows="3"></textarea>
							</div>
						</div>
						<p class="preencha mt-2">Preencha todos os campos corretamente.</p>
						<button type="submit" class="btn btn-success btn-sm w-100 mt-3 sombraVerde"><i
								class="fas fa-save mr-2"></i>Agendar Tarefa</button>
					</form>
				</div>
			</div>
		</div>
	</div> -->

	<!-- /#wrapper -->
	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top"> <i class="fas fa-angle-up"></i>
	</a>

	<!-- Bootstrap core JavaScript-->
	<script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- abre submenu recolhido quando hover-->
	<script src="${jsPath}/abreMenu.js"></script>
	<!-- Core plugin JavaScript-->
	<script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
	<!-- Custom scripts for all pages-->
	<script src="${jsPath}/sb-admin.min.js"></script>

	<!-- Plugin para aplicação de máscaras -->
	<script src="${jsPath}/jquery.mask.js"></script>
	<script src="${jsPath}/mascara.js"></script>

	<!-- colorindo itens ativos do menu -->
	<script>
		$(document).ready(function () {
			$(".sidebar li .dropdown-toggle:contains('Tarefas')").addClass("dropdownAtivo");
			$(".sidebar li .dropdown-menu .dropdown-item:contains('Lista de Tarefas')").addClass("itemAtivo");
		});
	</script>

</body>

</html>