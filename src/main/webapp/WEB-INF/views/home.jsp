<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <c:url value="/resources/img" var="imgPath" />
  <c:url value="/resources/css" var="cssPath" />
  <c:url value="/resources/js" var="jsPath" />
  <c:url value="/resources/vendor" var="vendorPath" />

  <link rel="icon" href="${imgPath}/favicon.ico">
  <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
  <link rel="manifest" href="${imgPath}/site.webmanifest">
  <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <title>Dashboard - Casa América</title>
  <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
  <link rel="stylesheet" href="${cssPath}/style.css" />
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

  <!-- Custom fonts for this template-->
  <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="${cssPath}/sb-admin.css" rel="stylesheet">
  <link href="${cssPath}/jquery-ui.css" rel="stylesheet">
</head>

<body id="page-top" class="home">
  <!-- Menu Topo -->
  <%@include file="/WEB-INF/views/modulos/menu.jsp" %>

  <div id="wrapper">
    <!-- Sidebar -->
    <%@include file="/WEB-INF/views/modulos/sidebar.jsp" %>
    <div id="content-wrapper">
      <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item active">
            <!-- <a href="/">Gestão de Pedidos</a> -->
            Dashboard
          </li>
          <!-- <li class="breadcrumb-item active">Visão Geral</li> -->
        </ol>

        <!-- GRÁFICOS -->
        <div class="card mb-3 cardGraficoHome">
          <div class="card-header">
            <i class="fas fa-chart-area"></i>
            Visão Geral</div>
          <div class="card-body">
            <div class="row">
              <div class="col-12">
                <canvas id="graficoDash"></canvas>
              </div>

              <div class="col-12 mt-5">
                <nav>
                  <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-valor-tab" data-toggle="tab" href="#nav-valor"
                      role="tab" aria-controls="nav-valor" aria-selected="true">Valor de Pedidos Faturados</a>
                    <a class="nav-item nav-link" id="nav-quantidade-tab" data-toggle="tab" href="#nav-quantidade"
                      role="tab" aria-controls="nav-quantidade" aria-selected="false">Quantidade de Pedidos Faturados</a>
                    <a class="nav-item nav-link" id="nav-situacao-tab" data-toggle="tab" href="#nav-situacao" role="tab"
                      aria-controls="nav-situacao" aria-selected="false">Situação de Pedidos</a>
                  </div>
                </nav>

                <div class="tab-content" id="nav-tabContent">
                  
                  <!-- valor de pedidos -->
                  <div class="tab-pane fade show active" id="nav-valor" role="tabpanel" aria-labelledby="nav-valor-tab">
                    <div class="container">
                      <div class="row font-weight-bold text-center mt-4 mb-3">
                        <div class="col">
                          <p>Hoje</p>
                        </div>

                        <div class="col">
                          <p>Nessa Semana</p>
                        </div>

                        <div class="col">
                          <p>Mês Atual</p>
                        </div>

                        <div class="col">
                          <p>Mês Passado</p>
                        </div>
                      </div>
                    </div>

                    <hr>

                    <div class="container">
                      <div class="row text-center my-2">
                        <div class="col">
                          <p>${receitaHoje}</p>
                        </div>

                        <div class="col">
                          <p>${receitaSemanal}</p>
                        </div>

                        <div class="col">
                          <p>${receitaMensal}</p>
                        </div>

                        <div class="col">
                          <p>${receitaMesPassado}</p>
                        </div>
                      </div>
                    </div>

                  </div>

                  <!-- quantidade de pedidos -->
                  <div class="tab-pane fade" id="nav-quantidade" role="tabpanel" aria-labelledby="nav-quantidade-tab">
                    <div class="container">
                      <div class="row font-weight-bold text-center mt-4 mb-3">
                        <div class="col">
                          <p>Hoje</p>
                        </div>

                        <div class="col">
                          <p>Nessa Semana</p>
                        </div>

                        <div class="col">
                          <p>Mês Atual</p>
                        </div>

                        <div class="col">
                          <p>Mês Passado</p>
                        </div>
                      </div>
                    </div>

                    <hr>

                    <div class="container">
                      <div class="row text-center my-2">
                        <div class="col">
                          <p>${qtdPedidosHoje}</p>
                        </div>

                        <div class="col">
                          <p>${qtdPedidosSemana}</p>
                        </div>

                        <div class="col">
                          <p>${qtdPedidosMensaisAtual}</p>
                        </div>

                        <div class="col">
                          <p>${qtdPedidosMensaisPassado}</p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- situacao de pedidos -->
                  <div class="tab-pane fade" id="nav-situacao" role="tabpanel" aria-labelledby="nav-situacao-tab">
                    <div class="container">
                      <div class="row font-weight-bold text-center mt-4 mb-3">
                        <div class="col">
                          <p>Faturados</p>
                        </div>

                        <div class="col">
                          <p>Cancelados</p>
                        </div>

                        <div class="col">
                          <p>Preparando Entrega</p>
                        </div>

                        <div class="col">
                          <p>Aguardando Pagamento</p>
                        </div>
                      </div>
                    </div>

                    <hr>

                    <div class="container">
                      <div class="row text-center my-2">
                        <div class="col">
                          <p>${faturadosMes}</p>
                        </div>

                        <div class="col">
                          <p>${canceladosMes}</p>
                        </div>

                        <div class="col">
                          <p>${prepEntregaMes}</p>
                        </div>

                        <div class="col">
                          <p>${agPgtoMes}</p>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- fim do grafico -->

        <!-- inicio resumo anual -->
        <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i> Resumo de Pedidos Anual</div>
            <div class="card-body">
    
              <!-- linha 1 -->
              <div class="row">
                <div class="col-xl-4 col-sm-6 mb-3">
                  <div class="card text-white o-hidden">
                    <div class="card-body">
                      <div class="card-body-icon float-left">
                        <i class="fas fa-shopping-cart"></i>
                      </div>
                      <div class="numero">${vendasAnuais}</div>
                      <div class="desc">Pedidos Faturados</div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-sm-6 mb-3">
                  <div class="card text-white o-hidden">
                    <div class="card-body">
                      <div class="card-body-icon float-left">
                        <i class="fas fa-money-bill-alt"></i>
                      </div>
                      <div class="numero money">${ticketMedioAnual}</div>
                      <div class="desc">Ticket Anual</div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-sm-6 mb-3">
                  <div class="card text-white o-hidden">
                    <div class="card-body">
                      <div class="card-body-icon float-left">
                        <i class="fas fa-dollar-sign"></i>
                      </div>
                      <div class="numero money">${receitaAnual}</div>
                      <div class="desc">Receita Anual</div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-sm-6 mb-3">
                  <div class="card text-white o-hidden">
                    <div class="card-body">
                      <div class="card-body-icon float-left">
                        <i class="fas fa-ban"></i>
                      </div>
                      <div class="numero">${canceladosAno}</div>
                      <div class="desc">Cancelados</div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-sm-6 mb-3">
                  <div class="card text-white o-hidden">
                    <div class="card-body">
                      <div class="card-body-icon float-left">
                        <i class="fas fa-list-ol"></i>
                      </div>
                      <div class="numero">${prepEntregaAno}</div>
                      <div class="desc">Preparando Entrega</div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-sm-6 mb-3">
                  <div class="card text-white o-hidden">
                    <div class="card-body">
                      <div class="card-body-icon float-left">
                        <i class="fas fa-hand-holding-usd"></i>
                      </div>
                      <div class="numero">${agPgtoAno}</div>
                      <div class="desc">Aguardando Pagamento</div>
                    </div>
                  </div>
                </div>
              </div>
    
            </div>
          </div>
          <!-- fim do resumo anual -->
      </div>
      <!-- Rodape -->
			<%@include file="/WEB-INF/views/modulos/rodape.jsp"%>
    </div>
    <!-- /.content-wrapper -->
  </div>
  <!-- /#wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  <!-- Bootstrap core JavaScript-->
  <script src="${vendorPath}/jquery/jquery.min.js"></script>
  <script src="${jsPath}/jquery-ui.js"></script>
  <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- abre submenu recolhido quando hover-->
  <script src="${jsPath}/abreMenu.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
  <!-- Page level plugin JavaScript-->
  <!-- <script src="${vendorPath}/chart.js/Chart.min.js"></script> -->
  <script src="${vendorPath}/datatables/jquery.dataTables.js"></script>
  <script src="${vendorPath}/datatables/dataTables.bootstrap4.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="${jsPath}/sb-admin.min.js"></script>
  <!-- Demo scripts for this page-->
  <script src="${jsPath}/demo/datatables-demo.js"></script>
  <!-- <script src="${jsPath}/demo/chart-area-demo.js"></script> -->
  <script src="${jsPath}/moment.js"></script>
  <script src="${vendorPath}/chart.js/Chart.js"></script>
  <script src="${jsPath}/graficoDash.js"></script>

  <!-- filtro por data de inicio e data final-->
  <!-- <script src="${jsPath}/dateRange.js"></script> -->

  <!-- Plugin para aplicação de máscaras -->
  <!-- <script src="${jsPath}/jquery.mask.js"></script> -->

  <script>
    $(function(){
      console.log('${receitaVetorMensalAtual}');
      graficoGeral('${receitaVetorMensalAtual}'.split(";"), '${receitaVetorMensalPassado}'.split(";"));
    });
  </script>
</body>

</html>