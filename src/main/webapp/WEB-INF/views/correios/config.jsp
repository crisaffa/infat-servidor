<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <c:url value="/resources/img" var="imgPath" />
    <c:url value="/resources/css" var="cssPath" />
    <c:url value="/resources/js" var="jsPath" />
    <c:url value="/resources/vendor" var="vendorPath" />
    <link rel="icon" href="${imgPath}/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
    <link rel="manifest" href="${imgPath}/site.webmanifest">
    <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Adicionar Conta dos Correios - Casa América</title>
    <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
    <link rel="stylesheet" href="${cssPath}/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css">
    <!-- Page level plugin CSS-->
    <link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="${cssPath}/sb-admin.css" rel="stylesheet">
    <link href="${cssPath}/jquery-ui.css" rel="stylesheet">
    <!-- jPList Core -->
    <script src="${vendorPath}/jquery/jquery.min.js"></script>
    <link href="${cssPath}/jplist.demo-pages.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.core.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.filter-toggle-bundle.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.pagination-bundle.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.history-bundle.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.textbox-filter.min.css" rel="stylesheet" type="text/css" />
    <link href="${cssPath}/jplist.jquery-ui-bundle.min.css" rel="stylesheet" type="text/css" />
    <script src="${jsPath}/jquery-ui.js"></script>
    <script src="${jsPath}/jplist.core.min.js"></script>
    <script src="${jsPath}/jplist.filter-toggle-bundle.min.js"></script>
    <script src="${jsPath}/jplist.history-bundle.min.js"></script>
    <script src="${jsPath}/jplist.jquery-ui-bundle.min.js"></script>
    <script src="${jsPath}/jplist.pagination-bundle.min.js"></script>
    <script src="${jsPath}/jplist.sort-bundle.min.js"></script>
    <script src="${jsPath}/jplist.textbox-filter.min.js"></script>
</head>
<body id="page-top" class="configCorreios">
    <!-- Menu Topo -->
    <%@include file="/WEB-INF/views/modulos/menu.jsp" %>
    <div id="wrapper">
        <!-- Sidebar -->
        <%@include file="/WEB-INF/views/modulos/sidebar.jsp" %>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">
                        <a href="/">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active"> Cadastrar Transportadora</li>
                </ol>
                <!-- CONTEUDO -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i> Adicionar Conta da Transportadora
                    </div>
                    <div class="card-body">
                        ${sucesso}
                        <!-- FORMULARIO DE CADASTRO -->                       
                        <form:form action="infat/correios/configuracoes" method="post" commandName="correios">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>CNPJ da Empresa</label>                             
                                    <input type="text" class="form-control" name="identificador" id="inputIdentificador" maxlength="14" placeholder="CNPJ (14) (só números)">
                                    <form:errors path="identificador"
                                        style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Número do Cartão de Postagem</label>
                                    <input type="text" class="form-control" name="cartaoPostagem" id="inputCartaoPostagem" maxlength="10" placeholder="Número do Cartão Postagem (10)">
                                    <form:errors path="cartaoPostagem"
                                        style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;" />    
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Número do Contrato</label>
                                    <input type="text" class="form-control" name="contrato" id="inputContrato" maxlength="10" placeholder="Número do contrato (10)" >
                                    <form:errors path="contrato"
                                        style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Código Administrativo</label>
                                    <input type="text" class="form-control" name="codigoAdministrativo" id="inputCodigoAdministrativo" maxlength="8" placeholder="Código Administrativo (8)">
                                    <form:errors path="codigoAdministrativo"
                                        style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;" />
                                </div>
                                
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Nome do Usuário</label>
                                    <input type="text" class="form-control" name="usuario" id="inputUsuario" placeholder="Usuário">
                                    <form:errors path="usuario"
                                        style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Senha</label>
                                    <input type="text" class="form-control" name="senha" id="inputSenha" placeholder="Senha">
                                    <form:errors path="senha"
                                        style="color: #721c24; background-color: #f8d7da; border: 1px solid #f5c6cb; padding: 2px 6px; border-radius: .25rem; margin-top: 5px; display: block; font-size: 14px;" />
                                </div>
                            </div>
                           
                            <p class="preencha mt-2">Preencha todos os campos corretamente.</p>
                            <button type="submit" class="btn btn-success btn-sm w-100 mt-3 sombraVerde"><i class="fas fa-save mr-2"></i>Salvar Conta da Transportadora</button>
                        </form:form>
                        <!-- fim do conteudo -->
                    </div>
                        <div class="card-footer small text-muted">Atualizado em ${atualiza}</div>
                 </div>
            </div>
            <!-- /.container-fluid -->
            <!-- Rodape -->
            <%@include file="/WEB-INF/views/modulos/rodape.jsp" %>
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- Bootstrap core JavaScript-->
    <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- abre submenu recolhido quando hover-->
    <script src="${jsPath}/abreMenu.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="${jsPath}/sb-admin.min.js"></script>
    <!-- Plugin para aplicação de máscaras -->
    <script src="${jsPath}/jquery.mask.js"></script>
    <script src="${jsPath}/mascara.js"></script>
    <!-- colorindo itens ativos do menu -->
    <script>
        $(document).ready(function() {
            $(".sidebar li .dropdown-toggle:contains('Transportadora')").addClass("dropdownAtivo");
            $(".sidebar li .dropdown-menu .dropdown-item:contains('Cadastrar Transportadora')").addClass("itemAtivo");
        });
    </script>
</body>
</html>