<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <c:url value="/resources/img" var="imgPath" />
    <c:url value="/resources/css" var="cssPath" />
    <c:url value="/resources/js" var="jsPath" />
    <c:url value="/resources/vendor" var="vendorPath" />

    <link rel="icon" href="${imgPath}/favicon.ico">
    <link rel="apple-touch-icon" sizes="144x144" href="${imgPath}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${imgPath}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${imgPath}/favicon-16x16.png">
    <link rel="manifest" href="${imgPath}/site.webmanifest">
    <link rel="mask-icon" href="${imgPath}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>Contas Correios Cadastrados - Casa América</title>
    <link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
    <link rel="stylesheet" href="${cssPath}/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="${vendorPath}/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css">
    <!-- Page level plugin CSS-->
    <link href="${vendorPath}/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="${cssPath}/sb-admin.css" rel="stylesheet">
    <link href="${cssPath}/jquery-ui.css" rel="stylesheet">

    <!-- jPList Core -->
    <script src="${vendorPath}/jquery/jquery.min.js"></script>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

</head>

<body id="page-top" class="cadastroContas">
    <!-- Menu Topo -->
    <%@include file="/WEB-INF/views/modulos/menu.jsp"%>

    <div id="wrapper">
        <!-- Sidebar -->
        <%@include file="/WEB-INF/views/modulos/sidebar.jsp"%>
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active"><a href="/">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">Transportadora</li>
                </ol>

                <!-- TABELA DE PEDIDOS -->
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i> Lista de Transportadoras Cadastradas
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-sm btn-success float-right" data-toggle="modal" data-target="#modalAddTransp"><i class="fas fa-plus-square"></i></button>
                    </div>

                    <div class="card-body p-0">
                        ${sucesso}

                        <c:forEach items="${correios}" var="correios" varStatus="status">

                            <div class="row">
                                <div class="col">
                                    <div class="card card${status.index} px-2 py-1">
                                        <p class="d-none">${correios.identificador}</p>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                var cnpj = "${correios.identificador}";
                                                cnpj = cnpj.replace(/\D/g, '');
                                                var layout = '<form class="formBtn" action="//correios/cadastroCnpj/' + cnpj + '">' +
                                                    '<button type="submit"  class="escolherCnpj" id="checkCnpj${status.index}"><i class="far fa-check-square mr-2"></i> Inativo</button> <span><b>Transportadora</b>: ${correios.usuario}</span>' +
                                                    '</form>' +
                                                    '<button type="submit"  class="btn btn-primary detalhesCnpj sombraAzul" data-toggle="collapse" data-target="#collapse${status.index}" aria-expanded="false" aria-controls="collapse${status.index}"><i class="fas fa-angle-down"></i></button>' +
                                                    '<form class="formBtn" action="/correios/removerCnpj/' + cnpj + '">' +
                                                    '<button type="submit"  class="removerCnpj sombraVermelho"><i class="far fa-trash-alt"></i></button>' +
                                                    '</form>';
                                                $('.card${status.index}').append(layout);

                                                if ('SIM' == '${correios.escolhido}') {
                                                    $('.card${status.index}').addClass('ativo');
                                                    $('#checkCnpj${status.index}').addClass('escolherCnpjAtivo');
                                                    document.getElementById("checkCnpj${status.index}").innerHTML = "<i class=\"fas fa-check-square mr-2\"></i> Ativo";
                                                }
                                            });
                                        </script>

                                        <div class="collapse" id="collapse${status.index}">
                                            <form:form action="/updateCorreios" method="post" class="mt-2 formCorreios">

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>CNPJ do Contratante</label>
                                                        <input type="text" class="form-control" class="cnpj" name="identificador"
                                                            value="${correios.identificador}"
                                                            id="inputCnpj${status.index}"
                                                            placeholder="CNPJ" required>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Número do Cartão Postagem</label>
                                                        <input type="text" class="form-control"
                                                            value="${correios.cartaoPostagem}" name="cartaoPostagem"
                                                            id="inputCartaoPostagem${status.index}" maxlength="10"
                                                            placeholder="Número do Cartão Postagem (10)" required>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>Número do Contrato da Transportadora</label>
                                                        <input type="text" class="form-control"
                                                            value="${correios.contrato}" name="contrato"
                                                            id="inputContrato${status.index}" maxlength="10"
                                                            placeholder="Número do contrato (10)" required>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Código Administrativo</label>
                                                        <input type="text" class="form-control"
                                                            value="${correios.codigoAdministrativo}"
                                                            name="codigoAdministrativo" id="inputCodigoAdministrativo${status.index}"
                                                            maxlength="8" placeholder="Código Administrativo (8)" required>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>Nome da Transportadora</label>
                                                        <input type="text" class="form-control"
                                                            value="${correios.usuario}" name="usuario" id="inputUsuario${status.index}"
                                                            placeholder="Usuário" required>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Senha</label>
                                                        <input type="password" class="form-control"
                                                            value="${correios.senha}" name="senha" id="inputSenha${status.index}"
                                                            placeholder="Senha" required>
                                                    </div>
                                                </div>

                                                <button type="submit"
                                                    class="btn btn-success btn-sm w-100 mt-3 sombraVerde">
                                                    <i class="fas fa-save mr-2"></i>Salvar Alterações
                                                </button>

                                            </form:form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                        <!-- fim do conteudo -->

                    </div>
                    <div class="card-footer small text-muted">Atualizado em ${atualiza}</div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- Rodape -->
            <%@include file="/WEB-INF/views/modulos/rodape.jsp"%>
        </div>

        <!-- /.content-wrapper -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalAddTransp" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Cadastrar Transportadora</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/correios/configuracoes" method="post" commandName="correios">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>CNPJ da Empresa</label>                             
                                <input type="text" class="form-control cnpj" name="identificador" id="inputIdentificador" placeholder="CNPJ" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Número do Cartão de Postagem</label>
                                <input type="text" class="form-control" name="cartaoPostagem" id="inputCartaoPostagem" maxlength="10" placeholder="Número do Cartão Postagem (10)" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Número do Contrato</label>
                                <input type="text" class="form-control" name="contrato" id="inputContrato" maxlength="10" placeholder="Número do contrato (10)" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Código Administrativo</label>
                                <input type="text" class="form-control" name="codigoAdministrativo" id="inputCodigoAdministrativo" maxlength="8" placeholder="Código Administrativo (8)" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Nome do Usuário</label>
                                <input type="text" class="form-control" name="usuario" id="inputUsuario" placeholder="Usuário" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Senha</label>
                                <input type="password" class="form-control" name="senha" id="inputSenha" placeholder="Senha" required>
                            </div>
                        </div>
                       
                        <p class="preencha mt-2">Preencha todos os campos corretamente.</p>
                        <button type="submit" class="btn btn-success btn-sm w-100 mt-3 sombraVerde"><i class="fas fa-save mr-2"></i>Salvar Conta da Transportadora</button>
                    </form>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submi" class="btn btn-primary">Save changes</button>
                </div> -->
            </div>
        </div>
    </div>

    <!-- /#wrapper -->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top"> <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="${vendorPath}/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- abre submenu recolhido quando hover-->
    <script src="${jsPath}/abreMenu.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="${vendorPath}/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="${jsPath}/sb-admin.min.js"></script>

    <!-- Plugin para aplicação de máscaras -->
    <script src="${jsPath}/jquery.mask.js"></script>
    <script src="${jsPath}/mascara.js"></script>

    <!-- colorindo itens ativos do menu -->
    <script>
        $(document).ready(function () {
            $(".sidebar li .dropdown-toggle:contains('Transportadora')").addClass("dropdownAtivo");
            $(".sidebar li .dropdown-menu .dropdown-item:contains('Transportadoras Cadastradas')").addClass("itemAtivo");
        });

        
        $(document).ready(function() {
            $('#validaForm').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    username: {
                        validators: {
                            notEmpty: {
                                message: 'The username is required'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            }
                        }
                    }
                }
            });

            $('#modalAddTransp').on('shown.bs.modal', function() {
                $('#validaForm').bootstrapValidator('resetForm', true);
            });
        });

            
        
    </script>

</body>

</html>